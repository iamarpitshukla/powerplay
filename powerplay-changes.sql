CREATE TABLE `contest_winning_distribution` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `contest_id` int(11) DEFAULT NULL,
  `start_rank` int(11) DEFAULT NULL,
  `end_rank` int(11) DEFAULT NULL,
  `winning_amount` int(11) DEFAULT NULL,
  `enabled` tinyint(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

CREATE TABLE `contests` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `match_id` int(11) DEFAULT NULL,
  `playing_count` int(11) DEFAULT NULL,
  `total_count` int(11) DEFAULT NULL,
  `entry_fees` int(11) DEFAULT NULL,
  `inning` int(11) DEFAULT NULL,
  `start_over` int(11) DEFAULT NULL,
  `end_over` int(11) DEFAULT NULL,
  `type` varchar(30) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `enabled` tinyint(4) DEFAULT NULL,
  `time_crossed` tinyint(4) DEFAULT NULL,
  `points` text,
  `contest_over` tinyint(4) DEFAULT NULL,
  `prize_pool` int(11) DEFAULT NULL,
  `winner_count` int(11) DEFAULT NULL,
  `reviewed` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

CREATE TABLE `match` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `match_unique_id` int(11) DEFAULT NULL,
  `team1` varchar(50) DEFAULT NULL,
  `team2` varchar(50) DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  `sub_type` varchar(20) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `squad` tinyint(4) DEFAULT NULL,
  `toss_winner_team` varchar(50) DEFAULT NULL,
  `match_started` tinyint(4) DEFAULT NULL,
  `winner_team` varchar(50) DEFAULT NULL,
  `players` text,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `enabled` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `INDX_MATCH_UNIQUE_ID` (`match_unique_id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=latin1;

CREATE TABLE `match_data` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `match_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `enabled` tinyint(4) DEFAULT NULL,
  `data` text,
  `points` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

CREATE TABLE `payment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `enabled` tinyint(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `source` varchar(20) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `paid_amount` decimal(11,0) DEFAULT NULL,
  `refund_amount` decimal(11,0) DEFAULT NULL,
  `user_match_team_contest_mapping_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

CREATE TABLE `payment_gateway` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `merchant_id` varchar(50) DEFAULT NULL,
  `secret_key` varchar(100) DEFAULT NULL,
  `api_url` varchar(100) DEFAULT NULL,
  `additional_fields` varchar(200) DEFAULT NULL,
  `enabled` tinyint(4) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `name` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

CREATE TABLE `payment_txn` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  `pg_name` varchar(20) DEFAULT NULL,
  `pg_txn_id` varchar(100) DEFAULT NULL,
  `pg_incoming_params` varchar(200) DEFAULT NULL,
  `source` varchar(20) DEFAULT NULL,
  `sub_source` varchar(20) DEFAULT NULL,
  `ip_address` varchar(15) DEFAULT NULL,
  `enabled` tinyint(4) DEFAULT NULL,
  `status` varchar(11) DEFAULT NULL,
  `payment_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

CREATE TABLE `roles` (
  `mobile` varchar(20) NOT NULL DEFAULT '',
  `id` int(11) DEFAULT NULL,
  `role` varchar(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `enabled` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`mobile`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(200) DEFAULT NULL,
  `mobile` varchar(40) NOT NULL DEFAULT '',
  `email` varchar(50) DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `source` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

CREATE TABLE `wallet` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `amount` int(11) DEFAULT NULL,
  `wallet_type` varchar(10) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `enabled` tinyint(4) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

CREATE TABLE `user_match_team_mapping` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `enabled` tinyint(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `match_id` int(11) DEFAULT NULL,
  `players` varchar(200) DEFAULT NULL,
  `super_player` varchar(30) DEFAULT NULL,
  `team_number` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

CREATE TABLE `user_match_team_contest_mapping` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `enabled` tinyint(11) DEFAULT NULL,
  `contests_id` int(11) DEFAULT NULL,
  `user_match_team_mapping_id` int(11) DEFAULT NULL,
  `code` varchar(20) DEFAULT NULL,
  `points` double DEFAULT NULL,
  `rank` int(11) DEFAULT NULL,
  `winning_amount` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

CREATE TABLE `wallet_history` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `wallet_id` int(11) DEFAULT NULL,
  `enabled` tinyint(4) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `source` varchar(10) DEFAULT NULL,
  `remarks` varchar(200) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `wallet_type` varchar(10) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;