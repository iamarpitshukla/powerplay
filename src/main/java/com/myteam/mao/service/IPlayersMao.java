package com.myteam.mao.service;

import java.util.List;

import com.myteam.doc.PlayersDoc;

public interface IPlayersMao{

	void savePlayersDoc(PlayersDoc playerDoc);

	List<PlayersDoc> getPlayerDocsByPIds(List<Integer> pids);

	PlayersDoc getPlayerDocByPId(Integer pid);

}
