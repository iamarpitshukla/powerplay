package com.myteam.mao.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.myteam.doc.MatchConfigDetailDoc;
import com.myteam.mao.service.IMatchConfigMao;

@Repository("matchConfigMao")
public class MatchConfigMaoImpl implements IMatchConfigMao{
	
	@Autowired
	private MongoTemplate mongotemplate;

	@Override
	public List<MatchConfigDetailDoc> getMatchConfigById(List<Integer> matchIds) {
		Query query = new Query();
		query.addCriteria(Criteria.where("enabled").is(true));
		query.addCriteria(Criteria.where("matchId").in(matchIds));
		return (List<MatchConfigDetailDoc>) mongotemplate.find(query, MatchConfigDetailDoc.class);
	}
}
