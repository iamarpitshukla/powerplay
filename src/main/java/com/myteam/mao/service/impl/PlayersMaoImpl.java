package com.myteam.mao.service.impl;


import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.myteam.doc.PlayersDoc;
import com.myteam.mao.service.IPlayersMao;

@Repository("playerMao")
public class PlayersMaoImpl implements IPlayersMao{

	@Autowired
	private MongoTemplate mongotemplate;
	
	@Override
    public void savePlayersDoc(PlayersDoc playersDoc) {
        if (playersDoc.getCreated() == null) {
            playersDoc.setCreated(new Date());
        }
        playersDoc.setUpdated(new Date());
        mongotemplate.save(playersDoc, "playersDoc");
    }
	
	@Override
	public PlayersDoc getPlayerDocByPId(Integer pid) {
		Query query = new Query();
		query.addCriteria(Criteria.where("pid").is(pid));
		return mongotemplate.findOne(query, PlayersDoc.class);
	}
	
	@Override
	public List<PlayersDoc> getPlayerDocsByPIds(List<Integer> pids) {
		Query query = new Query();
		query.addCriteria(Criteria.where("pid").in(pids));
		return mongotemplate.find(query, PlayersDoc.class);
	}
	
}
