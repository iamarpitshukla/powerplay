package com.myteam.mao.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.myteam.doc.UserAuthToken;
import com.myteam.mao.service.IUserAuthTokenMao;

@Repository("userAuthTokenMao")
public class UserAuthTokenMaoImpl implements IUserAuthTokenMao {
	
	@Autowired
	private MongoTemplate mongotemplate;
	
	@Override
    public void saveUserAuthToken(UserAuthToken userAuthToken) {
        if (userAuthToken.getCreated() == null) {
        	userAuthToken.setCreated(new Date());
        }
        userAuthToken.setUpdated(new Date());
        mongotemplate.save(userAuthToken, "userAuthToken");
    }
	
	@Override
	public UserAuthToken getUserAuthTokenByAuthToken(String authToken) {
		Query query = new Query((Criteria.where("key").is(authToken).and("valid").is(true)));
		return mongotemplate.findOne(query, UserAuthToken.class);
	}
	
	@Override
	public UserAuthToken getUserAuthTokenByUserId(Integer userId) {
		Query query = new Query((Criteria.where("userId").is(userId).and("valid").is(true)));
		return mongotemplate.findOne(query, UserAuthToken.class);
	}
	
	
	@Override
    public void deleteAuthToken(UserAuthToken authToken) {
		mongotemplate.remove(authToken);
    }

}
