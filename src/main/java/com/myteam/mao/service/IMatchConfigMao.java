package com.myteam.mao.service;

import java.util.List;

import com.myteam.doc.MatchConfigDetailDoc;

public interface IMatchConfigMao {

	List<MatchConfigDetailDoc> getMatchConfigById(List<Integer> matchIds);

}
