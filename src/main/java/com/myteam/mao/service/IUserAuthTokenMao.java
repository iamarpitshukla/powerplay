package com.myteam.mao.service;

import com.myteam.doc.UserAuthToken;

public interface IUserAuthTokenMao {

	UserAuthToken getUserAuthTokenByAuthToken(String authToken);

	void saveUserAuthToken(UserAuthToken userAuthToken);

	void deleteAuthToken(UserAuthToken authToken);

	UserAuthToken getUserAuthTokenByUserId(Integer userId);

}
