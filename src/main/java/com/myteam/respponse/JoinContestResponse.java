package com.myteam.respponse;

import java.util.ArrayList;
import java.util.List;

import com.myteam.dto.JoinedContestDTO;

public class JoinContestResponse extends ServiceResponse{
	
	List<JoinedContestDTO> joinedContests = new ArrayList<>();

	public List<JoinedContestDTO> getJoinedContests() {
		return joinedContests;
	}

	public void setJoinedContests(List<JoinedContestDTO> joinedContests) {
		this.joinedContests = joinedContests;
	}

	@Override
	public String toString() {
		return "JoinContestResponse [joinedContests=" + joinedContests + "]";
	}
}
