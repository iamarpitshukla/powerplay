package com.myteam.respponse;

import com.myteam.entity.Payment;
import com.myteam.entity.PaymentGateway;
import com.myteam.entity.PaymentTxn;

public class InitiatePaymentResponse {

	private PaymentTxn paymentTxn;
	
	private PaymentGateway pg;
	
	private Payment payment;
	
	private boolean fromWeb;
	
	public boolean isFromWeb() {
		return fromWeb;
	}

	public void setFromWeb(boolean fromWeb) {
		this.fromWeb = fromWeb;
	}

	public Payment getPayment() {
		return payment;
	}

	public void setPayment(Payment payment) {
		this.payment = payment;
	}

	public PaymentGateway getPg() {
		return pg;
	}

	public void setPg(PaymentGateway pg) {
		this.pg = pg;
	}

	public PaymentTxn getPaymentTxn() {
		return paymentTxn;
	}

	public void setPaymentTxn(PaymentTxn paymentTxn) {
		this.paymentTxn = paymentTxn;
	}
}
