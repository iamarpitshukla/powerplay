package com.myteam.respponse;

public class GetUserReferalDetailsResponse extends ServiceResponse{
	
	private String referalCode;
	
	private Integer referredCount;
	
	private Integer totalWinners;
	
	public Integer getTotalWinners() {
		return totalWinners;
	}

	public void setTotalWinners(Integer totalWinners) {
		this.totalWinners = totalWinners;
	}

	public String getReferalCode() {
		return referalCode;
	}

	public void setReferalCode(String referalCode) {
		this.referalCode = referalCode;
	}

	public Integer getReferredCount() {
		return referredCount;
	}

	public void setReferredCount(Integer referredCount) {
		this.referredCount = referredCount;
	}

	@Override
	public String toString() {
		return "GetUserReferalDetailsResponse [referalCode=" + referalCode + ", referredCount=" + referredCount
				+ ", totalWinners=" + totalWinners + "]";
	}

	
}
