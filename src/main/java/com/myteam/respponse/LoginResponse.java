package com.myteam.respponse;

public class LoginResponse extends ServiceResponse{

	private String authToken;

	public String getAuthToken() {
		return authToken;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}
}
