package com.myteam.respponse;

import java.util.ArrayList;
import java.util.List;

public class GetMatchStatsResponse extends ServiceResponse {

	private boolean matchStarted;
	
	private List<InnningWiseScoreDTO> scores = new ArrayList<>();
	
	private String scores1;
	
	private String scores2;
	
	private String winningTeam;
	
	private String additionalMsg;
	
	public String getScores1() {
		return scores1;
	}

	public void setScores1(String scores1) {
		this.scores1 = scores1;
	}

	public String getScores2() {
		return scores2;
	}

	public void setScores2(String scores2) {
		this.scores2 = scores2;
	}

	public String getAdditionalMsg() {
		return additionalMsg;
	}

	public void setAdditionalMsg(String additionalMsg) {
		this.additionalMsg = additionalMsg;
	}

	public String getWinningTeam() {
		return winningTeam;
	}

	public void setWinningTeam(String winningTeam) {
		this.winningTeam = winningTeam;
	}

	public boolean isMatchStarted() {
		return matchStarted;
	}

	public void setMatchStarted(boolean matchStarted) {
		this.matchStarted = matchStarted;
	}

	public List<InnningWiseScoreDTO> getScores() {
		return scores;
	}

	public void setScores(List<InnningWiseScoreDTO> scores) {
		this.scores = scores;
	}

	@Override
	public String toString() {
		return "GetMatchStatsResponse [matchStarted=" + matchStarted + ", scores=" + scores + ", scores1=" + scores1
				+ ", scores2=" + scores2 + ", winningTeam=" + winningTeam + ", additionalMsg=" + additionalMsg + "]";
	}
}
