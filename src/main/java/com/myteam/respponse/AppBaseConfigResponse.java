package com.myteam.respponse;

import java.util.List;

public class AppBaseConfigResponse extends ServiceResponse{
	
	private String baseUrl;
	
	private String s3Path;
	
	private List<String> bannerUrls;
	
	private Integer maxNumberAllowed;
	
	public Integer getMaxNumberAllowed() {
		return maxNumberAllowed;
	}

	public void setMaxNumberAllowed(Integer maxNumberAllowed) {
		this.maxNumberAllowed = maxNumberAllowed;
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	public String getS3Path() {
		return s3Path;
	}

	public void setS3Path(String s3Path) {
		this.s3Path = s3Path;
	}

	public List<String> getBannerUrls() {
		return bannerUrls;
	}

	public void setBannerUrls(List<String> bannerUrls) {
		this.bannerUrls = bannerUrls;
	}

	@Override
	public String toString() {
		return "AppBaseConfigResponse [baseUrl=" + baseUrl + ", s3Path=" + s3Path + ", bannerUrls=" + bannerUrls
				+ ", maxNumberAllowed=" + maxNumberAllowed + "]";
	}
}
