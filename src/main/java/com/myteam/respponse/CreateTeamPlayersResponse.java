package com.myteam.respponse;

import java.util.List;

import com.myteam.dto.PlayersDTO;

public class CreateTeamPlayersResponse extends ServiceResponse{
	
	private List<PlayersDTO> players;
	
	private Integer superPlayerId;
	
	private Integer userMatchTeamMappingId;
	
	public Integer getUserMatchTeamMappingId() {
		return userMatchTeamMappingId;
	}

	public void setUserMatchTeamMappingId(Integer userMatchTeamMappingId) {
		this.userMatchTeamMappingId = userMatchTeamMappingId;
	}

	public List<PlayersDTO> getPlayers() {
		return players;
	}

	public void setPlayers(List<PlayersDTO> players) {
		this.players = players;
	}

	public Integer getSuperPlayerId() {
		return superPlayerId;
	}

	public void setSuperPlayerId(Integer superPlayerId) {
		this.superPlayerId = superPlayerId;
	}

	@Override
	public String toString() {
		return "CreateTeamPlayersResponse [players=" + players + ", superPlayerId=" + superPlayerId
				+ ", userMatchTeamMappingId=" + userMatchTeamMappingId + "]";
	}
}
