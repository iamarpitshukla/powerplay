package com.myteam.respponse;

public class PaytmCheckStatusResponse extends BasicPaytmTransactionResponse{



	/**
	 * 
	 */
	private static final long serialVersionUID = -4510219472183378347L;
	private String TXN_TYPE;
	private String REFUNDAMT;
	private String GATEWAYNAME;
	private String BANKNAME;

	public String getGATEWAYNAME() {
		return GATEWAYNAME;
	}

	public void setGATEWAYNAME(String gATEWAYNAME) {
		GATEWAYNAME = gATEWAYNAME;
	}

	public String getBANKNAME() {
		return BANKNAME;
	}

	public void setBANKNAME(String bANKNAME) {
		BANKNAME = bANKNAME;
	}

	public String getTXN_TYPE() {
		return TXN_TYPE;
	}

	public void setTXN_TYPE(String tXN_TYPE) {
		TXN_TYPE = tXN_TYPE;
	}

	public String getREFUNDAMT() {
		return REFUNDAMT;
	}

	public void setREFUNDAMT(String rEFUNDAMT) {
		REFUNDAMT = rEFUNDAMT;
	}


}
