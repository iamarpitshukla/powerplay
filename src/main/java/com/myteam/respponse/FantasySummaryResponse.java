package com.myteam.respponse;

public class FantasySummaryResponse {

	private String dateTimeGMT;
	
	private MatchSummaryResponse data = new MatchSummaryResponse();
	
	private String type;

	public String getDateTimeGMT() {
		return dateTimeGMT;
	}

	public void setDateTimeGMT(String dateTimeGMT) {
		this.dateTimeGMT = dateTimeGMT;
	}

	public MatchSummaryResponse getData() {
		return data;
	}

	public void setData(MatchSummaryResponse data) {
		this.data = data;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "FantasySummaryResponse [dateTimeGMT=" + dateTimeGMT + ", data=" + data + ", type=" + type + "]";
	}
}
