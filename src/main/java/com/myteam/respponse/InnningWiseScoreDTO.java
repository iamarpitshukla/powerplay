package com.myteam.respponse;

public class InnningWiseScoreDTO {

	private String title;

	private String battingScore;

	private Integer run;

	private Integer wicket;
	
	private String bowlingScore;

	public Integer getRun() {
		return run;
	}

	public void setRun(Integer run) {
		this.run = run;
	}

	public Integer getWicket() {
		return wicket;
	}

	public void setWicket(Integer wicket) {
		this.wicket = wicket;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBattingScore() {
		return battingScore;
	}

	public void setBattingScore(String battingScore) {
		this.battingScore = battingScore;
	}

	public String getBowlingScore() {
		return bowlingScore;
	}

	public void setBowlingScore(String bowlingScore) {
		this.bowlingScore = bowlingScore;
	}

	@Override
	public String toString() {
		return "InnningWiseScoreDTO [title=" + title + ", battingScore=" + battingScore + ", run=" + run + ", wicket="
				+ wicket + ", bowlingScore=" + bowlingScore + "]";
	}
}
