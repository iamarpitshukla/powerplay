package com.myteam.respponse;

import java.util.ArrayList;
import java.util.List;

import com.myteam.dto.UserWalletDTO;

public class GetUserWalletResponse extends ServiceResponse{

	private List<UserWalletDTO> transactions = new ArrayList<>();

	public List<UserWalletDTO> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<UserWalletDTO> transactions) {
		this.transactions = transactions;
	}

	@Override
	public String toString() {
		return "GetUserWalletResponse [transactions=" + transactions + "]";
	}
}
