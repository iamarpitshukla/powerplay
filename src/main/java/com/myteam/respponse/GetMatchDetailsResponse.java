package com.myteam.respponse;

import java.util.ArrayList;
import java.util.List;

import com.myteam.dto.ContestDTO;
import com.myteam.dto.SquadDTO;

public class GetMatchDetailsResponse extends ServiceResponse{

	private List<SquadDTO> squad = new ArrayList<>();
	
	private List<ContestDTO> contestDTOs = new ArrayList<>();
	
	public List<SquadDTO> getSquad() {
		return squad;
	}

	public void setSquad(List<SquadDTO> squad) {
		this.squad = squad;
	}

	public List<ContestDTO> getContestDTOs() {
		return contestDTOs;
	}

	public void setContestDTOs(List<ContestDTO> contestDTOs) {
		this.contestDTOs = contestDTOs;
	}

	@Override
	public String toString() {
		return "GetSquadResponse [squad=" + squad + "]";
	}
}
