package com.myteam.respponse;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;
import com.myteam.dto.MatchBattingDTO;
import com.myteam.dto.MatchBowlingDTO;
import com.myteam.dto.MatchFieldingDTO;
import com.myteam.dto.TeamDTO;

public class MatchSummaryResponse {

	private List<MatchFieldingDTO> fielding = new ArrayList<>();

	private List<MatchBattingDTO> batting = new ArrayList<>();

	private List<MatchBowlingDTO> bowling = new ArrayList<>();
	
	private List<TeamDTO> team = new ArrayList<>();
 
	@SerializedName("toss_winner_team")
	private String tossWinnerTeam;
	
	@SerializedName("winner_team")
	private String winnerTeam;
	
	private boolean matchStarted;
	
	private String additionalMessage;
	
	public String getAdditionalMessage() {
		return additionalMessage;
	}

	public void setAdditionalMessage(String additionalMessage) {
		this.additionalMessage = additionalMessage;
	}

	public List<MatchFieldingDTO> getFielding() {
		return fielding;
	}

	public void setFielding(List<MatchFieldingDTO> fielding) {
		this.fielding = fielding;
	}

	public List<MatchBattingDTO> getBatting() {
		return batting;
	}

	public void setBatting(List<MatchBattingDTO> batting) {
		this.batting = batting;
	}

	public List<MatchBowlingDTO> getBowling() {
		return bowling;
	}

	public void setBowling(List<MatchBowlingDTO> bowling) {
		this.bowling = bowling;
	}

	public String getTossWinnerTeam() {
		return tossWinnerTeam;
	}

	public void setTossWinnerTeam(String tossWinnerTeam) {
		this.tossWinnerTeam = tossWinnerTeam;
	}

	public boolean isMatchStarted() {
		return matchStarted;
	}

	public void setMatchStarted(boolean matchStarted) {
		this.matchStarted = matchStarted;
	}
	
	public List<TeamDTO> getTeam() {
		return team;
	}

	public void setTeam(List<TeamDTO> team) {
		this.team = team;
	}
	
	public String getWinnerTeam() {
		return winnerTeam;
	}

	public void setWinnerTeam(String winnerTeam) {
		this.winnerTeam = winnerTeam;
	}

	@Override
	public String toString() {
		return "MatchSummaryResponse [fielding=" + fielding + ", batting=" + batting + ", bowling=" + bowling
				+ ", team=" + team +  ", tossWinnerTeam=" + tossWinnerTeam
				+ ", winnerTeam=" + winnerTeam + ", matchStarted=" + matchStarted + "]";
	}
}
