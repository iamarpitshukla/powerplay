package com.myteam.respponse;

import com.myteam.doc.PlayersDoc;
import com.myteam.dto.PlayerDetailDTO;

public class GetPlayerStatsResponse extends ServiceResponse{

	private Integer pid;
	
	private String profile;
	
	private String imageURL;
	
	private String battingStyle;
	
	private String bowlingStyle;
	
	private String majorTeams;
	
	private String currentAge;
	
	private String born;
	
	private String fullName;
	
	private String name;
	
	private String country;
	
	private String playingRole;
	
	private PlayerDetailDTO data;
	
	public GetPlayerStatsResponse() {
		super();
	}
	
	public GetPlayerStatsResponse(PlayersDoc playerDoc) {
		super();
		this.pid = playerDoc.getPid();
		this.profile = playerDoc.getProfile();
		this.imageURL = playerDoc.getImageURL();
		this.battingStyle = playerDoc.getBattingStyle();
		this.bowlingStyle = playerDoc.getBowlingStyle();
		this.majorTeams = playerDoc.getMajorTeams();
		this.currentAge = playerDoc.getCurrentAge();
		this.born = playerDoc.getBorn();
		this.fullName = playerDoc.getFullName();
		this.name = playerDoc.getName();
		this.playingRole = playerDoc.getPlayingRole();
		this.data = playerDoc.getData();
		this.country = playerDoc.getCountry();
	}

	public Integer getPid() {
		return pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	public String getBattingStyle() {
		return battingStyle;
	}

	public void setBattingStyle(String battingStyle) {
		this.battingStyle = battingStyle;
	}

	public String getBowlingStyle() {
		return bowlingStyle;
	}

	public void setBowlingStyle(String bowlingStyle) {
		this.bowlingStyle = bowlingStyle;
	}

	public String getMajorTeams() {
		return majorTeams;
	}

	public void setMajorTeams(String majorTeams) {
		this.majorTeams = majorTeams;
	}

	public String getCurrentAge() {
		return currentAge;
	}

	public void setCurrentAge(String currentAge) {
		this.currentAge = currentAge;
	}

	public String getBorn() {
		return born;
	}

	public void setBorn(String born) {
		this.born = born;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPlayingRole() {
		return playingRole;
	}

	public void setPlayingRole(String playingRole) {
		this.playingRole = playingRole;
	}

	public PlayerDetailDTO getData() {
		return data;
	}

	public void setData(PlayerDetailDTO data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "GetPlayerStatsResponse [pid=" + pid + ", profile=" + profile + ", imageURL=" + imageURL
				+ ", battingStyle=" + battingStyle + ", bowlingStyle=" + bowlingStyle + ", majorTeams=" + majorTeams
				+ ", currentAge=" + currentAge + ", born=" + born + ", fullName=" + fullName + ", name=" + name
				+ ", country=" + country + ", playingRole=" + playingRole + ", data=" + data + "]";
	}
}
