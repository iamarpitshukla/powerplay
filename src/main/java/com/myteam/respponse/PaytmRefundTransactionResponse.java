package com.myteam.respponse;

public class PaytmRefundTransactionResponse extends BasicPaytmTransactionResponse{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	


	private String GATEWAY;
	private String REFUNDAMOUNT;
	private String TOTALREFUNDAMT;
	private String REFUNDDATE;
	private String REFUNDTYPE;
	private String REFID;
	private String REFUNDID;

	public String getGATEWAY() {
		return GATEWAY;
	}

	public void setGATEWAY(String gATEWAY) {
		GATEWAY = gATEWAY;
	}

	public String getREFUNDAMOUNT() {
		return REFUNDAMOUNT;
	}

	public void setREFUNDAMOUNT(String rEFUNDAMOUNT) {
		REFUNDAMOUNT = rEFUNDAMOUNT;
	}

	public String getTOTALREFUNDAMT() {
		return TOTALREFUNDAMT;
	}

	public void setTOTALREFUNDAMT(String tOTALREFUNDAMT) {
		TOTALREFUNDAMT = tOTALREFUNDAMT;
	}

	public String getREFUNDDATE() {
		return REFUNDDATE;
	}

	public void setREFUNDDATE(String rEFUNDDATE) {
		REFUNDDATE = rEFUNDDATE;
	}

	public String getREFUNDTYPE() {
		return REFUNDTYPE;
	}

	public void setREFUNDTYPE(String rEFUNDTYPE) {
		REFUNDTYPE = rEFUNDTYPE;
	}

	public String getREFID() {
		return REFID;
	}

	public void setREFID(String rEFID) {
		REFID = rEFID;
	}

	public String getREFUNDID() {
		return REFUNDID;
	}

	public void setREFUNDID(String rEFUNDID) {
		REFUNDID = rEFUNDID;
	}



}
