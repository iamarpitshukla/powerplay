package com.myteam.respponse;

public class GetTeamAndJoinedContestCountResponse extends ServiceResponse{

	private Integer teamCount;
	
	private Integer contestCount;

	public Integer getTeamCount() {
		return teamCount;
	}

	public void setTeamCount(Integer teamCount) {
		this.teamCount = teamCount;
	}

	public Integer getContestCount() {
		return contestCount;
	}

	public void setContestCount(Integer contestCount) {
		this.contestCount = contestCount;
	}

	@Override
	public String toString() {
		return "GetTeamAndJoinedContestCountResponse [teamCount=" + teamCount + ", contestCount=" + contestCount + "]";
	}
	
}
