package com.myteam.respponse;

import java.util.ArrayList;
import java.util.List;

import com.myteam.dto.MatchDTO;

public class GetFutureMatchResponse extends ServiceResponse{

	private List<MatchDTO> matches = new ArrayList<>();

	public List<MatchDTO> getMatches() {
		return matches;
	}

	public void setMatches(List<MatchDTO> matches) {
		this.matches = matches;
	}
}
