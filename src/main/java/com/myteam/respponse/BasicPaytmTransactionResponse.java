package com.myteam.respponse;

public class BasicPaytmTransactionResponse extends AbstractPaymentGatewayResponse{



	/**
	 * 
	 */
	private static final long serialVersionUID = 9199298192481228202L;
	private String MID;
	private String TXNID;
	private String ORDERID;
	private String BANKTXNID;
	private String TXNAMOUNT;
	private String STATUS;
	private String RESPCODE;
	private String RESPMSG;
	private String TXNDATE;
	private String PAYMENTMODE;

	public String getMID() {
		return MID;
	}

	public void setMID(String mID) {
		MID = mID;
	}

	public String getTXNID() {
		return TXNID;
	}

	public void setTXNID(String tXNID) {
		TXNID = tXNID;
	}

	public String getORDERID() {
		return ORDERID;
	}

	public void setORDERID(String oRDERID) {
		ORDERID = oRDERID;
	}

	public String getBANKTXNID() {
		return BANKTXNID;
	}

	public void setBANKTXNID(String bANKTXNID) {
		BANKTXNID = bANKTXNID;
	}

	public String getTXNAMOUNT() {
		return TXNAMOUNT;
	}

	public void setTXNAMOUNT(String tXNAMOUNT) {
		TXNAMOUNT = tXNAMOUNT;
	}

	public String getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}

	public String getRESPCODE() {
		return RESPCODE;
	}

	public void setRESPCODE(String rESPCODE) {
		RESPCODE = rESPCODE;
	}

	public String getRESPMSG() {
		return RESPMSG;
	}

	public void setRESPMSG(String rESPMSG) {
		RESPMSG = rESPMSG;
	}

	public String getTXNDATE() {
		return TXNDATE;
	}

	public void setTXNDATE(String tXNDATE) {
		TXNDATE = tXNDATE;
	}

	public String getPAYMENTMODE() {
		return PAYMENTMODE;
	}

	public void setPAYMENTMODE(String pAYMENTMODE) {
		PAYMENTMODE = pAYMENTMODE;
	}

	@Override
	public String toString() {
		return "BasicPaytmTransactionResponse [MID=" + MID + ", TXNID=" + TXNID + ", ORDERID=" + ORDERID
				+ ", BANKTXNID=" + BANKTXNID + ", TXNAMOUNT=" + TXNAMOUNT + ", STATUS=" + STATUS + ", RESPCODE="
				+ RESPCODE + ", RESPMSG=" + RESPMSG + ", TXNDATE=" + TXNDATE + ", PAYMENTMODE=" + PAYMENTMODE + "]";
	}


}
