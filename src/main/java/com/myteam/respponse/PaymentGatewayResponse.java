package com.myteam.respponse;

import java.util.HashMap;
import java.util.Map;

public class PaymentGatewayResponse {

    public enum PGResponseStatus {
        SUCCESS("success"), FAIL("fail"), PENDING_VERIFICATION("pending_verification");

        private String responseType;

        private PGResponseStatus(String responseType) {
            this.responseType = responseType;
        }

        public String responseType() {
            return responseType;
        }

    }
    
    private String              status;
    private String              message;
    private Map<String, Object> items;
    private String				pgId;
	private String 				code;
    
    public PaymentGatewayResponse(PGResponseStatus status) {
        this.status = status.responseType();
    }

    public PaymentGatewayResponse(PGResponseStatus status, Map<String, Object> items) {
        this.status = status.responseType();
        this.items = items;
    }
    
    public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setItems(Map<String, Object> items) {
        this.items = items;
    }

    public Map<String, Object> getItems() {
        return items;
    }

    public void addItem(String name, Object value) {
        if (items == null) {
            items = new HashMap<String, Object>();
        }
        items.put(name, value);
    }

	public String getPgId() {
		return pgId;
	}

	public void setPgId(String pgId) {
		this.pgId = pgId;
	}

	@Override
	public String toString() {
		return "PaymentGatewayResponse [status=" + status + ", message=" + message + ", items=" + items + ", pgId="
				+ pgId + "]";
	}


}
