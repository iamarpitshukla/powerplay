package com.myteam.respponse;

import java.util.ArrayList;
import java.util.List;

import com.myteam.dto.WinningDistributionDTO;

public class GetWinningBreakUpResponse extends ServiceResponse{

	private List<WinningDistributionDTO> distributionDTO = new ArrayList<>();
	
	private Integer prizePool;
	
	public Integer getPrizePool() {
		return prizePool;
	}

	public void setPrizePool(Integer prizePool) {
		this.prizePool = prizePool;
	}

	public List<WinningDistributionDTO> getDistributionDTO() {
		return distributionDTO;
	}

	public void setDistributionDTO(List<WinningDistributionDTO> distributionDTO) {
		this.distributionDTO = distributionDTO;
	}

	@Override
	public String toString() {
		return "GetWinningBreakUpResponse [distributionDTO=" + distributionDTO + ", prizePool=" + prizePool + "]";
	}
}
