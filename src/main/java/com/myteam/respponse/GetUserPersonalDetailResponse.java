package com.myteam.respponse;

import com.myteam.dto.UserDetailDTO;

public class GetUserPersonalDetailResponse extends ServiceResponse{
	
	private UserDetailDTO user = new UserDetailDTO();

	public UserDetailDTO getUser() {
		return user;
	}

	public void setUser(UserDetailDTO user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "GetUserPersonalDetailResponse [user=" + user + "]";
	}
}
