package com.myteam.respponse;

import java.math.BigDecimal;

public class GetUserWalletDetailResponse extends ServiceResponse {

	private BigDecimal totalAmount;

	private BigDecimal depositedAmount;

	private BigDecimal wonAmount;

	private BigDecimal bonus = new BigDecimal(0);

	public BigDecimal getBonus() {
		return bonus;
	}

	public void setBonus(BigDecimal bonus) {
		this.bonus = bonus;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public BigDecimal getDepositedAmount() {
		return depositedAmount;
	}

	public void setDepositedAmount(BigDecimal depositedAmount) {
		this.depositedAmount = depositedAmount;
	}

	public BigDecimal getWonAmount() {
		return wonAmount;
	}

	public void setWonAmount(BigDecimal wonAmount) {
		this.wonAmount = wonAmount;
	}

	@Override
	public String toString() {
		return "GetUserWalletDetailResponse [totalAmount=" + totalAmount + ", depositedAmount=" + depositedAmount
				+ ", wonAmount=" + wonAmount + ", bonus=" + bonus + "]";
	}
}
