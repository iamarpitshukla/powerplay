package com.myteam.respponse;

import java.util.ArrayList;
import java.util.List;

import com.myteam.dto.SpecificTeamPointDTO;

public class GetSpecificTeamPointsResponse extends ServiceResponse{

	private List<SpecificTeamPointDTO> teamDetails = new ArrayList<>();
	
	private Integer teamNumber;
	
	public Integer getTeamNumber() {
		return teamNumber;
	}

	public void setTeamNumber(Integer teamNumber) {
		this.teamNumber = teamNumber;
	}

	public List<SpecificTeamPointDTO> getTeamDetails() {
		return teamDetails;
	}

	public void setTeamDetails(List<SpecificTeamPointDTO> teamDetails) {
		this.teamDetails = teamDetails;
	}

	@Override
	public String toString() {
		return "GetSpecificTeamPointsResponse [teamDetails=" + teamDetails + ", teamNumber=" + teamNumber + "]";
	}
}
