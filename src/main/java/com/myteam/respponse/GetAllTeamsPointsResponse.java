package com.myteam.respponse;

import java.util.List;

import com.myteam.dto.UserTeamPointDetailDTO;

public class GetAllTeamsPointsResponse extends ServiceResponse{

	private List<UserTeamPointDetailDTO> teams;
	
	public List<UserTeamPointDetailDTO> getTeams() {
		return teams;
	}

	public void setTeams(List<UserTeamPointDetailDTO> teams) {
		this.teams = teams;
	}

	@Override
	public String toString() {
		return "GetAllTeamsPointsResponse [teams=" + teams + "]";
	}
}
