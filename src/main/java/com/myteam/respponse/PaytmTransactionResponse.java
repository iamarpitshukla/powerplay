package com.myteam.respponse;

import java.io.Serializable;

public class PaytmTransactionResponse extends BasicPaytmTransactionResponse implements Serializable{
	


	/**
	 * 
	 */
	private static final long serialVersionUID = -1144341209022134931L;
	private String CURRENCY;
	private String GATEWAYNAME;
	private String BANKNAME;
	private String PROMO_CAMP_ID;
	private String PROMO_STATUS;
	private String PROMO_RESPCODE;
	private String CHECKSUMHASH;
	private String MERC_UNQ_REF;
	
	public String getCURRENCY() {
		return CURRENCY;
	}

	public void setCURRENCY(String cURRENCY) {
		CURRENCY = cURRENCY;
	}

	public String getGATEWAYNAME() {
		return GATEWAYNAME;
	}

	public void setGATEWAYNAME(String gATEWAYNAME) {
		GATEWAYNAME = gATEWAYNAME;
	}

	public String getBANKNAME() {
		return BANKNAME;
	}

	public void setBANKNAME(String bANKNAME) {
		BANKNAME = bANKNAME;
	}

	public String getPROMO_CAMP_ID() {
		return PROMO_CAMP_ID;
	}

	public void setPROMO_CAMP_ID(String pROMO_CAMP_ID) {
		PROMO_CAMP_ID = pROMO_CAMP_ID;
	}

	public String getPROMO_STATUS() {
		return PROMO_STATUS;
	}

	public void setPROMO_STATUS(String pROMO_STATUS) {
		PROMO_STATUS = pROMO_STATUS;
	}

	public String getPROMO_RESPCODE() {
		return PROMO_RESPCODE;
	}

	public void setPROMO_RESPCODE(String pROMO_RESPCODE) {
		PROMO_RESPCODE = pROMO_RESPCODE;
	}

	public String getCHECKSUMHASH() {
		return CHECKSUMHASH;
	}

	public void setCHECKSUMHASH(String cHECKSUMHASH) {
		CHECKSUMHASH = cHECKSUMHASH;
	}
	
	public String getMERC_UNQ_REF() {
		return MERC_UNQ_REF;
	}

	public void setMERC_UNQ_REF(String mERC_UNQ_REF) {
		MERC_UNQ_REF = mERC_UNQ_REF;
	}

	@Override
	public String toString() {
		return "PaytmTransactionResponse [CURRENCY=" + CURRENCY + ", GATEWAYNAME=" + GATEWAYNAME + ", BANKNAME="
				+ BANKNAME + ", PROMO_CAMP_ID=" + PROMO_CAMP_ID + ", PROMO_STATUS=" + PROMO_STATUS + ", PROMO_RESPCODE="
				+ PROMO_RESPCODE + ", CHECKSUMHASH=" + CHECKSUMHASH + ", MERC_UNQ_REF=" + MERC_UNQ_REF + ", getMID()="
				+ getMID() + ", getTXNID()=" + getTXNID() + ", getORDERID()=" + getORDERID() + ", getBANKTXNID()="
				+ getBANKTXNID() + ", getTXNAMOUNT()=" + getTXNAMOUNT() + ", getSTATUS()=" + getSTATUS()
				+ ", getRESPCODE()=" + getRESPCODE() + ", getRESPMSG()=" + getRESPMSG() + ", getTXNDATE()="
				+ getTXNDATE() + ", getPAYMENTMODE()=" + getPAYMENTMODE() + ", toString()=" + super.toString()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
	}

}
