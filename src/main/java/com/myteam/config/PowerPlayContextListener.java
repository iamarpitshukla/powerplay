package com.myteam.config;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.myteam.services.IPowerPlayStartupService;

public class PowerPlayContextListener implements ServletContextListener {

	private static final Logger LOG = LoggerFactory.getLogger(PowerPlayContextListener.class);

    @Override
    public void contextInitialized(ServletContextEvent arg0) {
         LOG.info("Initializing Powerplay Context...");
        WebApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(arg0.getServletContext());
        IPowerPlayStartupService powerPlaytartupService = context.getBean(IPowerPlayStartupService.class);
        try {
            powerPlaytartupService.loadAll();
        } catch (Exception e) {
            LOG.error("Error while initializing application ", e);
            return;
        }
    }

	@Override
	public void contextDestroyed(ServletContextEvent sce) {

	}

}
