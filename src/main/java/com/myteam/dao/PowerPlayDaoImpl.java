package com.myteam.dao;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.myteam.entity.ContestWinningDistribution;
import com.myteam.entity.Contests;
import com.myteam.entity.Match;
import com.myteam.entity.MatchData;
import com.myteam.entity.SmsTemplate;
import com.myteam.entity.SystemProperties;
import com.myteam.entity.User;
import com.myteam.entity.UserMatchTeamContestMapping;
import com.myteam.entity.UserMatchTeamMapping;
import com.myteam.request.GetFutureMatchRequest;
import com.myteam.request.GetFutureMatchRequest.Type;
import com.myteam.utils.CollectionUtils;
import com.myteam.utils.DateUtils;
import com.myteam.utils.StringUtils;

@Repository("powerPlayDao")
public class PowerPlayDaoImpl implements IPowerPlayDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Match saveMatch(Match match) {
		if (match.getCreated() == null) {
			match.setCreated(new Date());
		}
		match.setUpdated(new Date());
		return  (Match) sessionFactory.getCurrentSession().merge(match);
	}
	
	@Override
	public void saveMatchData(MatchData matchdata) {
		if (matchdata.getCreated() == null) {
			matchdata.setCreated(new Date());
		}
		matchdata.setUpdated(new Date());
		sessionFactory.getCurrentSession().merge(matchdata);
	}
	
	@Override
	public Contests saveContests(Contests contests) {
		if (contests.getCreated() == null) {
			contests.setCreated(new Date());
		}
		contests.setUpdated(new Date());
		return (Contests) sessionFactory.getCurrentSession().merge(contests);
	}
	
	@Override
	public ContestWinningDistribution saveContestWinningDistribution(ContestWinningDistribution contestWinningDistribution) {
		if (contestWinningDistribution.getCreated() == null) {
			contestWinningDistribution.setCreated(new Date());
		}
		contestWinningDistribution.setUpdated(new Date());
		return (ContestWinningDistribution) sessionFactory.getCurrentSession().merge(contestWinningDistribution);
	}

	@Override
	public void saveUserMatchContestPlayersMapping(UserMatchTeamContestMapping userMatchContestPlayersMapping) {
		if (userMatchContestPlayersMapping.getCreated() == null) {
			userMatchContestPlayersMapping.setCreated(new Date());
		}
		userMatchContestPlayersMapping.setUpdated(new Date());
		sessionFactory.getCurrentSession().merge(userMatchContestPlayersMapping);
	}
	
	@Override
	public UserMatchTeamMapping saveUserMatchTeamMapping(UserMatchTeamMapping userMatchTeamMapping) {
		if (userMatchTeamMapping.getCreated() == null) {
			userMatchTeamMapping.setCreated(new Date());
		}
		userMatchTeamMapping.setUpdated(new Date());
		return (UserMatchTeamMapping) sessionFactory.getCurrentSession().merge(userMatchTeamMapping);
	}
	
	@Override
	public UserMatchTeamContestMapping getUserMatchContestPlayersMappingByCode(String code) {
		StringBuilder queryString = new StringBuilder("select * from `user_match_team_contest_mapping` where enabled = 1 ");
		if(StringUtils.isNotEmpty(code)) {
			queryString.append(" and code =:code ");
		}
		NativeQuery<UserMatchTeamContestMapping> query = sessionFactory.getCurrentSession().createNativeQuery(queryString.toString(),
				UserMatchTeamContestMapping.class);
		query.setParameter("code", code);
		query.setMaxResults(1);
		return query.uniqueResult();
	}
	
	@Override
	public UserMatchTeamMapping getUserMatchTeamMappingById(Integer userMatchTeamMappingId) {
		StringBuilder queryString = new StringBuilder("select * from `user_match_team_mapping` where enabled = 1 ");
		if(userMatchTeamMappingId != null) {
			queryString.append(" and id =:userMatchTeamMappingId ");
		}
		NativeQuery<UserMatchTeamMapping> query = sessionFactory.getCurrentSession().createNativeQuery(queryString.toString(),
				UserMatchTeamMapping.class);
		query.setParameter("userMatchTeamMappingId", userMatchTeamMappingId);
		query.setMaxResults(1);
		return query.uniqueResult();
	}
	
	@Override
	@Transactional
	public List<UserMatchTeamMapping> getUserMatchTeamMappingByUserIdAndMatchId(Integer userId, Integer matchId) {
		StringBuilder queryString = new StringBuilder("select * from `user_match_team_mapping` where enabled = 1 and match_id =:matchId and user_id =:userId ");
		NativeQuery<UserMatchTeamMapping> query = sessionFactory.getCurrentSession().createNativeQuery(queryString.toString(),
				UserMatchTeamMapping.class);
		query.setParameter("userId", userId);
		query.setParameter("matchId", matchId);
		return query.list();
	}
	
	@Override
	public List<UserMatchTeamContestMapping> getUserMatchContestPlayersMappingByContestId(Integer contestId, Integer start, Integer maxResults, Integer userId, Boolean include, Integer rank) {
		StringBuilder queryString = new StringBuilder("select * from `user_match_team_contest_mapping` where enabled = 1  and contests_id =:contestId  ");
		if(include == null) {
			
		}else if(include) {
			queryString.append(" and user_id =:userId ");
		}else {
			queryString.append(" and user_id !=:userId ");
		}
		if(rank != null) {
			queryString.append(" and rank =:rank ");
		}
		queryString.append(" order by points desc ");	
		NativeQuery<UserMatchTeamContestMapping> query = sessionFactory.getCurrentSession().createNativeQuery(queryString.toString(),
				UserMatchTeamContestMapping.class);
		query.setParameter("contestId", contestId);
		if(include != null) {
			query.setParameter("userId", userId);
		}
		if(rank != null) {
			query.setParameter("rank", rank);
		}
		if(start != null) {
			query.setFirstResult(start);
		}
		if(maxResults != null) {
			query.setMaxResults(maxResults);
		}
		return query.list();
	}
	
	@Override
	public void saveContest(Contests contest) {
		if (contest.getCreated() == null) {
			contest.setCreated(new Date());
		}
		contest.setUpdated(new Date());
		sessionFactory.getCurrentSession().save(contest);
	}
	
	@Override
	public List<Match> getMatches(GetFutureMatchRequest request) {
		StringBuilder queryString = new StringBuilder("select * from `match` where enabled = 1 and squad = 1 ");
		if(Type.UPCOMMING.code().equalsIgnoreCase(request.getType())) {
			queryString.append(" and  start_date >:now  order by start_date ");
		}else if(Type.LIVE.code().equalsIgnoreCase(request.getType())) {
			queryString.append(" and match_started= 1  and winner_team is NULL  order by start_date ");
		}else if(Type.PAST.code().equalsIgnoreCase(request.getType())) {
			queryString.append(" and match_started= 1 and winner_team is NOT NULL   order by start_date desc ");
		}
		NativeQuery<Match> query = sessionFactory.getCurrentSession().createNativeQuery(queryString.toString(),
				Match.class);
		query.setParameter("now", new Date());
		query.setFirstResult(request.getStart());
		query.setMaxResults(request.getMaxResults());
		return query.list();
	}
	
	@Override
	public List<UserMatchTeamContestMapping> getUserMatches(GetFutureMatchRequest request, Integer userId) {
		StringBuilder queryString = new StringBuilder("select umtcm.* from  `match` m inner join user_match_team_contest_mapping umtcm on m.id = umtcm.match_id ");
		queryString.append(" where umtcm.user_id =:userId ");
		if(Type.UPCOMMING.code().equalsIgnoreCase(request.getType())) {
			queryString.append(" and m.match_started= 0 group by m.id  order by m.start_date ");
		}else if(Type.LIVE.code().equalsIgnoreCase(request.getType())) {
			queryString.append(" and m.match_started= 1  and winner_team is NULL group by m.id order by m.start_date  ");
		}else if(Type.PAST.code().equalsIgnoreCase(request.getType())) {
			queryString.append(" and m.match_started= 1 and winner_team is NOT NULL group by m.id  order by m.start_date desc ");
		}
		NativeQuery<UserMatchTeamContestMapping> query = sessionFactory.getCurrentSession().createNativeQuery(queryString.toString(),
				UserMatchTeamContestMapping.class);
		query.setParameter("userId", userId);
		query.setFirstResult(request.getStart());
		query.setMaxResults(request.getMaxResults());
		return query.list();
	}
	
	@Override
	public List<UserMatchTeamContestMapping> getUserMatchTeamMappingsByMatchIds(List<Integer> matchIds, Integer userId) {
		StringBuilder queryString = new StringBuilder("select * from user_match_team_contest_mapping umtcm where umtcm.match_id in (:matchIds) and user_id =:userId group by umtcm.contests_id ");
		NativeQuery<UserMatchTeamContestMapping> query = sessionFactory.getCurrentSession().createNativeQuery(queryString.toString(),
				UserMatchTeamContestMapping.class);
		query.setParameterList("matchIds", matchIds);
		query.setParameter("userId", userId);
		return query.list();
	}
	
	@Override
	public List<Match> getMatchesWithoutSquad(GetFutureMatchRequest request) {
		StringBuilder queryString = new StringBuilder("select * from `match` where enabled = 1 and squad = 1 and players is NULL ");
		NativeQuery<Match> query = sessionFactory.getCurrentSession().createNativeQuery(queryString.toString(),
				Match.class);
		query.setFirstResult(request.getStart());
		query.setMaxResults(request.getMaxResults());
		return query.list();
	}
	
	@Override
	public List<Match> getMatchesByUniqueIds(List<Integer> uniqueIds) {
		StringBuilder queryString = new StringBuilder("select * from `match` where match_unique_id in (:uniqueIds) ");
		NativeQuery<Match> query = sessionFactory.getCurrentSession().createNativeQuery(queryString.toString(),
				Match.class);
		if(CollectionUtils.isNotEmpty(uniqueIds)) {
			query.setParameterList("uniqueIds", uniqueIds);
		}
		return query.list();
	}
	
	@Override
	public Match getMatcheByMatchId(Integer matchId) {
		StringBuilder queryString = new StringBuilder("select * from `match` where enabled = 1 ");
		if(matchId != null) {
			queryString.append(" and id =:matchId ");
		}
		NativeQuery<Match> query = sessionFactory.getCurrentSession().createNativeQuery(queryString.toString(),
				Match.class);
		query.setParameter("matchId", matchId);
		query.setMaxResults(1);
		return query.uniqueResult();
	}
	
	@Override
	public List<Contests> getContestsForMatch(Integer matchId) {
		StringBuilder queryString = new StringBuilder("select * from `contests` where enabled = 1 and "
				+ "match_id=:matchId and time_crossed = 0 and playing_count < total_count  ");
		NativeQuery<Contests> query = sessionFactory.getCurrentSession().createNativeQuery(queryString.toString(),
				Contests.class);
			query.setParameter("matchId", matchId);
		return query.list();
	}
	
	@Override
	public List<Contests> getContestsForMatchIds(List<Integer> matchIds) {
		StringBuilder queryString = new StringBuilder("select * from `contests` where enabled = 1 and match_id in (:matchIds) ");
		NativeQuery<Contests> query = sessionFactory.getCurrentSession().createNativeQuery(queryString.toString(),
				Contests.class);
			query.setParameterList("matchIds", matchIds);
		return query.list();
	}
	
	@Override
	public List<Contests> getContestsFromStartDate(Date startDate) {
		StringBuilder queryString = new StringBuilder("select * from `contests` where enabled = 1 and created>=:startDate ");
		NativeQuery<Contests> query = sessionFactory.getCurrentSession().createNativeQuery(queryString.toString(),
				Contests.class);
			query.setParameter("startDate", startDate);
		return query.list();
	}
	
	@Override
	public List<SystemProperties> getSystemProperties() {
		StringBuilder queryString = new StringBuilder("select * from `powerplay_properties` ");
		NativeQuery<SystemProperties> query = sessionFactory.getCurrentSession().createNativeQuery(queryString.toString(),
				SystemProperties.class);
		return query.list();
	}
	
	@Override
	public Contests getContestsById(Integer contestId) {
		StringBuilder queryString = new StringBuilder("select * from `contests` where enabled = 1 and id=:contestId ");
		NativeQuery<Contests> query = sessionFactory.getCurrentSession().createNativeQuery(queryString.toString(),
				Contests.class);
			query.setParameter("contestId", contestId);
		return query.uniqueResult();
	}
	
	@Override
	public Match getMatchById(Integer matchId) {
		StringBuilder queryString = new StringBuilder("select * from `match` where enabled = 1 and id=:matchId ");
		NativeQuery<Match> query = sessionFactory.getCurrentSession().createNativeQuery(queryString.toString(),
				Match.class);
			query.setParameter("matchId", matchId);
		return query.uniqueResult();
	}
	
	@Override
	public List<Match> getCurrentlyRunningMatches(){
		StringBuilder queryString = new StringBuilder("select * from `match`  where match_started = 1 and winner_team is NULL and toss_winner_team !='no toss' and squad= 1 and enabled = 1  order by id");
		NativeQuery<Match> query = sessionFactory.getCurrentSession().createNativeQuery(queryString.toString(),
				Match.class);
		return query.list();
	}
	
	@Override
	public MatchData getMatchDataByMatchId(Integer matchId) {
		StringBuilder queryString = new StringBuilder("select * from `match_data` where enabled = 1 ");
		if(matchId != null) {
			queryString.append(" and match_id =:matchId ");
		}
		NativeQuery<MatchData> query = sessionFactory.getCurrentSession().createNativeQuery(queryString.toString(),
				MatchData.class);
		query.setParameter("matchId", matchId);
		query.setMaxResults(1);
		return query.uniqueResult();
	}
	
	@Override
	public List<Contests> getAllOverContests(Integer matchId) {
		StringBuilder queryString = new StringBuilder("select * from `contests` where enabled = 1 and match_id=:matchId and contest_over = 1 order by end_over ");
		NativeQuery<Contests> query = sessionFactory.getCurrentSession().createNativeQuery(queryString.toString(),
				Contests.class);
			query.setParameter("matchId", matchId);
		return query.list();
	}
	
	@Override
	public List<Contests> getAllCurrentlyWorkingContests(Integer matchId, Integer endOver, Integer inning) {
		StringBuilder queryString = new StringBuilder("select * from `contests` where enabled = 1 and match_id=:matchId and contest_over = 0  order by end_over ");
		NativeQuery<Contests> query = sessionFactory.getCurrentSession().createNativeQuery(queryString.toString(),
				Contests.class);
			query.setParameter("matchId", matchId);
		return query.list();
	}
	
	
	@Override
	public List<Contests> getTimeCrossedContestsForMatchAndEndOversAndInning(Integer matchId, Integer startOver, Integer inning) {
		StringBuilder queryString = new StringBuilder("select * from `contests` where enabled = 1 and match_id=:matchId and start_over <=:startOver and inning=:inning and time_crossed = 0 ");
		NativeQuery<Contests> query = sessionFactory.getCurrentSession().createNativeQuery(queryString.toString(),
				Contests.class);
			query.setParameter("matchId", matchId);
			query.setParameter("startOver", startOver);
			query.setParameter("inning", inning);
		return query.list();
	}
	
	@Override
	public List<Contests> getAllNonReviewedContests() {
		StringBuilder queryString = new StringBuilder("select * from `contests` where enabled = 1 and reviewed=0 and contest_over = 1 ");
		NativeQuery<Contests> query = sessionFactory.getCurrentSession().createNativeQuery(queryString.toString(),
				Contests.class);
		return query.list();
	}
	
	@Override
	public List<SmsTemplate> getSmsTemplates() {
		StringBuilder queryString = new StringBuilder("select * from `sms_template` where enabled = 1 ");
		NativeQuery<SmsTemplate> query = sessionFactory.getCurrentSession().createNativeQuery(queryString.toString(),
				SmsTemplate.class);
		return query.list();
	}
	
	@Override
	public List<UserMatchTeamContestMapping> getJoinedContests(Integer matchId, Integer userId) {
		StringBuilder queryString = new StringBuilder("select umtcm.* from user_match_team_contest_mapping umtcm  join contests c on umtcm.contests_id = c.id where umtcm.match_id =:matchId and user_id=:userId group by umtcm.contests_id order by c.reviewed, umtcm.id ");
		NativeQuery<UserMatchTeamContestMapping> query = sessionFactory.getCurrentSession().createNativeQuery(queryString.toString(),
				UserMatchTeamContestMapping.class);
			query.setParameter("matchId", matchId);
			query.setParameter("userId", userId);
		return query.list();
	}
	
	@Override
	public void updateUMTCMWinningAmountByIds(Integer amount, List<Integer> ids) {
		StringBuilder queryString = new StringBuilder("update user_match_team_contest_mapping umtcm  set umtcm.winning_amount =:amount  where umtcm.id in (:ids)  ");
		sessionFactory.getCurrentSession().createNativeQuery(queryString.toString())
		.setParameter("amount", amount)
		.setParameterList("ids", ids)
		.executeUpdate();
	}
	
	@Override
	public List<UserMatchTeamContestMapping> getContestWiseJoinedContests(Integer contestId, Integer userId) {
		StringBuilder queryString = new StringBuilder("select * from user_match_team_contest_mapping umtcm  where umtcm.contests_id =:contestId and user_id=:userId  ");
		NativeQuery<UserMatchTeamContestMapping> query = sessionFactory.getCurrentSession().createNativeQuery(queryString.toString(),
				UserMatchTeamContestMapping.class);
			query.setParameter("contestId", contestId);
			query.setParameter("userId", userId);
		return query.list();
	}
	
	@Override
	public List<Match> getMatchesWithoutPlayingEleven() {
		StringBuilder queryString = new StringBuilder("select * from `match` where enabled = 1 and start_date <=:checkTossTime and winner_team is  null and playing_eleven is NULL ");
		NativeQuery<Match> query = sessionFactory.getCurrentSession().createNativeQuery(queryString.toString(),
				Match.class);
		query.setParameter("checkTossTime", DateUtils.getDateTimeFomDateAfterMin(new Date(), 60));
		return query.list();
	}
	
	@Override
	public List<Match> getMatchesToCreateAutoTeams() {
		StringBuilder queryString = new StringBuilder("	select * from `match` where favourite_players is not NULL and toss_winner_team is not null and winner_team is NULL and start_date >=:currTime ");
		NativeQuery<Match> query = sessionFactory.getCurrentSession().createNativeQuery(queryString.toString(),
				Match.class);
		query.setParameter("currTime", new Date());
		return query.list();
	}
	
	@Override
	public List<Contests> getAutoTeamCreateContestsForMatch(Integer matchId) {
		StringBuilder queryString = new StringBuilder("select * from `contests` where enabled = 1 and "
				+ "match_id=:matchId and auto_team_create = 1 and playing_count < total_count  ");
		NativeQuery<Contests> query = sessionFactory.getCurrentSession().createNativeQuery(queryString.toString(),
				Contests.class);
			query.setParameter("matchId", matchId);
		return query.list();
	}
}
