package com.myteam.dao;

import java.util.List;

import com.myteam.entity.User;
import com.myteam.entity.UserReferralDetail;
import com.myteam.entity.UtilCredentials;

public interface IUserDao {
	
	User saveUser(User user);

	List<User> list();

	User getUserByMobileOrEmail(String mobile, String email, boolean enabled);

	User getUserByUserId(Integer userId);

	UserReferralDetail saveUserReferralDetail(UserReferralDetail userReferralDetail);

	UserReferralDetail getUserReferralDetailByCode(String code);

	List<UserReferralDetail> getUserReferralDetailUserId(Integer userId, boolean referred);

	List<User> getUsersByUserId(List<Integer> userIds);

	User getUserByMobile(String mobile);

	UtilCredentials getUtilCredentials(String type, String host);
}
