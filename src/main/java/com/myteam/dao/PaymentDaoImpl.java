package com.myteam.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.myteam.entity.Payment;
import com.myteam.entity.PaymentGateway;
import com.myteam.entity.PaymentTxn;
import com.myteam.entity.Wallet;
import com.myteam.entity.WalletHistory;
import com.myteam.utils.StringUtils;

@Repository("paymentDao")
public class PaymentDaoImpl implements IPaymentDao{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public Wallet saveWallet(Wallet wallet) {
		if (wallet.getCreated() == null) {
			wallet.setCreated(new Date());
		}
		wallet.setUpdated(new Date());
		return (Wallet) sessionFactory.getCurrentSession().merge(wallet);
	}
	
	@Override
	public WalletHistory saveWalletHistory(WalletHistory walletHistory) {
		if (walletHistory.getCreated() == null) {
			walletHistory.setCreated(new Date());
		}
		walletHistory.setUpdated(new Date());
		return (WalletHistory) sessionFactory.getCurrentSession().merge(walletHistory);
	}
	
	@Override
	public PaymentTxn savePaymentTxn(PaymentTxn paymentTxn) {
		if (paymentTxn.getCreated() == null) {
			paymentTxn.setCreated(new Date());
		}
		paymentTxn.setUpdated(new Date());
		return (PaymentTxn) sessionFactory.getCurrentSession().merge(paymentTxn);
	}
	
	@Override
	public Payment savePayment(Payment payment) {
		if (payment.getCreated() == null) {
			payment.setCreated(new Date());
		}
		payment.setUpdated(new Date());
		return (Payment) sessionFactory.getCurrentSession().merge(payment);
	}
	
	@Override
	public Wallet getUserWallet(Integer userId) {
		StringBuilder queryString = new StringBuilder("select * from `wallet` where enabled = 1 ");
		if(userId != null) {
			queryString.append(" and user_id =:userId ");
		}
		NativeQuery<Wallet> query = sessionFactory.getCurrentSession().createNativeQuery(queryString.toString(),
				Wallet.class);
		query.setParameter("userId", userId);
		query.setMaxResults(1);
		return query.uniqueResult();
	}
	
	@Override
	public PaymentTxn getPaymentTxnByCode(String code) {
		StringBuilder queryString = new StringBuilder("select * from `payment_txn` where enabled = 1 ");
		if(StringUtils.isNotEmpty(code)) {
			queryString.append(" and code =:code ");
		}
		queryString.append(" order by created");
		NativeQuery<PaymentTxn> query = sessionFactory.getCurrentSession().createNativeQuery(queryString.toString(),
				PaymentTxn.class);
		query.setParameter("code", code);
		query.setMaxResults(1);
		return query.uniqueResult();
	}
	
	@Override
	public PaymentGateway getPaymentGatewayByName(String name) {
		StringBuilder queryString = new StringBuilder("select * from `payment_gateway` where enabled = 1 ");
		if(StringUtils.isNotEmpty(name)) {
			queryString.append(" and name =:name ");
		}
		NativeQuery<PaymentGateway> query = sessionFactory.getCurrentSession().createNativeQuery(queryString.toString(),
				PaymentGateway.class);
		query.setParameter("name", name);
		query.setMaxResults(1);
		return query.uniqueResult();
	}
	
	@Override
	public List<WalletHistory> getUserWalletHistory(Integer walletId, Integer start, Integer maxResults) {
		StringBuilder queryString = new StringBuilder("select * from `wallet_history` where enabled = 1 ");
		if(walletId != null) {
			queryString.append(" and wallet_id =:walletId ");
		}
		queryString.append(" order by id desc ");
		NativeQuery<WalletHistory> query = sessionFactory.getCurrentSession().createNativeQuery(queryString.toString(),
				WalletHistory.class);
		query.setParameter("walletId", walletId);
		query.setFirstResult(start);
		query.setMaxResults(maxResults);
		return query.list();
	}
	
	
}
