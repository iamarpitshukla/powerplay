package com.myteam.dao;

import java.util.Date;
import java.util.List;

import com.myteam.entity.ContestWinningDistribution;
import com.myteam.entity.Contests;
import com.myteam.entity.Match;
import com.myteam.entity.MatchData;
import com.myteam.entity.SmsTemplate;
import com.myteam.entity.SystemProperties;
import com.myteam.entity.User;
import com.myteam.entity.UserMatchTeamContestMapping;
import com.myteam.entity.UserMatchTeamMapping;
import com.myteam.request.GetFutureMatchRequest;

public interface IPowerPlayDao {

	Match saveMatch(Match match);

	void saveContest(Contests contest);

	List<Match> getMatches(GetFutureMatchRequest request);

	List<Match> getMatchesByUniqueIds(List<Integer> uniqueIds);

	List<Match> getMatchesWithoutSquad(GetFutureMatchRequest request);

	Match getMatcheByMatchId(Integer uniqueId);

	List<Contests> getContestsForMatch(Integer matchId);

	List<Match> getCurrentlyRunningMatches();

	void saveMatchData(MatchData matchdata);

	MatchData getMatchDataByMatchId(Integer matchId);

	Contests saveContests(Contests contests);

	List<Contests> getTimeCrossedContestsForMatchAndEndOversAndInning(Integer matchId, Integer endOver, Integer inning);

	List<Contests> getAllCurrentlyWorkingContests(Integer matchId, Integer endOver, Integer inning);

	List<Contests> getAllOverContests(Integer matchId);

	void saveUserMatchContestPlayersMapping(UserMatchTeamContestMapping userMatchContestsTeamMapping);

	UserMatchTeamContestMapping getUserMatchContestPlayersMappingByCode(String code);

	Contests getContestsById(Integer contestId);

	Match getMatchById(Integer matchId);

	List<UserMatchTeamContestMapping> getUserMatchContestPlayersMappingByContestId(Integer contestId, Integer start,
			Integer maxResults, Integer userId, Boolean include, Integer rank);

	List<Contests> getAllNonReviewedContests();

	List<Contests> getContestsFromStartDate(Date startDate);

	UserMatchTeamMapping getUserMatchTeamMappingById(Integer id);

	UserMatchTeamMapping saveUserMatchTeamMapping(UserMatchTeamMapping userMatchTeamMapping);

	List<UserMatchTeamMapping> getUserMatchTeamMappingByUserIdAndMatchId(Integer userId, Integer matchId);

	List<SmsTemplate> getSmsTemplates();

	List<UserMatchTeamContestMapping> getUserMatches(GetFutureMatchRequest request, Integer userId);

	List<UserMatchTeamContestMapping> getJoinedContests(Integer matchId, Integer userId);

	List<UserMatchTeamContestMapping> getUserMatchTeamMappingsByMatchIds(List<Integer> matchIds, Integer userId);

	ContestWinningDistribution saveContestWinningDistribution(ContestWinningDistribution contestWinningDistribution);

	List<Contests> getContestsForMatchIds(List<Integer> matchIds);

	void updateUMTCMWinningAmountByIds(Integer amount, List<Integer> ids);

	List<SystemProperties> getSystemProperties();

	List<UserMatchTeamContestMapping> getContestWiseJoinedContests(Integer contestId, Integer userId);

	List<Match> getMatchesWithoutPlayingEleven();

	List<Match> getMatchesToCreateAutoTeams();

	List<Contests> getAutoTeamCreateContestsForMatch(Integer matchId);

}
