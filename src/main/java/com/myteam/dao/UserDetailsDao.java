package com.myteam.dao;

import com.myteam.entity.User;

public interface UserDetailsDao {
	 User findUserByUsername(String username);

}
