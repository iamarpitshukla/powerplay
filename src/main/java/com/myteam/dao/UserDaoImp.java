package com.myteam.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.myteam.dto.UserDetailDTO;
import com.myteam.entity.User;
import com.myteam.entity.UserReferralDetail;
import com.myteam.entity.UtilCredentials;
import com.myteam.utils.CollectionUtils;
import com.myteam.utils.StringUtils;

@Repository
public class UserDaoImp implements IUserDao {

   @Autowired
   private SessionFactory sessionFactory;

   @Override
   public User saveUser(User user) {
	   if(user.getCreated() == null) {
		  user.setCreated(new Date()); 
	   }
	   user.setUpdated(new Date());
	   return (User) sessionFactory.getCurrentSession().merge(user);
   }
   
   @Override
   public UserReferralDetail saveUserReferralDetail(UserReferralDetail userReferralDetail) {
	   if(userReferralDetail.getCreated() == null) {
		  userReferralDetail.setCreated(new Date()); 
	   }
	   userReferralDetail.setUpdated(new Date());
	   return (UserReferralDetail) sessionFactory.getCurrentSession().merge(userReferralDetail);
   }

   @Override
   public List<User> list() {
      @SuppressWarnings("unchecked")
      TypedQuery<User> query = sessionFactory.getCurrentSession().createQuery("from User");
      return query.getResultList();
   }
   
   @Override
   public User getUserByMobileOrEmail(String mobile, String email, boolean enabled) {
      StringBuilder queryString = new StringBuilder("select * from user ");
      queryString.append(" where ( enabled = 0 or enabled = 1 ) ");
      if(StringUtils.isNotEmpty(mobile)) {
    	  queryString.append(" and mobile =:mobile ");
      }
      if(StringUtils.isNotEmpty(email)) {
    	  queryString.append(" and email =:email ");
      }
      NativeQuery<User> query = sessionFactory.getCurrentSession().createNativeQuery(queryString.toString(), User.class);
      if(StringUtils.isNotEmpty(mobile)) {
    	  query.setParameter("mobile", mobile);
      }
      if(StringUtils.isNotEmpty(email)) {
    	  query.setParameter("email", email);
      }
      return query.uniqueResult();
   }
   
   @Override
   public User getUserByMobile(String mobile) {
      StringBuilder queryString = new StringBuilder("select * from user ");
      if(StringUtils.isNotEmpty(mobile)) {
    	  queryString.append(" where mobile =:mobile ");
      }
      
      NativeQuery<User> query = sessionFactory.getCurrentSession().createNativeQuery(queryString.toString(), User.class);
      if(StringUtils.isNotEmpty(mobile)) {
    	  query.setParameter("mobile", mobile);
      }
      return query.uniqueResult();
   }
   
   @Override
   public User getUserByUserId(Integer userId) {
	   StringBuilder queryString = new StringBuilder("select * from user where enabled = 1 ");
	      if(userId != null) {
	    	  queryString.append(" and id =:userId ");
	      }
	      NativeQuery<User> query = sessionFactory.getCurrentSession().createNativeQuery(queryString.toString(), User.class);
	      if(userId != null) {
	    	  query.setParameter("userId", userId);
	      }
	      return query.uniqueResult();
   }
   
   @Override
   public List<User> getUsersByUserId(List<Integer> userIds) {
	   StringBuilder queryString = new StringBuilder("select * from user where enabled = 1 ");
	      if(CollectionUtils.isNotEmpty(userIds)) {
	    	  queryString.append(" and id in (:userIds) ");
	      }
	      NativeQuery<User> query = sessionFactory.getCurrentSession().createNativeQuery(queryString.toString(), User.class);
	      if(CollectionUtils.isNotEmpty(userIds)) {
	    	  query.setParameter("userIds", userIds);
	      }
	      return  query.list();
   }
   
   @Override
   public UserReferralDetail getUserReferralDetailByCode(String code) {
      StringBuilder queryString = new StringBuilder("select * from user_referral_detail where enabled = 1 ");
      if(StringUtils.isNotEmpty(code)) {
    	  queryString.append(" and code =:code ");
      }
      NativeQuery<UserReferralDetail> query = sessionFactory.getCurrentSession().createNativeQuery(queryString.toString(), UserReferralDetail.class);
      if(StringUtils.isNotEmpty(code)) {
    	  query.setParameter("code", code);
      }
      return query.uniqueResult();
   }
   
   @Override
   public List<UserReferralDetail> getUserReferralDetailUserId(Integer userId, boolean referred) {
	   StringBuilder queryString = new StringBuilder("select * from user_referral_detail where enabled = 1 ");
	      if(referred) {
	    	  queryString.append(" and user_id =:userId ");
	      }else {
	    	  queryString.append(" and referred_by =:userId ");
	      }
	      NativeQuery<UserReferralDetail> query = sessionFactory.getCurrentSession().createNativeQuery(queryString.toString(), UserReferralDetail.class);
	      query.setParameter("userId", userId);
	      return query.list();
   }
   
   @Override
	public UtilCredentials getUtilCredentials(String type, String host) {
		StringBuilder queryString = new StringBuilder("select * from credentials  ");
		if (StringUtils.isNotEmpty(type)) {
			queryString.append(" where type =:type ");
		}
		if (StringUtils.isNotEmpty(host)) {
			queryString.append(" and host =:host ");
		}
		NativeQuery<UtilCredentials> query = sessionFactory.getCurrentSession()
				.createNativeQuery(queryString.toString(), UtilCredentials.class);
		if (StringUtils.isNotEmpty(type)) {
			query.setParameter("type", type);
		}
		if (StringUtils.isNotEmpty(host)) {
			query.setParameter("host", host);
		}
		return query.uniqueResult();
	}
   

}
