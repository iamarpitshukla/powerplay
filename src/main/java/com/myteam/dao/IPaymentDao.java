package com.myteam.dao;

import java.util.List;

import com.myteam.entity.Payment;
import com.myteam.entity.PaymentGateway;
import com.myteam.entity.PaymentTxn;
import com.myteam.entity.Wallet;
import com.myteam.entity.WalletHistory;

public interface IPaymentDao {

	Wallet saveWallet(Wallet wallet);

	WalletHistory saveWalletHistory(WalletHistory walletHistory);

	Wallet getUserWallet(Integer userId);

	PaymentTxn getPaymentTxnByCode(String code);

	PaymentTxn savePaymentTxn(PaymentTxn paymentTxn);

	Payment savePayment(Payment payment);

	PaymentGateway getPaymentGatewayByName(String name);

	List<WalletHistory> getUserWalletHistory(Integer walletId, Integer start, Integer maxResults);

}
