package com.myteam.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.CredentialsContainer;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.myteam.entity.Roles;
import com.myteam.entity.User;

public class MyTeamUser implements UserDetails, CredentialsContainer  {

	 /**
	 * 
	 */
	private static final long serialVersionUID = -3960495817548567619L;
	private User user;
	
	 private List<GrantedAuthority>        authorities       = new ArrayList<GrantedAuthority>();
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public MyTeamUser() {
		super();
	}
	
	public MyTeamUser(User user) {
		 this.user = user;
		 for(Roles roles: user.getRoles()) {
			 Authority authority = new Authority(roles.getRole());
			 this.authorities.add(authority);
		 }
	}

	@Override
	public String getUsername() {
		return this.user.getUsername();
	}

	@Override
	public void eraseCredentials() {
		this.user.setPassword(null);
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		for(Roles roles: user.getRoles()) {
			 Authority authority = new Authority(roles.getRole());
			 authorities.add(authority);
		 }
		return this.authorities;
	}

	@Override
	public String getPassword() {
		return this.user.getPassword();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
}
