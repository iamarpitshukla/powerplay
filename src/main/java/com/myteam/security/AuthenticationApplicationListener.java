package com.myteam.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.RememberMeAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.event.InteractiveAuthenticationSuccessEvent;


public class AuthenticationApplicationListener  implements ApplicationListener<InteractiveAuthenticationSuccessEvent>{

	private static final Logger LOG = LoggerFactory.getLogger(AuthenticationApplicationListener.class);

    @Override
    public void onApplicationEvent(InteractiveAuthenticationSuccessEvent event) {
        LOG.info("Inside security handler .... checking event: {}", event.getGeneratedBy());
        MyTeamUser user = null;
        if (event.getSource() instanceof UsernamePasswordAuthenticationToken) {
            UsernamePasswordAuthenticationToken authToken = (UsernamePasswordAuthenticationToken) event.getSource();
            user = (MyTeamUser) authToken.getPrincipal();
        } else {
            RememberMeAuthenticationToken authToken = (RememberMeAuthenticationToken) event.getSource();
            user = (MyTeamUser) authToken.getPrincipal();
            LOG.info("Login success for user: {}", user.getUsername());
        }
    }
}
