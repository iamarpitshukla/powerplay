package com.myteam.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

import com.myteam.services.IUserService;


public class CustomSuccessHandler  extends SavedRequestAwareAuthenticationSuccessHandler {
	
    private static final Logger LOG = LoggerFactory.getLogger(CustomSuccessHandler.class);

	@Autowired
    private IUserService        userService;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws ServletException, IOException {
        MyTeamUser user = (MyTeamUser) authentication.getPrincipal();
        LOG.info("Login success for user: {}", user.getUsername());
        try{ 
            LOG.info("User :{} Logged in at :{}",user.getUser().getUsername());
        }catch(Exception e){
            LOG.error("Error adding login entry",e);
        }
        super.onAuthenticationSuccess(request, response, authentication);
    }

    @Override
    protected String determineTargetUrl(HttpServletRequest request, HttpServletResponse response) {
        StringBuilder builder = new StringBuilder();
        String targetUrl = super.determineTargetUrl(request, response);
        if (!targetUrl.endsWith("login")) {
            builder.append(targetUrl);
        } else {
            builder.append("/home");
        }
        return builder.toString();
    }
}
