package com.myteam.cache;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.myteam.annotation.Cache;
import com.myteam.dto.ContestDetailDTO;
import com.myteam.entity.Contests;

@Cache(name = "contestCache")
public class ContestCache {
	
	private static final Logger LOG = LoggerFactory.getLogger(ContestCache.class);
	
	private Map<Integer, ContestDetailDTO> contestIdToDistributionMap;
	
	public void addContestWiseDistribution(List<Contests> contests) {
		contestIdToDistributionMap = new HashMap<>();
		for(Contests contests2: contests) {
			ContestDetailDTO contestDetailDTO = new ContestDetailDTO();
			contestDetailDTO.setContestDistribution(contests2.getContestWinnerRank());
			contestDetailDTO.setPrizePool(contests2.getPrizePool());
			contestIdToDistributionMap.put(contests2.getId(), contestDetailDTO);
			LOG.info("adding contest : {}", contests2.getId());
		}
	}
	
	public ContestDetailDTO getContestWiseDistributionById(Integer contestId){
		return contestIdToDistributionMap.get(contestId);
	}
	
}
