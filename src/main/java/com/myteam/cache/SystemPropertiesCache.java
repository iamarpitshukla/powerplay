package com.myteam.cache;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.myteam.annotation.Cache;
import com.myteam.entity.SystemProperties;

@Cache(name = "propertiesCache")
public class SystemPropertiesCache {

	private static final Logger LOG = LoggerFactory.getLogger(SystemPropertiesCache.class);
	
	public static String POWERPLAY_BASE_URL= "powerplay.base.url";
	
	public static String POWERPLAY_S3_PATH= "powerplay.s3.path";

	public static String POWERPLAY_BANNER_URLS= "powerplay.banner.urls";
	
	public static String POWERPLAY_TEAM_FLAGS= "powerplay.team.flags";
	
	public static String POWERPLAY_MAXIMUM_ALLOWED_PLAYERS= "powerplay.maximum.allowed.players";
	
	public static String POWERPLAY_RANDOM_NUMBRE= "powerplay.random.number";
	
	public static String MAXIMUM_ALLOWED_TRANSACTION= "maximum.allowed.transaction";



	private Map<String, SystemProperties> propertNameToValueMap = new HashMap<>();

	public void addSystemProperties(List<SystemProperties> sps) {
		for(SystemProperties sp: sps) {
			propertNameToValueMap.put(sp.getName(), sp);
			LOG.info("adding property : {}", sp.getName());
		}
	}
	
	public String getSystemPropertiesByName(String name) {
			return propertNameToValueMap.get(name).getValue();
	}
}
