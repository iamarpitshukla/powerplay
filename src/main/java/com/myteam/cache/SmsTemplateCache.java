package com.myteam.cache;

import java.util.HashMap;
import java.util.Map;

import com.myteam.annotation.Cache;
import com.myteam.communication.SmsTemplateVO;
import com.myteam.communication.Template;
import com.myteam.entity.SmsTemplate;
import com.myteam.utils.StringUtils;


@Cache(name = "smsTemplateCache")
public class SmsTemplateCache {

    private Map<Integer, SmsTemplateVO>             tempalteIdToTemplateMap        = new HashMap<Integer, SmsTemplateVO>();
    private Map<String, Map<String, SmsTemplateVO>> langToNametoSmsTemplateMap     = new HashMap<String, Map<String, SmsTemplateVO>>();
    private Map<String, Map<String, SmsTemplateVO>> langToCodetoSmsTemplateMap     = new HashMap<String, Map<String, SmsTemplateVO>>();
    private Map<String, String>                     templateNameToDisplayNameCache = new HashMap<String, String>();

	public void addSmsTemplate(SmsTemplate template) {
		SmsTemplateVO templateVO = new SmsTemplateVO();
		templateVO.setTemplateId(template.getId());
		templateVO.setName(template.getName());
		templateVO.setCode(template.getCode());
		templateVO.setBodyTemplate(Template.compile(template.getBodyTemplate()));
		templateVO.setLang(template.getLang());
		Map<String, SmsTemplateVO> nameToSmsTemplateMap;
		if (langToNametoSmsTemplateMap.get(template.getLang().toLowerCase()) != null) {
			nameToSmsTemplateMap = langToNametoSmsTemplateMap.get(template.getLang().toLowerCase());
		} else {
			nameToSmsTemplateMap = new HashMap<String, SmsTemplateVO>();
		}
		nameToSmsTemplateMap.put(template.getName(), templateVO);
		langToNametoSmsTemplateMap.put(template.getLang().toLowerCase(), nameToSmsTemplateMap);
		tempalteIdToTemplateMap.put(template.getId(), templateVO);
		templateNameToDisplayNameCache.put(template.getName(), template.getDisplayName());
	}

    public SmsTemplateVO getTemplateByNameAndLang(String name, String lang) {
        Map<String, SmsTemplateVO> templatesByLanguage = langToNametoSmsTemplateMap.get(lang.toLowerCase());
        if (templatesByLanguage == null || templatesByLanguage.get(name) == null) {
            templatesByLanguage = langToNametoSmsTemplateMap.get("EN");
        }

        return templatesByLanguage.get(name);
    }
    
    public SmsTemplateVO getTemplateByCodeAndLang(String code, String lang) {
    	if(StringUtils.isEmpty(code)){
    		return null;
    	}
        Map<String, SmsTemplateVO> templatesByLanguage = langToCodetoSmsTemplateMap.get(lang.toLowerCase());
        if (templatesByLanguage == null || templatesByLanguage.get(code) == null) {
            templatesByLanguage = langToCodetoSmsTemplateMap.get("EN");
        }
        return templatesByLanguage.get(code);
    }

    public SmsTemplateVO getTemplateById(Integer smsTemplateId) {
        return tempalteIdToTemplateMap.get(smsTemplateId);
    }

    public String getTemplateDisplayName(String templateName) {
        return templateNameToDisplayNameCache.containsKey(templateName) ? templateNameToDisplayNameCache.get(templateName) : "";

    }

}
