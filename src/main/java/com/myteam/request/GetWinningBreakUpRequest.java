package com.myteam.request;

import org.springframework.lang.NonNull;

public class GetWinningBreakUpRequest {

	@NonNull
	private Integer contestId;

	public Integer getContestId() {
		return contestId;
	}

	public void setContestId(Integer contestId) {
		this.contestId = contestId;
	}

	@Override
	public String toString() {
		return "GetWinningBreakUpRequest [contestId=" + contestId + "]";
	}
}
