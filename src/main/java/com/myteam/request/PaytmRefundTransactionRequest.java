package com.myteam.request;

public class PaytmRefundTransactionRequest extends BasicPaytmTransactionRequest{


	/**
	 * 
	 */
	private static final long serialVersionUID = -1891499503425950425L;
	private String MID;
	private String ORDERID;
	private String REFID;

	public String getMID() {
		return MID;
	}

	public void setMID(String mID) {
		MID = mID;
	}

	public String getORDERID() {
		return ORDERID;
	}

	public void setORDERID(String oRDERID) {
		ORDERID = oRDERID;
	}

	public String getREFID() {
		return REFID;
	}

	public void setREFID(String rEFID) {
		REFID = rEFID;
	}


}
