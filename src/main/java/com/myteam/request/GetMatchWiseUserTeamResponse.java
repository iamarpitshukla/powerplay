package com.myteam.request;

import java.util.ArrayList;
import java.util.List;

import com.myteam.dto.UserMatchTeamDTO;
import com.myteam.respponse.ServiceResponse;

public class GetMatchWiseUserTeamResponse extends ServiceResponse{
	
	List<UserMatchTeamDTO> teams = new ArrayList<>();
	
	public List<UserMatchTeamDTO> getTeams() {
		return teams;
	}

	public void setTeams(List<UserMatchTeamDTO> teams) {
		this.teams = teams;
	}

	@Override
	public String toString() {
		return "GetMatchWiseUserTeamResponse [teams=" + teams + "]";
	}

}
