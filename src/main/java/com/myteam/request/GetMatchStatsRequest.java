package com.myteam.request;

public class GetMatchStatsRequest {
	private Integer unique_id;

	public Integer getUnique_id() {
		return unique_id;
	}

	public void setUnique_id(Integer unique_id) {
		this.unique_id = unique_id;
	}

	@Override
	public String toString() {
		return "GetMatchStatsRequest [unique_id=" + unique_id + "]";
	}
}
