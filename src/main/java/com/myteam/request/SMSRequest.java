package com.myteam.request;

import java.util.List;

public class SMSRequest {

	private String message;
	
	private List<String> to;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<String> getTo() {
		return to;
	}

	public void setTo(List<String> to) {
		this.to = to;
	}

	@Override
	public String toString() {
		return "SMSRequest [message=" + message + ", to=" + to + "]";
	}
}
