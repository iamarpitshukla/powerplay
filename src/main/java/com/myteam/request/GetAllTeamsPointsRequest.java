package com.myteam.request;

public class GetAllTeamsPointsRequest extends ServiceRequest{
	
	private Integer contestId;

	private Integer start;
	
	private Integer maxResults;
	
	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public Integer getMaxResults() {
		return maxResults;
	}

	public void setMaxResults(Integer maxResults) {
		this.maxResults = maxResults;
	}

	public Integer getContestId() {
		return contestId;
	}

	public void setContestId(Integer contestId) {
		this.contestId = contestId;
	}

	@Override
	public String toString() {
		return "GetAllTeamsPointsRequest [contestId=" + contestId + ", start=" + start + ", maxResults=" + maxResults
				+ "]";
	}
}
