package com.myteam.request;

public class GetWalletHistoryRequest extends ServiceRequest{
	
	private Integer start =0;
	
	private Integer maxResults = 50;

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public Integer getMaxResults() {
		return maxResults;
	}

	public void setMaxResults(Integer maxResults) {
		this.maxResults = maxResults;
	}

	@Override
	public String toString() {
		return "GetWalletHistoryRequest [start=" + start + ", maxResults=" + maxResults + "]";
	}
}
