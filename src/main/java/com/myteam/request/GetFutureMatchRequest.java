package com.myteam.request;

public class GetFutureMatchRequest extends ServiceRequest{

	private Integer start = 0;
	
	private Integer maxResults = 200;
	
	private String type = Type.UPCOMMING.code();
	
	public enum Type {
		UPCOMMING("U"), LIVE("L"),PAST("P");
		private String code;

		private Type(String code) {
			this.code = code;
		}

		public String code() {
			return code;
		}
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public Integer getMaxResults() {
		return maxResults;
	}

	public void setMaxResults(Integer maxResults) {
		this.maxResults = maxResults;
	}

	@Override
	public String toString() {
		return "GetFutureMatchRequest [start=" + start + ", maxResults=" + maxResults + ", type=" + type + "]";
	}
}
