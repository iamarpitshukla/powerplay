package com.myteam.request;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

public class PaymentRequest extends ServiceRequest{

	@NotNull
	private String source;  // OS

	@NotNull
	private String pgName;
	
	@NotNull
	private BigDecimal amount;

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getPgName() {
		return pgName;
	}

	public void setPgName(String pgName) {
		this.pgName = pgName;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "PaymentRequest [source=" + source + ", pgName=" + pgName + ", amount=" + amount + "]";
	}
}
