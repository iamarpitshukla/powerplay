package com.myteam.request;

import javax.validation.constraints.NotNull;

public class UpdatePasswordRequest {

	@NotNull
	private String mobile;
	
	private String otp;
	
	private String password;
	
	private String repeatPassword;
	
	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRepeatPassword() {
		return repeatPassword;
	}

	public void setRepeatPassword(String repeatPassword) {
		this.repeatPassword = repeatPassword;
	}

	@Override
	public String toString() {
		return "UpdatePasswordRequest [mobile=" + mobile + ", otp=" + otp + ", password=" + password
				+ ", repeatPassword=" + repeatPassword + "]";
	}
}
