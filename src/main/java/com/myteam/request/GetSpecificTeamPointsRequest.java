package com.myteam.request;

import javax.validation.constraints.NotNull;

public class GetSpecificTeamPointsRequest {
	
	@NotNull
	private String ucpmCode;

	public String getUcpmCode() {
		return ucpmCode;
	}

	public void setUcpmCode(String ucpmCode) {
		this.ucpmCode = ucpmCode;
	}

	@Override
	public String toString() {
		return "GetSpecificTeamPointsRequest [ucpmCode=" + ucpmCode + "]";
	}
}
