package com.myteam.request;

import java.io.Serializable;
import java.util.List;

public class Msg91Request implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String sender;
	
	private String route;
	
	private String country;
	
	private List<SMSRequest> sms;

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public List<SMSRequest> getSms() {
		return sms;
	}

	public void setSms(List<SMSRequest> sms) {
		this.sms = sms;
	}

	@Override
	public String toString() {
		return "Msg91Request [sender=" + sender + ", route=" + route + ", country=" + country + ", sms=" + sms + "]";
	}
}
