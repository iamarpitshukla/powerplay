package com.myteam.request;

public class PaytmCheckStatusRequest extends BasicPaytmTransactionRequest{



	/**
	 * 
	 */
	private static final long serialVersionUID = 3132178204671503255L;
	private String MID;
	private String ORDERID;
	private String CHECKSUMHASH;

	public String getMID() {
		return MID;
	}

	public void setMID(String mID) {
		MID = mID;
	}

	public String getORDERID() {
		return ORDERID;
	}

	public void setORDERID(String oRDERID) {
		ORDERID = oRDERID;
	}

	public String getCHECKSUMHASH() {
		return CHECKSUMHASH;
	}

	public void setCHECKSUMHASH(String cHECKSUMHASH) {
		CHECKSUMHASH = cHECKSUMHASH;
	}


}
