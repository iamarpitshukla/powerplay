package com.myteam.request;

public class PaytmTransactionRequest extends BasicPaytmTransactionRequest{



	/**
	 * 
	 */
	private static final long serialVersionUID = 4479711973484990765L;
	private String REQUEST_TYPE; // required true
	private String MID; // required true
	private String ORDER_ID; // required true
	private String CUST_ID; // required true
	private String TXN_AMOUNT; // required true
	private String CHANNEL_ID; // required true
	private String INDUSTRY_TYPE_ID; // required true
	private String WEBSITE; // required true
	private String CHECKSUMHASH; // required true
	private String MOBILE_NO; // required true
	private String EMAIL; // required true
	private String PAYMENT_MODE_ONLY; // required false
	private String AUTH_MODE; // required false
	private String PAYMENT_TYPE_ID; // required false
	private String CARD_TYPE; // required false
	private String BANK_CODE; // required false
	private String PROMO_CAMP_ID; // required false
	private String ORDER_DETAILS; // required false
	private String DOB; // required false
	private String VERIFIED_BY; // required false
	private String IS_USER_VERIFIED; // required false
	private String ADDRESS_1; // required false
	private String ADDRESS_2; // required false
	private String CITY; // required false
	private String STATE; // required false
	private String PINCODE; // required false
	private String LOGIN_THEME; // required false
	private String CALLBACK_URL; // required false
	private String THEME; // required false
	private String MERC_UNQ_REF;	// required false
	private String MAKE_TXN_URL;
	
	public String getMAKE_TXN_URL() {
		return MAKE_TXN_URL;
	}

	public void setMAKE_TXN_URL(String mAKE_TXN_URL) {
		MAKE_TXN_URL = mAKE_TXN_URL;
	}

	public String getREQUEST_TYPE() {
		return REQUEST_TYPE;
	}

	public void setREQUEST_TYPE(String rEQUEST_TYPE) {
		REQUEST_TYPE = rEQUEST_TYPE;
	}

	public String getMID() {
		return MID;
	}

	public void setMID(String mID) {
		MID = mID;
	}

	public String getORDER_ID() {
		return ORDER_ID;
	}

	public void setORDER_ID(String oRDER_ID) {
		ORDER_ID = oRDER_ID;
	}

	public String getCUST_ID() {
		return CUST_ID;
	}

	public void setCUST_ID(String cUST_ID) {
		CUST_ID = cUST_ID;
	}

	public String getTXN_AMOUNT() {
		return TXN_AMOUNT;
	}

	public void setTXN_AMOUNT(String tXN_AMOUNT) {
		TXN_AMOUNT = tXN_AMOUNT;
	}

	public String getCHANNEL_ID() {
		return CHANNEL_ID;
	}

	public void setCHANNEL_ID(String cHANNEL_ID) {
		CHANNEL_ID = cHANNEL_ID;
	}

	public String getINDUSTRY_TYPE_ID() {
		return INDUSTRY_TYPE_ID;
	}

	public void setINDUSTRY_TYPE_ID(String iNDUSTRY_TYPE_ID) {
		INDUSTRY_TYPE_ID = iNDUSTRY_TYPE_ID;
	}

	public String getWEBSITE() {
		return WEBSITE;
	}

	public void setWEBSITE(String wEBSITE) {
		WEBSITE = wEBSITE;
	}

	public String getCHECKSUMHASH() {
		return CHECKSUMHASH;
	}

	public void setCHECKSUMHASH(String cHECKSUMHASH) {
		CHECKSUMHASH = cHECKSUMHASH;
	}

	public String getMOBILE_NO() {
		return MOBILE_NO;
	}

	public void setMOBILE_NO(String mOBILE_NO) {
		MOBILE_NO = mOBILE_NO;
	}

	public String getEMAIL() {
		return EMAIL;
	}

	public void setEMAIL(String eMAIL) {
		EMAIL = eMAIL;
	}

	public String getPAYMENT_MODE_ONLY() {
		return PAYMENT_MODE_ONLY;
	}

	public void setPAYMENT_MODE_ONLY(String pAYMENT_MODE_ONLY) {
		PAYMENT_MODE_ONLY = pAYMENT_MODE_ONLY;
	}

	public String getAUTH_MODE() {
		return AUTH_MODE;
	}

	public void setAUTH_MODE(String aUTH_MODE) {
		AUTH_MODE = aUTH_MODE;
	}

	public String getPAYMENT_TYPE_ID() {
		return PAYMENT_TYPE_ID;
	}

	public void setPAYMENT_TYPE_ID(String pAYMENT_TYPE_ID) {
		PAYMENT_TYPE_ID = pAYMENT_TYPE_ID;
	}

	public String getCARD_TYPE() {
		return CARD_TYPE;
	}

	public void setCARD_TYPE(String cARD_TYPE) {
		CARD_TYPE = cARD_TYPE;
	}

	public String getBANK_CODE() {
		return BANK_CODE;
	}

	public void setBANK_CODE(String bANK_CODE) {
		BANK_CODE = bANK_CODE;
	}

	public String getPROMO_CAMP_ID() {
		return PROMO_CAMP_ID;
	}

	public void setPROMO_CAMP_ID(String pROMO_CAMP_ID) {
		PROMO_CAMP_ID = pROMO_CAMP_ID;
	}

	public String getORDER_DETAILS() {
		return ORDER_DETAILS;
	}

	public void setORDER_DETAILS(String oRDER_DETAILS) {
		ORDER_DETAILS = oRDER_DETAILS;
	}

	public String getDOB() {
		return DOB;
	}

	public void setDOB(String dOB) {
		DOB = dOB;
	}

	public String getVERIFIED_BY() {
		return VERIFIED_BY;
	}

	public void setVERIFIED_BY(String vERIFIED_BY) {
		VERIFIED_BY = vERIFIED_BY;
	}

	public String getIS_USER_VERIFIED() {
		return IS_USER_VERIFIED;
	}

	public void setIS_USER_VERIFIED(String iS_USER_VERIFIED) {
		IS_USER_VERIFIED = iS_USER_VERIFIED;
	}

	public String getADDRESS_1() {
		return ADDRESS_1;
	}

	public void setADDRESS_1(String aDDRESS_1) {
		ADDRESS_1 = aDDRESS_1;
	}

	public String getADDRESS_2() {
		return ADDRESS_2;
	}

	public void setADDRESS_2(String aDDRESS_2) {
		ADDRESS_2 = aDDRESS_2;
	}

	public String getCITY() {
		return CITY;
	}

	public void setCITY(String cITY) {
		CITY = cITY;
	}

	public String getSTATE() {
		return STATE;
	}

	public void setSTATE(String sTATE) {
		STATE = sTATE;
	}

	public String getPINCODE() {
		return PINCODE;
	}

	public void setPINCODE(String pINCODE) {
		PINCODE = pINCODE;
	}

	public String getLOGIN_THEME() {
		return LOGIN_THEME;
	}

	public void setLOGIN_THEME(String lOGIN_THEME) {
		LOGIN_THEME = lOGIN_THEME;
	}

	public String getCALLBACK_URL() {
		return CALLBACK_URL;
	}

	public void setCALLBACK_URL(String cALLBACK_URL) {
		CALLBACK_URL = cALLBACK_URL;
	}

	@Override
	public String toString() {
		return "PaytmTransactionRequest [REQUEST_TYPE=" + REQUEST_TYPE + ", MID=" + MID + ", ORDER_ID=" + ORDER_ID
				+ ", CUST_ID=" + CUST_ID + ", TXN_AMOUNT=" + TXN_AMOUNT + ", CHANNEL_ID=" + CHANNEL_ID
				+ ", INDUSTRY_TYPE_ID=" + INDUSTRY_TYPE_ID + ", WEBSITE=" + WEBSITE + ", CHECKSUMHASH=" + CHECKSUMHASH
				+ ", MOBILE_NO=" + MOBILE_NO + ", EMAIL=" + EMAIL + ", PAYMENT_MODE_ONLY=" + PAYMENT_MODE_ONLY
				+ ", AUTH_MODE=" + AUTH_MODE + ", PAYMENT_TYPE_ID=" + PAYMENT_TYPE_ID + ", CARD_TYPE=" + CARD_TYPE
				+ ", BANK_CODE=" + BANK_CODE + ", PROMO_CAMP_ID=" + PROMO_CAMP_ID + ", ORDER_DETAILS=" + ORDER_DETAILS
				+ ", DOB=" + DOB + ", VERIFIED_BY=" + VERIFIED_BY + ", IS_USER_VERIFIED=" + IS_USER_VERIFIED
				+ ", ADDRESS_1=" + ADDRESS_1 + ", ADDRESS_2=" + ADDRESS_2 + ", CITY=" + CITY + ", STATE=" + STATE
				+ ", PINCODE=" + PINCODE + ", LOGIN_THEME=" + LOGIN_THEME + ", CALLBACK_URL=" + CALLBACK_URL
				+ ", THEME=" + THEME + ", MERC_UNQ_REF=" + MERC_UNQ_REF + ", MAKE_TXN_URL=" + MAKE_TXN_URL + "]";
	}

	public String getTHEME() {
		return THEME;
	}

	public void setTHEME(String tHEME) {
		THEME = tHEME;
	}

	public String getMERC_UNQ_REF() {
		return MERC_UNQ_REF;
	}

	public void setMERC_UNQ_REF(String mERC_UNQ_REF) {
		MERC_UNQ_REF = mERC_UNQ_REF;
	}
}
