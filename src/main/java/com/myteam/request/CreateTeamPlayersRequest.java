package com.myteam.request;

import java.util.ArrayList;
import java.util.List;

import com.myteam.dto.PlayersDTO;

public class CreateTeamPlayersRequest extends ServiceRequest{
	
	private Integer matchId;
	
	private List<PlayersDTO> players = new ArrayList<>();
	
	private Integer superPlayer;
	
	private Integer userMatchTeamMappingId;
	
	public Integer getUserMatchTeamMappingId() {
		return userMatchTeamMappingId;
	}

	public void setUserMatchTeamMappingId(Integer userMatchTeamMappingId) {
		this.userMatchTeamMappingId = userMatchTeamMappingId;
	}

	public Integer getMatchId() {
		return matchId;
	}

	public void setMatchId(Integer matchId) {
		this.matchId = matchId;
	}
	
	public List<PlayersDTO> getPlayers() {
		return players;
	}

	public void setPlayers(List<PlayersDTO> players) {
		this.players = players;
	}

	public Integer getSuperPlayer() {
		return superPlayer;
	}

	public void setSuperPlayer(Integer superPlayer) {
		this.superPlayer = superPlayer;
	}

	@Override
	public String toString() {
		return "CreateTeamPlayersRequest [matchId=" + matchId + ", players=" + players + ", superPlayer=" + superPlayer
				+", authToken "+this.getAuthToken()+ "]";
	}
}
