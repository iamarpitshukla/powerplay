package com.myteam.request;

import javax.validation.constraints.NotNull;

public class GetJoinedContestRequest extends ServiceRequest{
	
	@NotNull
	private Integer matchId;

	public Integer getMatchId() {
		return matchId;
	}

	public void setMatchId(Integer matchId) {
		this.matchId = matchId;
	}

	@Override
	public String toString() {
		return "GetJoinedContestRequest [matchId=" + matchId + "]";
	}
	
}
