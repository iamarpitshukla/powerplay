package com.myteam.request;

public class GetMatchDetailRequest {
	
	private Integer unique_id;  //unique_id is match id

	public Integer getUnique_id() {
		return unique_id;
	}

	public void setUnique_id(Integer unique_id) {
		this.unique_id = unique_id;
	}

	@Override
	public String toString() {
		return "GetSquadRequest [unique_id=" + unique_id + "]";
	}
	
}
