package com.myteam.request;

public class LoginRequest extends ServiceRequest{


    private static final long serialVersionUID = 5704397240973763250L;

    private String            email;
    private String            password;
    private String 			  mobile;

    public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

	@Override
	public String toString() {
		return "LoginRequest [email=" + email + ", password=" + password + ", mobile=" + mobile + "]";
	}

}
