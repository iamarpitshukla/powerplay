package com.myteam.request;

public class JoinContestRequest extends ServiceRequest {

	private Integer contestId;

	private Integer userMatchTeamMappingId;

	private String umtcCode;

	public Integer getUserMatchTeamMappingId() {
		return userMatchTeamMappingId;
	}

	public void setUserMatchTeamMappingId(Integer userMatchTeamMappingId) {
		this.userMatchTeamMappingId = userMatchTeamMappingId;
	}

	public String getUmtcCode() {
		return umtcCode;
	}

	public void setUmtcCode(String umtcCode) {
		this.umtcCode = umtcCode;
	}

	public Integer getContestId() {
		return contestId;
	}

	public void setContestId(Integer contestId) {
		this.contestId = contestId;
	}

	@Override
	public String toString() {
		return "JoinContestRequest [contestId=" + contestId + ", userMatchTeamMappingId=" + userMatchTeamMappingId
				+ ", umtcCode=" + umtcCode + "]";
	}
}
