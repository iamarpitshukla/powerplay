package com.myteam.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

public class DateUtils {
	
    private static final Logger         LOG                   = LoggerFactory.getLogger(DateUtils.class);


	public static Date stringToDate(String date, String pattern) {
		DateFormat format = new SimpleDateFormat(pattern);
		try {
			return format.parse(date);
		} catch (ParseException e) {
            LOG.error(e.getMessage());
        }
        return null;
    }
	
	public static String dateToString(Date date, String pattern) {
		if (date != null && StringUtils.hasText(pattern)) {
			DateFormat format = new SimpleDateFormat(pattern);
			return format.format(date);
		} else {
			return null;
		}
	}
	
	public static long getDateDiffInMin(Date current, Date dealtime) {
		long diff;
		diff = current.getTime() - dealtime.getTime();
		diff = diff / (1000 * 60);
		return diff;
	}
	
	public static String dateToString(Date date, String format, String timeZone) { 

        // create SimpleDateFormat object 
        SimpleDateFormat sdf = new SimpleDateFormat(format); 

        // default system timezone if passed null or empty 
        if (timeZone == null || "".equalsIgnoreCase(timeZone.trim())) { 

            timeZone = Calendar.getInstance().getTimeZone().getID(); 

        } 

        // set timezone to SimpleDateFormat 
        sdf.setTimeZone(TimeZone.getTimeZone(timeZone)); 

        // return Date in required format with timezone as String 
        return sdf.format(date); 

    } 
	
	public static Date getDateBeforeGivenWorkingDays(Date fromDate, int noOfDays) {
		Calendar c1 = Calendar.getInstance();
		if (fromDate != null) {
			c1.setTime(fromDate);
			c1.add(Calendar.DATE, -1 * noOfDays);
			return c1.getTime();
		}
		return null;
	}

	public static Date getDateTimeFomDateAfterMin(Date date, int min) {
        Calendar calender = Calendar.getInstance();
        calender.setTime(date);
        calender.add(Calendar.MINUTE, min);
        return calender.getTime();
    }
}
