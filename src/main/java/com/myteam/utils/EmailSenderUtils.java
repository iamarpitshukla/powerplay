package com.myteam.utils;

import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.myteam.entity.UtilCredentials;
import com.myteam.services.UserCredentialsService;

@Service("emailService")
public class EmailSenderUtils {
	
    private static final Logger         LOG                   = LoggerFactory.getLogger(EmailSenderUtils.class);
    
    @Autowired
    UserCredentialsService userCredentialsService;

    @Async
	public boolean sendEmailMailer(String recipients, String subject, String mailBody){
	    Properties props                           = new Properties();
	    boolean isEmailSent                        = false;
	    UtilCredentials uc = userCredentialsService.getUtilCredentials("email", null);
	    props.put("mail.smtp.host", uc.getHost());
        props.put("mail.smtp.port", uc.getPort());
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        
        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(uc.getLogin(),uc.getPassword());
            }
        });
        
        try {

            Message message = new MimeMessage(session);
            
            //From
            InternetAddress from = new InternetAddress();
            from.setAddress(uc.getFrom());
            from.setPersonal(uc.getFromName());
            
            message.setFrom(from);
            
            //Reply To
            if(uc.getReplyTo() != null && !uc.getReplyTo().isEmpty()){
                InternetAddress customerReplyTo = new InternetAddress();
                customerReplyTo.setAddress(uc.getReplyTo());
                customerReplyTo.setPersonal(uc.getReplyToName());

                InternetAddress[] addressList = {customerReplyTo};
                message.setReplyTo(addressList);

            } else {
                InternetAddress[] addressList = {from};
                message.setReplyTo(addressList);
            }
            
            //To
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(recipients));
            
            //CC
            if(uc.getCcRecipients() != null && !uc.getCcRecipients().isEmpty()){
                message.addRecipients(Message.RecipientType.CC, 
                        InternetAddress.parse(uc.getCcRecipients()));
            }
            //Subject
            message.setSubject(subject);
            
            //Body
            final MimeBodyPart htmlPart = new MimeBodyPart();
            htmlPart.setContent(mailBody, "text/html");
            // Create the Multipart.  Add BodyParts to it.
            final Multipart mp = new MimeMultipart("alternative");
            mp.addBodyPart(htmlPart);
            
            message.setContent(mp);

            //Send
            Transport.send(message);

            LOG.info("Mail Sent to " + recipients);
            isEmailSent = true;

        } catch (MessagingException e) {
            LOG.error("Mail Failure",e);
            throw new RuntimeException(e);
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return isEmailSent;
	}
}
