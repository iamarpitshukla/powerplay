package com.myteam.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.myteam.communication.SmsTemplateVO;
import com.myteam.entity.UtilCredentials;
import com.myteam.request.Msg91Request;
import com.myteam.request.SMSRequest;
import com.myteam.respponse.SendSMSResponse;
import com.myteam.services.UserCredentialsService;
import com.myteam.transport.HttpSender;

@Service("smsServiceUtil")
public class SMSSenderUtils {

    private static final Logger         LOG                   = LoggerFactory.getLogger(SMSSenderUtils.class);
    
    @Autowired
    UserCredentialsService userCredentialsService;
    
    UtilCredentials uc = null;
    
    @Async
	public void sendSMS(SmsTemplateVO template, String mobile){
    	uc = userCredentialsService.getUtilCredentials("sms","msg91");
    	LOG.info("UC is :{}", uc.getHost());
    	sendSMS(template, uc.getApiUrl(), mobile);
    }
    
	private void sendSMS(SmsTemplateVO template, String url, String mobile) {
		String response = "";
		String requestJson = getSMSRequest(template, mobile);
		LOG.info("Request to send sms is :{}", requestJson);
		HttpSender httpSender = new HttpSender();
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-type", "application/json");
		headers.put("authKey", uc.getPassword());
		try {
			response = httpSender.executePostContent(url, null, headers, requestJson);
			LOG.info("Send SMS POST request : {}, response : {}", requestJson, response);
		} catch (Exception e) {
			LOG.error("Transport Exception while hitting post request {} : and  : ", url, e);
		}
		SendSMSResponse res = new Gson().fromJson(response, SendSMSResponse.class);
		if (res != null && res.getType().equalsIgnoreCase("success")) {
				createSMSTxn(res,template);
		}
	}

	private String getSMSRequest(SmsTemplateVO template, String mobile) {
		Msg91Request request = new Msg91Request();
		request.setCountry("91");
		request.setSender(uc.getFromName());
		if(template.getCode().equals("TXNL")) {
			request.setRoute("4");
		}else {
			request.setRoute("1");
		}
		List<SMSRequest> sms = new ArrayList<SMSRequest>();
		SMSRequest smsReq = new SMSRequest();
		if(StringUtils.isNotEmpty(template.getOtp())) {
			smsReq.setMessage("Use OTP "+template.getOtp()+" to verify your mobile on Powerplay.");
		}else if(StringUtils.isNotEmpty(template.getAppLink())){
			smsReq.setMessage("Use link "+template.getAppLink()+" to download Powerplay app.");
		}
		List<String> to = new ArrayList<>();
		to.add(mobile);
		smsReq.setTo(to);
		sms.add(smsReq);
		request.setSms(sms);
		String requestJson = new Gson().toJson(request);
		return requestJson;
	}

	private void createSMSTxn(SendSMSResponse res, SmsTemplateVO template) {
		LOG.info("Creating sms txn");
	}
	
}
