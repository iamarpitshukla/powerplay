package com.myteam.utils;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import com.myteam.constants.Constants;

public class StringUtils {


	
	public static String removeBracket(String s)
	{
		String temp=s.replace('[', ' ').replace(']', ' ').trim();
		return temp;
	}
	public static String getValidString(String e)
	{
		if(e==null)
			return null;
		else if(String.valueOf(e).isEmpty())
			return null;
		else
			return e;	
	}
	public static Boolean checkStringEmpty(String e)
	{
		if(e==null)
			return false;
		else if(String.valueOf(e).isEmpty())
			return false;
		else
			return true;	
	}
	public static Integer getValidInteger(Boolean b)
	{
		if(b==null)
			return null;
		if(b==false)
			return 0;
		else if(b==true)
			return 1;
		else
			return null;
	}
	
	public static String getExtensionWithDot(String filename) {
        if (isNotEmpty(filename) && filename.contains(".")) {
            int index = filename.lastIndexOf('.');
            return filename.substring(index);
        }
        return "";
    }
	
	public static String getRandomAlphaNumeric(int length) {
        String aplhaNumberic = getRandom().toLowerCase().replaceAll("[^\\da-z]", "");
        return aplhaNumberic.substring(aplhaNumberic.length() - length);
    }
	
	public static String getRandom() {
        return UUID.randomUUID().toString();
    }
	
	public static String getRandom(int length) {
        String randomString = getRandom();
        return randomString.substring(randomString.length() - length);
    }
	
	public static boolean isEmpty(String str) {
        if (str == null)
            return true;
        return "".equals(str.trim());
    }

	public static String getThumbnailPath(String path, String thumbSize) {
        if (StringUtils.isEmpty(path)) {
            return null;
        }
        int lastSlashIndex = path.lastIndexOf("/");
        return path.substring(0, lastSlashIndex + 1) + thumbSize + path.substring(lastSlashIndex);
    }
	
    public static boolean isNotEmpty(String str) {
        return !isEmpty(str);
    }
    
    public static List<Integer> getListFromString(String string, String delimeter){
    	List<Integer> dataList = new ArrayList<>();
    	if(isEmpty(string) || isEmpty(delimeter)) {
    		return dataList;
    	}
    	for(String data : string.split(delimeter)) {
    		dataList.add(Integer.parseInt(data));
    	}
    	return dataList;
    }
    
    public static List<String> getStringListFromString(String string, String delimeter){
    	List<String> dataList = new ArrayList<>();
    	if(isEmpty(string) || isEmpty(delimeter)) {
    		return dataList;
    	}
    	for(String data : string.split(delimeter)) {
    		dataList.add(data);
    	}
    	return dataList;
    }
    
	 public static Double parsePrice(String number) {
        number = number.replaceAll("[^\\.\\d,-]", "");
        DecimalFormat df = new DecimalFormat("#,##,###.##");
        Number n;
        try {
            n = df.parse(number);
        } catch (ParseException e) {
            return 0.0;
        }
        return n.doubleValue();
    }
	 
	 public static String getNameInitials(String teamName) {
			StringBuilder sb = new StringBuilder();
			if(teamName.split(" ").length == 1 && teamName.length() >= 3) {
				sb.append(teamName.charAt(0));
				sb.append(teamName.charAt(1));
				sb.append(teamName.charAt(2));
			}else {
				for(String word: teamName.split(" ")) {
					sb.append(word.charAt(0));
				}
			}
	        return sb.toString().toUpperCase();
	    }
	 
	 public static String calculateRemainTime(Date scheduledDate){
		    // date format
		    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		    // two dates
		    Calendar current = Calendar.getInstance();
		    java.util.Date currentDate;
		    String timeLeft = "";
		    String current_date = format.format(current.getTime());
		        try {
		            currentDate = format.parse(current_date);
		            long diffInMillies = scheduledDate.getTime() - currentDate.getTime();
		            long diffence_in_minute = TimeUnit.MINUTES.convert(diffInMillies,TimeUnit.MILLISECONDS);
			           if(diffence_in_minute/1440 >0) {
			        	   long res = diffence_in_minute/1440;
			        	   if(res > 1) {
			        		   timeLeft = res +" days"; 
			        	   }else {
			        		   timeLeft = res +" day"; 
			        	   }
			        	  
			           }else if(diffence_in_minute/60 >0) {
			        	   long res = diffence_in_minute/60;
			        	   if(res > 1) {
			        		   timeLeft = res +" hrs"; 
			        	   }else {
			        		   timeLeft = res +" hr"; 
			        	   }
			           }else if(diffence_in_minute >1){
			        	   long res = diffence_in_minute;
			        	   if(res > 1) {
			        		   timeLeft = res +" mins"; 
			        	   }else {
			        		   timeLeft = res +" min"; 
			        	   }
			           }else {
			        	   timeLeft =  diffInMillies/1000 +" secs";
			           }
		        } catch (ParseException e) {
		            e.printStackTrace();
		        }
		        if(timeLeft.charAt(0)=='-') {
		        	timeLeft = Constants.RUNNING_MATCH;
		        }else {
		        	timeLeft = timeLeft+" left";
		        }
				return timeLeft;
		}
	 
	 public static String getShortName(String fullName) {
			if(fullName.length() <= 11) {
				return fullName;
			}else {
				String[] names = fullName.split(" ");
				StringBuilder shortName = new StringBuilder("");
				Integer count = 0;
				for(String name : names) {
					if(count == 0) {
						shortName.append(name.charAt(0)+".");
					}else {
						shortName.append(" "+name);
					}
					count++;
				}
				return shortName.toString();
			}
		}
}
