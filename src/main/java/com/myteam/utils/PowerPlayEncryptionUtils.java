package com.myteam.utils;

import java.nio.charset.Charset;
import java.util.zip.CRC32;

import com.myteam.exception.MyTeamException;


public class PowerPlayEncryptionUtils {



    private static final String MD5_SALT_PASSWORD_ENCRYPTION = "thiscanbeuserasasaltforLybratepasswordscretedbyrncs123461341";
    
    private static final String MD5_SALT_MEDICAL_DOCUMENT_ENCRYPTION = "thfdsisisfdstheffdsuckingsaltforencrrytptingsfdmedicalgoasddshcj234$$%^&%$%^&hvsdh";
    
    private static final String MD5_SALT_HEALTH_PACKAGE_ENCRYPTION   = "healthpackageencryptionsalt";
    
    private static final String MD5_UNIQUE_REQUEST_KEY               = "tbhxmh6w74";

    public static String getMD5EncodedPassword(String text) {
        return MD5ChecksumUtils.md5Encode(text, MD5_SALT_PASSWORD_ENCRYPTION);
    }

    public static String getMD5EncodedMedicalDocumentText(String text) {
        return MD5ChecksumUtils.md5Encode(text, MD5_SALT_MEDICAL_DOCUMENT_ENCRYPTION);
    }

    public static String getMD5EncodedRfPatientUPEC(String text, String salt) {
        return MD5ChecksumUtils.md5Encode(text, salt);
    }

    public static String getMD5EncodedGGCode(String text, String salt) {
        return MD5ChecksumUtils.md5Encode(text, salt);
    }
    
    public static String getMD5EncodedHealthPackageText(String text) {
        return MD5ChecksumUtils.md5Encode(text, MD5_SALT_HEALTH_PACKAGE_ENCRYPTION);
    }
    
    public static String getMD5UniqueRequestKey(String text) {
        return MD5ChecksumUtils.md5Encode(text, MD5_UNIQUE_REQUEST_KEY);
    }

    public static String getCRCChecksum(String rawText) throws MyTeamException {
        if(StringUtils.isEmpty(rawText)) {
            throw new MyTeamException("Error");
        }

        try {
            byte[] rawBytes = rawText.getBytes(Charset.forName("UTF-8"));

            CRC32 encoder = new CRC32();

            encoder.update(rawBytes);

            String encodedText = Long.toHexString(encoder.getValue());

            if(StringUtils.isEmpty(encodedText)) {
            	  throw new MyTeamException("Error");            }

            return encodedText;

        } catch (Exception e) {
        	  throw new MyTeamException("Error");
       }

    }

    public static void main(String[] args) {
        String time = "2016-7-28 14:31";
        String userId = "123";
        try {
            System.out.println(getCRCChecksum("mygalaxy" + time + userId + "awesomeeveryday" + time));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
