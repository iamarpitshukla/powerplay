package com.myteam.utils;

import java.util.List;

import com.google.gson.Gson;
import com.myteam.cache.CacheManager;
import com.myteam.cache.SystemPropertiesCache;
import com.myteam.dto.FlagsMapping;
import com.myteam.dto.TeamFlagDTO;

public class TeamFlagUtils {
	
	public static String getTeamFlagByName(String name) {
		String teamFlags = CacheManager.getInstance().getCache(SystemPropertiesCache.class).getSystemPropertiesByName(SystemPropertiesCache.POWERPLAY_TEAM_FLAGS);
		FlagsMapping flags = new Gson().fromJson(teamFlags, FlagsMapping.class);
			List<TeamFlagDTO> teams = flags.getFlags();
			for(TeamFlagDTO team : teams) {
				if(name.equalsIgnoreCase(team.getName())) {
					return team.getFlag();
				}
			}
		return "default.jpg";
	}

}
