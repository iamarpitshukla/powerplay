package com.myteam.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class CollectionUtils {


    /**
     * @param <T>
     * @param c
     * @return
     */
    public static <T> List<T> asList(Collection<T> c) {
        if (c instanceof List) {
            return (List<T>) c;
        } else {
            List<T> list = new ArrayList<T>();
            list.addAll(c);
            return list;
        }
    }

    public static <T> boolean isEmpty(Collection<T> c) {
        if (c == null)
            return true;
        if (c.size() == 0)
            return true;
        return false;
    }
    
    public static <T> boolean isNotEmpty(Collection<T> c){
        return !isEmpty(c);
    }
    
    public static <T> List<T> getSubList(List<T> c, Integer start, Integer maxResults) {
        if (isEmpty(c)) {
            return c;
        }
        if (c.size() > start) {
            int lastIndex = c.size();
            if (start + maxResults <= lastIndex) {
                c = c.subList(start, start + maxResults);
            } else {
                c = c.subList(start, lastIndex);
            }
            return c;
        }
        return null;
    }

    public static <T> List<T> getMaxElements(List<T> c, Integer max) {
        if (isEmpty(c)) {
            return c;
        }
        if(c.size() > max){
        	return c.subList(0, max);
        }
        return c;
    }

	public static <T> void getRandomMaxElements(List<T> c, Integer max, int randomIndex, List<T> returnList) {
		if (isEmpty(c)) {
			return;
		}
		for (int i = 0; i < randomIndex; i++) {
			Collections.shuffle(c);
		}
		for (T t : c) {
			returnList.add(t);
			if (returnList.size() >= max) {
				return;
			}
		}
	}
	
	public static <T> int getSize(Collection<T> c) {
		if (CollectionUtils.isEmpty(c)) {
			return 0;
		}
		return c.size();
	}
	
	public static <T> int getListCapacity(List<T> c) {
	    return getSize(c)+1;
	}
	
	public static <T> int getHashMapCapacity(Collection<T> c) {
	    int i = getSize(c);
	    return (int)(i *4/3)+1;
    }
	
	@Deprecated
    public static <T> List<T> getMaxElements(List<T> c, Integer start, Integer max) {
        if (c.size() > max) {
            return c.subList(start, max);
        }
        return c;
    }

	public static<T> List<T> nullSafe(List<T> nonLybrateUsers) {
		if(nonLybrateUsers==null){
			return new ArrayList<T>();
		}
		return nonLybrateUsers;
	}


}
