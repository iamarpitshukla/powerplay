package com.myteam.doc;

import java.util.Date;

import javax.persistence.Id;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.myteam.dto.PlayerDetailDTO;
import com.myteam.respponse.GetPlayerStatsResponse;

@Document
public class PlayersDoc {

	@Id
	private String id;

	@Indexed
	private Integer pid;
	
	private String profile;
	
	private String imageURL;
	
	private String battingStyle;
	
	private String bowlingStyle;
	
	private String majorTeams;
	
	private String currentAge;
	
	private String born;
	
	private String fullName;
	
	private String name;
	
	private String country;
	
	private String playingRole;
	
	private PlayerDetailDTO data;
	
	private Date created;
	
	private Date updated;
	
	public PlayersDoc() {
		super();
	}
	
	public PlayersDoc(GetPlayerStatsResponse response) {
		super();
		this.pid = response.getPid();
		this.profile = response.getProfile();
		this.imageURL = response.getImageURL();
		this.battingStyle = response.getBattingStyle();
		this.bowlingStyle = response.getBowlingStyle();
		this.majorTeams = response.getMajorTeams();
		this.currentAge = response.getCurrentAge();
		this.born = response.getBorn();
		this.fullName = response.getFullName();
		this.name = response.getName();
		this.playingRole = response.getPlayingRole();
		this.data = response.getData();
		this.country = response.getCountry();
	}
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public Integer getPid() {
		return pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	public String getBattingStyle() {
		return battingStyle;
	}

	public void setBattingStyle(String battingStyle) {
		this.battingStyle = battingStyle;
	}

	public String getBowlingStyle() {
		return bowlingStyle;
	}

	public void setBowlingStyle(String bowlingStyle) {
		this.bowlingStyle = bowlingStyle;
	}

	public String getMajorTeams() {
		return majorTeams;
	}

	public void setMajorTeams(String majorTeams) {
		this.majorTeams = majorTeams;
	}

	public String getCurrentAge() {
		return currentAge;
	}

	public void setCurrentAge(String currentAge) {
		this.currentAge = currentAge;
	}

	public String getBorn() {
		return born;
	}

	public void setBorn(String born) {
		this.born = born;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPlayingRole() {
		return playingRole;
	}

	public void setPlayingRole(String playingRole) {
		this.playingRole = playingRole;
	}

	public PlayerDetailDTO getData() {
		return data;
	}

	public void setData(PlayerDetailDTO data) {
		this.data = data;
	}
}
