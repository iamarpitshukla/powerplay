package com.myteam.doc;

import javax.persistence.Id;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class MatchConfigDetailDoc {
	
	@Id
	private String id;

	@Indexed
	private Integer matchId;
	
	private boolean enabled;
	
	private String team1Flag;
	
	private String team2Flag;
	
	private String seriesName;
	
	public String getSeriesName() {
		return seriesName;
	}

	public void setSeriesName(String seriesName) {
		this.seriesName = seriesName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getMatchId() {
		return matchId;
	}

	public void setMatchId(Integer matchId) {
		this.matchId = matchId;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getTeam1Flag() {
		return team1Flag;
	}

	public void setTeam1Flag(String team1Flag) {
		this.team1Flag = team1Flag;
	}

	public String getTeam2Flag() {
		return team2Flag;
	}

	public void setTeam2Flag(String team2Flag) {
		this.team2Flag = team2Flag;
	}
}
