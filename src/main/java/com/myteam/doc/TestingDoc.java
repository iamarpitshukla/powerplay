package com.myteam.doc;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class TestingDoc {
	
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
