package com.myteam.cron;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.myteam.services.AutoTeamCreationService;
import com.myteam.services.ICricAPIService;

@Component("cricApiHandler")
@EnableScheduling
public class CricApiHandler {

    private static final Logger LOG = LoggerFactory.getLogger(CricApiHandler.class);
    
    @Autowired
    ICricAPIService cricAPIService;
    
    @Autowired
    AutoTeamCreationService autoTeamCreationService;
    
    @Scheduled(cron = "0 */30 * * * *")
    public void processFutureMatches() {
    	LOG.info("Strating processFutureMatches cron ");
    	cricAPIService.processFutureMatches();
    	LOG.info("Completed processFutureMatches cron ");
    }

    @Scheduled(cron = "0 */30 * * * *")
    public void populateSquad() {
    	LOG.info("Strating populateSquad cron ");
    	cricAPIService.populateSquad();
    	LOG.info("Completed populateSquad cron ");
    }
    
    @Scheduled(cron = "0 */2 * * * *")
    public void updatePoints() {
    	LOG.info("Starting updatePoints cron ");
    	cricAPIService.updateScores();
    	LOG.info("Completed updatePoints cron ");
    }
    
    @Scheduled(cron = "50 */2 * * * *")
    public void updateMatchPlayerFantasyPoints() {
    	LOG.info("Starting updateMatchPlayerFantasyPoints cron ");
    	cricAPIService.updateMatchPlayerFantasyPoints();
    	LOG.info("Completed updateMatchPlayerFantasyPoints cron ");
    }
    
    @Scheduled(cron = "30 */10 * * * *")
    public void updateWinningAmount() {
    	LOG.info("Starting updateWinningAmount cron ");
    	cricAPIService.updateWinningAmountUsingRank();
    	LOG.info("Completed updateWinningAmount cron ");
    }
    
    @Scheduled(cron = "0 */2 * * * *")
    public void updatePlayingEleven() {
    	LOG.info("Starting updatePlayingEleven cron ");
    	cricAPIService.updatePlayingEleven();
    	LOG.info("Completed updatePlayingEleven cron ");
    }
    
    //@Scheduled(cron = "0 */1 * * * *")
    public void createAutoTeams() {
    	LOG.info("Starting createAutoTeams cron ");
    	autoTeamCreationService.createAutoTeams();
    	LOG.info("Completed createAutoTeams cron ");
    }
    
}
