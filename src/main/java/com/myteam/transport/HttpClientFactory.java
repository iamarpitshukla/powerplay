package com.myteam.transport;

/*
 *  Copyright 2013 Lybrate INC  . All Rights Reserved.
 *  Lybrate INC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Aug 19, 2013
 *  @author rahul
 */

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NoHttpResponseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.conn.params.ConnRouteParams;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.socket.LayeredConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.ContentEncodingHttpClient;
import org.apache.http.impl.client.DefaultConnectionKeepAliveStrategy;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.ExecutionContext;
import org.apache.http.protocol.HttpContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * This is factory class for apache http client instances. Http clients are initialized with thread safe connection
 * manager and with a custom retry handler. Also it picks the random IP addresses available. Use of getCachedHttpClient
 * is recommended as it will cache the httpclient instances and will return the cached instance.
 * 
 * @author rahul
 */
public class HttpClientFactory {

    private static final Logger                  LOG             = LoggerFactory.getLogger(HttpClientFactory.class);
    private static final Map<String, HttpClient> httpClientCache = new HashMap<String, HttpClient>();

    /**
     * @param apiCacheName
     * @return the cached http client instance for the given name
     */
    public static HttpClient getCachedHttpClient(String apiCacheName) {
        return httpClientCache.get(apiCacheName);
    }

    public static HttpClient getOrCreateCachedHttpClient(String apiCacheName) {
        return getOrCreateCachedHttpClient(apiCacheName, null);
    }

    public static HttpClient getOrCreateCachedHttpClient(String apiCacheName, HttpClientConfig clientConfig) {
        HttpClient httpClient = getCachedHttpClient(apiCacheName);
        if (httpClient == null) {
            synchronized (httpClientCache) {
                httpClient = httpClientCache.get(apiCacheName);
                if (httpClient == null) {
                    httpClient = buildHttpClient(clientConfig, apiCacheName);
                    httpClientCache.put(apiCacheName, httpClient);
                }
            }
        }
        return httpClient;
    }

   
    private static HttpClient buildHttpClient(HttpClientConfig clientConfig, String apiCacheName) {
        if (clientConfig == null) {
            clientConfig = new HttpClientConfig();
        }
        HttpParams params = new BasicHttpParams();
        HttpConnectionParams.setSoTimeout(params, clientConfig.getSoTimeout());
        ConnRouteParams.setLocalAddress(params, clientConfig.getLocalAddress());
        HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
        SSLSocketFactory sf = null;
        if("lyrisEmailSystem".equals(apiCacheName)){ //Temporary workaround for Lyris SSL certificate issue
        	SSLContext ctx = null;
            try {
    			ctx = SSLContext.getInstance("TLS");
    		} catch (NoSuchAlgorithmException e) {
    			e.printStackTrace();
    		}
    		X509TrustManager tm = new X509TrustManager() {
    			 
    			public void checkClientTrusted(X509Certificate[] xcs, String string) {
    			}
    			 
    			public void checkServerTrusted(X509Certificate[] xcs, String string) {
    			}
    			 
    			public X509Certificate[] getAcceptedIssuers() {
    			return null;
    			}
    		};
    		try {
    			ctx.init(null, new TrustManager[]{(TrustManager) tm}, null);
    		} catch (KeyManagementException e) {
    			e.printStackTrace();
    		}	
    		sf = new SSLSocketFactory(ctx);
        } else {
        	sf = SSLSocketFactory.getSocketFactory();
        }
         
        sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
        SchemeRegistry schemeRegistry = new SchemeRegistry();
        schemeRegistry.register(new Scheme("http", 80, PlainSocketFactory.getSocketFactory()));
        schemeRegistry.register(new Scheme("https", 443, sf));

        ThreadSafeClientConnManager cm = new ThreadSafeClientConnManager(schemeRegistry);
        cm.setDefaultMaxPerRoute(clientConfig.getDefaultMaxPerRoute());
        cm.setMaxTotal(clientConfig.getMaxTotalConnections());
        LOG.info("Initializing ThreadSafeClientConnManager with defaultMaxPerRoute="+clientConfig.getDefaultMaxPerRoute() 
        + " and max total connections=" + clientConfig.getMaxTotalConnections() + " in apiCacheName=" + apiCacheName);
        DefaultHttpClient client = new ContentEncodingHttpClient(cm, params);
        HttpRequestRetryHandler retryHandler = new HttpRequestRetryHandler() {
            public boolean retryRequest(IOException exception, int executionCount, HttpContext context) {
                if (executionCount >= 2) {
                    // Do not retry if over max retry count
                    return false;
                }
                if (exception instanceof NoHttpResponseException) {
                    // Retry if the server dropped connection on us
                    return true;
                }
                if (exception instanceof SSLHandshakeException) {
                    // Do not retry on SSL handshake exception
                    return false;
                }
                HttpRequest request = (HttpRequest) context.getAttribute(ExecutionContext.HTTP_REQUEST);
                boolean idempotent = !(request instanceof HttpEntityEnclosingRequest);
                if (idempotent) {
                    // Retry if the request is considered idempotent
                    return true;
                }
                return false;
            }
        };
        client.setHttpRequestRetryHandler(retryHandler);

        //change connection keep alive strategy
        final long keepAlive = clientConfig.getKeepAlive();
        client.setKeepAliveStrategy(new DefaultConnectionKeepAliveStrategy() {
            @Override
            public long getKeepAliveDuration(HttpResponse response, HttpContext context) {
                return keepAlive;
            }
        });

        return client;
    }

    public static HttpClient buildHttpClient(boolean useTLS1_2) {
        if(useTLS1_2){
            return buildHttpClientTLS1_2();
        }
        return buildHttpClient(null, "");
    }

    private static HttpClient buildHttpClientTLS1_2() {
        SSLContext sslcontext = null;
        try {
            sslcontext = SSLContext.getInstance("TLSv1.2");
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        SSLContext.setDefault(sslcontext);
        try {
            sslcontext.init(null, null, new java.security.SecureRandom());
        } catch (KeyManagementException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        HttpClient client = HttpClientBuilder.create().setSSLSocketFactory((LayeredConnectionSocketFactory) new SSLConnectionSocketFactory(sslcontext, new String[] { "TLSv1.2" },
                null, SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER)).build();
        return client;
    }
}
