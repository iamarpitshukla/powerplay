package com.myteam.transport;

import java.net.InetAddress;

public class HttpClientConfig {

    private int         defaultMaxPerRoute      = 20;
    private int         maxTotalConnections     = 100;
    private InetAddress localAddress;
    private int         soTimeout               = 30 * 1000;
    private long        keepAlive               = -1;
    private boolean     idleConnectionMonitorOn = false;

    public int getDefaultMaxPerRoute() {
        return defaultMaxPerRoute;
    }

    public void setDefaultMaxPerRoute(int defaultMaxPerRoute) {
        this.defaultMaxPerRoute = defaultMaxPerRoute;
    }

    public int getMaxTotalConnections() {
        return maxTotalConnections;
    }

    public void setMaxTotalConnections(int maxTotalConnections) {
        this.maxTotalConnections = maxTotalConnections;
    }

    public InetAddress getLocalAddress() {
        return localAddress;
    }

    public void setLocalAddress(InetAddress localAddress) {
        this.localAddress = localAddress;
    }

    public int getSoTimeout() {
        return soTimeout;
    }

    public void setSoTimeout(int soTimeout) {
        this.soTimeout = soTimeout;
    }

    public long getKeepAlive() {
        return keepAlive;
    }

    public void setKeepAlive(long keepAlive) {
        this.keepAlive = keepAlive;
    }

    public boolean isIdleConnectionMonitorOn() {
        return idleConnectionMonitorOn;
    }

    public void setIdleConnectionMonitorOn(boolean idleConnectionMonitorOn) {
        this.idleConnectionMonitorOn = idleConnectionMonitorOn;
    }

}
