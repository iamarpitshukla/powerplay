package com.myteam.transport;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.myteam.dto.HttpSenderResponseDTO;
import com.myteam.utils.StringUtils;

public class HttpSender {


    private static final String CONTENT_ENCODING_UTF_8 = "UTF-8";
    private static final String LINE_SEPARATOR         = System.getProperty("line.separator");
    private static final Logger LOG                    = LoggerFactory.getLogger(HttpSender.class);
    private HttpClient          httpClient;
    private boolean             clientCached;

    /**
     * @param apiCacheName
     */
    public HttpSender(String apiCacheName) {
        this.clientCached = true;
        this.httpClient = HttpClientFactory.getOrCreateCachedHttpClient(apiCacheName);
    }

    /**
     * default constructor
     */
    public HttpSender(String apiCacheName, String proxyHost, Integer proxyPort) {
        this.clientCached = true;
        this.httpClient = HttpClientFactory.getOrCreateCachedHttpClient(apiCacheName);
        HttpHost proxy = new HttpHost(proxyHost, proxyPort);
        httpClient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);
    }

    public HttpSender(String apiCacheName, boolean createIfNotInCache) {
        this.clientCached = true;
        this.httpClient = HttpClientFactory.getCachedHttpClient(apiCacheName);
        if (createIfNotInCache && this.httpClient == null) {
            this.httpClient = HttpClientFactory.getOrCreateCachedHttpClient(apiCacheName);
        }
    }

    public HttpSender(String apiCacheName, HttpClientConfig clientConfig) {
        this.clientCached = true;
        this.httpClient = HttpClientFactory.getOrCreateCachedHttpClient(apiCacheName, clientConfig);
    }

    /**
     * default constructor
     */
    public HttpSender() {
        this.httpClient = HttpClientFactory.buildHttpClient(false);
        httpClient.getParams();
    }
    
    public HttpSender(boolean useTLS1_2) {
        this.httpClient = HttpClientFactory.buildHttpClient(useTLS1_2);
    }

    /**
     * @param timeout
     * @param timeunit
     */
    public void setTimeout(int timeout, TimeUnit timeunit) {
        httpClient.getParams().setIntParameter(CoreConnectionPNames.SO_TIMEOUT, (int) timeunit.toMillis(timeout));
    }

    /**
     * @param url
     * @param params
     * @return
     * @throws HttpTransportException
     */
    public String executeGet(String url, Map<String, String> params) throws HttpTransportException {
        return executeGet(url, params, null);
    }

    /**
     * @param url
     * @param params
     * @param headers
     * @return
     * @throws HttpTransportException
     */
    public String executeGet(String url, Map<String, String> params, Map<String, String> headers) throws HttpTransportException {
        HttpGet httpGet = new HttpGet(createURL(url, params));
        if (headers != null) {
            for (Map.Entry<String, String> hEntry : headers.entrySet()) {
                httpGet.addHeader(hEntry.getKey(), hEntry.getValue());
            }
        }
        try {
            HttpResponse response = httpClient.execute(httpGet);
            return getContent(response.getEntity());
        } catch (Exception e) {
            LOG.debug("http get failed for url:'{}' ", url);
            LOG.error("http get failed due to:{}", e.getMessage());
            throw new HttpTransportException("Unable to execute http get", e);
        }
    }
    
    public HttpSenderResponseDTO executeGetV2(String url, Map<String, String> params, Map<String, String> headers) throws HttpTransportException {
        HttpGet httpGet = new HttpGet(createURL(url, params));
        if (headers != null) {
            for (Map.Entry<String, String> hEntry : headers.entrySet()) {
                httpGet.addHeader(hEntry.getKey(), hEntry.getValue());
            }
        }
        try {
            HttpSenderResponseDTO responseDTO = new HttpSenderResponseDTO();
            HttpResponse response = httpClient.execute(httpGet);
            if(response != null) {
                responseDTO.setResponseCode(response.getStatusLine().getStatusCode());
                responseDTO.setResponse(getContent(response.getEntity()));
                return responseDTO;
            }
        } catch (Exception e) {
            LOG.debug("http get failed for url:'{}' ", url);
            LOG.error("http get failed due to:{}", e.getMessage());
            throw new HttpTransportException("Unable to execute http get", e);
        }
        return null;
    }
    
    public static String createURL(String url, Map<String, String> params) {
        if (params != null) {
            StringBuilder builder = new StringBuilder();
            builder.append(url);
            List<NameValuePair> nvps = new ArrayList<NameValuePair>();
            for (Map.Entry<String, String> pEntry : params.entrySet()) {
                nvps.add(new BasicNameValuePair(pEntry.getKey(), pEntry.getValue()));
            }
            builder.append('?').append(URLEncodedUtils.format(nvps, CONTENT_ENCODING_UTF_8));
            return builder.toString();
        } else {
            return url;
        }
    }

    /**
     * @param url
     * @param params
     * @param headers
     * @return
     * @throws HttpTransportException
     */
    public String executePost(String url, List<NameValuePair> nvps, Map<String, String> headers) throws HttpTransportException {
        HttpPost httpPost = new HttpPost(url);
        if (headers != null) {
            for (Map.Entry<String, String> hEntry : headers.entrySet()) {
                httpPost.addHeader(hEntry.getKey(), hEntry.getValue());
            }
        }
        if (nvps != null) {
//            List<NameValuePair> nvps = new ArrayList<NameValuePair>();
//            for (Map.Entry<String, String> pEntry : params.entrySet()) {
//                nvps.add(new BasicNameValuePair(pEntry.getKey(), pEntry.getValue()));
//            }
            try {
                httpPost.setEntity(new UrlEncodedFormEntity(nvps, CONTENT_ENCODING_UTF_8));
            } catch (UnsupportedEncodingException e) {
                LOG.debug("unable to encode params:'{}' ", nvps);
                throw new HttpTransportException("invalid character encoding", e);
            }
        }
        try {
            HttpResponse response = httpClient.execute(httpPost);
            return getContent(response.getEntity());
        } catch (Exception e) {
            LOG.debug("http post failed for url:'{}' ", url);
            throw new HttpTransportException("Unable to execute http post", e);
        }
    }
    
    /**
     * @param url
     * @param params
     * @param headers
     * @return
     * @throws HttpTransportException
     */
    public String executePost(String url, Map<String, String> params, Map<String, String> headers) throws HttpTransportException {
        HttpPost httpPost = new HttpPost(url);
        if (headers != null) {
            for (Map.Entry<String, String> hEntry : headers.entrySet()) {
                httpPost.addHeader(hEntry.getKey(), hEntry.getValue());
            }
        }
        if (params != null) {
            List<NameValuePair> nvps = new ArrayList<NameValuePair>();
            for (Map.Entry<String, String> pEntry : params.entrySet()) {
                nvps.add(new BasicNameValuePair(pEntry.getKey(), pEntry.getValue()));
            }
            try {
                httpPost.setEntity(new UrlEncodedFormEntity(nvps, CONTENT_ENCODING_UTF_8));
            } catch (UnsupportedEncodingException e) {
                LOG.debug("unable to encode params:'{}' ", params);
                throw new HttpTransportException("invalid character encoding", e);
            }
        }
        try {
            LOG.info(" http post :{}", httpPost.toString());
            HttpResponse response = httpClient.execute(httpPost);
            return getContent(response.getEntity());
        } catch (Exception e) {
            LOG.info("Params of post request: {}", params);
            LOG.info("Header of post request: {}", headers);
            LOG.info("URL of post request: {}", url);
            LOG.error("http post failed for url:'{}' ", url);
            throw new HttpTransportException("Unable to execute http post", e);
        }
    }
    
    public String executePostJSon(String url, Map<String, String> params) throws HttpTransportException {
        HttpPost httpPost = new HttpPost(url);
        JSONObject data = new JSONObject();
        if (params != null) {
            for (Map.Entry<String, String> pEntry : params.entrySet()) {
                try {
                    data.put(pEntry.getKey(),pEntry.getValue());
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    throw new HttpTransportException("Unable to execute http post", e);
                }
            }
        }
        
        //StringEntity se = null;
        try {
            //se = new StringEntity(data.toString());
            List<NameValuePair> nvps = new ArrayList<NameValuePair>();
            nvps.add(new BasicNameValuePair("JsonData", data.toString()));
            httpPost.setEntity(new UrlEncodedFormEntity(nvps, "ISO-8859-1"));
            
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            throw new HttpTransportException("Unable to execute http post", e);
        }
        
        
        httpPost.setHeader("Accept", "application/json");
        httpPost.setHeader("Content-type", "application/json");
        
        HttpResponse response;
        try {
            response = httpClient.execute(httpPost);
            return getContent(response.getEntity());
        } catch (Exception e) {
            LOG.debug("http post failed for url:'{}' ", url);
            throw new HttpTransportException("Unable to execute http post", e);
        }
    }

    /**
     * @param url
     * @param params
     * @return
     * @throws HttpTransportException
     */
    public String executePost(String url, Map<String, String> params) throws HttpTransportException {
        return executePost(url, params, null);
    }
    
    public String executePostJsonWithHeader(String postUrl, StringEntity stringEntity, Map<String,String> headers) throws HttpTransportException {
        HttpPost post = new HttpPost(postUrl);
        stringEntity.setContentType("application/json");
        post.setEntity(stringEntity);
        post.setHeader("Content-type", "application/json");
        post.addHeader("Accept","application/json");
        if (headers != null) {
            for (Map.Entry<String, String> pEntry : headers.entrySet()) {
                if (pEntry.getKey() != null && pEntry.getValue() != null) {
                    post.setHeader(pEntry.getKey(), pEntry.getValue());
                }
            }
        }
        try {
            HttpResponse  response = httpClient.execute(post);
            LOG.info("http response code is :{}",response.getStatusLine().getStatusCode());
            return getContent(response.getEntity());
        } catch (IOException e) {
            LOG.error("Error: {}",e);
        }
        return null;
    }
    
    public HttpSenderResponseDTO executePostJsonWithHeaderV2(String postUrl, StringEntity stringEntity, Map<String,String> headers) throws HttpTransportException {
        HttpPost post = new HttpPost(postUrl);
        if (stringEntity != null) {
            stringEntity.setContentType("application/json");
            post.setEntity(stringEntity);
        }
        post.setHeader("Content-type", "application/json");
        post.addHeader("Accept","application/json");
        if (headers != null) {
            for (Map.Entry<String, String> pEntry : headers.entrySet()) {
                if (pEntry.getKey() != null && pEntry.getValue() != null) {
                    post.setHeader(pEntry.getKey(), pEntry.getValue());
                }
            }
        }
        HttpSenderResponseDTO responseDTO = new HttpSenderResponseDTO();
        try {
            HttpResponse  response = httpClient.execute(post);
            LOG.info("http response code is :{}",response.getStatusLine().getStatusCode());
            responseDTO.setResponse(getContent(response.getEntity()));
            responseDTO.setResponseCode(response.getStatusLine().getStatusCode());
            return responseDTO;
        } catch (IOException e) {
            LOG.error("Error: {}",e);
        }
        return null;
    }
    
    public String executePostJson(String postUrl, StringEntity stringEntity, Map<String,String> params) throws HttpTransportException {
        JSONObject data = new JSONObject();
        if (params != null) {
            for (Map.Entry<String, String> pEntry : params.entrySet()) {
                try {
                    data.put(pEntry.getKey(),pEntry.getValue());
                    stringEntity = new StringEntity(data.toString());
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    throw new HttpTransportException("Unable to execute http post", e);
                }catch (UnsupportedEncodingException e ){
                    throw new HttpTransportException("Unable to execute http post", e);
                }
            }
        }
        HttpPost post = new HttpPost(postUrl);
        post.setEntity(stringEntity);
        post.setHeader("Content-type", "application/json");
        try {
            HttpResponse  response = httpClient.execute(post);
            return getContent(response.getEntity());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public String executePostContent(String url, Map<String, String> params, Map<String, String> headers, String body) throws HttpTransportException {
        StringBuilder sb = new StringBuilder(url);
        if (params != null) {
            if (!sb.toString().contains("?")) {
                sb.append('?');
            }
            for (Map.Entry<String, String> pEntry : params.entrySet()) {
                sb.append(pEntry.getKey()).append('=').append(pEntry.getValue()).append('&');
            }
        }

        HttpPost httpPost = new HttpPost(sb.toString());

        if (headers != null) {
            for (Map.Entry<String, String> hEntry : headers.entrySet()) {
                httpPost.addHeader(hEntry.getKey(), hEntry.getValue());
            }
        }
        try {
            httpPost.setEntity(new StringEntity(body, CONTENT_ENCODING_UTF_8));
        } catch (Exception e) {
            LOG.debug("unable to encode params:'{}' ", params);
            throw new HttpTransportException("invalid character encoding", e);
        }
        try {
            HttpResponse response = httpClient.execute(httpPost);
            return getContent(response.getEntity());
        } catch (Exception e) {
            LOG.debug("http post failed for url:'{}' ", url);
            throw new HttpTransportException("Unable to execute http post", e);
        }
    }

    public byte[] executePostContentAndReturnBytes(String url, Map<String, String> params, Map<String, String> headers, String body) throws HttpTransportException {
        StringBuilder sb = new StringBuilder(url);
        if (params != null) {
            if (!sb.toString().contains("?")) {
                sb.append('?');
            }
            for (Map.Entry<String, String> pEntry : params.entrySet()) {
                sb.append(pEntry.getKey()).append('=').append(pEntry.getValue()).append('&');
            }
        }

        HttpPost httpPost = new HttpPost(sb.toString());

        if (headers != null) {
            for (Map.Entry<String, String> hEntry : headers.entrySet()) {
                httpPost.addHeader(hEntry.getKey(), hEntry.getValue());
            }
        }
        try {
            httpPost.setEntity(new StringEntity(body, CONTENT_ENCODING_UTF_8));
        } catch (Exception e) {
            LOG.debug("unable to encode params:'{}' ", params);
            throw new HttpTransportException("invalid character encoding", e);
        }
        try {
            HttpResponse response = httpClient.execute(httpPost);
            byte[] data = EntityUtils.toByteArray(response.getEntity());
            EntityUtils.consume(response.getEntity());
            return data;
        } catch (Exception e) {
            LOG.debug("http post failed for url:'{}' ", url);
            throw new HttpTransportException("Unable to execute http post", e);
        }
    }

    public byte[] executePostContentAndReturnBytes(String url, Map<String, String> params, Map<String, String> headers, byte[] body) throws HttpTransportException {
        StringBuilder sb = new StringBuilder(url);
        if (params != null) {
            if (!sb.toString().contains("?")) {
                sb.append('?');
            }
            for (Map.Entry<String, String> pEntry : params.entrySet()) {
                sb.append(pEntry.getKey()).append('=').append(pEntry.getValue()).append('&');
            }
        }

        HttpPost httpPost = new HttpPost(sb.toString());

        if (headers != null) {
            for (Map.Entry<String, String> hEntry : headers.entrySet()) {
                httpPost.addHeader(hEntry.getKey(), hEntry.getValue());
            }
        }
        httpPost.setEntity(new ByteArrayEntity(body));
        try {
            HttpResponse response = httpClient.execute(httpPost);
            byte[] data = EntityUtils.toByteArray(response.getEntity());
            EntityUtils.consume(response.getEntity());
            int responseCode = response.getStatusLine().getStatusCode();
            if( responseCode != 200) {
                LOG.error("Invalid response received. Response code : {} and  data : {} ", responseCode, data);
                if(responseCode == 404) {
                    throw new HttpTransportException("Requested URL Does Not exist at destination. Response Code : 404");
                } else if (responseCode == 500) {
                    throw new HttpTransportException("Internal Server Error at destination. Response Code : 500");
                } else {
                    throw new HttpTransportException("Unknown Error at destination. Response Code : " + responseCode);
                }
            }    
            return data;
        } catch (Exception e) {
            LOG.debug("http post failed for url:'{}' ", url);
            throw new HttpTransportException("Unable to execute http post", e);
        }
    }

    /**
     * @param url
     * @param requestXml
     * @param headers
     * @return
     * @throws HttpTransportException
     */
    public String executePost(String url, String requestXml, Map<String, String> headers) throws HttpTransportException {
        HttpPost httpPost = new HttpPost(url);
        if (headers != null) {
            for (Map.Entry<String, String> hEntry : headers.entrySet()) {
                httpPost.addHeader(hEntry.getKey(), hEntry.getValue());
            }
        }
        try {
            httpPost.setEntity(new StringEntity(requestXml, "text/xml", CONTENT_ENCODING_UTF_8));
        } catch (UnsupportedEncodingException e) {
            throw new HttpTransportException("invalid character encoding", e);
        }
        try {
            HttpResponse response = httpClient.execute(httpPost);
            return getContent(response.getEntity());
        } catch (Exception e) {
            LOG.debug("http post failed for url:'{}' ", url);
            throw new HttpTransportException("Unable to execute http post", e);
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#finalize()
     */
    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        if (!clientCached) {
            httpClient.getConnectionManager().shutdown();
        }
    }

    /**
     * @param entity
     * @return
     * @throws IOException
     */
    private static String getContent(HttpEntity entity) throws IOException {
        String charset = EntityUtils.getContentCharSet(entity);
        InputStreamReader inputStreamReader = null;
        BufferedReader br = null;
        StringBuilder response = new StringBuilder();
        try {
            if (StringUtils.isNotEmpty(charset)) {
                inputStreamReader = new InputStreamReader(entity.getContent(), charset);
            } else {
                inputStreamReader = new InputStreamReader(entity.getContent());
            }
            br = new BufferedReader(inputStreamReader);
            String line = null;
            while ((line = br.readLine()) != null) {
                response.append(line).append(LINE_SEPARATOR);
            }
            // This is to ensure the resources are released properly
            EntityUtils.consume(entity);
        } finally {
            if (inputStreamReader != null) {
                inputStreamReader.close();
            }
            if (br != null) {
                br.close();
            }
        }
        return response.toString();
    }

    public byte[] executeGetAndReturnBytes(String url, Map<String, String> params, Map<String, String> headers) throws HttpTransportException {
        HttpGet httpGet = new HttpGet(createURL(url, params));
        if (headers != null) {
            for (Map.Entry<String, String> hEntry : headers.entrySet()) {
                httpGet.addHeader(hEntry.getKey(), hEntry.getValue());
            }
        }
        try {
            HttpResponse response = httpClient.execute(httpGet);
            byte[] data = EntityUtils.toByteArray(response.getEntity());
            EntityUtils.consume(response.getEntity());
            return data;
        } catch (Exception e) {
            LOG.debug("http get failed for url:'{}' ", url);
            throw new HttpTransportException("Unable to execute http get", e);
        }
    }

    private static byte[] getContentAsBytes(HttpEntity entity) throws IOException {
        InputStream is = entity.getContent();
        byte[] result = new byte[(int) entity.getContentLength()];
        is.read(result);
        return result;
    }


    public boolean isHttpClientCreated() {
        return httpClient != null;
    }
    
    public String executePostJsonV2(String postUrl, String jsonString, Map<String,String> headers){
        StringEntity stringEntity = null;
        try {
            stringEntity = new StringEntity(jsonString);
        } catch (UnsupportedEncodingException e1) {
            LOG.info("Was unable to parse json.");
            e1.printStackTrace();
        }
        HttpPost post = new HttpPost(postUrl);
        post.setEntity(stringEntity);
        if (headers != null) {
            for (Map.Entry<String, String> hEntry : headers.entrySet()) {
                post.addHeader(hEntry.getKey(), hEntry.getValue());
            }
        }
        post.setHeader("Content-Type", "application/json;charset=UTF-8");
        try {
            HttpResponse  response = httpClient.execute(post);
            return getContent(response.getEntity());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


}
