package com.myteam.transport;

/*
 *  Copyright 2013 Lybrate INC  . All Rights Reserved.
 *  Lybrate INC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Aug 19, 2013
 *  @author singla
 */
public class HttpTransportException extends Exception {

    private static final long serialVersionUID = -4896599313442168860L;

    /**
     * Constructs an <code>HttpTransportException</code> with the specified message.
     * 
     * @param message
     * @param cause
     */
    public HttpTransportException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructs an <code>HttpTransportException</code> with the specified message and root cause.
     * 
     * @param message
     */
    public HttpTransportException(String message) {
        super(message);
    }

}
