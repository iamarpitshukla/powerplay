package com.myteam.services;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.myteam.cache.CacheManager;
import com.myteam.cache.ContestCache;
import com.myteam.cache.SmsTemplateCache;
import com.myteam.cache.SystemPropertiesCache;
import com.myteam.entity.Contests;
import com.myteam.entity.SmsTemplate;
import com.myteam.entity.SystemProperties;
import com.myteam.utils.DateUtils;


@Service("powerPlayStartUpServiceImpl")
public class PowerPlayStartUpServiceImpl implements IPowerPlayStartupService{

	private static final Logger LOG = LoggerFactory.getLogger(PowerPlayStartUpServiceImpl.class);

    @Autowired
    private ApplicationContext  applicationContext;
    
    @Autowired
    private IPowerPlayService powerPlayService;
    
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    @Override
    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }
    
    @Override
    public void loadAll() {
    	LOG.info("Powerplay startup service starting");
    	IPowerPlayStartupService startupService = applicationContext.getBean(IPowerPlayStartupService.class);
    	startupService.loadSystemProperties();
    	startupService.loadContestWiseDistribution();
    	startupService.loadSmsTemplates();
    	LOG.info("Powerplay startup service completed");
    }
    
    @Override
    @Scheduled(cron = "0 */30 * * * *")
	public void loadSystemProperties() {
    	LOG.info("Powerplay loadSystemPropertiess service starting");
		List<SystemProperties> sps = powerPlayService.getSystemProperties();
		SystemPropertiesCache cache = new SystemPropertiesCache();
		cache.addSystemProperties(sps);
		CacheManager.getInstance().setCache(cache);
		LOG.info("Powerplay loadSystemPropertiess service completed");
	}
    
    @Override
	@Scheduled(cron = "0 */30 * * * *")
	public void loadContestWiseDistribution() {
    	LOG.info("Powerplay loadContestWiseDistribution service starting");
		Date StartDate = DateUtils.getDateBeforeGivenWorkingDays(new Date(), 20);
		List<Contests> last20DaysContests = powerPlayService.getContestsFromStartDate(StartDate);
		ContestCache cache = new ContestCache();
		cache.addContestWiseDistribution(last20DaysContests);
		CacheManager.getInstance().setCache(cache);
		LOG.info("Powerplay loadContestWiseDistribution service completed");
	}
    
    @Override
	public void loadSmsTemplates() {
		LOG.info("Loading Powerplay sms notification templates");
		List<SmsTemplate> templates = powerPlayService.getSmsTemplates();
		SmsTemplateCache cache = new SmsTemplateCache();
		for (SmsTemplate template : templates) {
			cache.addSmsTemplate(template);
		}
		CacheManager.getInstance().setCache(cache);
		LOG.info("Loaded Powerplay sms notification templates... SUCCESSFULLY");
	}

}
