package com.myteam.services;

import java.util.List;

import com.myteam.doc.UserAuthToken;
import com.myteam.dto.UserDetailDTO;
import com.myteam.entity.User;
import com.myteam.entity.UserReferralDetail;
import com.myteam.respponse.GetUserReferalDetailsResponse;
import com.myteam.respponse.GetUserWalletDetailResponse;

public interface IUserService {
   User saveUser(User user);

   List<User> list();

UserAuthToken generateAuthToken(User user, String os);

UserAuthToken getAuthToken(String authToken);

void invalidateAuthToken(UserAuthToken authToken);

User getUserByUserId(Integer userId);

UserReferralDetail saveUserReferralDetail(UserReferralDetail userReferralDetail);

UserReferralDetail getUserReferralDetailByCode(String code);

UserDetailDTO getUserDetailDTO(Integer userId);

GetUserWalletDetailResponse getUserWalletDetails(Integer userId);

GetUserReferalDetailsResponse getUserReferalDetails(Integer userId);

List<UserReferralDetail> getUserReferralDetailUserId(Integer userId, boolean referred);

List<User> getUsersByUserId(List<Integer> userId);

User getUserByMobileOrEmail(String mobile, String email, boolean enabled);

User getUserByMobile(String mobile);

}
