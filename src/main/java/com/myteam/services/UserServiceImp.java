package com.myteam.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.myteam.cache.CacheManager;
import com.myteam.cache.SystemPropertiesCache;
import com.myteam.dao.IUserDao;
import com.myteam.doc.UserAuthToken;
import com.myteam.dto.UserDetailDTO;
import com.myteam.entity.SystemProperties;
import com.myteam.entity.User;
import com.myteam.entity.UserReferralDetail;
import com.myteam.entity.Wallet;
import com.myteam.mao.service.IUserAuthTokenMao;
import com.myteam.respponse.GetUserReferalDetailsResponse;
import com.myteam.respponse.GetUserWalletDetailResponse;
import com.myteam.utils.CollectionUtils;
import com.myteam.utils.StringUtils;

@Service
public class UserServiceImp implements IUserService {
	
	  
    private static final Logger LOG = LoggerFactory.getLogger(UserServiceImp.class);

	@Autowired
	private IUserDao userDao;
	
	@Autowired
	private IUserAuthTokenMao userMao;
	
	@Autowired
	IPaymentService paymentService;
	
	@Transactional
	@Override
	public User saveUser(User user) {
		return userDao.saveUser(user);
	}

	@Override
    public UserAuthToken generateAuthToken(User user, String os) {
		UserAuthToken token = userMao.getUserAuthTokenByUserId(user.getId());
		if(token == null) {
			token = new UserAuthToken();
	        token.setEmail(user.getEmail());
	        token.setUserId(user.getId());
	        token.setKey(StringUtils.getRandom());
	        token.setPhoneNumber(user.getUsername());
	        token.setEmail(user.getEmail());
	        token.setOs(os);
	        token.setValid(true);
	        userMao.saveUserAuthToken(token);
		}
        return token;
    }

	@Override
	@Transactional(readOnly = true)
	public List<User> list() {
		return userDao.list();
	}

	@Override
	@Transactional()
	public User getUserByMobileOrEmail(String mobile, String email, boolean enabled) {
		return userDao.getUserByMobileOrEmail(mobile, email, enabled);
	}
	
	@Override
	@Transactional()
	public User getUserByMobile(String mobile) {
		return userDao.getUserByMobile(mobile);
	}
	
	@Override
	public UserAuthToken getAuthToken(String authToken) {
		return userMao.getUserAuthTokenByAuthToken(authToken);
	}
	
	@Override
	public void invalidateAuthToken(UserAuthToken authToken) {
		userMao.deleteAuthToken(authToken);
	}

	@Override
	@Transactional()
	public User getUserByUserId(Integer userId) {
		return userDao.getUserByUserId(userId);
	}
	
	@Override
	@Transactional()
	public List<User> getUsersByUserId(List<Integer> userId) {
		return userDao.getUsersByUserId(userId);
	}
	
	@Override
	@Transactional()
	public UserReferralDetail saveUserReferralDetail(UserReferralDetail userReferralDetail) {
		return userDao.saveUserReferralDetail(userReferralDetail);
	}
	
	@Override
	@Transactional()
	public UserReferralDetail getUserReferralDetailByCode(String code) {
		return userDao.getUserReferralDetailByCode(code);
	}
	
	@Override
	@Transactional()
	public List<UserReferralDetail> getUserReferralDetailUserId(Integer userId, boolean referred) {
		return userDao.getUserReferralDetailUserId(userId, referred);
	}
	
	@Override
	@Transactional()
	public UserDetailDTO getUserDetailDTO(Integer userId) {
		User user = getUserByUserId(userId);
		return new UserDetailDTO(user);
	}
	
	@Override
	@Transactional()
	public GetUserWalletDetailResponse getUserWalletDetails(Integer userId) {
		GetUserWalletDetailResponse response = new GetUserWalletDetailResponse();
		Wallet wallet = paymentService.getUserWallet(userId);
		response.setTotalAmount(wallet.getAmount());
		response.setDepositedAmount(wallet.getDepositedAmount());
		response.setBonus(wallet.getBonus());
		response.setWonAmount(wallet.getWonAmount());
		return response;
	}
	
	@Override
	@Transactional()
	public GetUserReferalDetailsResponse getUserReferalDetails(Integer userId) {
		GetUserReferalDetailsResponse response = new GetUserReferalDetailsResponse();
		List<UserReferralDetail> referal = getUserReferralDetailUserId(userId, true);
		if(CollectionUtils.isNotEmpty(referal)) {
			response.setReferalCode(referal.get(0).getCode());
		}
		List<UserReferralDetail> referred = getUserReferralDetailUserId(userId, false);
		if(CollectionUtils.isNotEmpty(referred)) {
			response.setReferredCount(referred.size());
		}
		response.setTotalWinners(Integer.parseInt(CacheManager.getInstance().getCache(SystemPropertiesCache.class).getSystemPropertiesByName(SystemPropertiesCache.POWERPLAY_RANDOM_NUMBRE)));
		return response;
	}
	
}
