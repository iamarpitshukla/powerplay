package com.myteam.services;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;

public interface IPowerPlayStartupService {

	void setApplicationContext(ApplicationContext applicationContext) throws BeansException;

	ApplicationContext getApplicationContext();

	void loadAll();

	void loadContestWiseDistribution();

	void loadSmsTemplates();

	void loadSystemProperties();

}
