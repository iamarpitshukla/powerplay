package com.myteam.services;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.myteam.dao.UserDetailsDao;
import com.myteam.entity.Roles;
import com.myteam.entity.User;
import com.myteam.security.MyTeamUser;

@Service
public class UserDetailService implements UserDetailsService{

	@Autowired
	  private UserDetailsDao userDetailsDao;

	  @Transactional(readOnly = true)
	  @Override
	  public UserDetails loadUserByUsername(String mobile) throws UsernameNotFoundException {
	    User user = userDetailsDao.findUserByUsername(mobile);
	    UserBuilder builder = null;
	    if (user != null) {
	      builder = org.springframework.security.core.userdetails.User.withUsername(mobile);
	      builder.disabled(!user.isEnabled());
	      builder.password(user.getPassword());
	      Set<Roles> roles = new HashSet<>();
	      Roles role = new Roles();
	      role.setRole("ROLE_USER");
	      roles.add(role);
	      user.setRoles(roles);
	      String[] authorities = roles.stream().map(a -> a.getRole()).toArray(String[]::new);
	          builder.authorities(authorities);
	    } else {
	      throw new UsernameNotFoundException("User not found.");
	    }
	    return new MyTeamUser(user);
	  }

}
