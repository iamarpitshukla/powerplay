package com.myteam.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.myteam.annotation.Cache;
import com.myteam.cache.CacheManager;
import com.myteam.cache.SystemPropertiesCache;
import com.myteam.dao.IPaymentDao;
import com.myteam.dto.UserWalletDTO;
import com.myteam.entity.Contests;
import com.myteam.entity.Payment;
import com.myteam.entity.Payment.PaymentStatus;
import com.myteam.entity.PaymentGateway;
import com.myteam.entity.PaymentTxn;
import com.myteam.entity.PaymentTxn.PaymentTxnStatus;
import com.myteam.entity.Wallet.WalletType;
import com.myteam.entity.User;
import com.myteam.entity.Wallet;
import com.myteam.entity.WalletHistory;
import com.myteam.exception.MyTeamException;
import com.myteam.payment.gateway.service.AbstractPaymentGatewayProvider.PaymentRedirectForm;
import com.myteam.payment.gateway.service.PaytmPaymentGatewayService;
import com.myteam.request.GetWalletHistoryRequest;
import com.myteam.request.PaymentRequest;
import com.myteam.respponse.GetUserWalletResponse;
import com.myteam.respponse.InitiatePaymentResponse;
import com.myteam.respponse.PaymentGatewayResponse;
import com.myteam.respponse.PaymentGatewayResponse.PGResponseStatus;
import com.myteam.respponse.PaytmTransactionResponse;
import com.myteam.utils.StringUtils;

@Service("paymentService")
public class PaymentServiceImpl implements IPaymentService{

    private static final Logger LOG = LoggerFactory.getLogger(PaymentServiceImpl.class);
    
    @Autowired
    IPaymentDao paymentDao;
    
    @Autowired
    PaytmPaymentGatewayService paytmGateWayService;
    
    @Autowired
    IUserService userService;

	@Override
	@Transactional
	public GetUserWalletResponse getWalletDetails(GetWalletHistoryRequest request, User user) throws MyTeamException{
		GetUserWalletResponse response = new GetUserWalletResponse();
		Wallet wallet = getUserWallet(user.getId());
		if(wallet == null) {
			throw new MyTeamException("No Wallet exists");
		}
		List<WalletHistory> walletHistories = getUserWalletHistory(wallet.getId(), request.getStart(), request.getMaxResults());
		List<UserWalletDTO> walletDTOs = new ArrayList<>();
		for (WalletHistory walletHistory : walletHistories) {
			UserWalletDTO userWalletDTO = new UserWalletDTO(walletHistory);
			walletDTOs.add(userWalletDTO);
		}
		response.setTransactions(walletDTOs);
		return response;
	}
	
	@Override
	@Transactional
	public Wallet saveWallet(Wallet wallet) {
		return paymentDao.saveWallet(wallet);
	}
	
	@Override
	@Transactional
	public WalletHistory saveWalletHistory(WalletHistory walletHistory) {
		return paymentDao.saveWalletHistory(walletHistory);
	}
	
	@Override
	@Transactional
	public Wallet getUserWallet(Integer userId) {
		return paymentDao.getUserWallet(userId);
	}
	
	@Override
	@Transactional
	public PaymentTxn getPaymentTxnByCode(String code) {
		return paymentDao.getPaymentTxnByCode(code);
	}
	
	@Override
	@Transactional
	public PaymentGateway getPaymentGatewaybyName(String name) {
		return paymentDao.getPaymentGatewayByName(name);
	}
	
	
	@Override
	@Transactional
	public List<WalletHistory> getUserWalletHistory(Integer walletId, Integer start, Integer maxResults) {
		return paymentDao.getUserWalletHistory(walletId, start, maxResults);
	}
	
	@Override
	@Transactional
	public Payment savePayment(Payment payment) {
		return paymentDao.savePayment(payment);
	}
	
	@Override
	@Transactional
	public PaymentTxn savePaymentTxn(PaymentTxn paymentTxn) {
		return paymentDao.savePaymentTxn(paymentTxn);
	}
	
	@Override
	@Transactional
	public InitiatePaymentResponse initatePayment(PaymentRequest request, User user) throws MyTeamException{
//		BigDecimal maxAllowedTransaction = new BigDecimal(CacheManager.getInstance().getCache(SystemPropertiesCache.class).getSystemPropertiesByName(SystemPropertiesCache.MAXIMUM_ALLOWED_TRANSACTION));
//		if(request.getAmount().compareTo(maxAllowedTransaction) > 0) {
//			throw new MyTeamException("Max amount limit exceeded");
//		}
		InitiatePaymentResponse initiatePaymentResponse = new InitiatePaymentResponse();
		Wallet wallet = getUserWallet(user.getId());
		if(wallet == null) {
			wallet = new Wallet();
			wallet.setUser(user);
			wallet = saveWallet(wallet);
		}
		Payment payment = new Payment();
		payment.setStatus(PaymentStatus.INIT.code());
		payment.setPaidAmount(request.getAmount());
		payment.setSource(request.getSource());
		payment.setUser(user);
		payment =  savePayment(payment);
		initiatePaymentResponse.setPayment(payment);
		PaymentGateway pg = getPaymentGatewaybyName(request.getPgName());
		initiatePaymentResponse.setPg(pg);
		return initiatePaymentResponse;
	}

	@Override
	@Transactional
	public InitiatePaymentResponse createPaymentTxn(InitiatePaymentResponse response) {
		PaymentTxn paymentTxn = new PaymentTxn();
		paymentTxn.setPayment(response.getPayment());
		paymentTxn.setCode(StringUtils.getRandom(3) + new Date().getTime());
		paymentTxn.setStatus(PaymentTxnStatus.INIT.code());
		paymentTxn.setSource(response.getPayment().getSource());
		paymentTxn.setPgName(response.getPg().getName());
		paymentTxn = savePaymentTxn(paymentTxn);
		response.setPaymentTxn(paymentTxn);
		return response;
	}
	
	@Override
	@Transactional
	public PaymentRedirectForm getPaymentRedirectForm(InitiatePaymentResponse initiatePaymentResponse, User user) throws Exception{
		paytmGateWayService.setPaymentGateway(initiatePaymentResponse.getPg());
		String callBackUrl = "/payment/paytmResponse";
		if(initiatePaymentResponse.isFromWeb()) {
			callBackUrl = "/payment/paytmResponseWeb";
		}
		return paytmGateWayService.getRedirectionForm(initiatePaymentResponse.getPaymentTxn().getCode(), initiatePaymentResponse.getPaymentTxn().getPayment().getPaidAmount(), user.getName(), user.getEmail(), user.getId(), null, user.getUsername(), callBackUrl, null, null, null, false);
	}
	
	@Transactional
    @Override
    public PaymentTxn validatePaytmPaymentRequest(PaytmTransactionResponse request)
            throws Exception {
		ObjectMapper oMapper = new ObjectMapper();
		oMapper.setSerializationInclusion(Include.NON_NULL);
		oMapper.setPropertyNamingStrategy(new CustomPropertyNamingStrategy());
		oMapper.enable(SerializationFeature.INDENT_OUTPUT);
		@SuppressWarnings("unchecked")
		Map<String, String> parameters = oMapper.convertValue(request, Map.class);
        PaymentGatewayResponse response = null;
        String txnCode = request.getORDERID();
        PaymentTxn paymentTxn = getPaymentTxnByCode(txnCode);
        if(paymentTxn == null) {
        	LOG.info("Payment txn null for txn code :{}", txnCode);
        	throw new Exception("Invalid payment txn id");
        }
        Payment payment = paymentTxn.getPayment();
        try {
			response = paytmGateWayService.validatePaytmPaymentGatewayResponse(request, parameters, payment.getPaidAmount());
        } catch (Exception e) {
			LOG.info("Exception while validating payment :{}", e);
            throw new Exception("Error while validating paytm payment gateway params",
                    e);
        }
        if (paymentTxn == null || !PaymentStatus.INIT.code().equals(paymentTxn.getStatus())) {
            throw new Exception("Invalid payment txn id");
        }
        paymentTxn.setPgTxnId(request.getTXNID());
        paymentTxn.setPgIncomingParams(request.toString());
        if (PGResponseStatus.SUCCESS.responseType().equals(response.getStatus())) {
            paymentTxn.setStatus(PaymentTxnStatus.SUCCESS.code());
            payment.setStatus(PaymentStatus.COMPLETED.code());
        } else if (PGResponseStatus.PENDING_VERIFICATION.responseType().equals(response.getStatus())) {
            paymentTxn.setStatus(PaymentTxnStatus.AUTH_PENDING.code());
            payment.setStatus(PaymentStatus.ON_HOLD.code());
        } else {
            paymentTxn.setStatus(PaymentTxnStatus.FAILED.code());
            payment.setStatus(PaymentStatus.FAILED.code());
        }
        paymentTxn = savePaymentTxn(paymentTxn);
        payment = savePayment(payment);
        if (paymentTxn.getStatus().equals(PaymentTxnStatus.SUCCESS.code())) {
        	updateWalletAndWalletHistory(payment);
        }
        return paymentTxn;
    }
	
	private void updateWalletAndWalletHistory(Payment payment) {
		Integer userId = payment.getUser().getId();
		Wallet wallet = getUserWallet(userId);
		if(PaymentStatus.COMPLETED.code().equalsIgnoreCase(payment.getStatus())) {
			wallet.setAmount(wallet.getAmount().add(payment.getPaidAmount()));
			wallet.setDepositedAmount(wallet.getDepositedAmount().add(payment.getPaidAmount()));
			wallet = saveWallet(wallet);
			createWalletHistory(wallet, WalletType.ADD_MONEY.code(), payment.getPaidAmount(), null);
		}
		
	}

	@Override
	@Transactional
	public void createWalletAndWalletHistory(User user) {
		user = userService.getUserByMobileOrEmail(user.getUsername(), user.getEmail(), true);
		Wallet wallet = getUserWallet(user.getId());
		if(wallet == null) {
			wallet = new Wallet();
			wallet.setAmount(new BigDecimal(100.00));
			wallet.setBonus(new BigDecimal(100.00));
			wallet.setUser(user);
			wallet.setWalletType(WalletType.DEFAULT.code());
			wallet = saveWallet(wallet);
			createWalletHistory(wallet, WalletType.JOINING_BONUS.code(), wallet.getAmount(), null);
		}
	}
	
	@Override
	@Transactional
	public Wallet getUpdatedWalletAfterPaymentTxn(String txnCode) {
		PaymentTxn txn = getPaymentTxnByCode(txnCode);
		Wallet wallet = getUserWallet(txn.getPayment().getUser().getId());
		return wallet;
	}
	
	@Override
	@Transactional
	public void createWalletHistory(Wallet wallet, String type, BigDecimal amount, Contests contests) {
		WalletHistory walletHistory = new WalletHistory();
		walletHistory.setWallet(wallet);
		if(WalletType.JOINED_CONTEST.code().equalsIgnoreCase(type)) {
			walletHistory.setRemarks("Debited due to joining a contest");
		}else if(WalletType.WINNING.code().equalsIgnoreCase(type)) {
			walletHistory.setRemarks("Credited due to winning a contest");
		}else if(WalletType.JOINING_BONUS.code().equalsIgnoreCase(type)) {
			walletHistory.setRemarks("Credited as joining bonus");
		}else if(WalletType.REFERRAL_BONUS.code().equalsIgnoreCase(type)) {
			walletHistory.setRemarks("Credited as referral bonus");
		}else if(WalletType.ADD_MONEY.code().equalsIgnoreCase(type)) {
			walletHistory.setRemarks("Added money");
		}
		walletHistory.setAmount(amount);
		walletHistory.setContest(contests);
		saveWalletHistory(walletHistory);
	}

}
