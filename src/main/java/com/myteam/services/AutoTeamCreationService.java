package com.myteam.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.myteam.entity.Contests;
import com.myteam.entity.Match;
import com.myteam.utils.DateUtils;

@Service("autoTeamCreationService")
public class AutoTeamCreationService {
	
	private static final Logger LOG = LoggerFactory.getLogger(AutoTeamCreationService.class);

	@Autowired
	IPowerPlayService powerPlayService;

	@Transactional
	public void createAutoTeams() {
		// get matches to create auto teams
		List<Match> matchesToCreateTeam = powerPlayService.getMatchesToCreateAutoTeams();
		for (Match match : matchesToCreateTeam) {
			LOG.info("Processing for match :{}",match.getId());
			// get matche wise contests to create auto teams
			List<Contests> contestsToCreateAutoTeams = powerPlayService.getAutoTeamCreateContestsForMatch(match.getId());
			for (Contests contests : contestsToCreateAutoTeams) {
				LOG.info("Processing for contest :{}",contests.getId());
				createAutoTeam(contests.getId(), match);
			}
		}
	}

	private void createAutoTeam(Integer contestId, Match match) {
		LOG.info("Processing for match :{} and contest :{}", match.getId(), contestId);
		Contests contest = powerPlayService.getContestsById(contestId);
		Integer totalCount = contest.getTotalCount();
		int playingCount = contest.getPlayingCount();
		Date contestEndTime = match.getStartDate();
		Date currTime = new Date();
		LOG.info("Processing at time :{}", currTime);
		long timediff = DateUtils.getDateDiffInMin(contestEndTime, currTime);
		long teamsToCreate = (totalCount - playingCount) / timediff;
		playingCount += teamsToCreate;
		LOG.info("creating team :{}", teamsToCreate);
		contest.setPlayingCount(playingCount);
		powerPlayService.saveContests(contest);

	}
}
