package com.myteam.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.myteam.cache.CacheManager;
import com.myteam.cache.SmsTemplateCache;
import com.myteam.communication.SmsTemplateVO;
import com.myteam.utils.SMSSenderUtils;

@Service("smsService")
public class SMSServiceImpl implements ISMSService{

    private static final Logger         LOG                   = LoggerFactory.getLogger(SMSServiceImpl.class);
    
    @Autowired
    SMSSenderUtils smsSender;
    
    @Override
    @Async
    public void sendOTP(String mobile, String otp){
    	LOG.info("Sending otp sms to user :{}", mobile);
    	SmsTemplateVO template = CacheManager.getInstance().getCache(SmsTemplateCache.class).getTemplateByNameAndLang("userOTPSMS", "EN");
    	LOG.info("Template found is :{}", template.getName());
    	template.setOtp(otp);
    	smsSender.sendSMS(template, mobile);
    }
    
    @Override
    @Async
    public void sendAppLink(String mobile, String appLink){
    	LOG.info("Sending otp sms to user :{}", mobile);
    	SmsTemplateVO template = CacheManager.getInstance().getCache(SmsTemplateCache.class).getTemplateByNameAndLang("appLinkSMS", "EN");
    	LOG.info("Template found is :{}", template.getName());
    	template.setAppLink(appLink);
    	smsSender.sendSMS(template, mobile);
    }

}
