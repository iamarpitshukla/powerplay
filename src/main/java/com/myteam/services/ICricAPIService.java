package com.myteam.services;

import java.util.List;

import com.myteam.dto.MatchDTO;
import com.myteam.entity.User;
import com.myteam.exception.MyTeamException;
import com.myteam.request.CreateTeamPlayersRequest;
import com.myteam.request.GetFutureMatchRequest;
import com.myteam.request.JoinContestRequest;
import com.myteam.respponse.CreateTeamPlayersResponse;
import com.myteam.respponse.GetMatchDetailsResponse;
import com.myteam.respponse.GetMatchStatsResponse;
import com.myteam.respponse.GetPlayerStatsResponse;
import com.myteam.respponse.JoinContestResponse;

public interface ICricAPIService {

	GetMatchDetailsResponse getPlayersForMatch(Integer matchId);

	List<MatchDTO> getFutureMatches();

	GetMatchStatsResponse getMatchStats(Integer matchId);

	GetPlayerStatsResponse getPlayerStats(Integer playerId);

	void processFutureMatches();

	List<MatchDTO> getMatches(GetFutureMatchRequest request);

	void populateSquad();

	GetMatchDetailsResponse getMatchDetails(Integer matchId) throws MyTeamException;

	void updateScores();

	CreateTeamPlayersResponse createTeam(CreateTeamPlayersRequest request, User user) throws MyTeamException;

	void updateWinningAmountUsingRank();

	JoinContestResponse joinContest(JoinContestRequest request, User user) throws MyTeamException;

	void updateMatchPlayerFantasyPoints();

	void createContests(List<Integer> matchIds);

	void updatePlayingEleven();

	void createMegaContests(List<Integer> matchIds);

}
