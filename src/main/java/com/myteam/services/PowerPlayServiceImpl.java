package com.myteam.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.transaction.Transactional;

import org.hibernate.query.NativeQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.myteam.cache.CacheManager;
import com.myteam.cache.ContestCache;
import com.myteam.constants.Constants;
import com.myteam.dao.IPowerPlayDao;
import com.myteam.doc.PlayersDoc;
import com.myteam.doc.UserAuthToken;
import com.myteam.dto.ContestDetailDTO;
import com.myteam.dto.FantasyPointDetails;
import com.myteam.dto.JoinedContestDTO;
import com.myteam.dto.OtpVerificationRequest;
import com.myteam.dto.PlayersDTO;
import com.myteam.dto.SpecificTeamPointDTO;
import com.myteam.dto.UserDTO;
import com.myteam.dto.UserMatchTeamDTO;
import com.myteam.dto.UserTeamPointDetailDTO;
import com.myteam.dto.WinningDistributionDTO;
import com.myteam.entity.ContestWinningDistribution;
import com.myteam.entity.Contests;
import com.myteam.entity.Match;
import com.myteam.entity.MatchData;
import com.myteam.entity.SmsTemplate;
import com.myteam.entity.SystemProperties;
import com.myteam.entity.User;
import com.myteam.entity.UserMatchTeamContestMapping;
import com.myteam.entity.UserMatchTeamMapping;
import com.myteam.entity.UserReferralDetail;
import com.myteam.entity.Wallet;
import com.myteam.entity.Wallet.WalletType;
import com.myteam.exception.MyTeamException;
import com.myteam.exception.MyTeamException.ErrorCode;
import com.myteam.mao.service.IPlayersMao;
import com.myteam.request.GetAllTeamsPointsRequest;
import com.myteam.request.GetAppLinkRequest;
import com.myteam.request.GetFutureMatchRequest;
import com.myteam.request.GetJoinedContestRequest;
import com.myteam.request.GetMatchWiseUserTeamRequest;
import com.myteam.request.GetMatchWiseUserTeamResponse;
import com.myteam.request.GetSpecificTeamPointsRequest;
import com.myteam.request.GetWinningBreakUpRequest;
import com.myteam.request.UpdatePasswordRequest;
import com.myteam.respponse.GetAllTeamsPointsResponse;
import com.myteam.respponse.GetSpecificTeamPointsResponse;
import com.myteam.respponse.GetTeamAndJoinedContestCountResponse;
import com.myteam.respponse.GetWinningBreakUpResponse;
import com.myteam.utils.CollectionUtils;
import com.myteam.utils.StringUtils;

@Service("powerPlayService")
public class PowerPlayServiceImpl implements IPowerPlayService {

	private static final Logger LOG = LoggerFactory.getLogger(PowerPlayServiceImpl.class);

	@Autowired
	IPowerPlayDao powerPlayDao;
	
	@Autowired
	IPlayersMao playerMao;
	
	@Autowired
	IPaymentService paymentService;
	
	@Autowired
	IUserService userService;
	
	@Autowired
	ISMSService smsService;
	
	@Override
	@Transactional
	public List<Match> getMatches(GetFutureMatchRequest request) {
		if (StringUtils.isNotEmpty(request.getAuthToken())) {
			UserAuthToken token = userService.getAuthToken(request.getAuthToken());
			List<UserMatchTeamContestMapping> teams = powerPlayDao.getUserMatches(request, token.getUserId());
			List<Match> matches = new ArrayList<>();
			if (CollectionUtils.isNotEmpty(teams)) {
				for (UserMatchTeamContestMapping teamMapping : teams) {
					matches.add(teamMapping.getMatch());
				}
			}
			return matches;
		} else {
			return powerPlayDao.getMatches(request);
		}
	}
	
	@Override
	@Transactional
	public Map<Integer, Integer> getContestsNumberForMatches(List<Integer> matchIds, String authToken){
		UserAuthToken token = userService.getAuthToken(authToken);
		Map<Integer, Integer> matchIdToNumberOfContest = new HashMap<>();
		List<UserMatchTeamContestMapping> teams = powerPlayDao.getUserMatchTeamMappingsByMatchIds(matchIds, token.getUserId());
		if (CollectionUtils.isNotEmpty(teams)) {
			for (UserMatchTeamContestMapping teamMapping : teams) {
				if(matchIdToNumberOfContest.get(teamMapping.getMatch().getId()) != null) {
					matchIdToNumberOfContest.put(teamMapping.getMatch().getId(),matchIdToNumberOfContest.get(teamMapping.getMatch().getId())+ 1);
				}else {
					matchIdToNumberOfContest.put(teamMapping.getMatch().getId(),1);
				}
			}
		}
		return matchIdToNumberOfContest;
		
	}

	@Override
	@Transactional
	public Match saveMatch(Match match) {
		return powerPlayDao.saveMatch(match);
	}
	
	@Override
	@Transactional
	public List<Match> getMatchesByUniqueIds(List<Integer> uniqueIds) {
		return powerPlayDao.getMatchesByUniqueIds(uniqueIds);
	}
	
	@Override
	@Transactional
	public Match getMatcheByMatchId(Integer matchId) {
		return powerPlayDao.getMatcheByMatchId(matchId);
	}
	
	@Override
	@Transactional
	public List<Match> getMatchesWithoutSquad(GetFutureMatchRequest request) {
		return powerPlayDao.getMatchesWithoutSquad(request);
	}
	
	@Override
	@Transactional
	public List<Contests> getContestsForMatch(Integer matchId) {
		return powerPlayDao.getContestsForMatch(matchId);
	}
	
	@Override
	@Transactional
	public List<Contests> getContestsForMatchIds(List<Integer> matchIds) {
		return powerPlayDao.getContestsForMatchIds(matchIds);
	}
	
	
	@Override
	@Transactional
	public List<Contests> getContestsFromStartDate(Date  startDate) {
		return powerPlayDao.getContestsFromStartDate(startDate);
	}
	
	@Override
	@Transactional
	public List<SystemProperties> getSystemProperties() {
		return powerPlayDao.getSystemProperties();
	}
	
	@Override
	@Transactional
	public Contests getContestsById(Integer contestId) {
		return powerPlayDao.getContestsById(contestId);
	}
	
	@Override
	@Transactional
	public Match getMatchById(Integer matchId) {
		return powerPlayDao.getMatchById(matchId);
	}
	
	@Override
	@Transactional
	public List<Match> getCurrentlyRunningMatches(){
		return powerPlayDao.getCurrentlyRunningMatches();
	}
	
	@Override
	@Transactional
	public MatchData getMatchDataByMatchId(Integer matchId) {
		return powerPlayDao.getMatchDataByMatchId(matchId);
	}
	
	@Override
	@Transactional
	public void saveMatchData(MatchData matchData) {
		 powerPlayDao.saveMatchData(matchData);
	}
	
	@Override
	@Transactional
	public List<Contests> getAllOverContestsOfAMatch(Integer matchId) {
		return powerPlayDao.getAllOverContests(matchId);
	}
	
	@Override
	public List<Contests> getAllNonReviewedContests() {
		return powerPlayDao.getAllNonReviewedContests();
	}
	
	@Override
	@Transactional
	public List<Contests> getAllCurrentlyRunningContests(Integer matchId, Integer endOver, Integer inning) {
		return powerPlayDao.getAllCurrentlyWorkingContests(matchId, endOver, inning);
	}
	
	@Override
	@Transactional
	public Contests saveContests(Contests contests) {
		 return powerPlayDao.saveContests(contests);
	}
	
	@Override
	@Transactional
	public ContestWinningDistribution saveContestWinningDistribution(ContestWinningDistribution contestWinningDistribution) {
		 return powerPlayDao.saveContestWinningDistribution(contestWinningDistribution);
	}
	
	@Override
	@Transactional
	public List<Contests> getTimeCrossedContestsForMatchAndEndOversAndInning(Integer matchId, Integer endOver, Integer inning) {
		return powerPlayDao.getTimeCrossedContestsForMatchAndEndOversAndInning(matchId, endOver, inning);
	}
	
	@Override
	@Transactional
	public void saveUserMatchContestPlayersMapping(UserMatchTeamContestMapping userMatchContestPlayersMapping) {
		 powerPlayDao.saveUserMatchContestPlayersMapping(userMatchContestPlayersMapping);
	}
	
	@Override
	@Transactional
	public UserMatchTeamMapping saveUserMatchTeamMapping(UserMatchTeamMapping userMatchTeamMapping) {
		 return powerPlayDao.saveUserMatchTeamMapping(userMatchTeamMapping);
	}
	
	@Override
	@Transactional
	public UserMatchTeamContestMapping getUserMatchContestPlayersMappingByCode(String code) {
		return powerPlayDao.getUserMatchContestPlayersMappingByCode(code);
	}
	
	@Override
	@Transactional
	public UserMatchTeamMapping getUserMatchTeamMappingById(Integer id) {
		return powerPlayDao.getUserMatchTeamMappingById(id);
	}
	
	@Override
	@Transactional
	public List<UserMatchTeamMapping> getUserMatchTeamMappingByUserIdAndMatchId(Integer userId, Integer matchId) {
		return powerPlayDao.getUserMatchTeamMappingByUserIdAndMatchId(userId,matchId);
	}
	
	@Override
	@Transactional
	public GetAllTeamsPointsResponse getUserMatchContestPlayersMappingByContestId(GetAllTeamsPointsRequest request,
			User user) throws MyTeamException {
		GetAllTeamsPointsResponse response = new GetAllTeamsPointsResponse();
		Contests contests = getContestsById(request.getContestId());
		if(contests == null) {
			throw new MyTeamException("No such contest exists");
		}
		LOG.info("User id to fetch teams is :{}", user.getId());
		List<UserMatchTeamContestMapping> selfDetail = getUserMatchContestPlayersMappingByContestId(
				request.getContestId(), request.getStart(), request.getMaxResults(), user.getId(), true, null);
		if (CollectionUtils.isEmpty(selfDetail) && request.getStart() == 0) {
			throw new MyTeamException("You are not part of this contest");
		}
		List<UserTeamPointDetailDTO> selfDTOs = new ArrayList<UserTeamPointDetailDTO>();
		for (UserMatchTeamContestMapping self : selfDetail) {
			UserTeamPointDetailDTO selfDTO = new UserTeamPointDetailDTO(self);
			selfDTO.setSelfTeam(true);
			selfDTOs.add(selfDTO);
		}
		List<UserMatchTeamContestMapping> othersDetail = getUserMatchContestPlayersMappingByContestId(
				request.getContestId(), request.getStart(), request.getMaxResults(), user.getId(), false, null);
		if (CollectionUtils.isNotEmpty(othersDetail)) {
			List<UserTeamPointDetailDTO> otherDTOs = new ArrayList<UserTeamPointDetailDTO>();
			for (UserMatchTeamContestMapping other : othersDetail) {
				UserTeamPointDetailDTO otherDTO = new UserTeamPointDetailDTO(other);
				otherDTO.setSelfTeam(false);
				otherDTOs.add(otherDTO);
			}
			if(CollectionUtils.isNotEmpty(selfDTOs)) {
				selfDTOs.addAll(otherDTOs);
			}
		}
		response.setTeams(selfDTOs);
		return response;
	}
	
	@Override
	@Transactional
	public GetMatchWiseUserTeamResponse getTeamByMatchId(GetMatchWiseUserTeamRequest request,
			User user) throws MyTeamException {
		GetMatchWiseUserTeamResponse response = new GetMatchWiseUserTeamResponse();
		List<UserMatchTeamMapping> userMatchTeamMappings = getUserMatchTeamMappingByUserIdAndMatchId(user.getId(), request.getMatchId());
		if(CollectionUtils.isEmpty(userMatchTeamMappings)) {
			throw new MyTeamException("No Team Found");
		}
		List<UserMatchTeamDTO> teams = new ArrayList<>();
		for(UserMatchTeamMapping userMatchTeamMapping: userMatchTeamMappings) {
			UserMatchTeamDTO team = new UserMatchTeamDTO();
			List<Integer> players = StringUtils.getListFromString(userMatchTeamMapping.getPlayers(), ",");
			List<PlayersDoc> playersDocs = playerMao.getPlayerDocsByPIds(players);
			team.setUserTeamMappingId(userMatchTeamMapping.getId());
			List<PlayersDTO> playersDTOs = new ArrayList<>();
			for(PlayersDoc doc : playersDocs) {
				PlayersDTO playersDTO = new PlayersDTO(doc);
				if(doc.getPid() == Integer.parseInt(userMatchTeamMapping.getSuperPlayer())) {
					playersDTO.setSuperPlayer(true);
				}
				playersDTOs.add(playersDTO);
			}
			team.setTeamNumber(userMatchTeamMapping.getTeamNumber());
			team.setUserName(user.getName());
			team.setPlayers(playersDTOs);
			teams.add(team);
		}
		response.setTeams(teams);
		return response;
	}
	
	@Override
	@Transactional
	public List<UserMatchTeamContestMapping> getUserMatchContestPlayersMappingByContestId(Integer contestId,
			Integer start, Integer maxResults, Integer userId, Boolean include, Integer rank) {
		List<UserMatchTeamContestMapping> userMatchTeamContestMappings = powerPlayDao.getUserMatchContestPlayersMappingByContestId(contestId, start, maxResults, userId, include, rank);
		if(CollectionUtils.isNotEmpty(userMatchTeamContestMappings)) {
			for(UserMatchTeamContestMapping userMatchTeamContestMapping: userMatchTeamContestMappings) {
				userMatchTeamContestMapping.getUserMatchTeamMapping().getMatch();
			}
		}
		return userMatchTeamContestMappings;
	}
	
	@Override
	@Transactional
	public GetWinningBreakUpResponse getWinningBreakUp(GetWinningBreakUpRequest request) throws MyTeamException{
		GetWinningBreakUpResponse response = new GetWinningBreakUpResponse();
		ContestDetailDTO contests = CacheManager.getInstance().getCache(ContestCache.class).getContestWiseDistributionById(request.getContestId());
		if(contests == null) {
			Contests contest = getContestsById(request.getContestId());
			if(contest == null) {
				throw new MyTeamException("No Such Contest");
			}
			contests = new ContestDetailDTO();
			contests.setContestDistribution(contest.getContestWinnerRank());
			contests.setPrizePool(contest.getPrizePool());
		}
		response.setPrizePool(contests.getPrizePool());
		List<ContestWinningDistribution> winningDistributions = contests.getContestDistribution();
		List<WinningDistributionDTO> winningDistributionDTOs = new ArrayList<>();
		if(CollectionUtils.isNotEmpty(winningDistributions)) {
			for(ContestWinningDistribution winningDistribution: winningDistributions) {
				WinningDistributionDTO winningDistributionDTO = new WinningDistributionDTO();
				winningDistributionDTO.setWinningAmount(winningDistribution.getWinningAmount());
				if(winningDistribution.getStartRank() == winningDistribution.getEndRank()) {
					winningDistributionDTO.setRankString(Constants.RANK_STRING +" " +winningDistribution.getStartRank());
				}else {
					winningDistributionDTO.setRankString(Constants.RANK_STRING +" " +winningDistribution.getStartRank() +"-"+winningDistribution.getEndRank());
				}
				winningDistributionDTOs.add(winningDistributionDTO);
			}
			response.setDistributionDTO(winningDistributionDTOs);
		}
		return response;
	}
	
	@Override
	@Transactional
	public GetSpecificTeamPointsResponse getSpecificTeamPointsResponse(GetSpecificTeamPointsRequest request) throws MyTeamException{
		GetSpecificTeamPointsResponse response = new GetSpecificTeamPointsResponse();
		UserMatchTeamContestMapping umContestPlayersMapping  = getUserMatchContestPlayersMappingByCode(request.getUcpmCode());
		if(umContestPlayersMapping == null) {
			throw new MyTeamException("No user contest found");
		}
		Contests contests = umContestPlayersMapping.getContests();
		String pointsString = contests.getPoints();
		Map<Integer, Double> contestPointMap =  new HashMap<>();
		if(StringUtils.isNotEmpty(pointsString)) {
			FantasyPointDetails points = new Gson().fromJson(pointsString, FantasyPointDetails.class);
			contestPointMap =  points.getPidToFantasyPointMap();
		}
		LOG.info("Points map is :{}",contestPointMap);
		UserMatchTeamMapping userMatchTeamMapping = umContestPlayersMapping.getUserMatchTeamMapping();
		List<Integer> playerIds = StringUtils.getListFromString(userMatchTeamMapping.getPlayers(),",");
		List<PlayersDoc> docs = playerMao.getPlayerDocsByPIds(playerIds);
		Map<Integer, PlayersDoc> pidToPlayerDoc = new HashMap<>();
		if(CollectionUtils.isNotEmpty(docs)) {
			for(PlayersDoc doc : docs) {
				pidToPlayerDoc.put(doc.getPid(), doc);
			}
		}
		List<SpecificTeamPointDTO> teamDetails = new ArrayList<>();
		for (Integer playerId : playerIds) {
			SpecificTeamPointDTO teamPointDTO = new SpecificTeamPointDTO();
			PlayersDTO player = new PlayersDTO(pidToPlayerDoc.get(playerId));
			teamPointDTO.setPlayer(player);
			if (playerId.equals(Integer.parseInt(userMatchTeamMapping.getSuperPlayer()))) {
				player.setSuperPlayer(true);
				LOG.info("Processing super player :{}", playerId);
				if(contestPointMap.get(playerId)!= null) {
					teamPointDTO.setPoints(contestPointMap.get(playerId)* Constants.SUPER_PLAYER_BOOST);
				}else {
					teamPointDTO.setPoints(0.0);
				}
			} else {
				teamPointDTO.setPoints(contestPointMap.get(playerId) != null ? contestPointMap.get(playerId) : 0.0);
			}
			teamDetails.add(teamPointDTO);
		}
		response.setTeamNumber(userMatchTeamMapping.getTeamNumber());
		response.setTeamDetails(teamDetails);
		return response;
	}
	
	@Override
	@Transactional
	public User createNewUser(UserDTO userDto) throws Exception{
		isValidRequest(userDto);
		User user = new User();
		String encoded = new BCryptPasswordEncoder().encode(userDto.getPassword());
		user.setPassword(encoded);
		user.setUsername(userDto.getUsername());
		user.setEmail(userDto.getEmail());
		user.setName(userDto.getName());
		user.setSource(userDto.getOs());
		user.setEnabled(false);
		String otp = String.valueOf(getOtp());
		user.setOtp(otp);
		user = userService.saveUser(user);
		return user;
	}
	
	private int getOtp() {
		Random rand = new Random();
        int randMax = 999999;
        int randMin = 100001;
        int OTP = randMin + rand.nextInt((randMax - randMin) + 1);
        return OTP;
	}
	
	@Override
	@Transactional
	public User verifyNewUser(OtpVerificationRequest request) throws MyTeamException {
		validateRequest(request);
		User user = userService.getUserByMobileOrEmail(request.getMobile(), null, false);
		if(request.getOtp().equalsIgnoreCase(user.getOtp())) {
			user.setEnabled(true);
			user = userService.saveUser(user);
			User referredBy = null;
			if(StringUtils.isNotEmpty(request.getReferralCode())) {
				UserReferralDetail referralDetail = userService.getUserReferralDetailByCode(request.getReferralCode());
				if(referralDetail == null) {
					throw new MyTeamException("No such referral code");
				}
				if(referralDetail.getUsedCount() == referralDetail.getMaxAllowedCount()) {
					throw new MyTeamException("This referral code cant be used now, It has been reached its limit");
				}
				referredBy = referralDetail.getUser();
				Integer updatedUsedCount = referralDetail.getUsedCount()+1;
				referralDetail.setUsedCount(updatedUsedCount);
				userService.saveUserReferralDetail(referralDetail);
				processRefferalBonus(referredBy);
			}
			createReferralDetails(user, referredBy);
			paymentService.createWalletAndWalletHistory(user);
		}else {
			throw new MyTeamException("Otp is not valid");
		}
		return user;
	}
	
	private void validateRequest(OtpVerificationRequest request) throws MyTeamException {
		User user = userService.getUserByMobileOrEmail(request.getMobile(), null, false);
		if(!request.getOtp().equalsIgnoreCase(user.getOtp())) {
			throw new MyTeamException("Otp is not valid");
		}
		if(StringUtils.isNotEmpty(request.getReferralCode())) {
			UserReferralDetail referralDetail = userService.getUserReferralDetailByCode(request.getReferralCode());
			if(referralDetail == null) {
				throw new MyTeamException("No such referral code");
			}
			if(referralDetail.getUsedCount() == referralDetail.getMaxAllowedCount()) {
				throw new MyTeamException("This referral code cant be used now, It has been reached its limit");
			}
		}
	}

	private void processRefferalBonus(User referredBy) {
		Wallet wallet = paymentService.getUserWallet(referredBy.getId());
		wallet.setAmount(wallet.getAmount().add(Constants.REFERRAL_BONUS));
		wallet.setBonus(wallet.getBonus().add(Constants.REFERRAL_BONUS));
		wallet = paymentService.saveWallet(wallet);
		paymentService.createWalletHistory(wallet,WalletType.REFERRAL_BONUS.code(), Constants.REFERRAL_BONUS, null);
	}

	@Override
	public User updatePassword(UpdatePasswordRequest request) throws MyTeamException {
		User user = userService.getUserByMobileOrEmail(request.getMobile(), null, true);
		if(!request.getOtp().equalsIgnoreCase(user.getOtp())) {
			throw new MyTeamException("Otp is not correct");
		}
		if(request.getPassword().equalsIgnoreCase(request.getRepeatPassword())) {
			String encoded = new BCryptPasswordEncoder().encode(request.getPassword());
			user.setPassword(encoded);
			user.setEnabled(true);
			user = userService.saveUser(user);
		}else {
			throw new MyTeamException("Both password not matching");
		}
		return user;
	}
	
	@Override
	public void sendOtp(OtpVerificationRequest request) throws MyTeamException {
		User user = userService.getUserByMobile(request.getMobile());
		if(user ==null) {
			throw new MyTeamException("No user with this detail");
		}
		String otp = String.valueOf(getOtp());
		user.setOtp(otp);
		user = userService.saveUser(user);
		try {
			smsService.sendOTP(request.getMobile(), otp);
		}catch(Exception e) {
			LOG.error("Error while sending sms :{}",e);
		}
	}
	
	@Override
	public void sendAppLink(GetAppLinkRequest request) throws MyTeamException {
		try {
			String appLink = getAppLink(request.getOs());
			smsService.sendAppLink(request.getMobile(), appLink);
		}catch(Exception e) {
			LOG.error("Error while sending sms :{}",e);
		}
	}
	
	private String getAppLink(String os) throws MyTeamException {
		String appLink = "http://13.127.217.102:8000/file/apk/";
		if(os.equals("android")) {
			appLink =appLink+ "Powerplay_Android.apk";
		}else if (os.equals("ios")) {
			appLink =appLink+ "Powerplay_Ios.apk";
		}else {
			throw new MyTeamException("No os found");
		}
		return appLink;
	}
	
	private void createReferralDetails(User user, User referredBy) {
		user = userService.getUserByMobileOrEmail(user.getUsername(), user.getEmail(), true);
		UserReferralDetail userReferralDetail = new UserReferralDetail();
		userReferralDetail.setUser(user);
		userReferralDetail.setReferredBy(referredBy);
		userReferralDetail.setMaxAllowedCount(100);
		userReferralDetail.setUsedCount(0);
		userReferralDetail.setCode(StringUtils.getRandom(6));
		userService.saveUserReferralDetail(userReferralDetail);
	}
	
	private void isValidRequest(UserDTO userDto) throws Exception {
		if(userDto.getUsername().trim().length()!= 10 || userDto.getUsername().contains(" ")) {
			throw new MyTeamException(ErrorCode.MOBILE_NUMBER_INVALID);
		}
		if(StringUtils.isEmpty(userDto.getPassword())) {
			throw new MyTeamException(ErrorCode.PASSWORD_EMPLTY);
		}
		if (!userDto.getPassword().equals(userDto.getRepeatPassword())) {
			throw new MyTeamException(ErrorCode.PASSWORD_NOT_MATCH);
		}
		if(StringUtils.isEmpty(userDto.getName())) {
			throw new MyTeamException(ErrorCode.NAME_CANT_BE_EMPTY);
		}
	}
	
	@Override
	@Transactional
	public List<SmsTemplate> getSmsTemplates() {
		return powerPlayDao.getSmsTemplates();
	}
	
	@Override
	@Transactional
	public List<JoinedContestDTO> getJoinedContests(GetJoinedContestRequest request, Integer userId) throws MyTeamException{
		List<UserMatchTeamContestMapping> teamMappings = powerPlayDao.getJoinedContests(request.getMatchId(),userId);
		List<JoinedContestDTO> joinedContests = new ArrayList<JoinedContestDTO>();
		if(CollectionUtils.isNotEmpty(teamMappings)) {
			for(UserMatchTeamContestMapping teamContestMapping: teamMappings) {
				JoinedContestDTO joinedContestDTO = new JoinedContestDTO();
				joinedContestDTO.setContestId(teamContestMapping.getContests().getId());
				joinedContestDTO.setEntryFees(teamContestMapping.getContests().getEntryFees());
				joinedContestDTO.setPoints(teamContestMapping.getPoints());
				joinedContestDTO.setRank(teamContestMapping.getRank());
				joinedContestDTO.setTeamNumber(teamContestMapping.getUserMatchTeamMapping().getTeamNumber());
				joinedContestDTO.setTotalPrize(teamContestMapping.getContests().getPrizePool());
				joinedContestDTO.setWinningAmount(teamContestMapping.getWinningAmount());
				joinedContestDTO.setTotalWinners(teamContestMapping.getContests().getWinnerCount());
				joinedContestDTO.setStatus(teamContestMapping.getContests().getContestStatus());
				joinedContests.add(joinedContestDTO);
			}
		}
		return joinedContests;
	}
	
	@Override
	@Transactional
	public GetTeamAndJoinedContestCountResponse getTeamAndJoinedContestCountResponse(GetJoinedContestRequest request, Integer userId) throws MyTeamException{
		GetTeamAndJoinedContestCountResponse res = new GetTeamAndJoinedContestCountResponse();
		List<UserMatchTeamMapping> teams = getUserMatchTeamMappingByUserIdAndMatchId(userId, request.getMatchId());
		if(CollectionUtils.isNotEmpty(teams)) {
			res.setTeamCount(teams.size());
		}
		List<UserMatchTeamContestMapping> joinedContests = powerPlayDao.getJoinedContests(request.getMatchId(), userId);
		if(CollectionUtils.isNotEmpty(joinedContests)) {
			res.setContestCount(joinedContests.size());
		}
		return res;
	}
	
	@Override
	@Transactional
	public void updateUMTCMWinningAmountByIds(Integer amount, List<Integer> ids) {
		 powerPlayDao.updateUMTCMWinningAmountByIds(amount, ids);
	}
	
	@Override
	@Transactional
	public void updateWinningAmountInWallet(Integer amount,
			List<UserMatchTeamContestMapping> userMatchContestPlayersMappings) {
		Map<Integer, Integer> umctmIdToUserIdMap = new HashMap<>();
		for (UserMatchTeamContestMapping userMatchContestPlayersMapping : userMatchContestPlayersMappings) {
			umctmIdToUserIdMap.put(userMatchContestPlayersMapping.getId(), userMatchContestPlayersMapping.getUserId());
		}
		for (UserMatchTeamContestMapping userMatchContestPlayersMapping : userMatchContestPlayersMappings) {
			Wallet wallet = paymentService
					.getUserWallet(umctmIdToUserIdMap.get(userMatchContestPlayersMapping.getId()));
			wallet.setAmount(wallet.getAmount().add(new BigDecimal(amount)));
			wallet.setWonAmount(wallet.getWonAmount().add(new BigDecimal(amount)));
			wallet = paymentService.saveWallet(wallet);
			paymentService.createWalletHistory(wallet, WalletType.WINNING.code(), new BigDecimal(amount), null);
		}
	}
	
	@Override
	@Transactional
	public List<UserMatchTeamContestMapping> getContestWiseJoinedContests(Integer contestId, Integer userId) {
		return powerPlayDao.getContestWiseJoinedContests(contestId, userId);
	}
	
	@Override
	@Transactional
	public List<Match> getMatchesWithoutPlayingEleven() {
		return powerPlayDao.getMatchesWithoutPlayingEleven();
	}
	
	@Override
	@Transactional
	public List<Match> getMatchesToCreateAutoTeams() {
		return powerPlayDao.getMatchesToCreateAutoTeams();
	}
	
	@Override
	public List<Contests> getAutoTeamCreateContestsForMatch(Integer matchId) {
		return powerPlayDao.getAutoTeamCreateContestsForMatch(matchId);
	}
}
