package com.myteam.services;

import java.lang.reflect.Modifier;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.introspect.AnnotatedField;
import com.fasterxml.jackson.databind.introspect.AnnotatedMethod;

public class CustomPropertyNamingStrategy extends PropertyNamingStrategy{


	/**
	 * 
	 */
	private static final long serialVersionUID = 5355110670718877375L;

	@Override
	public String nameForField(MapperConfig<?> config, AnnotatedField field, String defaultName) {
		return convertForField(defaultName);
	}

	@Override
	public String nameForGetterMethod(MapperConfig<?> config, AnnotatedMethod method, String defaultName) {
		return convertForMethod(method, defaultName);
	}

	@Override
	public String nameForSetterMethod(MapperConfig<?> config, AnnotatedMethod method, String defaultName) {
		return convertForMethod(method, defaultName);
	}

	private String convertForField(String defaultName) {
		return defaultName;
	}

	private String convertForMethod(AnnotatedMethod method, String defaultName) {
		if (isGetter(method)) {
			return method.getName().substring(3);
		}
		if (isSetter(method)) {
			return method.getName().substring(3);
		}
		return defaultName;
	}

	private boolean isGetter(AnnotatedMethod method) {
		if (Modifier.isPublic(method.getModifiers()) && method.getGenericParameterTypes().length == 0) {
			if (method.getName().matches("^get[A-Z].*") && !method.getGenericType().equals(void.class))
				return true;
			if (method.getName().matches("^is[A-Z].*") && method.getGenericType().equals(boolean.class))
				return true;
		}
		return false;
	}

	private boolean isSetter(AnnotatedMethod method) {
		return Modifier.isPublic(method.getModifiers()) && method.getGenericType().equals(void.class)
				&& method.getGenericParameterTypes().length == 1 && method.getName().matches("^set[A-Z].*");
	}

}
