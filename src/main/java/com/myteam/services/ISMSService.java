package com.myteam.services;

public interface ISMSService {

	void sendOTP(String mobile, String otp);

	void sendAppLink(String mobile, String otp);

}
