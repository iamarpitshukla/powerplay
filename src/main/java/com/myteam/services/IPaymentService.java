package com.myteam.services;

import java.math.BigDecimal;
import java.util.List;

import com.myteam.entity.Contests;
import com.myteam.entity.Payment;
import com.myteam.entity.PaymentGateway;
import com.myteam.entity.PaymentTxn;
import com.myteam.entity.User;
import com.myteam.entity.Wallet;
import com.myteam.entity.WalletHistory;
import com.myteam.exception.MyTeamException;
import com.myteam.payment.gateway.service.AbstractPaymentGatewayProvider.PaymentRedirectForm;
import com.myteam.request.GetWalletHistoryRequest;
import com.myteam.request.PaymentRequest;
import com.myteam.respponse.GetUserWalletResponse;
import com.myteam.respponse.InitiatePaymentResponse;
import com.myteam.respponse.PaytmTransactionResponse;

public interface IPaymentService {

	GetUserWalletResponse getWalletDetails(GetWalletHistoryRequest request, User user) throws MyTeamException;

	Wallet saveWallet(Wallet wallet);

	WalletHistory saveWalletHistory(WalletHistory walletHistory);

	Wallet getUserWallet(Integer userId);

	PaymentTxn validatePaytmPaymentRequest(PaytmTransactionResponse request)
			throws Exception;

	PaymentTxn getPaymentTxnByCode(String code);

	Payment savePayment(Payment payment);

	PaymentTxn savePaymentTxn(PaymentTxn paymentTxn);

	InitiatePaymentResponse initatePayment(PaymentRequest request, User user) throws MyTeamException;

	PaymentRedirectForm getPaymentRedirectForm(InitiatePaymentResponse initiatePaymentResponse, User user) throws Exception;

	PaymentGateway getPaymentGatewaybyName(String name);

	InitiatePaymentResponse createPaymentTxn(InitiatePaymentResponse reponse);

	void createWalletAndWalletHistory(User user);

	void createWalletHistory(Wallet wallet, String type, BigDecimal amount, Contests contests);

	Wallet getUpdatedWalletAfterPaymentTxn(String txnCode);

	List<WalletHistory> getUserWalletHistory(Integer walletId, Integer start, Integer maxResults);

}
