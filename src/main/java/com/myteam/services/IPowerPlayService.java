package com.myteam.services;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.myteam.dto.JoinedContestDTO;
import com.myteam.dto.OtpVerificationRequest;
import com.myteam.dto.UserDTO;
import com.myteam.entity.ContestWinningDistribution;
import com.myteam.entity.Contests;
import com.myteam.entity.Match;
import com.myteam.entity.MatchData;
import com.myteam.entity.SmsTemplate;
import com.myteam.entity.SystemProperties;
import com.myteam.entity.User;
import com.myteam.entity.UserMatchTeamContestMapping;
import com.myteam.entity.UserMatchTeamMapping;
import com.myteam.exception.MyTeamException;
import com.myteam.request.GetAllTeamsPointsRequest;
import com.myteam.request.GetAppLinkRequest;
import com.myteam.request.GetFutureMatchRequest;
import com.myteam.request.GetJoinedContestRequest;
import com.myteam.request.GetMatchWiseUserTeamRequest;
import com.myteam.request.GetMatchWiseUserTeamResponse;
import com.myteam.request.GetSpecificTeamPointsRequest;
import com.myteam.request.GetWinningBreakUpRequest;
import com.myteam.request.UpdatePasswordRequest;
import com.myteam.respponse.GetAllTeamsPointsResponse;
import com.myteam.respponse.GetSpecificTeamPointsResponse;
import com.myteam.respponse.GetTeamAndJoinedContestCountResponse;
import com.myteam.respponse.GetWinningBreakUpResponse;

public interface IPowerPlayService {

	List<Match> getMatches(GetFutureMatchRequest request);

	Match saveMatch(Match match);

	List<Match> getMatchesByUniqueIds(List<Integer> uniqueIds);

	List<Match> getMatchesWithoutSquad(GetFutureMatchRequest request);

	Match getMatcheByMatchId(Integer uniqueId);

	List<Contests> getContestsForMatch(Integer matchId);

	List<Match> getCurrentlyRunningMatches();

	MatchData getMatchDataByMatchId(Integer matchId);

	void saveMatchData(MatchData matchData);

	Contests saveContests(Contests contests);

	List<Contests> getTimeCrossedContestsForMatchAndEndOversAndInning(Integer matchId, Integer endOver, Integer inning);

	List<Contests> getAllCurrentlyRunningContests(Integer matchId, Integer endOver, Integer inning);

	List<Contests> getAllOverContestsOfAMatch(Integer matchId);

	void saveUserMatchContestPlayersMapping(UserMatchTeamContestMapping userMatchContestPlayersMapping);

	UserMatchTeamContestMapping getUserMatchContestPlayersMappingByCode(String code);

	Contests getContestsById(Integer contestId);

	Match getMatchById(Integer matchId);

	GetAllTeamsPointsResponse getUserMatchContestPlayersMappingByContestId(GetAllTeamsPointsRequest request, User user)
			throws MyTeamException;

	List<UserMatchTeamContestMapping> getUserMatchContestPlayersMappingByContestId(Integer contestId, Integer start,
			Integer maxResults, Integer userId, Boolean include, Integer rank);

	List<Contests> getAllNonReviewedContests();

	GetWinningBreakUpResponse getWinningBreakUp(GetWinningBreakUpRequest request) throws MyTeamException;

	GetSpecificTeamPointsResponse getSpecificTeamPointsResponse(GetSpecificTeamPointsRequest request) throws MyTeamException;

	List<Contests> getContestsFromStartDate(Date startDate);

	UserMatchTeamMapping getUserMatchTeamMappingById(Integer id);

	UserMatchTeamMapping saveUserMatchTeamMapping(UserMatchTeamMapping userMatchTeamMapping);

	GetMatchWiseUserTeamResponse getTeamByMatchId(GetMatchWiseUserTeamRequest request, User user)
			throws MyTeamException;

	List<UserMatchTeamMapping> getUserMatchTeamMappingByUserIdAndMatchId(Integer userId, Integer matchId);

	User createNewUser(UserDTO userDto) throws  Exception;

	List<SmsTemplate> getSmsTemplates();

	List<JoinedContestDTO> getJoinedContests(GetJoinedContestRequest request, Integer userId) throws MyTeamException;

	Map<Integer, Integer> getContestsNumberForMatches(List<Integer> matchIds, String authToken);

	GetTeamAndJoinedContestCountResponse getTeamAndJoinedContestCountResponse(GetJoinedContestRequest request,
			Integer userId) throws MyTeamException;

	ContestWinningDistribution saveContestWinningDistribution(ContestWinningDistribution contestWinningDistribution);

	List<Contests> getContestsForMatchIds(List<Integer> matchIds);

	void updateUMTCMWinningAmountByIds(Integer amount, List<Integer> ids);

	void updateWinningAmountInWallet(Integer amount, List<UserMatchTeamContestMapping> userMatchContestPlayersMappings);

	List<SystemProperties> getSystemProperties();

	User verifyNewUser(OtpVerificationRequest request) throws MyTeamException;

	void sendOtp(OtpVerificationRequest request) throws MyTeamException;

	User updatePassword(UpdatePasswordRequest request) throws MyTeamException;

	void sendAppLink(GetAppLinkRequest request) throws MyTeamException;

	List<UserMatchTeamContestMapping> getContestWiseJoinedContests(Integer contestId, Integer userId);

	List<Match> getMatchesWithoutPlayingEleven();

	List<Match> getMatchesToCreateAutoTeams();

	List<Contests> getAutoTeamCreateContestsForMatch(Integer matchId);

}
