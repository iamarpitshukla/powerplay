package com.myteam.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.myteam.constants.Constants;
import com.myteam.doc.MatchConfigDetailDoc;
import com.myteam.doc.PlayersDoc;
import com.myteam.dto.BattingScoreDTO;
import com.myteam.dto.BowlingScoreDTO;
import com.myteam.dto.ContestDTO;
import com.myteam.dto.FantasyPointDetails;
import com.myteam.dto.FieldingScoreDTO;
import com.myteam.dto.MatchBattingDTO;
import com.myteam.dto.MatchBowlingDTO;
import com.myteam.dto.MatchDTO;
import com.myteam.dto.MatchFieldingDTO;
import com.myteam.dto.OverDetailsDTO;
import com.myteam.dto.PlayersDTO;
import com.myteam.dto.PlayingElevenDTO;
import com.myteam.dto.SquadDTO;
import com.myteam.dto.TeamDTO;
import com.myteam.entity.ContestWinningDistribution;
import com.myteam.entity.Contests;
import com.myteam.entity.Match;
import com.myteam.entity.MatchData;
import com.myteam.entity.User;
import com.myteam.entity.UserMatchTeamContestMapping;
import com.myteam.entity.UserMatchTeamMapping;
import com.myteam.entity.Wallet;
import com.myteam.entity.Wallet.WalletType;
import com.myteam.exception.MyTeamException;
import com.myteam.exception.MyTeamException.ErrorCode;
import com.myteam.mao.service.IMatchConfigMao;
import com.myteam.mao.service.IPlayersMao;
import com.myteam.request.CreateTeamPlayersRequest;
import com.myteam.request.GetFutureMatchRequest;
import com.myteam.request.GetMatchDetailRequest;
import com.myteam.request.GetMatchStatsRequest;
import com.myteam.request.GetPlayerStatsRequest;
import com.myteam.request.JoinContestRequest;
import com.myteam.respponse.CreateTeamPlayersResponse;
import com.myteam.respponse.FantasySummaryResponse;
import com.myteam.respponse.GetFutureMatchResponse;
import com.myteam.respponse.GetMatchDetailsResponse;
import com.myteam.respponse.GetMatchStatsResponse;
import com.myteam.respponse.GetPlayerStatsResponse;
import com.myteam.respponse.InnningWiseScoreDTO;
import com.myteam.respponse.JoinContestResponse;
import com.myteam.respponse.MatchSummaryResponse;
import com.myteam.transport.HttpSender;
import com.myteam.utils.CollectionUtils;
import com.myteam.utils.StringUtils;
import com.myteam.utils.TeamFlagUtils;

@Service
public class CricAPIService implements ICricAPIService {

	private static final Logger LOG = LoggerFactory.getLogger(CricAPIService.class);

	@Autowired
	IPowerPlayService powerPlayService;

	@Autowired
	IPlayersMao playersMao;

	@Autowired
	IPaymentService paymentService;

	@Autowired
	IMatchConfigMao matchConfigMao;

	@Override
	@Transactional
	public List<MatchDTO> getMatches(GetFutureMatchRequest request) {
		List<MatchDTO> matchDTOs = new ArrayList<>();
		List<Match> matches = null;
		if (StringUtils.isNotEmpty(request.getAuthToken())) {
			matches = powerPlayService.getMatches(request);
		} else {
			matches = powerPlayService.getMatches(request);
		}
		List<Integer> matchIds = new ArrayList<>();
		Map<Integer, MatchConfigDetailDoc> configMap = new HashMap<>();
		if (CollectionUtils.isNotEmpty(matches)) {
			for (Match match : matches) {
				matchIds.add(match.getId());
			}
			List<MatchConfigDetailDoc> configs = matchConfigMao.getMatchConfigById(matchIds);
			if (CollectionUtils.isNotEmpty(configs)) {
				for (MatchConfigDetailDoc config : configs) {
					configMap.put(config.getMatchId(), config);
				}
			}
		}
		Map<Integer, Integer> matchIdToNumberOfContest = new HashMap<>();
		if (StringUtils.isNotEmpty(request.getAuthToken())) {
			if (CollectionUtils.isNotEmpty(matchIds)) {
				matchIdToNumberOfContest = powerPlayService.getContestsNumberForMatches(matchIds,
						request.getAuthToken());
			}
		}
		Map<Integer, List<Contests>> matchIdToContestMap = new HashMap<>();
		Map<Integer, List<Contests>> matchIdToOpenContestMap = new HashMap<>();
		if (CollectionUtils.isNotEmpty(matchIds)) {
			List<Contests> contests = powerPlayService.getContestsForMatchIds(matchIds);
			if (CollectionUtils.isNotEmpty(contests)) {
				for (Contests contest : contests) {
					List<Contests> con = new ArrayList<>();
					if (matchIdToContestMap.get(contest.getMatch().getId()) == null) {
						con.add(contest);
					} else {
						con = matchIdToContestMap.get(contest.getMatch().getId());
						con.add(contest);
					}
					matchIdToContestMap.put(contest.getMatch().getId(), con);
				}
			}
			if (CollectionUtils.isNotEmpty(contests)) {
				for (Contests contest : contests) {
					List<Contests> openCon = new ArrayList<>();
					if (matchIdToOpenContestMap.get(contest.getMatch().getId()) == null) {
						if (!contest.isTimeCrossed()) {
							openCon.add(contest);
						}
					} else {
						if (!contest.isTimeCrossed()) {
							openCon = matchIdToOpenContestMap.get(contest.getMatch().getId());
							openCon.add(contest);
						}
					}
					matchIdToOpenContestMap.put(contest.getMatch().getId(), openCon);
				}
			}
		}
		for (Match match : matches) {
			MatchDTO matchDTO = new MatchDTO(match);
			if (CollectionUtils.isNotEmpty(matchIdToOpenContestMap.get(matchDTO.getMatchId()))) {
				matchDTO.setContestOpen(true);
			}
			matchDTO.setContestJoined(matchIdToNumberOfContest.get(matchDTO.getMatchId()));
			if (configMap.get(matchDTO.getMatchId()) != null) {
				MatchConfigDetailDoc configDetailDoc = configMap.get(matchDTO.getMatchId());
				matchDTO.setTeam1Flag(configDetailDoc.getTeam1Flag());
				matchDTO.setTeam2Flag(configDetailDoc.getTeam2Flag());
				if (StringUtils.isNotEmpty(configDetailDoc.getSeriesName())) {
					matchDTO.setSeriesName(configDetailDoc.getSeriesName());
				} else {
					matchDTO.setSeriesName(
							matchDTO.getTeam1() + " vs " + matchDTO.getTeam2() + " " + matchDTO.getType());
				}
			} else {
				// setting team flags from property
				matchDTO.setTeam1Flag(TeamFlagUtils.getTeamFlagByName(matchDTO.getTeam1()));
				matchDTO.setTeam2Flag(TeamFlagUtils.getTeamFlagByName(matchDTO.getTeam2()));
				matchDTO.setSeriesName(matchDTO.getTeam1() + " vs " + matchDTO.getTeam2() + " " + matchDTO.getType());
			}
			if (StringUtils.isEmpty(matchDTO.getSeriesName())) {
				matchDTO.setSeriesName(matchDTO.getTeam1() + " vs " + matchDTO.getTeam2() + " " + matchDTO.getType());
			}
			matchDTOs.add(matchDTO);
		}
		return matchDTOs;
	}

	@Override
	public List<MatchDTO> getFutureMatches() {
		GetFutureMatchRequest request = new GetFutureMatchRequest();
		String getSquadRequest = new Gson().toJson(request);
		String getSquadResponse = sendPOSTRequest(getSquadRequest, "http://cricapi.com/api/matches");
		LOG.info("Resposne is :{}", getSquadResponse);
		GetFutureMatchResponse response = new Gson().fromJson(getSquadResponse, GetFutureMatchResponse.class);
		return response.getMatches();

	}

	@Override
	public void processFutureMatches() {
		List<MatchDTO> matches = getFutureMatches();
		List<Integer> uniqueIds = new ArrayList<>();
		Map<Integer, Match> uniqueIdToMatchMap = new HashMap<>();
		for (MatchDTO matchDto : matches) {
			uniqueIds.add(matchDto.getUnique_id());
		}
		if (CollectionUtils.isNotEmpty(uniqueIds)) {
			List<Match> prevMatches = powerPlayService.getMatchesByUniqueIds(uniqueIds);
			for (Match match : prevMatches) {
				uniqueIdToMatchMap.put(match.getMatchUniqueId(), match);
			}
		}
		List<Integer> firstTimecreatedMatchIds = new ArrayList<>();
		Map<Integer, Match> matchIdToMatchMap = new HashMap<>();
		for (MatchDTO matchDto : matches) {
			try {
				Match match = uniqueIdToMatchMap.get(matchDto.getUnique_id());
				if (match == null) {
					match = new Match(matchDto);
					match = powerPlayService.saveMatch(match);
					firstTimecreatedMatchIds.add(match.getId());
					matchIdToMatchMap.put(match.getId(), match);
				} else {
					match.setTeam1(matchDto.getTeam1());
					match.setTeam2(matchDto.getTeam2());
					match.setSquad(matchDto.isSquad());
					if (!match.isMatchStarted()) {
						match.setMatchStarted(matchDto.isMatchStarted());
					}
					match.setTossWinnerTeam(matchDto.getToss_winner_team());
					if(StringUtils.isNotEmpty(matchDto.getToss_winner_team())) {
						match.setMatchStarted(true);
					}
					LOG.info("Updating existing match :{}", matchDto.getUnique_id());
					match = powerPlayService.saveMatch(match);
				}
			} catch (Exception e) {
				LOG.info("Error while saving match details :{}", e.getMessage());
			}
		}
		createDefaultContests(firstTimecreatedMatchIds, matchIdToMatchMap);
	}

	@Override
	public void createContests(List<Integer> matchIds) {
		Map<Integer, Match> matchIdToMatchMap = new HashMap<>();
		if (CollectionUtils.isNotEmpty(matchIds)) {
			List<Match> prevMatches = powerPlayService.getMatchesByUniqueIds(matchIds);
			for (Match match : prevMatches) {
				matchIdToMatchMap.put(match.getMatchUniqueId(), match);
			}
			createNonPraticeContests(matchIds, matchIdToMatchMap);
		}
	}
	
	@Override
	public void createMegaContests(List<Integer> matchIds) {
		if (CollectionUtils.isNotEmpty(matchIds)) {
			List<Match> prevMatches = powerPlayService.getMatchesByUniqueIds(matchIds);
			for (Match match : prevMatches) {
				createMegaContest(match, 1);
			}
		}
	}

	private void createNonPraticeContests(List<Integer> matchIds, Map<Integer, Match> matchIdToMatchMap) {
		LOG.info("Creating non practice contests for matches :{}", matchIds);
		for (Integer matchId : matchIds) {
			Match match = matchIdToMatchMap.get(matchId);
			createHeadToHeadContestsForMatch(match, 1);
			createWinnerTakeAllContestsForMatch(match, 1);
		}
	}

	private void createDefaultContests(List<Integer> matchIds, Map<Integer, Match> matchIdToMatchMap) {
		LOG.info("Creating default contests for matches :{}", matchIds);
		for (Integer matchId : matchIds) {
			Match match = matchIdToMatchMap.get(matchId);
			createPracticeContestForMatch(match, 1);
			createHeadToHeadContestsForMatch(match, 1);
			createWinnerTakeAllContestsForMatch(match, 1);
			createMegaContest(match,1);
		}
	}

	private void createMegaContest(Match match, int no) {
		Integer inninng = 2;
		Integer endOver = 20;
		if (match.getSubType().contains("Test") || match.getSubType().contains("test")) {
			inninng = 4;
			endOver = 90;
		} else if (match.getSubType().contains("ODI") || match.getSubType().contains("odi")) {
			endOver = 50;
		}
		for (int index = 0; index < no; index++) {
			for (int in = 1; in <= inninng; in++) {
				// Match match, Integer playingCount, Integer entryFees, Integer prizePool,
				// Integer totalCount,
				// Integer winnerCount, Integer startOver, Integer endOver, Integer inning,
				// String type
				Integer winningAmount = 700000;
				Contests contests = new Contests(match, 0, 100, winningAmount, 10000, 7900, 0, endOver, in, "Mega Contest");
				contests = powerPlayService.saveContests(contests);
				createMegaContestWinningDistribution(contests, winningAmount);
			}
		}
	}

	private void createMegaContestWinningDistribution(Contests contests, Integer winningAmount) {
		List<ContestWinningDistribution> contestWinnerRanks = new ArrayList<>();
		ContestWinningDistribution contestWinnerRank = new ContestWinningDistribution();
		contestWinnerRank.setContests(contests);
		contestWinnerRank.setEndRank(1);
		contestWinnerRank.setStartRank(1);
		contestWinnerRank.setWinningAmount(200000);
		contestWinnerRank = powerPlayService.saveContestWinningDistribution(contestWinnerRank);
		contestWinnerRanks.add(contestWinnerRank);
		contests.setContestWinnerRank(contestWinnerRanks);

		contestWinnerRank = new ContestWinningDistribution();
		contestWinnerRank.setContests(contests);
		contestWinnerRank.setEndRank(2);
		contestWinnerRank.setStartRank(2);
		contestWinnerRank.setWinningAmount(50000);
		contestWinnerRank = powerPlayService.saveContestWinningDistribution(contestWinnerRank);
		contestWinnerRanks.add(contestWinnerRank);
		contests.setContestWinnerRank(contestWinnerRanks);

		contestWinnerRank = new ContestWinningDistribution();
		contestWinnerRank.setContests(contests);
		contestWinnerRank.setEndRank(10);
		contestWinnerRank.setStartRank(3);
		contestWinnerRank.setWinningAmount(5000);
		contestWinnerRank = powerPlayService.saveContestWinningDistribution(contestWinnerRank);
		contestWinnerRanks.add(contestWinnerRank);
		contests.setContestWinnerRank(contestWinnerRanks);
		
		contestWinnerRank = new ContestWinningDistribution();
		contestWinnerRank.setContests(contests);
		contestWinnerRank.setEndRank(100);
		contestWinnerRank.setStartRank(11);
		contestWinnerRank.setWinningAmount(300);
		contestWinnerRank = powerPlayService.saveContestWinningDistribution(contestWinnerRank);
		contestWinnerRanks.add(contestWinnerRank);
		contests.setContestWinnerRank(contestWinnerRanks);

		contestWinnerRank = new ContestWinningDistribution();
		contestWinnerRank.setContests(contests);
		contestWinnerRank.setEndRank(500);
		contestWinnerRank.setStartRank(101);
		contestWinnerRank.setWinningAmount(100);
		contestWinnerRank = powerPlayService.saveContestWinningDistribution(contestWinnerRank);
		contestWinnerRanks.add(contestWinnerRank);
		contests.setContestWinnerRank(contestWinnerRanks);
		
		contestWinnerRank = new ContestWinningDistribution();
		contestWinnerRank.setContests(contests);
		contestWinnerRank.setEndRank(7000);
		contestWinnerRank.setStartRank(501);
		contestWinnerRank.setWinningAmount(50);
		contestWinnerRank = powerPlayService.saveContestWinningDistribution(contestWinnerRank);
		contestWinnerRanks.add(contestWinnerRank);
		contests.setContestWinnerRank(contestWinnerRanks);
		
		contestWinnerRank = new ContestWinningDistribution();
		contestWinnerRank.setContests(contests);
		contestWinnerRank.setEndRank(7900);
		contestWinnerRank.setStartRank(7001);
		contestWinnerRank.setWinningAmount(20);
		contestWinnerRank = powerPlayService.saveContestWinningDistribution(contestWinnerRank);
		contestWinnerRanks.add(contestWinnerRank);
		contests.setContestWinnerRank(contestWinnerRanks);
		
		contests = powerPlayService.saveContests(contests);
		
	}

	private void createWinnerTakeAllContestsForMatch(Match match, int no) {
		Integer inninng = 2;
		Integer endOver = 20;
		if (match.getSubType().contains("Test") || match.getSubType().contains("test")) {
			inninng = 4;
			endOver = 90;
		} else if (match.getSubType().contains("ODI") || match.getSubType().contains("odi")) {
			endOver = 50;
		}
		for (int index = 0; index < no; index++) {
			for (int in = 1; in <= inninng; in++) {
				// Match match, Integer playingCount, Integer entryFees, Integer prizePool,
				// Integer totalCount,
				// Integer winnerCount, Integer startOver, Integer endOver, Integer inning,
				// String type
				Integer winningAmount = 850;
				Integer totalWinners = 5;
				Contests contests = new Contests(match, 0, 100, winningAmount, 10, totalWinners, 0, endOver, in,
						"Winners Take All");
				contests = powerPlayService.saveContests(contests);
				createWinnersTakeAllContestWinningDistribution(contests, winningAmount);
			}
		}
	}

	private void createHeadToHeadContestsForMatch(Match match, int no) {
		Integer inninng = 2;
		Integer endOver = 20;
		if (match.getSubType().contains("Test") || match.getSubType().contains("test")) {
			inninng = 4;
			endOver = 90;
		} else if (match.getSubType().contains("ODI") || match.getSubType().contains("odi")) {
			endOver = 50;
		}
		for (int index = 0; index < no; index++) {
			for (int in = 1; in <= inninng; in++) {
				// Match match, Integer playingCount, Integer entryFees, Integer prizePool,
				// Integer totalCount,
				// Integer winnerCount, Integer startOver, Integer endOver, Integer inning,
				// String type
				Integer winningAmount = 180;
				Contests contests = new Contests(match, 0, 100, winningAmount, 2, 1, 0, endOver, in, "Head To Head");
				contests = powerPlayService.saveContests(contests);
				createHeadToHeadContestWinningDistribution(contests, winningAmount);
			}
		}
	}

	private void createPracticeContestForMatch(Match match, int no) {
		Integer inninng = 2;
		Integer endOver = 20;
		if (match.getSubType().contains("Test") || match.getSubType().contains("test")) {
			inninng = 4;
			endOver = 90;
		} else if (match.getSubType().contains("ODI") || match.getSubType().contains("odi")) {
			endOver = 50;
		}
		for (int index = 0; index < no; index++) {
			for (int in = 1; in <= inninng; in++) {
				Contests contests = new Contests(match, 0, 0, 1000, 1000, 100, 0, endOver, in, "Practice");
				contests = powerPlayService.saveContests(contests);
				createContestWinningDistribution(contests);
			}
		}
	}

	private void createWinnersTakeAllContestWinningDistribution(Contests contests, Integer amount) {
		List<ContestWinningDistribution> contestWinnerRanks = new ArrayList<>();
		ContestWinningDistribution contestWinnerRank = new ContestWinningDistribution();
		contestWinnerRank.setContests(contests);
		contestWinnerRank.setEndRank(1);
		contestWinnerRank.setStartRank(1);
		contestWinnerRank.setWinningAmount(500);
		contestWinnerRank = powerPlayService.saveContestWinningDistribution(contestWinnerRank);
		contestWinnerRanks.add(contestWinnerRank);
		contests.setContestWinnerRank(contestWinnerRanks);

		contestWinnerRank = new ContestWinningDistribution();
		contestWinnerRank.setContests(contests);
		contestWinnerRank.setEndRank(2);
		contestWinnerRank.setStartRank(2);
		contestWinnerRank.setWinningAmount(200);
		contestWinnerRank = powerPlayService.saveContestWinningDistribution(contestWinnerRank);
		contestWinnerRanks.add(contestWinnerRank);
		contests.setContestWinnerRank(contestWinnerRanks);

		contestWinnerRank = new ContestWinningDistribution();
		contestWinnerRank.setContests(contests);
		contestWinnerRank.setEndRank(5);
		contestWinnerRank.setStartRank(3);
		contestWinnerRank.setWinningAmount(50);
		contestWinnerRank = powerPlayService.saveContestWinningDistribution(contestWinnerRank);
		contestWinnerRanks.add(contestWinnerRank);
		contests.setContestWinnerRank(contestWinnerRanks);

		contests = powerPlayService.saveContests(contests);
	}

	private void createHeadToHeadContestWinningDistribution(Contests contests, Integer amount) {
		List<ContestWinningDistribution> contestWinnerRanks = new ArrayList<>();
		ContestWinningDistribution contestWinnerRank = new ContestWinningDistribution();
		contestWinnerRank.setContests(contests);
		contestWinnerRank.setEndRank(1);
		contestWinnerRank.setStartRank(1);
		contestWinnerRank.setWinningAmount(amount);
		contestWinnerRank = powerPlayService.saveContestWinningDistribution(contestWinnerRank);
		contestWinnerRanks.add(contestWinnerRank);
		contests.setContestWinnerRank(contestWinnerRanks);
		contests = powerPlayService.saveContests(contests);
	}

	private void createContestWinningDistribution(Contests contests) {
		List<ContestWinningDistribution> contestWinnerRanks = new ArrayList<>();
		ContestWinningDistribution contestWinnerRank = new ContestWinningDistribution();
		contestWinnerRank.setContests(contests);
		contestWinnerRank.setEndRank(100);
		contestWinnerRank.setStartRank(1);
		contestWinnerRank.setWinningAmount(10);
		contestWinnerRank = powerPlayService.saveContestWinningDistribution(contestWinnerRank);
		contestWinnerRanks.add(contestWinnerRank);
		contests.setContestWinnerRank(contestWinnerRanks);
		contests = powerPlayService.saveContests(contests);
	}

	@Override
	public void populateSquad() {
		GetFutureMatchRequest request = new GetFutureMatchRequest();
		Integer count = 0;
		while (true) {
			request.setStart(count);
			request.setMaxResults(10);
			List<Match> matchWithOutSquad = powerPlayService.getMatchesWithoutSquad(request);
			populateSquad(matchWithOutSquad);
			count += matchWithOutSquad.size();
			if (matchWithOutSquad.size() == 0) {
				return;
			}
		}
	}

	private void populateSquad(List<Match> matchWithOutSquad) {
		for (Match match : matchWithOutSquad) {
			GetMatchDetailsResponse response = getPlayersForMatch(match.getMatchUniqueId());
			if (CollectionUtils.isNotEmpty(response.getSquad())) {
				for (SquadDTO squad : response.getSquad()) {
					if (CollectionUtils.isNotEmpty(squad.getPlayers())) {
						for (PlayersDTO playersDTO : squad.getPlayers()) {
							populatePlayerDoc(playersDTO.getPid());
						}
					}
				}
			}
			match.setPlayers(new Gson().toJson(response));
			powerPlayService.saveMatch(match);
		}
	}

	public void populatePlayerDoc(Integer playerId) {
		PlayersDoc playersDoc = playersMao.getPlayerDocByPId(playerId);
		GetPlayerStatsResponse response = null;
		if (playersDoc == null) {
			GetPlayerStatsRequest request = new GetPlayerStatsRequest();
			request.setPid(playerId);
			String getSquadRequest = new Gson().toJson(request);
			String getSquadResponse = sendPOSTRequest(getSquadRequest, "http://cricapi.com/api/playerStats");
			response = new Gson().fromJson(getSquadResponse, GetPlayerStatsResponse.class);
			playersDoc = new PlayersDoc(response);
			playersMao.savePlayersDoc(playersDoc);
		} else {
			LOG.info("Already in Mongo");
		}
	}

	@Override
	public GetMatchDetailsResponse getMatchDetails(Integer matchId) throws MyTeamException {
		Match match = powerPlayService.getMatcheByMatchId(matchId);
		if (match == null) {
			throw new MyTeamException(ErrorCode.NO_MATCH_FOUND);
		}
		GetMatchDetailsResponse response = null;
		// fetching from playing eleven
		if(StringUtils.isNotEmpty(match.getPlayingEleven())) {
			PlayingElevenDTO playingTeam = new Gson().fromJson(match.getPlayingEleven(), PlayingElevenDTO.class);
			if(playingTeam != null && CollectionUtils.isNotEmpty(playingTeam.getTeam())) {
				 response = new GetMatchDetailsResponse();
				 List<SquadDTO> squads = new ArrayList<>(); 
				 List<TeamDTO> teams= playingTeam.getTeam();
				 for(TeamDTO team :teams) {
					 SquadDTO squad = new SquadDTO(team);
					 squads.add(squad);
				 }
				 response.setSquad(squads);
			}
		}else {
			// fetching from all players 
			response = new Gson().fromJson(match.getPlayers(), GetMatchDetailsResponse.class);
		}
		if (CollectionUtils.isNotEmpty(response.getSquad())) {
			for (SquadDTO squadDTO : response.getSquad()) {
				List<Integer> pids = new ArrayList<>();
				if (CollectionUtils.isNotEmpty(squadDTO.getPlayers())) {
					for (PlayersDTO playersDTO : squadDTO.getPlayers()) {
						pids.add(playersDTO.getPid());
					}
				}
				List<PlayersDTO> playerDTOs = getPlayerDTOFromPIds(pids);
				for (PlayersDTO playersDTO : playerDTOs) {
					playersDTO.setTeam(squadDTO.getName());
					playersDTO.setShortName(StringUtils.getShortName(playersDTO.getName()));
				}
				squadDTO.setShortName(StringUtils.getNameInitials(squadDTO.getName()));
				squadDTO.setPlayers(playerDTOs);
			}
		}
		// fetching all contests 
		List<Contests> contests = powerPlayService.getContestsForMatch(matchId);
		if (CollectionUtils.isEmpty(contests)) {
			throw new MyTeamException(ErrorCode.NO_CONTEST_FOUND);
		}
		List<ContestDTO> contestDTOs = new ArrayList<>();
		for (Contests con : contests) {
			contestDTOs.add(new ContestDTO(con));
		}
		response.setContestDTOs(contestDTOs);
		return response;
	}

	@Override
	public GetMatchDetailsResponse getPlayersForMatch(Integer matchId) {
		GetMatchDetailRequest request = new GetMatchDetailRequest();
		request.setUnique_id(matchId);
		String getSquadRequest = new Gson().toJson(request);
		String getSquadResponse = sendPOSTRequest(getSquadRequest, "http://cricapi.com/api/fantasySquad");
		LOG.info("Resposne is :{}", getSquadResponse);
		GetMatchDetailsResponse response = new Gson().fromJson(getSquadResponse, GetMatchDetailsResponse.class);
		return response;

	}

	@Override
	public GetPlayerStatsResponse getPlayerStats(Integer playerId) {
		PlayersDoc playersDoc = playersMao.getPlayerDocByPId(playerId);
		GetPlayerStatsResponse response = null;
		if (playersDoc == null) {
			GetPlayerStatsRequest request = new GetPlayerStatsRequest();
			request.setPid(playerId);
			String getSquadRequest = new Gson().toJson(request);
			String getSquadResponse = sendPOSTRequest(getSquadRequest, "http://cricapi.com/api/playerStats");
			LOG.info("Resposne is :{}", getSquadResponse);
			response = new Gson().fromJson(getSquadResponse, GetPlayerStatsResponse.class);
			playersDoc = new PlayersDoc(response);
			playersMao.savePlayersDoc(playersDoc);
		} else {
			response = new GetPlayerStatsResponse(playersDoc);
		}
		return response;

	}

	@Override
	public GetMatchStatsResponse getMatchStats(Integer matchId) {
		MatchData matchData = powerPlayService.getMatchDataByMatchId(matchId);
		GetMatchStatsResponse response = new GetMatchStatsResponse();
		if (matchData != null) {
			if (StringUtils.isNotEmpty(matchData.getAdditionalMessage())) {
				response.setAdditionalMsg(matchData.getAdditionalMessage());
			}
			MatchSummaryResponse data = new Gson().fromJson(matchData.getData(), MatchSummaryResponse.class);
			if (data != null) {
				response.setWinningTeam(data.getWinnerTeam());
				List<InnningWiseScoreDTO> scores = new ArrayList<>();
				scores = updateMatchStats(scores, data.getBatting(), data.getBowling(), data.getFielding());
				if (CollectionUtils.isNotEmpty(scores)) {
					response.setMatchStarted(true);
					if (scores.size() > 0) {
						if(StringUtils.isNotEmpty(scores.get(0).getTitle())) {
							response.setScores1(scores.get(0).getTitle() + "-" + scores.get(0).getBattingScore() + "("
									+ scores.get(0).getBowlingScore() + ")");
						}
					}
					if (scores.size() > 1) {
						if(StringUtils.isNotEmpty(scores.get(1).getTitle())) {
							response.setScores2(scores.get(1).getTitle() + "-" + scores.get(1).getBattingScore() + "("
									+ scores.get(1).getBowlingScore() + ")");
						}
					}
					if (StringUtils.isNotEmpty(data.getTossWinnerTeam())
							&& StringUtils.isEmpty(response.getScores1())) {
						response.setScores1(data.getTossWinnerTeam() + " won the toss.");
					}else if (StringUtils.isEmpty(response.getScores1())) {
						response.setScores1("Match not yet started.");
					}
					
				}else {
					if (StringUtils.isEmpty(response.getScores1())) {
						response.setScores1("Match not yet started.");
					}
				}
				response.setScores(scores);
			}
		} else {
			response.setScores1("Match not yet started.");
		}
		return response;

	}

	public List<InnningWiseScoreDTO> updateMatchStats(List<InnningWiseScoreDTO> scores, List<MatchBattingDTO> battings,
			List<MatchBowlingDTO> bowlings, List<MatchFieldingDTO> fieldings) {
		if (CollectionUtils.isNotEmpty(battings)) {
			for (MatchBattingDTO matchBattingDTO : battings) {
				InnningWiseScoreDTO innningWiseScoreDTO = new InnningWiseScoreDTO();
				innningWiseScoreDTO.setTitle(matchBattingDTO.getTitle());
				List<BattingScoreDTO> battingScores = matchBattingDTO.getScores();
				Integer runs = 0;
				for (BattingScoreDTO battingScore : battingScores) {
					if (!"Extras".equalsIgnoreCase(battingScore.getBatsman())) {
						if (StringUtils.isNotEmpty(battingScore.getR())) {
							runs += Integer.parseInt(battingScore.getR());
						}
					} else {
						String details = battingScore.getDetail();
						String[] extras = details.split("\\(");
						if (extras.length > 0) {
							if (StringUtils.isNotEmpty(extras[0].trim())) {
								runs += Integer.parseInt(extras[0].trim());
							}
						}
					}
				}
				innningWiseScoreDTO.setRun(runs);
				scores.add(innningWiseScoreDTO);
			}

			Integer inning = 1;
			for (MatchBowlingDTO matchBowlingDTO : bowlings) {
				Integer runOutWickets = 0;
				Integer stumpedWickets = 0;
				List<FieldingScoreDTO> fieldingScores = fieldings.get(inning - 1).getScores();
				for (FieldingScoreDTO fieldingScore : fieldingScores) {
					if (fieldingScore.getRunout() > 0) {
						runOutWickets += fieldingScore.getRunout();
					}
				}
				if(runOutWickets%2!=0) {
					 runOutWickets = runOutWickets +1; 
				}
				runOutWickets = (int) Math.ceil(runOutWickets / 2);
				InnningWiseScoreDTO innningWiseScoreDTO = scores.get(inning - 1);
				List<BowlingScoreDTO> bowlingScores = matchBowlingDTO.getScores();
				Integer overs = 0;
				Integer balls = 0;
				Integer wickets = runOutWickets + stumpedWickets;
				for (BowlingScoreDTO bowlingScore : bowlingScores) {
					wickets += Integer.parseInt(bowlingScore.getW());
					String overString = bowlingScore.getO();
					if (StringUtils.isNotEmpty(overString)) {
						String[] overSplit = overString.split("\\.");
						overs += Integer.parseInt(overSplit[0]);
						if (overSplit.length == 2) {
							Integer currBall = Integer.parseInt(overSplit[1]);
							if (currBall > 0) {
								balls = currBall;
							}
						}
					}
				}
				innningWiseScoreDTO.setWicket(wickets);
				innningWiseScoreDTO
						.setBattingScore(innningWiseScoreDTO.getRun() + "/" + innningWiseScoreDTO.getWicket());
				innningWiseScoreDTO.setBowlingScore(overs + "." + balls);
				inning++;
			}
		}
		return scores;
	}

	@Override
	public void updateScores() {
		List<Match> matches = powerPlayService.getCurrentlyRunningMatches();
		for (Match match : matches) {
			GetMatchStatsRequest request = new GetMatchStatsRequest();
			request.setUnique_id(match.getMatchUniqueId());
			String getFantasySummaryRequest = new Gson().toJson(request);
			String fantasySummaryResponse = sendPOSTRequest(getFantasySummaryRequest,
					"http://cricapi.com/api/fantasySummary");
			FantasySummaryResponse response = new Gson().fromJson(fantasySummaryResponse, FantasySummaryResponse.class);
			MatchData matchData = powerPlayService.getMatchDataByMatchId(match.getId());
			if (matchData == null) {
				matchData = new MatchData();
				matchData.setMatchId(match.getId());
			}
			matchData.setData(new Gson().toJson(response.getData()));
			powerPlayService.saveMatchData(matchData);
		}
	}

	@Override
	public void updateMatchPlayerFantasyPoints() {
		List<Match> matches = powerPlayService.getCurrentlyRunningMatches();
		for (Match match : matches) {
			MatchData matchData = powerPlayService.getMatchDataByMatchId(match.getId());
			if (matchData != null) {
				if (matchData.getData() != null) {
					MatchSummaryResponse data = new Gson().fromJson(matchData.getData(), MatchSummaryResponse.class);
					if (StringUtils.isNotEmpty(data.getWinnerTeam())) {
						match.setWinnerTeam(data.getWinnerTeam());
						powerPlayService.saveMatch(match);
					}
				}
				FantasyPointDetails previousPointDetails = null;
				if (StringUtils.isNotEmpty(matchData.getPoints())) {
					previousPointDetails = new Gson().fromJson(matchData.getPoints(), FantasyPointDetails.class);
				}
				Integer fullMatchOver = 50;
				MatchSummaryResponse data = new Gson().fromJson(matchData.getData(), MatchSummaryResponse.class);
				if (StringUtils.isNotEmpty(match.getSubType())) {
					if (match.getSubType().contains("ODI")) {
						fullMatchOver = 50;
					} else if (match.getSubType().contains("20")) {
						fullMatchOver = 20;
					} else if (match.getSubType().contains("Test")) {
						// Arpit TODO
					}
				}
				List<MatchBowlingDTO> bowling = data.getBowling();
				OverDetailsDTO overDetailsDTO = getOverDetailsDTOFromBowlingScore(bowling, fullMatchOver);
				FantasyPointDetails updatedsPointDetails = getFullMatchPointsFromMatchData(data);
				updatedsPointDetails.setOverDetailsDTO(overDetailsDTO);
				matchData.setPoints(new Gson().toJson(updatedsPointDetails));
				powerPlayService.saveMatchData(matchData);
				if (data != null && StringUtils.isNotEmpty(data.getTossWinnerTeam())) {
					match.setTossWinnerTeam(data.getTossWinnerTeam());
					powerPlayService.saveMatch(match);
				}
				updateContestWisePoints(updatedsPointDetails, match, fullMatchOver);
			}
		}
	}

	private void updateContestWisePoints(FantasyPointDetails fantasyPointDetails, Match match, Integer fullMatchOver) {
		LOG.info("Over Details are :{}", fantasyPointDetails.getOverDetailsDTO());
		Integer totalOvers = fantasyPointDetails.getOverDetailsDTO().getOvers();
		updateTimeCrossedForContests(fantasyPointDetails.getOverDetailsDTO(), match.getId());
		List<Contests> allRunningContests = powerPlayService.getAllCurrentlyRunningContests(match.getId(),
				fantasyPointDetails.getOverDetailsDTO().getBalls() == 0 ? totalOvers : totalOvers + 1,
				fantasyPointDetails.getOverDetailsDTO().getInning());
		for (Contests contests : allRunningContests) {
			if (contests.getInning() < fantasyPointDetails.getOverDetailsDTO().getInning()
					|| (contests.getInning() == fantasyPointDetails.getOverDetailsDTO().getInning()
							&& fantasyPointDetails.getOverDetailsDTO().getOvers() == fullMatchOver)
					|| StringUtils.isNotEmpty(match.getWinnerTeam())) {
				contests.setContestOver(true);
			}
			Map<Integer, Double> pointsMap = fantasyPointDetails.getInningWisePoints().get(contests.getInning());
			FantasyPointDetails points = new FantasyPointDetails();
			points.setPidToFantasyPointMap(pointsMap);
			contests.setPoints(new Gson().toJson(points));
			powerPlayService.saveContests(contests);
			updatePlayingTeamPointsForRunningContests(contests.getId(), pointsMap);
		}
	}

	private void updatePlayingTeamPointsForRunningContests(Integer contestId, Map<Integer, Double> contestPointMap) {
		Integer start = 0;
		Integer maxResults = 1;
		Integer responseSize = maxResults;
		while (responseSize == maxResults) {
			LOG.info("Fetching from start :{} and max :{}", start, maxResults);
			List<UserMatchTeamContestMapping> userMatchContestPlayersMappings = powerPlayService
					.getUserMatchContestPlayersMappingByContestId(contestId, start, maxResults, null, null, null);
			if (CollectionUtils.isNotEmpty(userMatchContestPlayersMappings)) {
				for (UserMatchTeamContestMapping userMatchContestPlayersMapping : userMatchContestPlayersMappings) {
					UserMatchTeamMapping userMatchTeamMapping = userMatchContestPlayersMapping
							.getUserMatchTeamMapping();
					String players = userMatchTeamMapping.getPlayers();
					String superPlayer = userMatchTeamMapping.getSuperPlayer();
					Double totalPointsForATeam = 0.0;
					for (Integer playerId : StringUtils.getListFromString(players, ",")) {
						if (playerId == Integer.parseInt(superPlayer)) {
							LOG.info("Adding super player boost for player :{}", playerId);
							totalPointsForATeam += contestPointMap.get(playerId) != null
									? contestPointMap.get(playerId) * Constants.SUPER_PLAYER_BOOST
									: 0.0 * Constants.SUPER_PLAYER_BOOST;
						} else {
							totalPointsForATeam += contestPointMap.get(playerId) != null ? contestPointMap.get(playerId)
									: 0.0;
						}
					}
					userMatchContestPlayersMapping.setPoints(totalPointsForATeam);
					powerPlayService.saveUserMatchContestPlayersMapping(userMatchContestPlayersMapping);
				}
			}
			responseSize = userMatchContestPlayersMappings.size();
			start += userMatchContestPlayersMappings.size();
		}
		updateRank(contestId);
	}

	private void updateRank(Integer contestId) {
		Integer start = 0;
		Integer maxResults = 200;
		Integer responseSize = maxResults;
		Integer rank = 0;
		while (responseSize == maxResults) {
			List<UserMatchTeamContestMapping> userMatchContestPlayersMappings = powerPlayService
					.getUserMatchContestPlayersMappingByContestId(contestId, start, maxResults, null, null, null);
			Double lastPoint = 0d;
			for (UserMatchTeamContestMapping userMatchTeamContestMapping : userMatchContestPlayersMappings) {
				if (!lastPoint.equals(userMatchTeamContestMapping.getPoints())) {
					rank++;
				}
				userMatchTeamContestMapping.setRank(rank);
				lastPoint = userMatchTeamContestMapping.getPoints();
				powerPlayService.saveUserMatchContestPlayersMapping(userMatchTeamContestMapping);
			}
			responseSize = userMatchContestPlayersMappings.size();
			start += userMatchContestPlayersMappings.size();
		}
	}

	private Map<Integer, Double> getPreviousContestPointMap(List<Contests> allPastContestsOfAType) {
		Map<Integer, Double> allPrevContestPointMap = new HashMap<>();
		for (Contests contests : allPastContestsOfAType) {
			FantasyPointDetails points = new Gson().fromJson(contests.getPoints(), FantasyPointDetails.class);
			Map<Integer, Double> prevContestPointMap = points.getPidToFantasyPointMap();
			prevContestPointMap.forEach((k, v) -> allPrevContestPointMap.merge(k, v, (v1, v2) -> v1 + v2));
		}
		return allPrevContestPointMap;
	}

	private Map<Integer, Double> getContestPointMap(Map<Integer, Double> matchPointsMap,
			Map<Integer, Double> prevContestPointMap) {
		Map<Integer, Double> updatedMap = new HashMap<>();
		updatedMap.forEach((k, v) -> matchPointsMap.merge(k, v, (v1, v2) -> v1 - v2));
		return updatedMap;
	}

	private FantasyPointDetails getFullMatchPointsFromMatchData(MatchSummaryResponse data) {
		FantasyPointDetails fantasyPointDetails = new FantasyPointDetails();
		Map<Integer, Double> pidToFantasyPointMapInningOne = new HashMap<>();
		Map<Integer, Double> pidToFantasyPointMapInningTwo = new HashMap<>();
		List<TeamDTO> teams = data.getTeam();
		LOG.info("Processing playing 11 point ");
		TeamDTO team = teams.get(0);
		LOG.info("Processing playing 11 point for team :{}", team.getName());
		List<PlayersDTO> playersDTOs = team.getPlayers();
		for (PlayersDTO playersDTO : playersDTOs) {
			pidToFantasyPointMapInningOne.put(playersDTO.getPid(), 0.0);
		}
		LOG.info("Processing fielding point ");
		List<MatchFieldingDTO> fieldings = data.getFielding();
		if (fieldings.size() > 0) {
			MatchFieldingDTO fieldingDTO = fieldings.get(0);
			LOG.info("Processing fielding point for team :{}", fieldingDTO.getTitle());
			List<FieldingScoreDTO> scores = fieldingDTO.getScores();
			for (FieldingScoreDTO scoreDTO : scores) {
				Double point = pidToFantasyPointMapInningOne.get(Integer.parseInt(scoreDTO.getPid()));
				if (point == null) {
					point = 0.0;
				}
				Double newPoint = 0d;
				newPoint += scoreDTO.getRunout() * Constants.RUNOUT;
				newPoint += scoreDTO.getStumped() * Constants.STUMPED;
				newPoint += scoreDTO.getCatchCount() * Constants.CATCH;
				Double updatedPoint = point + newPoint;
				pidToFantasyPointMapInningOne.put(Integer.parseInt(scoreDTO.getPid()), updatedPoint);
			}
		}
		if (fieldings.size() > 1) {
			MatchFieldingDTO fieldingDTO = fieldings.get(1);
			LOG.info("Processing fielding point for team :{}", fieldingDTO.getTitle());
			List<FieldingScoreDTO> scores = fieldingDTO.getScores();
			for (FieldingScoreDTO scoreDTO : scores) {
				Double point = pidToFantasyPointMapInningOne.get(Integer.parseInt(scoreDTO.getPid()));
				if (point == null) {
					point = 0.0;
				}
				Double newPoint = 0d;
				newPoint += scoreDTO.getRunout() * Constants.RUNOUT;
				newPoint += scoreDTO.getStumped() * Constants.STUMPED;
				newPoint += scoreDTO.getCatchCount() * Constants.CATCH;
				Double updatedPoint = point + newPoint;
				pidToFantasyPointMapInningTwo.put(Integer.parseInt(scoreDTO.getPid()), updatedPoint);
			}
		}
		LOG.info("Processing batting point ");
		List<MatchBattingDTO> batting = data.getBatting();
		if (batting.size() > 0) {
			MatchBattingDTO battingDTO = batting.get(0);
			LOG.info("Processing batting point for team :{}", battingDTO.getTitle());
			List<BattingScoreDTO> scores = battingDTO.getScores();
			for (BattingScoreDTO scoreDTO : scores) {
				if (Integer.parseInt(scoreDTO.getPid()) == 0) {
					continue; // this is the inning extra run case
				}
				Double point = pidToFantasyPointMapInningOne.get(Integer.parseInt(scoreDTO.getPid()));
				if (point == null) {
					point = 0.0;
				}
				Double newPoint = 0d;
				if (StringUtils.isNotEmpty(scoreDTO.getSixes())) {
					newPoint += Integer.parseInt(scoreDTO.getSixes()) * Constants.SIXES;
				}
				if (StringUtils.isNotEmpty(scoreDTO.getFours())) {
					newPoint += Integer.parseInt(scoreDTO.getFours()) * Constants.FOURS;
				}
				if (StringUtils.isNotEmpty(scoreDTO.getR())) {
					newPoint += Integer.parseInt(scoreDTO.getR()) * Constants.RUN;
				}
				if (StringUtils.isNotEmpty(scoreDTO.getSR()) && !scoreDTO.getSR().equalsIgnoreCase("0")) {
					Double strikeRate = Double.parseDouble(scoreDTO.getSR());
					if (strikeRate < 60) {
						newPoint -= Constants.STRIKE_RATE;
					}
					if (strikeRate > 140) {
						newPoint += Constants.STRIKE_RATE;
					}
				}
				Double updatedPoint = point + newPoint;
				pidToFantasyPointMapInningOne.put(Integer.parseInt(scoreDTO.getPid()), updatedPoint);
			}
		}

		if (batting.size() > 1) {
			MatchBattingDTO battingDTO = batting.get(1);
			LOG.info("Processing batting point for team :{}", battingDTO.getTitle());
			List<BattingScoreDTO> scores = battingDTO.getScores();
			for (BattingScoreDTO scoreDTO : scores) {
				if (Integer.parseInt(scoreDTO.getPid()) == 0) {
					continue; // this is the inning extra run case
				}
				Double point = pidToFantasyPointMapInningTwo.get(Integer.parseInt(scoreDTO.getPid()));
				if (point == null) {
					point = 0.0;
				}
				Double newPoint = 0d;
				if (StringUtils.isNotEmpty(scoreDTO.getSixes())) {
					newPoint += Integer.parseInt(scoreDTO.getSixes()) * Constants.SIXES;
				}
				if (StringUtils.isNotEmpty(scoreDTO.getFours())) {
					newPoint += Integer.parseInt(scoreDTO.getFours()) * Constants.FOURS;
				}
				if (StringUtils.isNotEmpty(scoreDTO.getR())) {
					newPoint += Integer.parseInt(scoreDTO.getR()) * Constants.RUN;
				}
				if (StringUtils.isNotEmpty(scoreDTO.getSR()) && !scoreDTO.getSR().equalsIgnoreCase("0")) {
					Double strikeRate = Double.parseDouble(scoreDTO.getSR());
					if (strikeRate < 60) {
						newPoint -= Constants.STRIKE_RATE;
					}
					if (strikeRate > 140) {
						newPoint += Constants.STRIKE_RATE;
					}
				}
				Double updatedPoint = point + newPoint;
				pidToFantasyPointMapInningTwo.put(Integer.parseInt(scoreDTO.getPid()), updatedPoint);
			}
		}

		LOG.info("Processing bowling point ");
		List<MatchBowlingDTO> bowling = data.getBowling();
		if (bowling.size() > 0) {
			MatchBowlingDTO bowlingDTO = bowling.get(0);
			LOG.info("Processing batting point for team :{}", bowlingDTO.getTitle());
			List<BowlingScoreDTO> scores = bowlingDTO.getScores();
			for (BowlingScoreDTO scoreDTO : scores) {
				if (Integer.parseInt(scoreDTO.getPid()) == 0) {
					continue; // this is the inning extra case
				}
				Double point = pidToFantasyPointMapInningOne.get(Integer.parseInt(scoreDTO.getPid()));
				if (point == null) {
					point = 0.0;
				}
				Double newPoint = 0d;
				if (StringUtils.isNotEmpty(scoreDTO.getW())) {
					newPoint += Integer.parseInt(scoreDTO.getW()) * Constants.WICKET;
				}
				if (StringUtils.isNotEmpty(scoreDTO.getM())) {
					newPoint += Integer.parseInt(scoreDTO.getM()) * Constants.MAIDEN;
				}
				if (StringUtils.isNotEmpty(scoreDTO.getEcon()) && !scoreDTO.getEcon().equalsIgnoreCase("0")) {
					try {
						Double economy = Double.parseDouble(scoreDTO.getEcon());
						if (economy < 5) {
							newPoint += Constants.ECONOMY;
						}
						if (economy > 12) {
							newPoint -= Constants.ECONOMY;
						}
					} catch (Exception e) {
						LOG.error("No economy found :{}", scoreDTO.getEcon());
					}

				}
				Double updatedPoint = point + newPoint;
				pidToFantasyPointMapInningOne.put(Integer.parseInt(scoreDTO.getPid()), updatedPoint);
			}
		}

		if (bowling.size() > 1) {
			MatchBowlingDTO bowlingDTO = bowling.get(1);
			LOG.info("Processing batting point for team :{}", bowlingDTO.getTitle());
			List<BowlingScoreDTO> scores = bowlingDTO.getScores();
			for (BowlingScoreDTO scoreDTO : scores) {
				if (Integer.parseInt(scoreDTO.getPid()) == 0) {
					continue; // this is the inning extra case
				}
				Double point = pidToFantasyPointMapInningTwo.get(Integer.parseInt(scoreDTO.getPid()));
				if (point == null) {
					point = 0.0;
				}
				Double newPoint = 0d;
				if (StringUtils.isNotEmpty(scoreDTO.getW())) {
					newPoint += Integer.parseInt(scoreDTO.getW()) * Constants.WICKET;
				}
				if (StringUtils.isNotEmpty(scoreDTO.getM())) {
					newPoint += Integer.parseInt(scoreDTO.getM()) * Constants.MAIDEN;
				}
				if (StringUtils.isNotEmpty(scoreDTO.getEcon())) {
					Double economy = Double.parseDouble(scoreDTO.getEcon());
					if (economy < 5) {
						newPoint += Constants.ECONOMY;
					}
					if (economy > 12) {
						newPoint -= Constants.ECONOMY;
					}
				}
				Double updatedPoint = point + newPoint;
				pidToFantasyPointMapInningTwo.put(Integer.parseInt(scoreDTO.getPid()), updatedPoint);
			}
		}
		fantasyPointDetails.getInningWisePoints().put(1, pidToFantasyPointMapInningOne);
		fantasyPointDetails.getInningWisePoints().put(2, pidToFantasyPointMapInningTwo);
		return fantasyPointDetails;
	}

	private Map<Integer, Double> getAddedPoint(Map<Integer, Double> inning1, Map<Integer, Double> inning2) {
		Map<Integer, Double> updatedMap = new HashMap<>();
		inning1.forEach((k, v) -> inning2.merge(k, v, (v1, v2) -> v1 + v2));
		return inning1;
	}

	public static void main(String[] args) {
		String overString = "5";
		String[] overSplit = overString.split("\\.");
		System.out.println(overSplit.length);
		Integer over = Integer.parseInt(overSplit[0]);
		System.out.println(over);
		String fantasySummaryResponse = "{\"dateTimeGMT\":\"2018-10-21T08:00:00.000Z\",\"data\":{\"fielding\":[{\"title\":\"Fielding for West Indies Innings\",\"scores\":[{\"name\":\"MS Dhoni\",\"runout\":0,\"stumped\":0,\"bowled\":0,\"lbw\":0,\"catch\":1,\"pid\":\"28081\"},{\"name\":\"S Dhawan\",\"runout\":0,\"stumped\":0,\"bowled\":0,\"lbw\":0,\"catch\":1,\"pid\":\"28235\"},{\"name\":\"RA Jadeja\",\"runout\":0,\"stumped\":0,\"bowled\":1,\"lbw\":0,\"catch\":0,\"pid\":\"234675\"},{\"name\":\"YS Chahal\",\"runout\":0,\"stumped\":0,\"bowled\":1,\"lbw\":2,\"catch\":0,\"pid\":\"430246\"},{\"name\":\"Mohammed Shami\",\"runout\":0,\"stumped\":0,\"bowled\":1,\"lbw\":0,\"catch\":0,\"pid\":\"481896\"},{\"name\":\"RR Pant\",\"runout\":0,\"stumped\":0,\"bowled\":0,\"lbw\":0,\"catch\":1,\"pid\":\"931581\"}]},{\"title\":\"Fielding for India Innings\",\"scores\":[{\"name\":\"O Thomas\",\"runout\":0,\"stumped\":0,\"bowled\":1,\"lbw\":0,\"catch\":0,\"pid\":\"914567\"}]}],\"bowling\":[{\"scores\":[{\"6s\":3,\"4s\":10,\"0s\":29,\"Econ\":\"8.10\",\"W\":\"2\",\"R\":\"81\",\"M\":\"0\",\"O\":\"10\",\"bowler\":\"Mohammed Shami\",\"pid\":\"481896\"},{\"6s\":0,\"4s\":8,\"0s\":30,\"Econ\":\"6.40\",\"W\":\"0\",\"R\":\"64\",\"M\":\"0\",\"O\":\"10\",\"bowler\":\"UT Yadav\",\"pid\":\"376116\"},{\"6s\":2,\"4s\":6,\"0s\":29,\"Econ\":\"6.40\",\"W\":\"1\",\"R\":\"64\",\"M\":\"0\",\"O\":\"10\",\"bowler\":\"KK Ahmed\",\"pid\":\"942645\"},{\"6s\":1,\"4s\":2,\"0s\":33,\"Econ\":\"4.10\",\"W\":\"3\",\"R\":\"41\",\"M\":\"0\",\"O\":\"10\",\"bowler\":\"YS Chahal\",\"pid\":\"430246\"},{\"6s\":3,\"4s\":4,\"0s\":24,\"Econ\":\"6.60\",\"W\":\"2\",\"R\":\"66\",\"M\":\"0\",\"O\":\"10\",\"bowler\":\"RA Jadeja\",\"pid\":\"234675\"}],\"title\":\"Bowling To West Indies Innings\"},{\"scores\":[{\"6s\":0,\"4s\":4,\"0s\":19,\"Econ\":\"4.80\",\"W\":\"0\",\"R\":\"24\",\"M\":\"0\",\"O\":\"5\",\"bowler\":\"KAJ Roach\",\"pid\":\"230553\"},{\"6s\":1,\"4s\":7,\"0s\":16,\"Econ\":\"8.60\",\"W\":\"1\",\"R\":\"43\",\"M\":\"0\",\"O\":\"4.6\",\"bowler\":\"O Thomas\",\"pid\":\"914567\"},{\"6s\":0,\"4s\":4,\"0s\":18,\"Econ\":\"5.33\",\"W\":\"0\",\"R\":\"32\",\"M\":\"0\",\"O\":\"6\",\"bowler\":\"JO Holder\",\"pid\":\"391485\"},{\"6s\":0,\"4s\":2,\"0s\":4,\"Econ\":\"6.66\",\"W\":\"0\",\"R\":\"20\",\"M\":\"0\",\"O\":\"3\",\"bowler\":\"AR Nurse\",\"pid\":\"315594\"},{\"6s\":2,\"4s\":2,\"0s\":7,\"Econ\":\"9.00\",\"W\":\"0\",\"R\":\"27\",\"M\":\"0\",\"O\":\"3\",\"bowler\":\"D Bishoo\",\"pid\":\"341593\"}],\"title\":\"Bowling To India Innings\"}],\"batting\":[{\"scores\":[{\"dismissal-by\":{\"name\":\"S Dhawan\",\"pid\":\"28235\"},\"dismissal\":\"catch\",\"SR\":130,\"6s\":2,\"4s\":6,\"B\":39,\"R\":51,\"dismissal-info\":\"c Dhawan b Ahmed\",\"batsman\":\"KOA Powell\",\"pid\":\"252932\"},{\"dismissal-by\":{\"name\":\"Mohammed Shami\",\"pid\":\"481896\"},\"dismissal\":\"bowled\",\"SR\":60,\"6s\":0,\"4s\":2,\"B\":15,\"R\":9,\"dismissal-info\":\" b Mohammed Shami\",\"batsman\":\"C Hemraj\",\"pid\":\"530811\"},{\"dismissal-by\":{\"name\":\"MS Dhoni\",\"pid\":\"28081\"},\"dismissal\":\"catch\",\"SR\":62,\"6s\":0,\"4s\":2,\"B\":51,\"R\":32,\"dismissal-info\":\"c  Dhoni b Mohammed Shami\",\"batsman\":\"SD Hope  \",\"pid\":\"581379\"},{\"dismissal-by\":{\"name\":\"YS Chahal\",\"pid\":\"430246\"},\"dismissal\":\"lbw\",\"SR\":0,\"6s\":0,\"4s\":0,\"B\":2,\"R\":0,\"dismissal-info\":\"lbw b Chahal\",\"batsman\":\"MN Samuels\",\"pid\":\"52983\"},{\"dismissal-by\":{\"name\":\"RR Pant\",\"pid\":\"931581\"},\"dismissal\":\"catch\",\"SR\":135,\"6s\":6,\"4s\":6,\"B\":78,\"R\":106,\"dismissal-info\":\"c Pant b Jadeja\",\"batsman\":\"SO Hetmyer\",\"pid\":\"670025\"},{\"dismissal-by\":{\"name\":\"RA Jadeja\",\"pid\":\"234675\"},\"dismissal\":\"bowled\",\"SR\":95,\"6s\":0,\"4s\":4,\"B\":23,\"R\":22,\"dismissal-info\":\" b Jadeja\",\"batsman\":\"R Powell\",\"pid\":\"820351\"},{\"dismissal-by\":{\"name\":\"YS Chahal\",\"pid\":\"430246\"},\"dismissal\":\"bowled\",\"SR\":90,\"6s\":0,\"4s\":5,\"B\":42,\"R\":38,\"dismissal-info\":\" b Chahal\",\"batsman\":\"JO Holder (c)\",\"pid\":\"391485\"},{\"dismissal-by\":{\"name\":\"YS Chahal\",\"pid\":\"430246\"},\"dismissal\":\"lbw\",\"SR\":100,\"6s\":0,\"4s\":0,\"B\":2,\"R\":2,\"dismissal-info\":\"lbw b Chahal\",\"batsman\":\"AR Nurse\",\"pid\":\"315594\"},{\"dismissal\":\"not out\",\"SR\":84,\"6s\":0,\"4s\":3,\"B\":26,\"R\":22,\"dismissal-info\":\"not out\",\"batsman\":\"D Bishoo\",\"pid\":\"341593\"},{\"dismissal\":\"not out\",\"SR\":118,\"6s\":1,\"4s\":2,\"B\":22,\"R\":26,\"dismissal-info\":\"not out\",\"batsman\":\"KAJ Roach\",\"pid\":\"230553\"},{\"SR\":\"\",\"6s\":\"\",\"4s\":\"\",\"B\":\"\",\"R\":\"\",\"dismissal-info\":\"\",\"detail\":\"14 (lb 6, w 8)\",\"batsman\":\"Extras\",\"pid\":0}],\"title\":\"West Indies Innings\"},{\"scores\":[{\"dismissal\":\"not out\",\"SR\":103,\"6s\":3,\"4s\":4,\"B\":52,\"R\":54,\"dismissal-info\":\"not out\",\"batsman\":\"RG Sharma\",\"pid\":\"34102\"},{\"dismissal-by\":{\"name\":\"O Thomas\",\"pid\":\"914567\"},\"dismissal\":\"bowled\",\"SR\":66,\"6s\":0,\"4s\":1,\"B\":6,\"R\":4,\"dismissal-info\":\" b Thomas\",\"batsman\":\"S Dhawan\",\"pid\":\"28235\"},{\"dismissal\":\"not out\",\"SR\":117,\"6s\":0,\"4s\":14,\"B\":74,\"R\":87,\"dismissal-info\":\"not out\",\"batsman\":\"V Kohli (c)\",\"pid\":\"253802\"},{\"SR\":\"\",\"6s\":\"\",\"4s\":\"\",\"B\":\"\",\"R\":\"\",\"dismissal-info\":\"\",\"detail\":\"3 (lb 2, w 1)\",\"batsman\":\"Extras\",\"pid\":0}],\"title\":\"India Innings\"}],\"team\":[{\"players\":[{\"name\":\"RG Sharma\",\"pid\":\"34102\"},{\"name\":\"S Dhawan\",\"pid\":\"28235\"},{\"name\":\"V Kohli\",\"pid\":\"253802\"},{\"name\":\"AT Rayudu\",\"pid\":\"33141\"},{\"name\":\"RR Pant\",\"pid\":\"931581\"},{\"name\":\"MS Dhoni\",\"pid\":\"28081\"},{\"name\":\"RA Jadeja\",\"pid\":\"234675\"},{\"name\":\"YS Chahal\",\"pid\":\"430246\"},{\"name\":\"UT Yadav\",\"pid\":\"376116\"},{\"name\":\"Mohammed Shami\",\"pid\":\"481896\"},{\"name\":\"KK Ahmed\",\"pid\":\"942645\"}],\"name\":\"India\"},{\"players\":[{\"name\":\"KOA Powell\",\"pid\":\"252932\"},{\"name\":\"C Hemraj\",\"pid\":\"530811\"},{\"name\":\"SD Hope\",\"pid\":\"581379\"},{\"name\":\"MN Samuels\",\"pid\":\"52983\"},{\"name\":\"SO Hetmyer\",\"pid\":\"670025\"},{\"name\":\"R Powell\",\"pid\":\"820351\"},{\"name\":\"JO Holder\",\"pid\":\"391485\"},{\"name\":\"AR Nurse\",\"pid\":\"315594\"},{\"name\":\"D Bishoo\",\"pid\":\"341593\"},{\"name\":\"KAJ Roach\",\"pid\":\"230553\"},{\"name\":\"O Thomas\",\"pid\":\"914567\"}],\"name\":\"West Indies\"}],\"man-of-the-match\":\"\",\"toss_winner_team\":\"India\",\"matchStarted\":true},\"type\":\"ODI\",\"cache3\":true,\"creditsLeft\":206,\"v\":\"1\",\"ttl\":9,\"provider\":{\"source\":\"Various\",\"url\":\"https://cricapi.com/\",\"pubDate\":\"2018-10-21T13:49:24.637Z\"}}";
		FantasySummaryResponse response = new Gson().fromJson(fantasySummaryResponse, FantasySummaryResponse.class);
//		FantasyPointDetails points = getPointsFromMatchData(response.getData());
//		System.out.println(points.getPidToFantasyPointMap().size());
//		System.out.println(points.getPidToFantasyPointMap());
		List<Contests> allPastContestsOfAType = new ArrayList<>();
		Map<Integer, Double> map = new HashMap<Integer, Double>();
		map.put(123, 10.0);
		FantasyPointDetails point = new FantasyPointDetails();
		point.setPidToFantasyPointMap(map);
		Contests contest = new Contests();
		contest.setPoints(new Gson().toJson(point));
		allPastContestsOfAType.add(contest);

		map = new HashMap<Integer, Double>();
		map.put(123, 20.0);
		point = new FantasyPointDetails();
		point.setPidToFantasyPointMap(map);
		contest = new Contests();
		contest.setPoints(new Gson().toJson(point));
		allPastContestsOfAType.add(contest);

		map = new HashMap<Integer, Double>();
		map.put(123, 30.0);
		point = new FantasyPointDetails();
		point.setPidToFantasyPointMap(map);
		contest = new Contests();
		contest.setPoints(new Gson().toJson(point));
		allPastContestsOfAType.add(contest);
		Map<Integer, Double> allPrevContestPointMap = new HashMap<>();
		for (Contests contests : allPastContestsOfAType) {
			// points = new Gson().fromJson(contests.getPoints(),
			// FantasyPointDetails.class);
			// Map<Integer, Double> prevContestPointMap = points.getPidToFantasyPointMap();
			// prevContestPointMap.forEach((k, v) -> allPrevContestPointMap.merge(k, v, (v1,
			// v2) -> v1 + v2));
		}
		System.out.println(allPrevContestPointMap);
		Map<Integer, Double> allPoints = new HashMap<>();
		allPoints.put(123, 100.0);
		System.out.println(allPoints);
		allPrevContestPointMap.forEach((k, v) -> allPoints.merge(k, v, (v1, v2) -> v1 - v2));
		System.out.println(allPoints);
	}

	private void updateTimeCrossedForContests(OverDetailsDTO overDetailsDTO, Integer matchId) {
		if(overDetailsDTO != null && overDetailsDTO.getOvers() != null && overDetailsDTO.getInning() != null) {
			List<Contests> contests = powerPlayService.getTimeCrossedContestsForMatchAndEndOversAndInning(matchId,
					overDetailsDTO.getOvers() + 5, overDetailsDTO.getInning());
			for (Contests contests2 : contests) {
				contests2.setTimeCrossed(true);
				powerPlayService.saveContests(contests2);
			}
		}
	}

	private OverDetailsDTO getOverDetailsDTOFromBowlingScore(List<MatchBowlingDTO> bowling, Integer fullMatchOver) {
		OverDetailsDTO overDetailsDTO = new OverDetailsDTO();
		if (CollectionUtils.isNotEmpty(bowling)) {
			if (CollectionUtils.isEmpty(bowling.get(0).getScores())
					&& CollectionUtils.isEmpty(bowling.get(1).getScores())) {
				return overDetailsDTO;
			}
			Integer inning = bowling.size();
			overDetailsDTO.setInning(inning);
			MatchBowlingDTO firstInningBowling = bowling.get(0);
			List<BowlingScoreDTO> scores = firstInningBowling.getScores();
			Integer over = 0;
			Integer balls = 0;
			for (BowlingScoreDTO scoreDTO : scores) {
				String overString = scoreDTO.getO();
				if (StringUtils.isNotEmpty(overString)) {
					String[] overSplit = overString.split("\\.");
					over += Integer.parseInt(overSplit[0]);
					if (overSplit.length == 2) {
						Integer currBall = Integer.parseInt(overSplit[1]);
						if (currBall > 0) {
							balls = currBall;
						}
					}
				}
			}
			overDetailsDTO.setOvers(over);
			overDetailsDTO.setBalls(balls);
			if (over == fullMatchOver) {
				overDetailsDTO.setInning(inning);
				if (bowling.size() == 2) {
					MatchBowlingDTO secondInningBowling = bowling.get(1);
					scores = secondInningBowling.getScores();
					over = 0;
					balls = 0;
					for (BowlingScoreDTO scoreDTO : scores) {
						String overString = scoreDTO.getO();
						String[] overSplit = overString.split("\\.");
						over += Integer.parseInt(overSplit[0]);
						if (overSplit.length == 2) {
							Integer currBall = Integer.parseInt(overSplit[1]);
							if (currBall > 0) {
								balls = currBall;
							}
						}
					}
					if (balls > 6) {
						overDetailsDTO.setOvers(over + 1);
						overDetailsDTO.setBalls(balls - 6);
					} else {
						overDetailsDTO.setOvers(over);
						overDetailsDTO.setBalls(balls);
					}
				}
			} else {
				return overDetailsDTO;
			}
		}
		return overDetailsDTO;
	}

	private String sendPOSTRequest(String requestJson, String url) {
		String response = "";
		HttpSender httpSender = new HttpSender();
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Accept", "application/json");
		headers.put("Content-type", "application/json");
		headers.put("apiKey", "e8d44TT6rkRGHvnrKe13x2TR6862");
		try {
			response = httpSender.executePostContent(url, null, headers, requestJson);
			LOG.info("Cric API POST request : {}, response : {}", requestJson, response);
		} catch (Exception e) {
			LOG.error("Transport Exception while hitting post request {} : and  : ", url, e);
		}
		return response;
	}

	private String sendGETRequest(Map<String, String> params, String url) {
		String response = "";
		HttpSender httpSender = new HttpSender();
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-type", "application/json");
		try {
			response = httpSender.executeGet(url, params, headers);
			LOG.info("Cric API generateAuthToken request :{} and  response : {}", params, response);
		} catch (Exception e) {
			LOG.error("Transport Exception while hitting get request {} : and : ", url, e);
		}
		return response;
	}

	@Override
	public CreateTeamPlayersResponse createTeam(CreateTeamPlayersRequest request, User user) throws MyTeamException {
		validateCreateTeamRequest(request);
		CreateTeamPlayersResponse response = new CreateTeamPlayersResponse();
		UserMatchTeamMapping userMatchTeamMapping = null;
		if (request.getUserMatchTeamMappingId() != null) {
			userMatchTeamMapping = powerPlayService.getUserMatchTeamMappingById(request.getUserMatchTeamMappingId());
			if (userMatchTeamMapping == null) {
				throw new MyTeamException("Not Able to Edit Team");
			}
		} else {
			userMatchTeamMapping = new UserMatchTeamMapping();
			Match match = powerPlayService.getMatchById(request.getMatchId());
			userMatchTeamMapping.setMatch(match);
			userMatchTeamMapping.setUser(user);
		}
		List<String> players = new ArrayList<>();
		for (PlayersDTO dto : request.getPlayers()) {
			players.add(String.valueOf(dto.getPid()));
		}
		List<PlayersDTO> playersDTOs = request.getPlayers();
		String playerIds = String.join(",", players);
		userMatchTeamMapping.setPlayers(playerIds);
		userMatchTeamMapping.setSuperPlayer(String.valueOf(request.getSuperPlayer()));
		Integer totalPreviousTeams = 0;
		List<UserMatchTeamMapping> teams = powerPlayService.getUserMatchTeamMappingByUserIdAndMatchId(user.getId(),
				request.getMatchId());
		if (CollectionUtils.isNotEmpty(teams)) {
			totalPreviousTeams = teams.size();
		}
		if (userMatchTeamMapping.getTeamNumber() == null) {
			userMatchTeamMapping.setTeamNumber(totalPreviousTeams + 1);
		}
		response.setSuperPlayerId(request.getSuperPlayer());
		response.setPlayers(playersDTOs);
		userMatchTeamMapping = powerPlayService.saveUserMatchTeamMapping(userMatchTeamMapping);
		response.setUserMatchTeamMappingId(userMatchTeamMapping.getId());
		return response;
	}

	private void validateCreateTeamRequest(CreateTeamPlayersRequest request) throws MyTeamException {
			Match match = powerPlayService.getMatchById(request.getMatchId());
			if(match != null) {
				if(match.getStartDate().compareTo(new Date()) <= 0) {
					throw new MyTeamException("Deadline is passed");
				}
			}
	}

	@Override
	@Transactional
	public JoinContestResponse joinContest(JoinContestRequest request, User user) throws MyTeamException {
		Wallet wallet = paymentService.getUserWallet(user.getId());
		Contests contest = powerPlayService.getContestsById(request.getContestId());
		if (StringUtils.isEmpty(request.getUmtcCode())) {
			validateJoiningContest(contest, wallet,user);
		} else {
			if (contest.isTimeCrossed()) {
				throw new MyTeamException("No editing allowed");
			}
			LOG.info("No need to validate all as only going to edit");
		}
		UserMatchTeamContestMapping umContestMapping = createContest(request, contest, user);
		if (umContestMapping != null && StringUtils.isEmpty(request.getUmtcCode())) {
			LOG.info("User :{} joined contest successfully", user.getId());
			updateWalletAndContestDetails(wallet, contest);
		}
		JoinContestResponse response = new JoinContestResponse();
		response.setStatus(200);
		response.setMessage("success");
		return response;
	}

	private void updateWalletAndContestDetails(Wallet wallet, Contests contest) {
		LOG.info("Previous wallet balance was :{} and entry fees was :{}", wallet.getAmount(), contest.getEntryFees());
		BigDecimal updatedWalletAmount = wallet.getAmount().subtract(new BigDecimal(contest.getEntryFees()));
		paymentService.createWalletHistory(wallet, WalletType.JOINED_CONTEST.code(),
				new BigDecimal(contest.getEntryFees()), contest);
		wallet.setAmount(updatedWalletAmount);
		wallet = updateDesiredWalletBalance(wallet, contest);
		wallet = paymentService.saveWallet(wallet);
		LOG.info("Updated wallet balance is :{}", wallet.getAmount());
		contest = powerPlayService.getContestsById(contest.getId());
		contest.setPlayingCount(contest.getPlayingCount() + 1);
		powerPlayService.saveContests(contest);
	}

	private Wallet updateDesiredWalletBalance(Wallet wallet, Contests contest) {
		BigDecimal checkWalletAmount = wallet.getDepositedAmount();
		LOG.info("checkWalletAmount is :{}", checkWalletAmount);
		if (checkWalletAmount.compareTo(new BigDecimal(contest.getEntryFees())) == 1
				|| checkWalletAmount.compareTo(new BigDecimal(contest.getEntryFees())) == 0) {
			wallet.setDepositedAmount(wallet.getDepositedAmount().subtract(new BigDecimal(contest.getEntryFees())));
			return wallet;
		}
		checkWalletAmount = checkWalletAmount.add(wallet.getWonAmount());
		LOG.info("checkWalletAmount is :{}", checkWalletAmount);
		if (checkWalletAmount.compareTo(new BigDecimal(contest.getEntryFees())) == 1
				|| checkWalletAmount.compareTo(new BigDecimal(contest.getEntryFees())) == 0) {
			BigDecimal oldDepositedValue = wallet.getDepositedAmount();
			wallet.setDepositedAmount(new BigDecimal(0.0));
			wallet.setWonAmount(
					wallet.getWonAmount().subtract(new BigDecimal(contest.getEntryFees()).subtract(oldDepositedValue)));
			return wallet;
		}
		checkWalletAmount = checkWalletAmount.add(wallet.getBonus());
		LOG.info("checkWalletAmount is :{}", checkWalletAmount);
		if (checkWalletAmount.compareTo(new BigDecimal(contest.getEntryFees())) == 1
				|| checkWalletAmount.compareTo(new BigDecimal(contest.getEntryFees())) == 0) {
			BigDecimal oldDepositedValue = wallet.getDepositedAmount().add(wallet.getWonAmount());
			wallet.setDepositedAmount(new BigDecimal(0.0));
			wallet.setWonAmount(new BigDecimal(0.0));
			wallet.setBonus(
					wallet.getBonus().subtract(new BigDecimal(contest.getEntryFees()).subtract(oldDepositedValue)));
			return wallet;
		}
		return wallet;
	}

	private UserMatchTeamContestMapping createContest(JoinContestRequest request, Contests contest, User user) {
		UserMatchTeamContestMapping umContestMapping = null;
		if (StringUtils.isNotEmpty(request.getUmtcCode())) {
			umContestMapping = powerPlayService.getUserMatchContestPlayersMappingByCode(request.getUmtcCode());
		}
		if (umContestMapping == null) {
			umContestMapping = new UserMatchTeamContestMapping();
			umContestMapping.setUserId(user.getId());
			umContestMapping.setCode(StringUtils.getRandom(10));
			umContestMapping.setContests(contest);
		}
		UserMatchTeamMapping userMatchTeamMapping = powerPlayService
				.getUserMatchTeamMappingById(request.getUserMatchTeamMappingId());
		umContestMapping.setUserMatchTeamMapping(userMatchTeamMapping);
		umContestMapping.setMatch(userMatchTeamMapping.getMatch());
		powerPlayService.saveUserMatchContestPlayersMapping(umContestMapping);
		return umContestMapping;
	}

	private void validateJoiningContest(Contests contest, Wallet wallet, User user) throws MyTeamException {
		if (wallet == null || !wallet.getEnabled()) {
			throw new MyTeamException("Wallet does not exist, please call 9695106880 for any assistance");
		}
		if (contest.isTimeCrossed() || contest.isReviewed() || contest.isContestOver()) {
			throw new MyTeamException("Contest is over");
		}
		if (contest.getTotalCount().equals(contest.getPlayingCount())) {
			throw new MyTeamException("Contest is full");
		}
		if (wallet.getAmount().compareTo(new BigDecimal(contest.getEntryFees())) < 0) {
			throw new MyTeamException("Add money to join contest");
		}
		if(contest.getType().equalsIgnoreCase("Practice")) {
			List<UserMatchTeamContestMapping> contests = powerPlayService.getContestWiseJoinedContests(contest.getId(), user.getId());
			if(contests.size() >=3) {
				throw new MyTeamException("Cant join more than 3 practice contests");
			}
		}
	}

	private List<PlayersDTO> getPlayerDTOFromPIds(List<Integer> pids) {
		List<PlayersDoc> docs = playersMao.getPlayerDocsByPIds(pids);
		Map<Integer, PlayersDoc> pidToPlayerDocMap = new HashMap<>();
		List<PlayersDTO> playersDTOs = new ArrayList<>();
		for (PlayersDoc doc : docs) {
			pidToPlayerDocMap.put(doc.getPid(), doc);
		}
		for (Integer playerId : pids) {
			PlayersDTO playersDTO = new PlayersDTO(pidToPlayerDocMap.get(playerId));
			playersDTOs.add(playersDTO);
		}
		return playersDTOs;
	}

	@Override
	@Transactional
	public void updateWinningAmountUsingRank() {
		List<Contests> allOverAndNonReviewedContests = powerPlayService.getAllNonReviewedContests();
		if (CollectionUtils.isNotEmpty(allOverAndNonReviewedContests)) {
			for (Contests contests : allOverAndNonReviewedContests) {
				updateContestWiseWinningAmount(contests);
				contests.setReviewed(true);
				powerPlayService.saveContests(contests);
			}
		}

	}

	private void updateContestWiseWinningAmount(Contests contests) {
		Integer start = 0;
		Integer maxResults = 200;
		Integer responseSize = maxResults;
		Map<Integer, Integer> rankToCountMap = new HashMap<>();
		while (responseSize == maxResults) {
			List<UserMatchTeamContestMapping> userMatchContestPlayersMappings = powerPlayService
					.getUserMatchContestPlayersMappingByContestId(contests.getId(), start, maxResults, null, null,
							null);
			for (UserMatchTeamContestMapping userMatchTeamContestMapping : userMatchContestPlayersMappings) {
				if (rankToCountMap.get(userMatchTeamContestMapping.getRank()) != null) {
					rankToCountMap.put(userMatchTeamContestMapping.getRank(),
							rankToCountMap.get(userMatchTeamContestMapping.getRank()) + 1);
				} else {
					rankToCountMap.put(userMatchTeamContestMapping.getRank(), 1);
				}
			}
			responseSize = userMatchContestPlayersMappings.size();
			start += userMatchContestPlayersMappings.size();
		}
		processRankMap(contests, rankToCountMap);
	}

	private void processRankMap(Contests contests, Map<Integer, Integer> rankToCountMap) {
		LOG.info("Rank to count map is :{} for contest :{}", rankToCountMap, contests.getId());
		Integer processedRanks = 0;
		Integer lowestRank  = contests.getWinnerCount();
		LOG.info("Lowest rank is :{} for contest :{}", lowestRank, contests.getId());
		for (Map.Entry<Integer, Integer> entry : rankToCountMap.entrySet()) {
			Integer rank = entry.getKey();
			try {
				LOG.info("processedRanks rank is :{}", processedRanks);
				if (processedRanks.equals(lowestRank)) {
					return;
				}
				List<UserMatchTeamContestMapping> userMatchContestPlayersMappings = powerPlayService
						.getUserMatchContestPlayersMappingByContestId(contests.getId(), null, null, null, null, rank);
				if (CollectionUtils.isNotEmpty(userMatchContestPlayersMappings)) {
					List<Integer> umcpmIds = new ArrayList<>();
					List<Integer> userIds = new ArrayList<>();
					Integer totalAmount = 0;
					for (UserMatchTeamContestMapping userMatchContestPlayersMapping : userMatchContestPlayersMappings) {
						umcpmIds.add(userMatchContestPlayersMapping.getId());
						userIds.add(userMatchContestPlayersMapping.getUserId());
						processedRanks += 1;
						Integer rankAmount = getWinningAmountByRank(contests, processedRanks);
						LOG.info("Winning amount for rank :{} is :{}", processedRanks, rankAmount);
						totalAmount += rankAmount;
					}
					Integer winningAmount = totalAmount / umcpmIds.size();
					if (CollectionUtils.isNotEmpty(umcpmIds)) {
						powerPlayService.updateUMTCMWinningAmountByIds(winningAmount, umcpmIds);
						powerPlayService.updateWinningAmountInWallet(winningAmount, userMatchContestPlayersMappings);
					}
				}
			} catch (Exception e) {
				LOG.error("Error while processing rank :{} for contest :{} and error :{}", rank, contests.getId(), e);
			}
		}
	}

	private Integer getWinningAmountByRank(Contests contests, Integer rank) {
		List<ContestWinningDistribution> winnings = contests.getContestWinnerRank();
		for (ContestWinningDistribution winning : winnings) {
			if (rank >= winning.getStartRank() && rank <= winning.getEndRank()) {
				return winning.getWinningAmount();
			}
		}
		return 0;
	}
	
	@Override
	@Transactional
	public void updatePlayingEleven() {
		List<Match> matches = powerPlayService.getMatchesWithoutPlayingEleven();
		for(Match match: matches) {
			LOG.info("Processing for matchUniqueId :{}", match.getMatchUniqueId());
			GetMatchStatsRequest request = new GetMatchStatsRequest();
			request.setUnique_id(match.getMatchUniqueId());
			String getFantasySummaryRequest = new Gson().toJson(request);
			String fantasySummaryResponse = sendPOSTRequest(getFantasySummaryRequest,
					"http://cricapi.com/api/fantasySummary");
			FantasySummaryResponse response = new Gson().fromJson(fantasySummaryResponse, FantasySummaryResponse.class);
			MatchSummaryResponse data = response.getData();
			if(data != null) {
				if(CollectionUtils.isNotEmpty(data.getTeam()) && CollectionUtils.isNotEmpty(data.getTeam().get(0).getPlayers())
						&& CollectionUtils.isNotEmpty(data.getTeam().get(1).getPlayers()) ) {
					PlayingElevenDTO playingTeam = new PlayingElevenDTO();
					playingTeam.setTeam(response.getData().getTeam());
					match.setPlayingEleven(new Gson().toJson(playingTeam));
				}
				if(StringUtils.isEmpty(match.getTossWinnerTeam())) {
					match.setTossWinnerTeam(data.getTossWinnerTeam());
				}
				powerPlayService.saveMatch(match);
			}
		}
	}
}
