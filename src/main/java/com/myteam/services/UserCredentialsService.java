package com.myteam.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.myteam.dao.IUserDao;
import com.myteam.entity.UtilCredentials;

@Service("userCredentialService")
public class UserCredentialsService {
	
	@Autowired
	private IUserDao userDao;
	
	@Transactional()
	public UtilCredentials getUtilCredentials(String type, String host) {
		return userDao.getUtilCredentials(type, host);
	}

}
