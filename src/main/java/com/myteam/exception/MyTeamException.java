package com.myteam.exception;

public class MyTeamException extends Exception {

	public enum ErrorCode {
		
		DUPLICATE_USER(800, "Mobile/ Email already registered"),
		PASSWORD_NOT_MATCH(801, "Both password is not matching"),
		USER_ALREADY_EXISTS(802, "User Already exists with same details"),
		NAME_CANT_BE_EMPTY(803, "Name cant be empty"),
		MOBILE_NUMBER_INVALID(804, "Mobile Number Invalid"),
		PASSWORD_EMPLTY(805, "Password cant be empty"),
		
		NO_MATCH_FOUND(900, "No Such Match"),
		NO_CONTEST_FOUND(901, "No Open Contest"),
		CONTEST_INVALID(902, "Contest is not open anymore");

		private int code;
		private String description;

		private ErrorCode(int code, String description) {
			this.code = code;
			this.description = description;
		}

		public int code() {
			return this.code;
		}

		public String description() {
			return this.description;
		}
	}

	private int code;
	private String message;

	public MyTeamException(int code, String message, Throwable cause) {
		super(message, cause);
		this.code = code;
	}

	public MyTeamException(ErrorCode errorCode, Throwable cause) {
		this(errorCode.code(), errorCode.description(), cause);
	}

	public MyTeamException(ErrorCode errorCode) {
		this.code = errorCode.code();
		this.message = errorCode.description();
	}

	public MyTeamException(String message) {
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}
}
