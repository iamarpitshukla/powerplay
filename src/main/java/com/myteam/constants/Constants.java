package com.myteam.constants;

import java.math.BigDecimal;

public class Constants {

	public static String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
	// Fielding points
	public static Double RUNOUT = 3d;
	public static Double CATCH = 3d;
	public static Double STUMPED = 3d;
	// Batting points
	public static Double RUN = 0.5d;
	public static Double FOURS = 1d;
	public static Double SIXES = 2d;
	public static Double STRIKE_RATE = 2d;
	public static Double FIFTIES = 5d;
	// Bowling points
	public static Double WICKET = 10d;
	public static Double ECONOMY = 5d;
	public static Double MAIDEN = 5d;
	
	public static Double SUPER_PLAYER_BOOST = 2d;
	
	public static String RANK_STRING = "RANK";
	public static BigDecimal REFERRAL_BONUS = new BigDecimal(100.0);
	public static String RUNNING_MATCH = "Running";

}
