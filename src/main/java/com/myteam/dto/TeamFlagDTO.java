package com.myteam.dto;

public class TeamFlagDTO {

	private String name;
	
	private String flag;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	@Override
	public String toString() {
		return "TeamFlagDTO [name=" + name + ", flag=" + flag + "]";
	}
}
