package com.myteam.dto;

import java.util.ArrayList;
import java.util.List;

public class FavouritePlayerDTO {

	private List<InningWiseFavouritePlayers> favouritePlayers = new ArrayList<>();

	public List<InningWiseFavouritePlayers> getFavouritePlayers() {
		return favouritePlayers;
	}

	public void setFavouritePlayers(List<InningWiseFavouritePlayers> favouritePlayers) {
		this.favouritePlayers = favouritePlayers;
	}

	@Override
	public String toString() {
		return "FavouritePlayerDTO [favouritePlayers=" + favouritePlayers + "]";
	}
}
