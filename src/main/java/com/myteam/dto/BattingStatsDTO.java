package com.myteam.dto;

import com.google.gson.annotations.SerializedName;

public class BattingStatsDTO {

	@SerializedName("50")
	private String fifties;
	
	@SerializedName("100")
	private String hundreds;
	
	private String St;
	
	private String Ct;
	
	@SerializedName("6s")
	private String sixes;
	
	@SerializedName("4s")
	private String foures;
	
	private String SR;
	
	private String BF;
	
	private String Ave;
	
	private String HS;
	
	private String Runs;
	
	@SerializedName("NO")
	private String notOuts;
	
	@SerializedName("Inns")
	private String innings;
	
	@SerializedName("Mat")
	private String matches;

	public String getFifties() {
		return fifties;
	}

	public void setFifties(String fifties) {
		this.fifties = fifties;
	}

	public String getHundreds() {
		return hundreds;
	}

	public void setHundreds(String hundreds) {
		this.hundreds = hundreds;
	}

	public String getSt() {
		return St;
	}

	public void setSt(String st) {
		St = st;
	}

	public String getCt() {
		return Ct;
	}

	public void setCt(String ct) {
		Ct = ct;
	}

	public String getSixes() {
		return sixes;
	}

	public void setSixes(String sixes) {
		this.sixes = sixes;
	}

	public String getFoures() {
		return foures;
	}

	public void setFoures(String foures) {
		this.foures = foures;
	}

	public String getSR() {
		return SR;
	}

	public void setSR(String sR) {
		SR = sR;
	}

	public String getBF() {
		return BF;
	}

	public void setBF(String bF) {
		BF = bF;
	}

	public String getAve() {
		return Ave;
	}

	public void setAve(String ave) {
		Ave = ave;
	}

	public String getHS() {
		return HS;
	}

	public void setHS(String hS) {
		HS = hS;
	}

	public String getRuns() {
		return Runs;
	}

	public void setRuns(String runs) {
		Runs = runs;
	}

	public String getNotOuts() {
		return notOuts;
	}

	public void setNotOuts(String notOuts) {
		this.notOuts = notOuts;
	}

	public String getInnings() {
		return innings;
	}

	public void setInnings(String innings) {
		this.innings = innings;
	}

	public String getMatches() {
		return matches;
	}

	public void setMatches(String matches) {
		this.matches = matches;
	}

	@Override
	public String toString() {
		return "BattingStatsDTO [fifties=" + fifties + ", hundreds=" + hundreds + ", St=" + St + ", Ct=" + Ct
				+ ", sixes=" + sixes + ", foures=" + foures + ", SR=" + SR + ", BF=" + BF + ", Ave=" + Ave + ", HS="
				+ HS + ", Runs=" + Runs + ", notOuts=" + notOuts + ", innings=" + innings + ", matches=" + matches
				+ "]";
	}
}
