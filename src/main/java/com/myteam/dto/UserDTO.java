package com.myteam.dto;

import javax.validation.constraints.NotNull;

public class UserDTO {

	@NotNull
	private String password;
	
	@NotNull
	private String username;
	
	@NotNull
	private String repeatPassword;
	
	@NotNull
	private String name;
	
	private String email;
	
	private String os;
	
	private String referralCode;
	
	public String getReferralCode() {
		return referralCode;
	}

	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public String getRepeatPassword() {
		return repeatPassword;
	}

	public void setRepeatPassword(String repeatPassword) {
		this.repeatPassword = repeatPassword;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "UserDTO [password=" + password + ", username=" + username + ", repeatPassword=" + repeatPassword
				+ ", name=" + name + ", email=" + email + "]";
	}

}
