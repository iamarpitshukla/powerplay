package com.myteam.dto;

import com.google.gson.annotations.SerializedName;

public class BowlingStatsDTO {
	
	@SerializedName("10")
	private String tenW;
	
	@SerializedName("5w")
	private String fiveW;
	
	@SerializedName("4w")
	private String fourW;
	
	@SerializedName("SR")
	private String strikeRate;
	
	@SerializedName("Econ")
	private String economyRate;
	
	@SerializedName("Ave")
	private String average;
	
	private String BBM;
	
	private String BBI;
	
	@SerializedName("Wkts")
	private String wickets;
	
	@SerializedName("Runs")
	private String runs;
	
	@SerializedName("Balls")
	private String balls;
	
	@SerializedName("Inns")
	private String innings;
	
	@SerializedName("Mat")
	private String matches;

	public String getTenW() {
		return tenW;
	}

	public void setTenW(String tenW) {
		this.tenW = tenW;
	}

	public String getFiveW() {
		return fiveW;
	}

	public void setFiveW(String fiveW) {
		this.fiveW = fiveW;
	}

	public String getFourW() {
		return fourW;
	}

	public void setFourW(String fourW) {
		this.fourW = fourW;
	}

	public String getStrikeRate() {
		return strikeRate;
	}

	public void setStrikeRate(String strikeRate) {
		this.strikeRate = strikeRate;
	}

	public String getEconomyRate() {
		return economyRate;
	}

	public void setEconomyRate(String economyRate) {
		this.economyRate = economyRate;
	}

	public String getAverage() {
		return average;
	}

	public void setAverage(String average) {
		this.average = average;
	}

	public String getBBM() {
		return BBM;
	}

	public void setBBM(String bBM) {
		BBM = bBM;
	}

	public String getBBI() {
		return BBI;
	}

	public void setBBI(String bBI) {
		BBI = bBI;
	}

	public String getWickets() {
		return wickets;
	}

	public void setWickets(String wickets) {
		this.wickets = wickets;
	}

	public String getRuns() {
		return runs;
	}

	public void setRuns(String runs) {
		this.runs = runs;
	}

	public String getBalls() {
		return balls;
	}

	public void setBalls(String balls) {
		this.balls = balls;
	}

	public String getInnings() {
		return innings;
	}

	public void setInnings(String innings) {
		this.innings = innings;
	}

	public String getMatches() {
		return matches;
	}

	public void setMatches(String matches) {
		this.matches = matches;
	}

	@Override
	public String toString() {
		return "BowlingStatsDTO [tenW=" + tenW + ", fiveW=" + fiveW + ", fourW=" + fourW + ", strikeRate=" + strikeRate
				+ ", economyRate=" + economyRate + ", average=" + average + ", BBM=" + BBM + ", BBI=" + BBI
				+ ", wickets=" + wickets + ", runs=" + runs + ", balls=" + balls + ", innings=" + innings + ", matches="
				+ matches + "]";
	}
}
