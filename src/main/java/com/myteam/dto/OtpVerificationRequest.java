package com.myteam.dto;

public class OtpVerificationRequest {
	
	private String otp;
	
	private String mobile;
	
	private String email;
	
	private String referralCode;
	
	public String getReferralCode() {
		return referralCode;
	}

	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	@Override
	public String toString() {
		return "OtpVerificationRequest [otp=" + otp + ", mobile=" + mobile + ", email=" + email + ", referralCode="
				+ referralCode + "]";
	}
}
