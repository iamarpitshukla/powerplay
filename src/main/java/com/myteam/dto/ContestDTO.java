package com.myteam.dto;

import com.myteam.entity.Contests;

public class ContestDTO {
	
	private Integer contestId;
	
	private Integer playingCount;
	
	private Integer totalCount;
	
	private Integer entryFees;
	
	private Integer totalPrize;
	
	private String displayContest;
	
	private boolean contestOver;
	
	private boolean timePassed;
	
	private String type;
	
	private Integer winnerCount;
	
	public Integer getWinnerCount() {
		return winnerCount;
	}

	public void setWinnerCount(Integer winnerCount) {
		this.winnerCount = winnerCount;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public ContestDTO() {
		super();
	}

	public ContestDTO(Contests contests) {
		this.contestId = contests.getId();
		this.playingCount = contests.getPlayingCount();
		this.totalCount = contests.getTotalCount();
		this.entryFees = contests.getEntryFees();
		this.totalPrize = contests.getPrizePool();
		this.displayContest = contests.getStartOver() +" - " + contests.getEndOver() +" overs" + " ("+contests.getInning()+" inning)";
		this.contestOver = contests.isContestOver();
		this.timePassed = contests.isTimeCrossed();
		this.type = contests.getType();
		this.winnerCount = contests.getWinnerCount();
	}
	
	public String getDisplayContest() {
		return displayContest;
	}

	public void setDisplayContest(String displayContest) {
		this.displayContest = displayContest;
	}

	public Integer getContestId() {
		return contestId;
	}

	public void setContestId(Integer contestId) {
		this.contestId = contestId;
	}

	public Integer getPlayingCount() {
		return playingCount;
	}

	public void setPlayingCount(Integer playingCount) {
		this.playingCount = playingCount;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public Integer getEntryFees() {
		return entryFees;
	}

	public void setEntryFees(Integer entryFees) {
		this.entryFees = entryFees;
	}

	public Integer getTotalPrize() {
		return totalPrize;
	}

	public void setTotalPrize(Integer totalPrize) {
		this.totalPrize = totalPrize;
	}
	
	public boolean isContestOver() {
		return contestOver;
	}

	public void setContestOver(boolean contestOver) {
		this.contestOver = contestOver;
	}

	public boolean isTimePassed() {
		return timePassed;
	}

	public void setTimePassed(boolean timePassed) {
		this.timePassed = timePassed;
	}

	@Override
	public String toString() {
		return "ContestDTO [contestId=" + contestId + ", playingCount=" + playingCount + ", totalCount=" + totalCount
				+ ", entryFees=" + entryFees + ", totalPrize=" + totalPrize + ", displayContest=" + displayContest
				+ ", contestOver=" + contestOver + ", timePassed=" + timePassed + "]";
	}
}
