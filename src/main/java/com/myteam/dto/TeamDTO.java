package com.myteam.dto;

import java.util.ArrayList;
import java.util.List;

public class TeamDTO {

	private List<PlayersDTO> players =  new ArrayList<>();
	
	private String name;

	public List<PlayersDTO> getPlayers() {
		return players;
	}

	public void setPlayers(List<PlayersDTO> players) {
		this.players = players;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
