package com.myteam.dto;

import java.util.ArrayList;
import java.util.List;

public class MatchBowlingDTO {

	private String title;
	
	private List<BowlingScoreDTO> scores = new ArrayList<>();
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public List<BowlingScoreDTO> getScores() {
		return scores;
	}
	public void setScores(List<BowlingScoreDTO> scores) {
		this.scores = scores;
	}
	@Override
	public String toString() {
		return "MatchBowlingDTO [title=" + title + ", scores=" + scores + "]";
	}
}
