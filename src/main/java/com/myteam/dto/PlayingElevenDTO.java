package com.myteam.dto;

import java.util.ArrayList;
import java.util.List;

public class PlayingElevenDTO {

	private List<TeamDTO> team = new ArrayList<>();

	public List<TeamDTO> getTeam() {
		return team;
	}

	public void setTeam(List<TeamDTO> team) {
		this.team = team;
	}

	@Override
	public String toString() {
		return "PlayingElevenDTO [team=" + team + "]";
	}
}
