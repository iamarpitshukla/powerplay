package com.myteam.dto;

public class BowlingScoreDTO {
	
	private String Econ;
	
	private String W;
	
	private String M; // maiden over
	
	private String bowler;
	
	private String pid;
	
	private String O;
	
	public String getO() {
		return O;
	}

	public void setO(String o) {
		O = o;
	}

	public String getM() {
		return M;
	}

	public void setM(String m) {
		M = m;
	}

	public String getEcon() {
		return Econ;
	}

	public void setEcon(String econ) {
		Econ = econ;
	}

	public String getW() {
		return W;
	}

	public void setW(String w) {
		W = w;
	}

	public String getBowler() {
		return bowler;
	}

	public void setBowler(String bowler) {
		this.bowler = bowler;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	@Override
	public String toString() {
		return "BowlingScoreDTO [Econ=" + Econ + ", W=" + W + ", M=" + M + ", bowler=" + bowler + ", pid=" + pid
				+ ", O=" + O + "]";
	}
}
