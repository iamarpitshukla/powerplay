package com.myteam.dto;

import java.util.Date;

import com.google.gson.annotations.SerializedName;
import com.myteam.entity.Match;
import com.myteam.utils.StringUtils;

public class MatchDTO {
	
	private Integer unique_id;
	
    @SerializedName("team-2")
	private String team2;
	
    @SerializedName("team-1")
	private String team1;
    
    private String type;
    
    private String date;
    
    private String dateTimeGMT;
    
    private boolean squad;
    
    private boolean contestOpen;
    
    private String toss_winner_team;
    
    private boolean matchStarted;
    
    private String winner_team;
    
    private Integer matchId;
    
    private Date startTime;
    
    private String team1ShortName;
    
    private String team2ShortName;
    
    private String team1Flag;
    
    private String team2Flag;
    
    private String seriesName;
    
    private Integer contestJoined;
    
    private String timeLeft;
    
    public String getTimeLeft() {
		return timeLeft;
	}

	public void setTimeLeft(String timeLeft) {
		this.timeLeft = timeLeft;
	}

	public boolean isContestOpen() {
		return contestOpen;
	}

	public void setContestOpen(boolean contestOpen) {
		this.contestOpen = contestOpen;
	}

	public Integer getContestJoined() {
		return contestJoined;
	}

	public void setContestJoined(Integer contestJoined) {
		this.contestJoined = contestJoined;
	}

	public String getSeriesName() {
		return seriesName;
	}

	public void setSeriesName(String seriesName) {
		this.seriesName = seriesName;
	}

	public String getTeam1Flag() {
		return team1Flag;
	}

	public void setTeam1Flag(String team1Flag) {
		this.team1Flag = team1Flag;
	}

	public String getTeam2Flag() {
		return team2Flag;
	}

	public void setTeam2Flag(String team2Flag) {
		this.team2Flag = team2Flag;
	}

	public String getTeam1ShortName() {
		return team1ShortName;
	}

	public void setTeam1ShortName(String team1ShortName) {
		this.team1ShortName = team1ShortName;
	}

	public String getTeam2ShortName() {
		return team2ShortName;
	}

	public void setTeam2ShortName(String team2ShortName) {
		this.team2ShortName = team2ShortName;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Integer getMatchId() {
		return matchId;
	}

	public void setMatchId(Integer matchId) {
		this.matchId = matchId;
	}

	public MatchDTO() {
    	super();
    }
    
    public MatchDTO(Match match) {
    	this.unique_id = match.getMatchUniqueId();
		this.squad = match.isSquad();
		this.type = match.getSubType();
		this.team1 = match.getTeam1();
		this.team2 = match.getTeam2();
		this.matchStarted = match.isMatchStarted();
		this.toss_winner_team =match.getTossWinnerTeam();
		this.winner_team = match.getWinnerTeam();
		this.matchId = match.getId();
		this.startTime = match.getStartDate();
		this.team1ShortName = StringUtils.getNameInitials(match.getTeam1());
		this.team2ShortName = StringUtils.getNameInitials(match.getTeam2());
		this.timeLeft = StringUtils.calculateRemainTime(this.startTime);
    }
    
	public String getWinner_team() {
		return winner_team;
	}

	public void setWinner_team(String winner_team) {
		this.winner_team = winner_team;
	}

	public Integer getUnique_id() {
		return unique_id;
	}

	public void setUnique_id(Integer unique_id) {
		this.unique_id = unique_id;
	}

	public String getTeam2() {
		return team2;
	}

	public void setTeam2(String team2) {
		this.team2 = team2;
	}

	public String getTeam1() {
		return team1;
	}

	public void setTeam1(String team1) {
		this.team1 = team1;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDateTimeGMT() {
		return dateTimeGMT;
	}

	public void setDateTimeGMT(String dateTimeGMT) {
		this.dateTimeGMT = dateTimeGMT;
	}

	public boolean isSquad() {
		return squad;
	}

	public void setSquad(boolean squad) {
		this.squad = squad;
	}

	public String getToss_winner_team() {
		return toss_winner_team;
	}

	public void setToss_winner_team(String toss_winner_team) {
		this.toss_winner_team = toss_winner_team;
	}

	public boolean isMatchStarted() {
		return matchStarted;
	}

	public void setMatchStarted(boolean matchStarted) {
		this.matchStarted = matchStarted;
	}

	@Override
	public String toString() {
		return "MatchDTO [unique_id=" + unique_id + ", team2=" + team2 + ", team1=" + team1 + ", type=" + type
				+ ", date=" + date + ", dateTimeGMT=" + dateTimeGMT + ", squad=" + squad + ", contestOpen="
				+ contestOpen + ", toss_winner_team=" + toss_winner_team + ", matchStarted=" + matchStarted
				+ ", winner_team=" + winner_team + ", matchId=" + matchId + ", startTime=" + startTime
				+ ", team1ShortName=" + team1ShortName + ", team2ShortName=" + team2ShortName + ", team1Flag="
				+ team1Flag + ", team2Flag=" + team2Flag + ", seriesName=" + seriesName + ", contestJoined="
				+ contestJoined + ", timeLeft=" + timeLeft + "]";
	}
}
