package com.myteam.dto;

import java.util.ArrayList;
import java.util.List;

public class InningWiseFavouritePlayers {
	
	private List<Integer> pids = new ArrayList<>();
	
	private List<Integer> superPids = new ArrayList<>();
	
	public List<Integer> getSuperPids() {
		return superPids;
	}

	public void setSuperPids(List<Integer> superPids) {
		this.superPids = superPids;
	}

	public List<Integer> getPids() {
		return pids;
	}

	public void setPids(List<Integer> pids) {
		this.pids = pids;
	}

	@Override
	public String toString() {
		return "InningWiseFavouritePlayers [pids=" + pids + ", superPids=" + superPids + "]";
	}
}
