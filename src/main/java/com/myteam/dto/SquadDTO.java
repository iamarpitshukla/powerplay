package com.myteam.dto;

import java.util.ArrayList;
import java.util.List;

public class SquadDTO {

	List<PlayersDTO> players = new ArrayList<>();
	
	private String name;
	
	private String shortName;
	
	public SquadDTO() {
		
	}

	public SquadDTO(TeamDTO team) {
		this.players = team.getPlayers();
		this.name = team.getName();
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<PlayersDTO> getPlayers() {
		return players;
	}

	public void setPlayers(List<PlayersDTO> players) {
		this.players = players;
	}

	@Override
	public String toString() {
		return "SquadDTO [players=" + players + ", name=" + name + ", shortName=" + shortName + "]";
	}
}
