package com.myteam.dto;

import java.util.HashMap;
import java.util.Map;

public class FantasyPointDetails {

	Map<Integer, Double> pidToFantasyPointMap = new HashMap<>();
	
	Map<Integer, Map<Integer, Double>> inningWisePoints = new HashMap<>();
	
	public Map<Integer, Map<Integer, Double>> getInningWisePoints() {
		return inningWisePoints;
	}

	public void setInningWisePoints(Map<Integer, Map<Integer, Double>> inningWisePoints) {
		this.inningWisePoints = inningWisePoints;
	}

	private OverDetailsDTO overDetailsDTO = new OverDetailsDTO();
	
	public OverDetailsDTO getOverDetailsDTO() {
		return overDetailsDTO;
	}

	public void setOverDetailsDTO(OverDetailsDTO overDetailsDTO) {
		this.overDetailsDTO = overDetailsDTO;
	}

	public Map<Integer, Double> getPidToFantasyPointMap() {
		return pidToFantasyPointMap;
	}

	public void setPidToFantasyPointMap(Map<Integer, Double> pidToFantasyPointMap) {
		this.pidToFantasyPointMap = pidToFantasyPointMap;
	}

	@Override
	public String toString() {
		return "FantasyPointDetails [pidToFantasyPointMap=" + pidToFantasyPointMap + ", inningWisePoints="
				+ inningWisePoints + ", overDetailsDTO=" + overDetailsDTO + "]";
	}
}
