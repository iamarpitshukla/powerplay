package com.myteam.dto;

import com.myteam.doc.PlayersDoc;
import com.myteam.utils.StringUtils;

public class PlayersDTO {

	private String name;
	
	private Integer pid;
	
	private String imageUrl;
	
	private String playingRole;
	
	private String battingStyle;
	
	private String bowlingStyle;
	
	private boolean isSuperPlayer;
	
	private String team;
	
	private boolean picAvailable = false;
	
	private String shortName;
	
	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public boolean isPicAvailable() {
		return picAvailable;
	}

	public void setPicAvailable(boolean picAvailable) {
		this.picAvailable = picAvailable;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	public boolean isSuperPlayer() {
		return isSuperPlayer;
	}

	public void setSuperPlayer(boolean isSuperPlayer) {
		this.isSuperPlayer = isSuperPlayer;
	}

	public PlayersDTO() {
		super();
	}
	
	public PlayersDTO(PlayersDoc doc) {
		super();
		if(doc != null) {
			this.name = doc.getName();
			this.pid = doc.getPid();
			this.imageUrl = doc.getImageURL();
			this.playingRole = doc.getPlayingRole();
			this.battingStyle = doc.getBattingStyle();
			this.bowlingStyle = doc.getBowlingStyle();
			if(StringUtils.isNotEmpty(doc.getImageURL())) {
				this.picAvailable = true;
			}
			this.shortName = StringUtils.getShortName(doc.getName());
		}
	}
	
	public String getPlayingRole() {
		return playingRole;
	}

	public void setPlayingRole(String playingRole) {
		this.playingRole = playingRole;
	}

	public String getBattingStyle() {
		return battingStyle;
	}

	public void setBattingStyle(String battingStyle) {
		this.battingStyle = battingStyle;
	}

	public String getBowlingStyle() {
		return bowlingStyle;
	}

	public void setBowlingStyle(String bowlingStyle) {
		this.bowlingStyle = bowlingStyle;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getPid() {
		return pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}

	@Override
	public String toString() {
		return "PlayersDTO [name=" + name + ", pid=" + pid + ", imageUrl=" + imageUrl + ", playingRole=" + playingRole
				+ ", battingStyle=" + battingStyle + ", bowlingStyle=" + bowlingStyle + ", isSuperPlayer="
				+ isSuperPlayer + ", team=" + team + "]";
	}
}
