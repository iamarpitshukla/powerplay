package com.myteam.dto;

import com.google.gson.annotations.SerializedName;

public class BattingScoreDTO {
	
	private String SR;
	
	@SerializedName("6s")
	private String sixes;
	
	@SerializedName("4s")
	private String fours;
	
	private String R;
	
	private String batsman;
	
	private String pid;
	
	private String detail;
	
	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getSR() {
		return SR;
	}

	public void setSR(String sR) {
		SR = sR;
	}

	public String getSixes() {
		return sixes;
	}

	public void setSixes(String sixes) {
		this.sixes = sixes;
	}

	public String getFours() {
		return fours;
	}

	public void setFours(String fours) {
		this.fours = fours;
	}

	public String getR() {
		return R;
	}

	public void setR(String r) {
		R = r;
	}

	public String getBatsman() {
		return batsman;
	}

	public void setBatsman(String batsman) {
		this.batsman = batsman;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	@Override
	public String toString() {
		return "BattingScoreDTO [SR=" + SR + ", sixes=" + sixes + ", fours=" + fours + ", R=" + R + ", batsman="
				+ batsman + ", pid=" + pid + ", detail=" + detail + "]";
	}
}
