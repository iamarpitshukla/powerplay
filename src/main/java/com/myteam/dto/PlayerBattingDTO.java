package com.myteam.dto;

public class PlayerBattingDTO {
	
	private BattingStatsDTO listA;
	
	private BattingStatsDTO firstClass;
	
	private BattingStatsDTO T20Is;
	
	private BattingStatsDTO ODIs;
	
	private BattingStatsDTO tests;

	public BattingStatsDTO getListA() {
		return listA;
	}

	public void setListA(BattingStatsDTO listA) {
		this.listA = listA;
	}

	public BattingStatsDTO getFirstClass() {
		return firstClass;
	}

	public void setFirstClass(BattingStatsDTO firstClass) {
		this.firstClass = firstClass;
	}

	public BattingStatsDTO getT20Is() {
		return T20Is;
	}

	public void setT20Is(BattingStatsDTO t20Is) {
		T20Is = t20Is;
	}

	public BattingStatsDTO getODIs() {
		return ODIs;
	}

	public void setODIs(BattingStatsDTO oDIs) {
		ODIs = oDIs;
	}

	public BattingStatsDTO getTests() {
		return tests;
	}

	public void setTests(BattingStatsDTO tests) {
		this.tests = tests;
	}

	@Override
	public String toString() {
		return "PlayerBattingDTO [listA=" + listA + ", firstClass=" + firstClass + ", T20Is=" + T20Is + ", ODIs=" + ODIs
				+ ", tests=" + tests + "]";
	}
}
