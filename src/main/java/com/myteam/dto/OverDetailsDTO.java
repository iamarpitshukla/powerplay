package com.myteam.dto;

public class OverDetailsDTO {

	private Integer inning;

	private Integer overs;

	private Integer balls;
	
	private Integer matchId;
	
	public Integer getMatchId() {
		return matchId;
	}

	public void setMatchId(Integer matchId) {
		this.matchId = matchId;
	}

	public Integer getInning() {
		return inning;
	}

	public void setInning(Integer inning) {
		this.inning = inning;
	}

	public Integer getOvers() {
		return overs;
	}

	public void setOvers(Integer overs) {
		this.overs = overs;
	}

	public Integer getBalls() {
		return balls;
	}

	public void setBalls(Integer balls) {
		this.balls = balls;
	}

	@Override
	public String toString() {
		return "OverDetailsDTO [inning=" + inning + ", overs=" + overs + ", balls=" + balls + ", matchId=" + matchId
				+ "]";
	}
}
