package com.myteam.dto;

public class PlayerBowlingDTO {

	private BowlingStatsDTO listA;

	private BowlingStatsDTO firstClass;

	private BowlingStatsDTO T20Is;

	private BowlingStatsDTO ODIs;

	private BowlingStatsDTO tests;

	public BowlingStatsDTO getListA() {
		return listA;
	}

	public void setListA(BowlingStatsDTO listA) {
		this.listA = listA;
	}

	public BowlingStatsDTO getFirstClass() {
		return firstClass;
	}

	public void setFirstClass(BowlingStatsDTO firstClass) {
		this.firstClass = firstClass;
	}

	public BowlingStatsDTO getT20Is() {
		return T20Is;
	}

	public void setT20Is(BowlingStatsDTO t20Is) {
		T20Is = t20Is;
	}

	public BowlingStatsDTO getODIs() {
		return ODIs;
	}

	public void setODIs(BowlingStatsDTO oDIs) {
		ODIs = oDIs;
	}

	public BowlingStatsDTO getTests() {
		return tests;
	}

	public void setTests(BowlingStatsDTO tests) {
		this.tests = tests;
	}

	@Override
	public String toString() {
		return "PlayerBowlingDTO [listA=" + listA + ", firstClass=" + firstClass + ", T20Is=" + T20Is + ", ODIs=" + ODIs
				+ ", tests=" + tests + "]";
	}
}
