package com.myteam.dto;

import com.google.gson.annotations.SerializedName;

public class FieldingScoreDTO {

	private String name;

	private String pid;

	private Integer runout;

	private Integer stumped;

	@SerializedName("catch")
	private Integer catchCount;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public Integer getRunout() {
		return runout;
	}

	public void setRunout(Integer runout) {
		this.runout = runout;
	}

	public Integer getStumped() {
		return stumped;
	}

	public void setStumped(Integer stumped) {
		this.stumped = stumped;
	}

	public Integer getCatchCount() {
		return catchCount;
	}

	public void setCatchCount(Integer catchCount) {
		this.catchCount = catchCount;
	}

	@Override
	public String toString() {
		return "FieldingScoreDTO [name=" + name + ", pid=" + pid + ", runout=" + runout + ", stumped=" + stumped
				+ ", catchCount=" + catchCount + "]";
	}
}
