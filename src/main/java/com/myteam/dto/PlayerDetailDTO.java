package com.myteam.dto;

public class PlayerDetailDTO {

	private PlayerBattingDTO batting;
	
	private PlayerBowlingDTO bowling;

	public PlayerBattingDTO getBatting() {
		return batting;
	}

	public void setBatting(PlayerBattingDTO batting) {
		this.batting = batting;
	}

	public PlayerBowlingDTO getBowling() {
		return bowling;
	}

	public void setBowling(PlayerBowlingDTO bowling) {
		this.bowling = bowling;
	}

	@Override
	public String toString() {
		return "PlayerDetailDTO [batting=" + batting + ", bowling=" + bowling + "]";
	}
}
