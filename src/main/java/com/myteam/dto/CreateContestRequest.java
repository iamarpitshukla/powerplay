package com.myteam.dto;

import java.util.ArrayList;
import java.util.List;

public class CreateContestRequest {
	
	private List<Integer> matchIds = new ArrayList<>();

	public List<Integer> getMatchIds() {
		return matchIds;
	}

	public void setMatchIds(List<Integer> matchIds) {
		this.matchIds = matchIds;
	}

	@Override
	public String toString() {
		return "CreateContestRequest [matchIds=" + matchIds + "]";
	}

}
