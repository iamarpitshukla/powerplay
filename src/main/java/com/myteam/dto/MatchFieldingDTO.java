package com.myteam.dto;

import java.util.ArrayList;
import java.util.List;

public class MatchFieldingDTO {

	private String title;

	private List<FieldingScoreDTO> scores = new ArrayList<>();

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<FieldingScoreDTO> getScores() {
		return scores;
	}

	public void setScores(List<FieldingScoreDTO> scores) {
		this.scores = scores;
	}

	@Override
	public String toString() {
		return "MatchFieldingDTO [title=" + title + ", scores=" + scores + "]";
	}
}
