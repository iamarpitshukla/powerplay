package com.myteam.dto;

import com.myteam.entity.User;

public class UserDetailDTO {

	private Integer userId;

	private String name;

	private String email;

	private String mobile;
	
	
	public UserDetailDTO() {
		super();
	}
	
	public UserDetailDTO(User user) {
		super();
		this.userId = user.getId();
		this.name = user.getName();
		this.email = user.getEmail();
		this.mobile = user.getUsername();
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

}
