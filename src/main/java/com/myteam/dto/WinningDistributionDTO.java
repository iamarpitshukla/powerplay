package com.myteam.dto;

public class WinningDistributionDTO {

	private String rankString;
	
	private Integer winningAmount;
	
	public String getRankString() {
		return rankString;
	}

	public void setRankString(String rankString) {
		this.rankString = rankString;
	}

	public Integer getWinningAmount() {
		return winningAmount;
	}

	public void setWinningAmount(Integer winningAmount) {
		this.winningAmount = winningAmount;
	}

	@Override
	public String toString() {
		return "WinningDistributionDTO [rankString=" + rankString + ", winningAmount=" + winningAmount + "]";
	}
}
