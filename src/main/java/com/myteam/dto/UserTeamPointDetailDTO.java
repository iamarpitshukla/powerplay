package com.myteam.dto;

import com.myteam.entity.UserMatchTeamContestMapping;

public class UserTeamPointDetailDTO {

	private Integer teamNumber;
	
	private Double points;
	
	private Integer rank;
	
	private Integer winningAmount;
	
	private String ucpmCode;
	
	private boolean selfTeam;
	
	private String teamName;
	
	private String profilePic;
	
	public String getProfilePic() {
		return profilePic;
	}

	public void setProfilePic(String profilePic) {
		this.profilePic = profilePic;
	}

	public boolean isSelfTeam() {
		return selfTeam;
	}

	public void setSelfTeam(boolean selfTeam) {
		this.selfTeam = selfTeam;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public UserTeamPointDetailDTO() {
		super();
	}
	
	public UserTeamPointDetailDTO(UserMatchTeamContestMapping umContestPlayersMapping) {
		super();
		this.teamNumber = umContestPlayersMapping.getUserMatchTeamMapping().getTeamNumber();
		this.points = umContestPlayersMapping.getPoints();
		this.rank = umContestPlayersMapping.getRank();
		this.winningAmount = umContestPlayersMapping.getWinningAmount();
		this.ucpmCode = umContestPlayersMapping.getCode();
		this.teamName = umContestPlayersMapping.getUserMatchTeamMapping().getUser().getName();
	}
	
	public String getUcpmCode() {
		return ucpmCode;
	}

	public void setUcpmCode(String ucpmCode) {
		this.ucpmCode = ucpmCode;
	}

	public Integer getTeamNumber() {
		return teamNumber;
	}

	public void setTeamNumber(Integer teamNumber) {
		this.teamNumber = teamNumber;
	}

	public Double getPoints() {
		return points;
	}

	public void setPoints(Double points) {
		this.points = points;
	}

	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public Integer getWinningAmount() {
		return winningAmount;
	}

	public void setWinningAmount(Integer winningAmount) {
		this.winningAmount = winningAmount;
	}

	@Override
	public String toString() {
		return "UserTeamPointDetailDTO [teamNumber=" + teamNumber + ", points=" + points + ", rank=" + rank
				+ ", winningAmount=" + winningAmount + ", ucpmCode=" + ucpmCode + "]";
	}
}
