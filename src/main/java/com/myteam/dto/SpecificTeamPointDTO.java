package com.myteam.dto;

public class SpecificTeamPointDTO {

	private PlayersDTO player;
	
	private Double points;

	public PlayersDTO getPlayer() {
		return player;
	}

	public void setPlayer(PlayersDTO player) {
		this.player = player;
	}

	public Double getPoints() {
		return points;
	}

	public void setPoints(Double points) {
		this.points = points;
	}

	@Override
	public String toString() {
		return "SpecificTeamPointDTO [player=" + player + ", points=" + points + "]";
	}
}
