package com.myteam.dto;

public class JoinedContestDTO {
	
	private Integer contestId;
		
	private Integer entryFees;
	
	private Integer totalPrize;
	
	private String displayContest;
	
	private Integer rank;
	
	private Double points;
	
	private Integer teamNumber;
	
	private Integer winningAmount;
	
	private Integer totalWinners;
	
	private String status; // open ,running,waiting for review,cancelled,completed 
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getTotalWinners() {
		return totalWinners;
	}

	public void setTotalWinners(Integer totalWinners) {
		this.totalWinners = totalWinners;
	}

	public Integer getContestId() {
		return contestId;
	}

	public void setContestId(Integer contestId) {
		this.contestId = contestId;
	}

	public Integer getEntryFees() {
		return entryFees;
	}

	public void setEntryFees(Integer entryFees) {
		this.entryFees = entryFees;
	}

	public Integer getTotalPrize() {
		return totalPrize;
	}

	public void setTotalPrize(Integer totalPrize) {
		this.totalPrize = totalPrize;
	}

	public String getDisplayContest() {
		return displayContest;
	}

	public void setDisplayContest(String displayContest) {
		this.displayContest = displayContest;
	}

	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public Double getPoints() {
		return points;
	}

	public void setPoints(Double points) {
		this.points = points;
	}

	public Integer getTeamNumber() {
		return teamNumber;
	}

	public void setTeamNumber(Integer teamNumber) {
		this.teamNumber = teamNumber;
	}

	public Integer getWinningAmount() {
		return winningAmount;
	}

	public void setWinningAmount(Integer winningAmount) {
		this.winningAmount = winningAmount;
	}

	@Override
	public String toString() {
		return "JoinedContestDTO [contestId=" + contestId + ", entryFees=" + entryFees + ", totalPrize=" + totalPrize
				+ ", displayContest=" + displayContest + ", rank=" + rank + ", points=" + points + ", teamNumber="
				+ teamNumber + ", winningAmount=" + winningAmount + ", totalWinners=" + totalWinners + "]";
	}
}
