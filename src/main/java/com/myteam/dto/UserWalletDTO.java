package com.myteam.dto;

import java.math.BigDecimal;

import com.myteam.entity.WalletHistory;
import com.myteam.utils.DateUtils;

public class UserWalletDTO {

	private BigDecimal amount;
	
	private String created;
	
	private String remark;
	
	public UserWalletDTO() {
		super();
	}

	public UserWalletDTO(WalletHistory history) {
		super();
		this.amount = history.getAmount();
		this.created = DateUtils.dateToString(history.getCreated(),"yyyy:MM:dd hh:mm");
		this.remark = history.getRemarks();
	}
	
	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return "UserWalletDTO [amount=" + amount + ", created=" + created + ", remark=" + remark + "]";
	}
}
