package com.myteam.dto;

import java.util.ArrayList;
import java.util.List;

public class MatchBattingDTO {
	
	private String title;
	
	private List<BattingScoreDTO> scores = new ArrayList<>();
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<BattingScoreDTO> getScores() {
		return scores;
	}

	public void setScores(List<BattingScoreDTO> scores) {
		this.scores = scores;
	}

	@Override
	public String toString() {
		return "MatchBattingDTO [title=" + title + ", scores=" + scores + "]";
	}
}
