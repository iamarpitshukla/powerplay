package com.myteam.dto;

import java.util.ArrayList;
import java.util.List;

import com.myteam.entity.ContestWinningDistribution;

public class ContestDetailDTO {
	
	private List<ContestWinningDistribution> contestDistribution = new ArrayList<>();
	
	private Integer prizePool;

	public List<ContestWinningDistribution> getContestDistribution() {
		return contestDistribution;
	}

	public void setContestDistribution(List<ContestWinningDistribution> contestDistribution) {
		this.contestDistribution = contestDistribution;
	}

	public Integer getPrizePool() {
		return prizePool;
	}

	public void setPrizePool(Integer prizePool) {
		this.prizePool = prizePool;
	}

	@Override
	public String toString() {
		return "ContestDetailDTO [contestDistribution=" + contestDistribution + ", prizePool=" + prizePool + "]";
	}
}
