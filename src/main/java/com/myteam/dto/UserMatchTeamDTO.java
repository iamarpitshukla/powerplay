package com.myteam.dto;

import java.util.ArrayList;
import java.util.List;

public class UserMatchTeamDTO {

	private Integer userTeamMappingId;
	
	private List<PlayersDTO> players = new ArrayList<>();
	
	private Integer teamNumber;
	
	private String userName;
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getTeamNumber() {
		return teamNumber;
	}

	public void setTeamNumber(Integer teamNumber) {
		this.teamNumber = teamNumber;
	}

	public Integer getUserTeamMappingId() {
		return userTeamMappingId;
	}

	public void setUserTeamMappingId(Integer userTeamMappingId) {
		this.userTeamMappingId = userTeamMappingId;
	}

	public List<PlayersDTO> getPlayers() {
		return players;
	}

	public void setPlayers(List<PlayersDTO> players) {
		this.players = players;
	}

	@Override
	public String toString() {
		return "UserMatchTeamDTO [userTeamMappingId=" + userTeamMappingId + ", players=" + players + ", teamNumber="
				+ teamNumber + ", userName=" + userName + "]";
	}
}
