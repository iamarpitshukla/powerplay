package com.myteam.dto;

import java.util.ArrayList;
import java.util.List;

public class FlagsMapping {

	private List<TeamFlagDTO> flags = new ArrayList<>();

	public List<TeamFlagDTO> getFlags() {
		return flags;
	}

	public void setFlags(List<TeamFlagDTO> flags) {
		this.flags = flags;
	}

	@Override
	public String toString() {
		return "FlagsMapping [flags=" + flags + "]";
	}
}
