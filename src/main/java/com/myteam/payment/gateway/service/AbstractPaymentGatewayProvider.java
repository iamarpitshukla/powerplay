package com.myteam.payment.gateway.service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.myteam.dto.ChargePaymentDTO;
import com.myteam.entity.PaymentGateway;
import com.myteam.exception.MyTeamException;
import com.myteam.respponse.AbstractPaymentGatewayResponse;
import com.myteam.respponse.PaymentGatewayResponse;
import com.myteam.utils.StringUtils;

public abstract class AbstractPaymentGatewayProvider {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractPaymentGatewayProvider.class);

	public static final String RESPONSE_CUSTOMER_NAME = "customerName";
	public static final String RESPONSE_CUSTOMER_ADDRESS = "customerAddress";
	public static final String RESPONSE_CUSTOMER_COUNTRY = "customerCountry";
	public static final String RESPONSE_CUSTOMER_TELEPHONE = "customerPhone";
	public static final String CUSTOMER_EMAIL = "email";
	public static final String RESPONSE_PG_PAYMENT_ID = "pgPaymentId";

	public static final String RESPONSE_PAID_AMOUNT = "amount";

	public static final String RESPONSE_TRANSACTION_STATUS_SUCCESS = "SUCCESS";
	public static final String RESPONSE_TRANSACTION_STATUS_PENDING = "PENDING";

	public static final String DOCTOR_PG_NAME = "doctorPayU";
	public static final String PAYU_PG_NAME = "payU";
	public static final String STRIPE_PG_NAME = "stripe";
	public static final String RBL_PG_NAME = "rbl";
	public static final String PAYTM_PG_NAME = "payTM";

	private PaymentGateway paymentGateway;

	public PaymentGateway getPaymentGateway() {
		return paymentGateway;
	}

	public void setPaymentGateway(PaymentGateway paymentGateway) {
		this.paymentGateway = paymentGateway;
	}

	public Map<String, String> getPGResponse(String txnCode) throws Exception {
		return null;
	}

	public PaymentRedirectForm getRedirectionForm(String txnCode, BigDecimal paymentAmount, String customerName,
			String email, Integer customerId, String productId, String phone, String rUrl, String udf1, String udf2,
			String udf3, boolean isDoctorAccountPgTransaction) throws Exception {
		return null;
	}

	public boolean validateAmount(PaymentGatewayResponse response, Integer paidAmount) {
		String amount = (String) response.getItems().get(AbstractPaymentGatewayProvider.RESPONSE_PAID_AMOUNT);
		LOG.info("Inside AbstractPaymentGateway ValidateAmount, Amount from PG : " + amount + ", amount from system : "
				+ paidAmount);
		if (StringUtils.isNotEmpty(amount)) {
			try {
				return paidAmount.equals(StringUtils.parsePrice(amount).intValue());
			} catch (Exception e) {
				return false;
			}
		} else {
			return false;
		}
	}

	public static class PaymentRedirectForm {

		private Map<String, String> params = new HashMap<String, String>();

		public void addParam(String key, String value) {
			params.put(key, value);
		}

		public Map<String, String> getParams() {
			return params;
		}

		public void setParams(Map<String, String> params) {
			this.params = params;
		}
	}

	public String getPaymentRedirectUrl() {
		return getPaymentGateway().getApiUrl();
	}
}
