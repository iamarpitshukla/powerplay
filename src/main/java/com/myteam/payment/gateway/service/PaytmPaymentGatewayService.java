package com.myteam.payment.gateway.service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.gson.Gson;
import com.myteam.entity.PaymentGateway;
import com.myteam.enums.PaytmTransactionStatus;
import com.myteam.exception.MyTeamException;
import com.myteam.request.BasicPaytmTransactionRequest;
import com.myteam.request.PaytmCheckStatusRequest;
import com.myteam.request.PaytmRefundTransactionRequest;
import com.myteam.request.PaytmTransactionRequest;
import com.myteam.respponse.AbstractPaymentGatewayResponse;
import com.myteam.respponse.PaymentGatewayResponse;
import com.myteam.respponse.PaymentGatewayResponse.PGResponseStatus;
import com.myteam.respponse.PaytmCheckStatusResponse;
import com.myteam.respponse.PaytmRefundTransactionResponse;
import com.myteam.respponse.PaytmTransactionResponse;
import com.myteam.services.CustomPropertyNamingStrategy;
import com.myteam.transport.HttpSender;
import com.myteam.transport.HttpTransportException;
import com.myteam.utils.StringUtils;
import com.paytm.pg.merchant.CheckSumServiceHelper;

@Service("paytmPaymentGatewayService")
public class PaytmPaymentGatewayService extends AbstractPaymentGatewayProvider{



	private static final Logger LOG = LoggerFactory.getLogger(PaytmPaymentGatewayService.class);
	private static final String REQUEST_TYPE = "REQUEST_TYPE";
	private static final String INDUSTRY_TYPE_ID = "INDUSTRY_TYPE_ID";
	private static final String MERC_UNQ_REF	=	"MERC_UNQ_REF";
	private static final String WEBSITE_WEB = "WEBSITE_WEB";
	private static final String WEBSITE_APP = "WEBSITE_APP";
	private static final String CHANNEL_ID_WEB = "CHANNEL_ID_WEB";
	private static final String CHANNEL_ID_APP = "CHANNEL_ID_APP";
	private static final String TRANSACTION_STATUS_API = "merchant-status/getTxnStatus";
	private static final String MAKE_TRANSACTION_API = "theia/processTransaction";
	private static final String REFUND_TRANSACTION_API = "/oltp/HANDLER_INTERNAL/REFUND";


	public PaymentGatewayResponse validatePaytmPaymentGatewayResponse(AbstractPaymentGatewayResponse pgResponse,
			Map<String, String> parameters, BigDecimal paidAmount) throws Exception {
		PaytmTransactionResponse transactionResponse = (PaytmTransactionResponse) pgResponse;
		LOG.info("Transaction Status after transaction :{}, for txnCode :{}", transactionResponse.getSTATUS(),
				transactionResponse.getORDERID());
		ObjectMapper oMapper = new ObjectMapper();
		oMapper.setSerializationInclusion(Include.NON_NULL);
		oMapper.setPropertyNamingStrategy(new CustomPropertyNamingStrategy());
		oMapper.enable(SerializationFeature.INDENT_OUTPUT);
		@SuppressWarnings("unchecked")
		Map<String, Object> paramMap = oMapper.convertValue(transactionResponse, HashMap.class);
		if (transactionResponse.getSTATUS().equalsIgnoreCase(PaytmTransactionStatus.TXN_SUCCESS.code())
				|| transactionResponse.getSTATUS().equalsIgnoreCase(PaytmTransactionStatus.TXN_PROCESSING.code())
				|| transactionResponse.getSTATUS().equalsIgnoreCase(PaytmTransactionStatus.PENDING.code())) {
			if (isValidCheckSum(parameters)) {
				PaytmCheckStatusRequest transactionCheckRequest = new PaytmCheckStatusRequest();
				transactionCheckRequest.setMID(transactionResponse.getMID());
				transactionCheckRequest.setORDERID(transactionResponse.getORDERID());
				transactionCheckRequest.setCHECKSUMHASH(transactionResponse.getCHECKSUMHASH());
				PaytmCheckStatusResponse paytmCheckStatusResponse = checkTransactionStatus(transactionCheckRequest);
				LOG.info("Transaction Status after transaction status check :{}, for txnCode :{} txnAmount :{}",
						paytmCheckStatusResponse.getSTATUS(), paytmCheckStatusResponse.getORDERID(), paytmCheckStatusResponse.getTXNAMOUNT());
				String amount = paidAmount  == null ? null : paidAmount.toString();
				if (paytmCheckStatusResponse.getSTATUS().equalsIgnoreCase(PaytmTransactionStatus.TXN_SUCCESS.code())) {
					if(paytmCheckStatusResponse.getTXNAMOUNT().equalsIgnoreCase(amount) || amount == null) {
						return new PaymentGatewayResponse(PGResponseStatus.SUCCESS, paramMap);
					} else {
						LOG.info("Amount is not same as received from status :{}, :{}", paytmCheckStatusResponse.getTXNAMOUNT(), amount);
						return new PaymentGatewayResponse(PGResponseStatus.FAIL, paramMap);
					}
				} else if(paytmCheckStatusResponse.getSTATUS().equalsIgnoreCase(PaytmTransactionStatus.TXN_PROCESSING.code())){
					return new PaymentGatewayResponse(PGResponseStatus.PENDING_VERIFICATION, paramMap);
				} else {
					return new PaymentGatewayResponse(PGResponseStatus.FAIL, paramMap);
				}
			} else {
				throw new MyTeamException("Paytm checksum not verified");
			}
		} else {
			return new PaymentGatewayResponse(PGResponseStatus.FAIL, paramMap);
		}
	}

	public boolean isValidCheckSum(Map<String, String> parameters) {
		CheckSumServiceHelper checksumHelper = CheckSumServiceHelper.getCheckSumServiceHelper();
		String merchantkey = getPaymentGateway().getSecretKey();
		String paytmChecksum = parameters.get("CHECKSUMHASH"); // sent by paytm
		TreeMap<String, String> paramMap = new TreeMap<String, String>();
		paramMap.putAll(parameters);
		try {
			return checksumHelper.verifycheckSum(merchantkey, paramMap, paytmChecksum);
		} catch (Exception e) {
			LOG.info("Exception while checking checksum :{}", e);
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public PaymentRedirectForm getRedirectionForm(String txnCode, BigDecimal paymentAmount, String customerName,
			String email, Integer customerId, String productId, String phone, String rUrl, String udf1, String udf2,
			String udf3, boolean isDoctorAccountPgTransaction) throws Exception {
		PaymentRedirectForm redirectForm = new PaymentRedirectForm();
		PaytmTransactionRequest request = updatePaytmTransactionRequest(txnCode, paymentAmount, customerName, email,
				customerId, productId, phone, rUrl, udf3);
		request.setMAKE_TXN_URL(getPaymentRedirectUrl());
		ObjectMapper oMapper = new ObjectMapper();
		oMapper.setSerializationInclusion(Include.NON_NULL);
		oMapper.setPropertyNamingStrategy(new CustomPropertyNamingStrategy());
		oMapper.enable(SerializationFeature.INDENT_OUTPUT);
		redirectForm.setParams(oMapper.convertValue(request, HashMap.class));
		return redirectForm;
	}

	@Override
	public String getPaymentRedirectUrl() {
		return getPaymentGateway().getApiUrl() + MAKE_TRANSACTION_API;
	}

	public PaytmCheckStatusResponse checkTransactionStatus(PaytmCheckStatusRequest request) throws MyTeamException {
		String resourceUrl = getPaymentGateway().getApiUrl();
		resourceUrl = resourceUrl + TRANSACTION_STATUS_API;
		request.setCHECKSUMHASH("");
		request.setCHECKSUMHASH(generateCheckSum(request));
		String json = new Gson().toJson(request);
		String response = executeGetRequest(json, resourceUrl);
		try {
			PaytmCheckStatusResponse paytmResponse = new Gson().fromJson(response, PaytmCheckStatusResponse.class);
			return paytmResponse;
		} catch (Exception e) {
			throw new MyTeamException("Paytm exception occurred");
		}
	}

	public PaytmRefundTransactionResponse refundTransactionAmount(PaytmRefundTransactionRequest request)
			throws MyTeamException {
		String resourceUrl = getPaymentGateway().getApiUrl();
		resourceUrl = resourceUrl + REFUND_TRANSACTION_API;
		String json = new Gson().toJson(request);
		String response = executeGetRequest(json, resourceUrl);
		try {
			PaytmRefundTransactionResponse paytmResponse = new Gson().fromJson(response,
					PaytmRefundTransactionResponse.class);
			return paytmResponse;
		} catch (Exception e) {
			throw new MyTeamException("Paytm exception occurred");
		}
	}

	public String executeGetRequest(String jsonRequest, String resourceUrl) {
		HttpSender sender = new HttpSender(true);
		Map<String, String> params = new HashMap<String, String>();
		params.put("JsonData", jsonRequest);
		try {
			String response = sender.executeGet(resourceUrl, params);
			LOG.info("Response from Paytm {}", response);
			return response;
		} catch (HttpTransportException e) {
			LOG.error("Error while calling paytm request API", e);
		}
		return null;
	}

	private PaytmTransactionRequest updatePaytmTransactionRequest(String txnCode, BigDecimal paymentAmount,
			String customerName, String email, Integer customerId, String productId, String phone, String rUrl, String udf) {
		PaytmTransactionRequest request = new PaytmTransactionRequest();
		request.setMOBILE_NO(phone);
		request.setCALLBACK_URL("http://13.127.217.102:8000"+rUrl);
		request.setCUST_ID(String.valueOf(customerId));
		request.setTXN_AMOUNT(String.valueOf(paymentAmount));
		if (StringUtils.isNotEmpty(email)) {
			request.setEMAIL(email);
		} else {
			request.setEMAIL(phone.replace("+", "") + "@powerplay.in");
		}
//		request.setREQUEST_TYPE("DEFAULT");
		PaymentGateway pg = getPaymentGateway();
		request.setMID(pg.getMerchantId());
		request.setORDER_ID(txnCode);
		Map<String,String> additionalCredentials = pg.getAdditionalCredentials();
		if(!additionalCredentials.isEmpty()){
			request.setINDUSTRY_TYPE_ID(additionalCredentials.get(INDUSTRY_TYPE_ID));
			request.setCHANNEL_ID(additionalCredentials.get(CHANNEL_ID_WEB));
			request.setWEBSITE(additionalCredentials.get(WEBSITE_WEB));
		} else {
			request.setINDUSTRY_TYPE_ID("Retail");
			request.setCHANNEL_ID("WEB");
			request.setWEBSITE("DEFAULT");
		}
		if(StringUtils.isNotEmpty(udf)) {
		    String udfWithAlphaNUmericOnly = udf.replaceAll("[^a-zA-Z0-9]", "");
			request.setMERC_UNQ_REF(udfWithAlphaNUmericOnly);
		}
		request.setCHECKSUMHASH(generateCheckSum(request));
		return request;
	}

	public String generateCheckSum(BasicPaytmTransactionRequest request) {
		CheckSumServiceHelper checksumHelper = CheckSumServiceHelper.getCheckSumServiceHelper();
		String key = getPaymentGateway().getSecretKey();
		ObjectMapper oMapper = new ObjectMapper();
		oMapper.setSerializationInclusion(Include.NON_NULL);
		oMapper.setPropertyNamingStrategy(new CustomPropertyNamingStrategy());
		oMapper.enable(SerializationFeature.INDENT_OUTPUT);
		@SuppressWarnings("unchecked")
		TreeMap<String, String> paramMap = oMapper.convertValue(request, TreeMap.class);
		LOG.info("Key is :{}",  key);
		LOG.info(" Request to generate checksum is :{}",  paramMap);
		try {
			String checksum = checksumHelper.genrateCheckSum(key, paramMap);
			LOG.info("Checksum is :{}",  checksum);
			return checksum;
		} catch (Exception e) {
			LOG.error("Error while generating cheksum :{}", e);
		}
		return null;
	}

	public static class PayTmPaymentResponse extends AbstractPaymentGatewayResponse {

		private Map<String, String> parameterMap = new HashMap<String, String>();

		public Map<String, String> getParameterMap() {
			return parameterMap;
		}

		public void setParameterMap(Map<String, String> parameterMap) {
			this.parameterMap = parameterMap;
		}

	}
}
