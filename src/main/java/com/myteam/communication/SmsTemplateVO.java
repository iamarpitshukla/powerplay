package com.myteam.communication;


import com.myteam.utils.StringUtils;

public class SmsTemplateVO {

	private Integer  templateId;
    private String   name;
    private String 	 capType;
    private String 	 code;
    private Template bodyTemplate;
    private int      smsChannelId;
    private boolean  dndScrubbingOn;
    private boolean  chargeable;
    private String   lang;
    private boolean  visibleToUser;
    private boolean  isDNDHourBlockingEnabled;
    private boolean  isIntlSmsAllowed;
    private String   type;
    private String   otp;  // temp implementation for otp
    private String   appLink;
    
    public String getAppLink() {
		return appLink;
	}

	public void setAppLink(String appLink) {
		this.appLink = appLink;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public Integer getTemplateId() {
		return templateId;
	}

	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}

	public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getCapType() {
		return capType;
	}

	public void setCapType(String capType) {
		this.capType = capType;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Template getBodyTemplate() {
        return bodyTemplate;
    }

    public void setBodyTemplate(Template bodyTemplate) {
        this.bodyTemplate = bodyTemplate;
    }

    public void setSmsChannelId(int smsChannelId) {
        this.smsChannelId = smsChannelId;
    }

    public int getSmsChannelId() {
        return smsChannelId;
    }

    public boolean isDndScrubbingOn() {
        return dndScrubbingOn;
    }

    public void setDndScrubbingOn(boolean dndScrubbingOn) {
        this.dndScrubbingOn = dndScrubbingOn;
    }

    public boolean isChargeable() {
        return chargeable;
    }

    public void setChargeable(boolean chargeable) {
        this.chargeable = chargeable;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public boolean isVisibleToUser() {
        return visibleToUser;
    }

    public void setVisibleToUser(boolean visibleToUser) {
        this.visibleToUser = visibleToUser;
    }

    public boolean isDNDHourBlockingEnabled() {
        return isDNDHourBlockingEnabled;
    }

    public void setDNDHourBlockingEnabled(boolean isDNDHourBlockingEnabled) {
        this.isDNDHourBlockingEnabled = isDNDHourBlockingEnabled;
    }

    public boolean isIntlSmsAllowed() {
        return isIntlSmsAllowed;
    }

    public void setIntlSmsAllowed(boolean isIntlSmsAllowed) {
        this.isIntlSmsAllowed = isIntlSmsAllowed;
    }
    
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDisplayName(){
		if (StringUtils.isNotEmpty(this.code)) {
			return this.code;
		}
		if (StringUtils.isNotEmpty(this.name)) {
			return this.name;
		}
		return this.capType;
    }

    @Override
    public String toString() {
        return "SmsTemplateVO [templateId=" + templateId + ", name=" + name + ", capType=" + capType + ", code=" + code + "]";
    }
    

}
