package com.myteam.communication;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.exception.MethodInvocationException;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;

import com.myteam.cache.CacheManager;
import com.myteam.utils.DateUtils;
import com.myteam.utils.StringUtils;

public class Template {

    private String template;

    private Template(String sTemplate) {
        this.template = sTemplate;
    }

    public static Template compile(String sTemplate) {
        return new Template(sTemplate);
    }
    
    

    public String getTemplate() {
		return template;
	}

	public String evaluate(Map<String, Object> params) {
        VelocityContext context = new VelocityContext();
        context.put("dateutils", new DateUtils());
        context.put("stringutils", new StringUtils());
        context.put("cache", CacheManager.getInstance());
        for (Map.Entry<String, Object> entry : params.entrySet()) {
            context.put(entry.getKey(), entry.getValue());
        }
        StringWriter outWriter = new StringWriter();
        try {
            Velocity.evaluate(context, outWriter, "", template);
        } catch (ParseErrorException e) {
            throw new RuntimeException(e);
        } catch (MethodInvocationException e) {
            throw new RuntimeException(e);
        } catch (ResourceNotFoundException e) {
            throw new RuntimeException(e);
        } 
        return outWriter.toString();
    }

    public String callbackFunction(String functionName, String value) {
        StringBuilder outBuilder = new StringBuilder();
        return outBuilder.append(functionName).append("(").append(value).append(")").toString();
    }

    public static void main(String[] args) throws ParseErrorException, MethodInvocationException, ResourceNotFoundException, IOException {
        Template t = new Template("${reponse.voucher == null ? 'INVALID' : (response.voucher.consumed ? 'REDEEMED' : (response.valid ? 'VALID' : 'INVALID'))}");
        Map<String, Object> params = new HashMap<String, Object>();
        System.out.println(t.evaluate(params));
    }


}
