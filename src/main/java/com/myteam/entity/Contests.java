package com.myteam.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "contests", catalog = "myteam")
public class Contests extends PowerPlayBase{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3186935576177383726L;

	@ManyToOne(fetch = FetchType.LAZY)
    @NotNull
    @JoinColumn(name = "match_id", nullable = false)
	private Match match;
	
	@Column(name = "playing_count")
	private Integer playingCount;
	
	@Column(name = "entry_fees")
	private Integer entryFees;
	
	@Column(name = "prize_pool")
	private Integer prizePool;
	
	@Column(name = "total_count")
	private Integer totalCount;
	
	@Column(name = "winner_count")
	private Integer winnerCount;
	
	public Contests() {
		super();
	}
	
	public enum Status {  // open ,running,waiting for review,cancelled,completed 
		OPEN("O"), RUNNING("R"),WAITING_FOR_REVIEW("WFR"), CANCELLED("CLD"),COMPLETED("CTD");
		private String code;

		private Status(String code) {
			this.code = code;
		}

		public String code() {
			return code;
		}

	}
	
	public Contests(Match match, Integer playingCount, Integer entryFees, Integer prizePool, Integer totalCount,
			Integer winnerCount, Integer startOver, Integer endOver, Integer inning, String type) {
		super();
		this.match = match;
		this.playingCount = playingCount;
		this.entryFees = entryFees;
		this.prizePool = prizePool;
		this.totalCount = totalCount;
		this.winnerCount = winnerCount;
		this.startOver = startOver;
		this.endOver = endOver;
		this.inning = inning;
		this.type = type;
	}

	@Column(name = "start_over")
	private Integer startOver;
	
	@Column(name = "end_over")
	private Integer endOver;
	
	@Column(name = "inning")
	private Integer inning;
	
	@Column(name = "type")
	private String type;
	
	@Column(name = "time_crossed", columnDefinition ="TINYINT")
	private boolean timeCrossed;
	
	@Column(name = "contest_over", columnDefinition ="TINYINT")
	private boolean contestOver;
	
	@Column(name = "points", columnDefinition ="TEXT")
	private String points;
	
	@Column(name = "reviewed", columnDefinition ="TINYINT")
	private boolean reviewed;
	
	@Column(name = "cancelled", columnDefinition ="TINYINT")
	private boolean cancelled;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "contests")
	private List<ContestWinningDistribution> contestWinnerRank;
	
	public List<ContestWinningDistribution> getContestWinnerRank() {
		return contestWinnerRank;
	}

	public void setContestWinnerRank(List<ContestWinningDistribution> contestWinnerRank) {
		this.contestWinnerRank = contestWinnerRank;
	}

	public boolean isContestOver() {
		return contestOver;
	}

	public void setContestOver(boolean contestOver) {
		this.contestOver = contestOver;
	}

	public boolean isTimeCrossed() {
		return timeCrossed;
	}

	public void setTimeCrossed(boolean timeCrossed) {
		this.timeCrossed = timeCrossed;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getStartOver() {
		return startOver;
	}

	public void setStartOver(Integer startOver) {
		this.startOver = startOver;
	}

	public Integer getEndOver() {
		return endOver;
	}

	public void setEndOver(Integer endOver) {
		this.endOver = endOver;
	}

	public Match getMatch() {
		return match;
	}

	public void setMatch(Match match) {
		this.match = match;
	}

	public Integer getPlayingCount() {
		return playingCount;
	}

	public void setPlayingCount(Integer playingCount) {
		this.playingCount = playingCount;
	}

	public Integer getEntryFees() {
		return entryFees;
	}

	public void setEntryFees(Integer entryFees) {
		this.entryFees = entryFees;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public Integer getInning() {
		return inning;
	}

	public void setInning(Integer inning) {
		this.inning = inning;
	}

	public String getPoints() {
		return points;
	}

	public void setPoints(String points) {
		this.points = points;
	}

	public Integer getWinnerCount() {
		return winnerCount;
	}

	public void setWinnerCount(Integer winnerCount) {
		this.winnerCount = winnerCount;
	}

	public Integer getPrizePool() {
		return prizePool;
	}

	public void setPrizePool(Integer prizePool) {
		this.prizePool = prizePool;
	}

	public boolean isReviewed() {
		return reviewed;
	}

	public void setReviewed(boolean reviewed) {
		this.reviewed = reviewed;
	}
	
	public boolean isCancelled() {
		return cancelled;
	}

	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}

	@Transient
	public String getContestStatus() {
		if(this.cancelled) {
			return Status.CANCELLED.name();
		}else if(this.reviewed) {
			return Status.COMPLETED.name();
		}else if (this.contestOver) {
			return Status.WAITING_FOR_REVIEW.name();
		}else if(this.timeCrossed) {
			return Status.RUNNING.name();
		}else {
			return Status.OPEN.name();
		}
	}
}
