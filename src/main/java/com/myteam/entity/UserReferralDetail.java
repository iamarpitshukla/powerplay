package com.myteam.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "user_referral_detail", catalog = "myteam")
public class UserReferralDetail extends PowerPlayBase {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@NotNull
	@Column(name = "code", unique = true, nullable = false, length = 10)
	private String code;
	@ManyToOne(fetch = FetchType.EAGER)
	@NotNull
	@JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
	private User user;
	@Column(name = "max_allowed_count")
	private Integer maxAllowedCount = 10;

	@Column(name = "used_count")
	private Integer usedCount;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "referred_by", referencedColumnName = "id", nullable = true)
	private User referredBy;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Integer getMaxAllowedCount() {
		return maxAllowedCount;
	}

	public void setMaxAllowedCount(Integer maxAllowedCount) {
		this.maxAllowedCount = maxAllowedCount;
	}

	public Integer getUsedCount() {
		return usedCount;
	}

	public void setUsedCount(Integer usedCount) {
		this.usedCount = usedCount;
	}

	public User getReferredBy() {
		return referredBy;
	}

	public void setReferredBy(User referredBy) {
		this.referredBy = referredBy;
	}
	
	
}
