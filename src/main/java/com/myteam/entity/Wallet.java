package com.myteam.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "wallet", catalog = "myteam")
public class Wallet extends PowerPlayBase {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5503268324307927547L;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id",referencedColumnName="id",  nullable = false)
	private User user;

	@Column(name = "amount")
	private BigDecimal amount = new BigDecimal(0);

	@Column(name = "wallet_type")
	private String walletType = WalletType.DEFAULT.code();

	public enum WalletType {
		WINNING("WIN"), DEFAULT("DFLT"),JOINED_CONTEST("JND-CNTST"), JOINING_BONUS("JN-BNS"),REFERRAL_BONUS("REF_BNS"),ADD_MONEY("ADD-MNY");
		private String code;

		private WalletType(String code) {
			this.code = code;
		}

		public String code() {
			return code;
		}

	}
	
	@Column(name = "bonus")
	private BigDecimal bonus = new BigDecimal(0);
	
	@Column(name = "deposited_amount")
	private BigDecimal depositedAmount = new BigDecimal(0);
	
	@Column(name = "won_amount")
	private BigDecimal wonAmount = new BigDecimal(0);
	
	public BigDecimal getDepositedAmount() {
		return depositedAmount;
	}

	public void setDepositedAmount(BigDecimal depositedAmount) {
		this.depositedAmount = depositedAmount;
	}

	public BigDecimal getWonAmount() {
		return wonAmount;
	}

	public void setWonAmount(BigDecimal wonAmount) {
		this.wonAmount = wonAmount;
	}

	public BigDecimal getBonus() {
		return bonus;
	}

	public void setBonus(BigDecimal bonus) {
		this.bonus = bonus;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getWalletType() {
		return walletType;
	}

	public void setWalletType(String walletType) {
		this.walletType = walletType;
	}
}
