package com.myteam.entity;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;

@Entity
@Table(name = "payment", catalog = "myteam")
public class Payment extends PowerPlayBase {

	@Column(name = "status")
	private String status = PaymentStatus.INIT.code();

	@Column(name = "source")
	private String source;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "payment")
	@OrderBy("id asc")
	private Set<PaymentTxn> paymentTxns = new HashSet<PaymentTxn>(1);

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="user_id",referencedColumnName="id", nullable = false)
	private User user;

	@Column(name = "paid_amount", columnDefinition="DECIMAL")
	private BigDecimal paidAmount;

	@Column(name = "refund_amount")
	private BigDecimal refundAmount;
	
	public enum PaymentStatus {

        INIT("INI"), SUCCESS("SCS"), FAILED("FAIL"), ON_HOLD("ON_HOLD"), COMPLETED("CTD");

        private String code;

        private PaymentStatus(String code) {
            this.code = code;
        }

        public String code() {
            return code;
        }

    }
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Set<PaymentTxn> getPaymentTxns() {
		return paymentTxns;
	}

	public void setPaymentTxns(Set<PaymentTxn> paymentTxns) {
		this.paymentTxns = paymentTxns;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public BigDecimal getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}

	public BigDecimal getRefundAmount() {
		return refundAmount;
	}

	public void setRefundAmount(BigDecimal refundAmount) {
		this.refundAmount = refundAmount;
	}
}
