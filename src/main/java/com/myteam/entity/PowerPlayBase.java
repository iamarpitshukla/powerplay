package com.myteam.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@MappedSuperclass
public abstract class PowerPlayBase implements Serializable{


	@Override
    public String toString() {
        return "PowerPlayBase [id=" + id + ", created=" + created + ", updated=" + updated + ", enabled=" + enabled + "]";
    }

    private static final long serialVersionUID = -1202334132879902232L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	protected Integer id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created",nullable=false)
	protected Date created=new Date();

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated",nullable=false)
	protected Date updated;

	@Column(name="enabled",nullable = false, columnDefinition = "TINYINT", length = 1)
	protected Boolean enabled=Boolean.TRUE;
	
	@PrePersist
	protected void onCreate() {
		created = new Date();
		updated=created;
	}

	@PreUpdate
	protected void onUpdate() {
		updated = new Date();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	

}
