package com.myteam.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "user_match_team_mapping", catalog = "myteam")
public class UserMatchTeamMapping extends PowerPlayBase{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotNull
	@JoinColumn(name = "user_id",referencedColumnName="id",  nullable = false)
	private User user;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotNull
	@JoinColumn(name = "match_id", nullable = false)
	private Match match;
	
	@Column(name = "players")
	private String players;
	
	@Column(name = "super_player")
	private String superPlayer;
	
	@Column(name = "team_number")
	private Integer teamNumber;
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Match getMatch() {
		return match;
	}

	public void setMatch(Match match) {
		this.match = match;
	}

	public Integer getTeamNumber() {
		return teamNumber;
	}

	public void setTeamNumber(Integer teamNumber) {
		this.teamNumber = teamNumber;
	}

	public String getPlayers() {
		return players;
	}

	public void setPlayers(String players) {
		this.players = players;
	}

	public String getSuperPlayer() {
		return superPlayer;
	}

	public void setSuperPlayer(String superPlayer) {
		this.superPlayer = superPlayer;
	}
}
