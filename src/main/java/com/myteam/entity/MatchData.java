package com.myteam.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "match_data", catalog = "myteam")
public class MatchData extends PowerPlayBase{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8452230733976646619L;

	@Column(name = "match_id")
	private Integer matchId;
	
	@Column(name = "data", columnDefinition = "TEXT")
	private String data;
	
	@Column(name = "points", columnDefinition = "TEXT")
	private String points;
	
	@Column(name = "additional_message")
	private String additionalMessage;
	
	public String getAdditionalMessage() {
		return additionalMessage;
	}

	public void setAdditionalMessage(String additionalMessage) {
		this.additionalMessage = additionalMessage;
	}

	public Integer getMatchId() {
		return matchId;
	}

	public void setMatchId(Integer matchId) {
		this.matchId = matchId;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getPoints() {
		return points;
	}

	public void setPoints(String points) {
		this.points = points;
	}
}
