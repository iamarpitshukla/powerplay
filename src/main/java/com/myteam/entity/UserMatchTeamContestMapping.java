package com.myteam.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "user_match_team_contest_mapping", catalog = "myteam")
public class UserMatchTeamContestMapping extends PowerPlayBase{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6461859589362188035L;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotNull
	@JoinColumn(name = "user_match_team_mapping_id",referencedColumnName="id",  nullable = false)
	private UserMatchTeamMapping userMatchTeamMapping;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotNull
	@JoinColumn(name = "contests_id", nullable = false)
	private Contests contests;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotNull
	@JoinColumn(name = "match_id", nullable = false)
	private Match match;
	
	private String code;
	
	@Column(name = "points")
	private Double points = 0.0;
	
	@Column(name = "rank")
	private Integer rank = 1;
	
	@Column(name = "winning_amount")
	private Integer winningAmount =0;
	
	@Column(name = "user_id", nullable = false)
	private Integer userId;
	
	@Column(name = "source")
	private String source;
	
	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Match getMatch() {
		return match;
	}

	public void setMatch(Match match) {
		this.match = match;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getWinningAmount() {
		return winningAmount;
	}

	public void setWinningAmount(Integer winningAmount) {
		this.winningAmount = winningAmount;
	}

	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public UserMatchTeamMapping getUserMatchTeamMapping() {
		return userMatchTeamMapping;
	}

	public void setUserMatchTeamMapping(UserMatchTeamMapping userMatchTeamMapping) {
		this.userMatchTeamMapping = userMatchTeamMapping;
	}

	public Contests getContests() {
		return contests;
	}

	public void setContests(Contests contests) {
		this.contests = contests;
	}

	public Double getPoints() {
		return points;
	}

	public void setPoints(Double points) {
		this.points = points;
	}
}
