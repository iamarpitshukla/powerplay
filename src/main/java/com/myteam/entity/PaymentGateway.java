package com.myteam.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.myteam.utils.StringUtils;

@Entity
@Table(name = "payment_gateway", catalog = "myteam")
public class PaymentGateway extends PowerPlayBase {

	private static final long serialVersionUID = -1938507091171026033L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@NotNull
	@Column(name = "name", nullable = false)
	private String name;

	@NotNull
	@Column(name = "merchant_id", nullable = false)
	private String merchantId;

	@NotNull
	@Column(name = "secret_key", nullable = false)
	private String secretKey;

	@NotNull
	@Column(name = "api_url", nullable = false)
	private String apiUrl;

	@Column(name = "additional_fields", nullable = true)
	private String additionalFields;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public String getApiUrl() {
		return apiUrl;
	}

	public void setApiUrl(String apiUrl) {
		this.apiUrl = apiUrl;
	}

	public String getAdditionalFields() {
		return additionalFields;
	}

	public void setAdditionalFields(String additionalFields) {
		this.additionalFields = additionalFields;
	}

	@Transient
	public Map<String, String> getAdditionalCredentials() {
		String commaSeperatedCredentials = this.additionalFields;
		if (StringUtils.isEmpty(commaSeperatedCredentials)) {
			return new HashMap<String, String>();
		}
		List<String> paytmCredentials = Arrays.asList(commaSeperatedCredentials.split(","));
		Map<String, String> credentialsWithKeyValue = new HashMap<String, String>();
		for (String paytmCredential : paytmCredentials) {
			List<String> keyAndValueOfCredentials = Arrays.asList(paytmCredential.split(":"));
			if (keyAndValueOfCredentials.size() > 1) {
				credentialsWithKeyValue.put(keyAndValueOfCredentials.get(0), keyAndValueOfCredentials.get(1));
			}
		}
		return credentialsWithKeyValue;
	}

}
