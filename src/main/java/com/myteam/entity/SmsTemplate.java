package com.myteam.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "sms_template", catalog = "myteam")
public class SmsTemplate extends PowerPlayBase {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7103811102364526040L;

	@NotNull
	@Column(name = "name", nullable = false, length = 100)
	private String name;

	@Column(name = "code", length = 45)
	private String code;

	@NotNull
	@Column(name = "body_template", nullable = false, columnDefinition = "TEXT")
	private String bodyTemplate;

	@NotNull
	@Column(name = "lang", nullable = false)
	private String lang;
	@NotNull
	@Column(name = "display_name", nullable = false, length = 100)
	private String displayName;

	public SmsTemplate() {
	}

	public SmsTemplate(String name, String bodyTemplate) {
		this.name = name;
		this.bodyTemplate = bodyTemplate;
	}

	public SmsTemplate(String name, String bodyTemplate, String lang) {
		this.name = name;
		this.bodyTemplate = bodyTemplate;
		this.lang = lang;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getBodyTemplate() {
		return this.bodyTemplate;
	}

	public void setBodyTemplate(String bodyTemplate) {
		this.bodyTemplate = bodyTemplate;
	}

	public String getLang() {
		return this.lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

}
