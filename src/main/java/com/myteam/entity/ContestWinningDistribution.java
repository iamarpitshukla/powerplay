package com.myteam.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "contest_winning_distribution", catalog = "myteam")
public class ContestWinningDistribution extends PowerPlayBase{

	
	@ManyToOne(fetch = FetchType.LAZY)
    @NotNull
    @JoinColumn(name = "contest_id", nullable = false)
	private Contests contests;
	
	@Column(name = "start_rank")
	private Integer startRank;
	
	@Column(name = "end_rank")
	private Integer endRank;
	
	@Column(name = "winning_amount")
	private Integer winningAmount;

	public Contests getContests() {
		return contests;
	}

	public void setContests(Contests contests) {
		this.contests = contests;
	}

	public Integer getStartRank() {
		return startRank;
	}

	public void setStartRank(Integer startRank) {
		this.startRank = startRank;
	}

	public Integer getEndRank() {
		return endRank;
	}

	public void setEndRank(Integer endRank) {
		this.endRank = endRank;
	}

	public Integer getWinningAmount() {
		return winningAmount;
	}

	public void setWinningAmount(Integer winningAmount) {
		this.winningAmount = winningAmount;
	}
}
