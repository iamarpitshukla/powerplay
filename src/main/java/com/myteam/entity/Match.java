package com.myteam.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.myteam.constants.Constants;
import com.myteam.dto.MatchDTO;
import com.myteam.utils.DateUtils;

@Entity
@Table(name = "match", catalog = "myteam")
public class Match extends PowerPlayBase{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1897905604298635769L;

	@Column(name = "match_unique_id")
	private Integer matchUniqueId;
	
	@Column(name = "team1")
	private String team1;
	
	@Column(name = "team2")
	private String team2;
	
	@Column(name = "type")
	private String type;   // cricket or football or any game
	
	@Column(name = "sub_type")
	private String subType;  // t20, odi or test
	
	@Column(name = "start_date")
	private Date   startDate;
	
	@Column(name = "squad", columnDefinition = "TINYINT")
	private boolean squad;
	
	@Column(name = "toss_winner_team")
	private String tossWinnerTeam;
	
	@Column(name = "match_started", columnDefinition = "TINYINT")
	private boolean matchStarted;
	
	@Column(name = "winner_team")
	private String winnerTeam;
	
	@Column(name = "players", columnDefinition = "TEXT")
	private String players; // squad list
	
	@Column(name = "playing_eleven", columnDefinition = "TEXT")
	private String playingEleven;
	
	@Column(name = "favourite_players", columnDefinition = "TEXT")
	private String favouritePlayers;
	
	public String getFavouritePlayers() {
		return favouritePlayers;
	}

	public void setFavouritePlayers(String favouritePlayers) {
		this.favouritePlayers = favouritePlayers;
	}

	public String getPlayingEleven() {
		return playingEleven;
	}

	public void setPlayingEleven(String playingEleven) {
		this.playingEleven = playingEleven;
	}
	
	public Match() {
		super();
	}
	
	public Match(MatchDTO match) {
		this.matchUniqueId = match.getUnique_id();
		this.squad = match.isSquad();
		this.subType = match.getType();
		if(match.getType().contains("Test")) { // not working for test for now
			this.enabled = false;
		}
		this.team1 = match.getTeam1();
		this.team2 = match.getTeam2();
		this.matchStarted = match.isMatchStarted();
		this.type = "C";
		Date startDateGMT = DateUtils.stringToDate(match.getDateTimeGMT(), Constants.DATE_FORMAT_PATTERN);
		this.startDate = DateUtils.getDateTimeFomDateAfterMin(startDateGMT, 330);
		this.tossWinnerTeam =match.getToss_winner_team();
		this.winnerTeam = match.getWinner_team();
	}

	public String getPlayers() {
		return players;
	}

	public void setPlayers(String players) {
		this.players = players;
	}

	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	public Integer getMatchUniqueId() {
		return matchUniqueId;
	}

	public void setMatchUniqueId(Integer matchUniqueId) {
		this.matchUniqueId = matchUniqueId;
	}

	public String getTeam1() {
		return team1;
	}

	public void setTeam1(String team1) {
		this.team1 = team1;
	}

	public String getTeam2() {
		return team2;
	}

	public void setTeam2(String team2) {
		this.team2 = team2;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public boolean isSquad() {
		return squad;
	}

	public void setSquad(boolean squad) {
		this.squad = squad;
	}

	public String getTossWinnerTeam() {
		return tossWinnerTeam;
	}

	public void setTossWinnerTeam(String tossWinnerTeam) {
		this.tossWinnerTeam = tossWinnerTeam;
	}

	public boolean isMatchStarted() {
		return matchStarted;
	}

	public void setMatchStarted(boolean matchStarted) {
		this.matchStarted = matchStarted;
	}

	public String getWinnerTeam() {
		return winnerTeam;
	}

	public void setWinnerTeam(String winnerTeam) {
		this.winnerTeam = winnerTeam;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
