package com.myteam.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "payment_txn", catalog = "myteam")
public class PaymentTxn extends PowerPlayBase {

	@ManyToOne(fetch = FetchType.EAGER)
	@NotNull
	@JoinColumn(name = "payment_id", nullable = false)
	private Payment payment;

	@Column(name = "code")
	private String code;
	@Column(name = "pg_name")
	private String pgName;
	@Column(name = "status")
	private String status;
	@Column(name = "pg_txn_id")
	private String pgTxnId;
	@Column(name = "pg_incoming_params")
	private String pgIncomingParams;
	@Column(name = "source")
	private String source;
	@Column(name = "ip_address")
	private String ipAddress;
	@Column(name = "sub_source")
	private String subSource;

	public enum PaymentTxnStatus {

		INIT("INI"), SUCCESS("SCS"), FAILED("FLD"), AUTH_PENDING("APN"), TIMEOUT("TMT");

		private String code;

		private PaymentTxnStatus(String code) {
			this.code = code;
		}

		public String code() {
			return code;
		}

	}

	public Payment getPayment() {
		return payment;
	}

	public void setPayment(Payment payment) {
		this.payment = payment;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getPgName() {
		return pgName;
	}

	public void setPgName(String pgName) {
		this.pgName = pgName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPgTxnId() {
		return pgTxnId;
	}

	public void setPgTxnId(String pgTxnId) {
		this.pgTxnId = pgTxnId;
	}

	public String getPgIncomingParams() {
		return pgIncomingParams;
	}

	public void setPgIncomingParams(String pgIncomingParams) {
		this.pgIncomingParams = pgIncomingParams;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getSubSource() {
		return subSource;
	}

	public void setSubSource(String subSource) {
		this.subSource = subSource;
	}
}
