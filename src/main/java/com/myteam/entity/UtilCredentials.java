package com.myteam.entity;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "credentials", catalog = "myteam")
public class UtilCredentials {

    private int id;
    private String host;
    private String login;
    private String password;
    private String port;
    private String from;
    private String type;
    private String mask;
    private String apiUrl;
    private String fromName;
    private String replyTo;
    private String replyToName;
    private String ccRecipients;
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    
    @Column(name = "host")
    public String getHost() {
        return host;
    }
    public void setHost(String host) {
        this.host = host;
    }
    
    @Column(name = "login")
    public String getLogin() {
        return login;
    }
    public void setLogin(String login) {
        this.login = login;
    }
    
    @Column(name = "password")
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    
    @Column(name = "port")
    public String getPort() {
        return port;
    }
    public void setPort(String port) {
        this.port = port;
    }
    
    @Column(name = "from")
    public String getFrom() {
        return from;
    }
    public void setFrom(String from) {
        this.from = from;
    }
    
    @Column(name = "type")
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    
    @Column(name = "mask")
    public String getMask() {
        return mask;
    }
    public void setMask(String mask) {
        this.mask = mask;
    }
    
    @Column(name = "api_url")
    public String getApiUrl() {
        return apiUrl;
    }
    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }
    
    @Column(name = "from_name")
    public String getFromName() {
        return fromName;
    }
    public void setFromName(String fromName) {
        this.fromName = fromName;
    }
    
    @Column(name = "reply_to")
    public String getReplyTo() {
        return replyTo;
    }
    public void setReplyTo(String replyTo) {
        this.replyTo = replyTo;
    }
    
    @Column(name = "reply_to_name")
    public String getReplyToName() {
        return replyToName;
    }
    public void setReplyToName(String replyToName) {
        this.replyToName = replyToName;
    }
    
    @Column(name = "cc_recipients")
    public String getCcRecipients() {
        return ccRecipients;
    }
    public void setCcRecipients(String ccRecipients) {
        this.ccRecipients = ccRecipients;
    }

}
