package com.myteam.enums;

public enum PaytmTransactionStatus {

	TXN_SUCCESS("TXN_SUCCESS"), TXN_FAILURE("TXN_FAILURE"), PENDING("PENDING"), OPEN("OPEN"), TXN_PROCESSING(
			"TXN_PROCESSING");
	private String code;

	private PaytmTransactionStatus(String code) {
		this.code = code;
	}

	public String code() {
		return code;
	}


}
