package com.myteam.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.myteam.doc.UserAuthToken;
import com.myteam.entity.PaymentTxn;
import com.myteam.entity.PaymentTxn.PaymentTxnStatus;
import com.myteam.entity.User;
import com.myteam.entity.Wallet;
import com.myteam.exception.MyTeamException;
import com.myteam.payment.gateway.service.AbstractPaymentGatewayProvider.PaymentRedirectForm;
import com.myteam.payment.gateway.service.PaytmPaymentGatewayService;
import com.myteam.request.GetWalletHistoryRequest;
import com.myteam.request.PaymentRequest;
import com.myteam.respponse.GetUserWalletResponse;
import com.myteam.respponse.InitiatePaymentResponse;
import com.myteam.respponse.InitiateWalletResponse;
import com.myteam.respponse.PaytmTransactionResponse;
import com.myteam.respponse.ServiceResponse;
import com.myteam.services.IPaymentService;
import com.myteam.services.IUserService;

@Controller
@RequestMapping("/payment")
public class PaymentController {

    private static final Logger LOG = LoggerFactory.getLogger(PaymentController.class);
    
    @Autowired
    IPaymentService paymentService;
    
    @Autowired
    PaytmPaymentGatewayService paytmPaymentGatewayService;
    
    @Autowired
    IUserService  userService;

    @RequestMapping("/getWallet")
	@ResponseBody
	public GetUserWalletResponse getWallet(@RequestBody GetWalletHistoryRequest request) {
    	GetUserWalletResponse response = new GetUserWalletResponse();
		try {
			UserAuthToken authToken = null;
			User user = null;
			try {
				authToken = userService.getAuthToken(request.getAuthToken());
				if (authToken == null) {
					response.setStatus(402);
					response.setMessage("Unable to logout. Please try again.");
					return response;
				}
				user = userService.getUserByUserId(authToken.getUserId());
			} catch (Exception exception) {
				response.setStatus(500);
				response.setMessage("Something went wrong");
				return response;
			}
			if (user == null) {
				response.setStatus(402);
				response.setMessage("Unable to logout. Please try again.");
				return response;
			}
			response = paymentService.getWalletDetails(request, user);
			response.setStatus(200);
			response.setMessage("success");
		} catch (MyTeamException mte) {
			LOG.info("MyTeam Exception occurred :{}", mte);
			response.setMessage(mte.getMessage());
			response.setStatus(500);
		} catch (Exception e) {
			LOG.info("Exception occurred :{}", e);
			response.setMessage("Someing went wrong");
			response.setStatus(500);
		}
		return response;
	}
    
    @RequestMapping(value ="/initiatePaymentWeb",method=RequestMethod.GET)
	public String initiatePaymentWeb(PaymentRequest request, Model model) {
    	LOG.info("Request received to initiate payment :{}", request);
    	InitiateWalletResponse response = new InitiateWalletResponse();
		try {
			UserAuthToken authToken = null;
			User user = null;
			try {
				authToken = userService.getAuthToken(request.getAuthToken());
				if (authToken == null) {
					response.setStatus(402);
					response.setMessage("Unable to logout. Please try again.");
					return new Gson().toJson(response);
				}
				user = userService.getUserByUserId(authToken.getUserId());
			} catch (Exception exception) {
				response.setStatus(500);
				response.setMessage("Something went wrong");
				return new Gson().toJson(response);
			}
			if (user == null) {
				response.setStatus(402);
				response.setMessage("Unable to logout. Please try again.");
				return new Gson().toJson(response);
			}
			InitiatePaymentResponse initiatePaymentResponse= paymentService.initatePayment(request, user);
			initiatePaymentResponse = paymentService.createPaymentTxn(initiatePaymentResponse);
			initiatePaymentResponse.setFromWeb(true);
			PaymentRedirectForm paymentRedirectForm = paymentService.getPaymentRedirectForm(initiatePaymentResponse, user);
			String hiddenFields = getHiddenFields(paymentRedirectForm);
			model.addAttribute("hiddenFields",hiddenFields);
			model.addAttribute("makeTxnUrl",paymentRedirectForm.getParams().get("MAKE_TXN_URL"));
			response.setParams(paymentRedirectForm.getParams());
		} catch (MyTeamException mte) {
			LOG.info("MyTeam Exception occurred :{}", mte);
			response.setMessage(mte.getMessage());
			response.setStatus(500);
		} catch (Exception e) {
			LOG.info("Exception occurred :{}", e);
			response.setMessage("Someing went wrong");
			response.setStatus(500);
		}
		return "processPayment"; 
	}
    
    @RequestMapping(value ="/initiatePayment",method=RequestMethod.POST)
    @ResponseBody
	public InitiateWalletResponse initiatePayment(@RequestBody PaymentRequest request) {
    	InitiateWalletResponse response = new InitiateWalletResponse();
		try {
			UserAuthToken authToken = null;
			User user = null;
			try {
				authToken = userService.getAuthToken(request.getAuthToken());
				if (authToken == null) {
					response.setStatus(402);
					response.setMessage("Unable to logout. Please try again.");
					return response;
				}
				user = userService.getUserByUserId(authToken.getUserId());
			} catch (Exception exception) {
				response.setStatus(500);
				response.setMessage("Something went wrong");
				return response;
			}
			if (user == null) {
				response.setStatus(402);
				response.setMessage("Unable to logout. Please try again.");
				return response;
			}
			InitiatePaymentResponse initiatePaymentResponse= paymentService.initatePayment(request, user);
			initiatePaymentResponse = paymentService.createPaymentTxn(initiatePaymentResponse);
			PaymentRedirectForm paymentRedirectForm = paymentService.getPaymentRedirectForm(initiatePaymentResponse, user);
			response.setParams(paymentRedirectForm.getParams());
		} catch (MyTeamException mte) {
			LOG.info("MyTeam Exception occurred :{}", mte);
			response.setMessage(mte.getMessage());
			response.setStatus(500);
		} catch (Exception e) {
			LOG.info("Exception occurred :{}", e);
			response.setMessage("Someing went wrong");
			response.setStatus(500);
		}
		return response; 
	}
    
    private String getHiddenFields(PaymentRedirectForm paymentRedirectForm) {
    	StringBuilder res = new StringBuilder();
    	res.append("<input type=\"hidden\" name=\"MID\" value=\"").append(paymentRedirectForm.getParams().get("MID")).append("\">");
    	res.append("<input type=\"hidden\" name=\"WEBSITE\" value=\"").append(paymentRedirectForm.getParams().get("WEBSITE")).append("\">");
    	res.append("<input type=\"hidden\" name=\"ORDER_ID\" value=\"").append(paymentRedirectForm.getParams().get("ORDER_ID")).append("\">");
    	res.append("<input type=\"hidden\" name=\"CUST_ID\" value=\"").append(paymentRedirectForm.getParams().get("CUST_ID")).append("\">");
    	res.append("<input type=\"hidden\" name=\"MOBILE_NO\" value=\"").append(paymentRedirectForm.getParams().get("MOBILE_NO")).append("\">");
    	res.append("<input type=\"hidden\" name=\"EMAIL\" value=\"").append(paymentRedirectForm.getParams().get("EMAIL")).append("\">");
    	res.append("<input type=\"hidden\" name=\"INDUSTRY_TYPE_ID\" value=\"").append(paymentRedirectForm.getParams().get("INDUSTRY_TYPE_ID")).append("\">");
    	res.append("<input type=\"hidden\" name=\"CHANNEL_ID\" value=\"").append(paymentRedirectForm.getParams().get("CHANNEL_ID")).append("\">");
    	res.append("<input type=\"hidden\" name=\"TXN_AMOUNT\" value=\"").append(paymentRedirectForm.getParams().get("TXN_AMOUNT")).append("\">");
    	res.append("<input type=\"hidden\" name=\"CALLBACK_URL\" value=\"").append(paymentRedirectForm.getParams().get("CALLBACK_URL")).append("\">");
    	res.append("<input type=\"hidden\" name=\"CHECKSUMHASH\" value=\"").append(paymentRedirectForm.getParams().get("CHECKSUMHASH")).append("\">");
    	return res.toString();
	}

	@RequestMapping("/paytmResponseWeb")
	public String pgPaytmResponseWeb(PaytmTransactionResponse request, Model model) {
		LOG.info("PG incoming params: {}", request);
		LOG.info("PG incoming params: {} and {} and :{}", request.getTXNAMOUNT(), request.getSTATUS(), request.getRESPMSG());
		model.addAttribute("orderCode", request.getORDERID());
		model.addAttribute("response", request.getSTATUS());
		ServiceResponse response = new ServiceResponse();
		PaymentTxn paymentTxn = null;
		try {
			paymentTxn = paymentService.validatePaytmPaymentRequest(request);
			if (paymentTxn != null) {
				Wallet wallet = paymentService.getUpdatedWalletAfterPaymentTxn(paymentTxn.getCode());
				model.addAttribute("walletBalance", wallet.getAmount());
				response.setStatus(200);
				response.setMessage("success");
			}
		} catch (MyTeamException mte) {
			LOG.error("Error while validating payment exception:{}:{}", mte);
			response.setStatus(202);
			response.setMessage(mte.getMessage());
		} catch (Exception e) {
			LOG.error("Error while validating payment :{}:{}", e);
			response.setStatus(500);
			response.setMessage("Something went wrong");
		}
		return "paymentResponse";
	}
	
	@RequestMapping("/paytmResponse")
	@ResponseBody
	public ServiceResponse pgPaytmResponse(PaytmTransactionResponse request) {
		LOG.info("PG incoming params: {}", request);
		ServiceResponse response = new ServiceResponse();
		PaymentTxn paymentTxn = null;
		try {
			paymentTxn = paymentService.validatePaytmPaymentRequest(request);
			if (paymentTxn != null) {
				if(paymentTxn.getStatus().equalsIgnoreCase(PaymentTxnStatus.SUCCESS.code())) {
					response.setStatus(200);
					response.setMessage("success");
				}else {
					response.setStatus(202);
					response.setMessage("complete but not success");
				}
			}
		} catch (MyTeamException mte) {
			LOG.error("Error while validating payment exception:{}:{}", mte);
			response.setStatus(202);
			response.setMessage(mte.getMessage());
		} catch (Exception e) {
			LOG.error("Error while validating payment :{}:{}", e);
			response.setStatus(500);
			response.setMessage("Something went wrong");
		}
		return response;
	}
}
