package com.myteam.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.myteam.cache.CacheManager;
import com.myteam.cache.SystemPropertiesCache;
import com.myteam.doc.UserAuthToken;
import com.myteam.exception.MyTeamException;
import com.myteam.request.CreateTeamPlayersRequest;
import com.myteam.request.GetAllTeamsPointsRequest;
import com.myteam.request.GetAppLinkRequest;
import com.myteam.request.GetFutureMatchRequest;
import com.myteam.request.GetJoinedContestRequest;
import com.myteam.request.GetMatchDetailRequest;
import com.myteam.request.GetMatchStatsRequest;
import com.myteam.request.GetMatchWiseUserTeamRequest;
import com.myteam.request.GetMatchWiseUserTeamResponse;
import com.myteam.request.GetPlayerStatsRequest;
import com.myteam.request.GetSpecificTeamPointsRequest;
import com.myteam.request.GetWinningBreakUpRequest;
import com.myteam.request.JoinContestRequest;
import com.myteam.respponse.AppBaseConfigResponse;
import com.myteam.respponse.CreateTeamPlayersResponse;
import com.myteam.respponse.GetAllTeamsPointsResponse;
import com.myteam.respponse.GetFutureMatchResponse;
import com.myteam.respponse.GetMatchDetailsResponse;
import com.myteam.respponse.GetMatchStatsResponse;
import com.myteam.respponse.GetPlayerStatsResponse;
import com.myteam.respponse.GetSpecificTeamPointsResponse;
import com.myteam.respponse.GetTeamAndJoinedContestCountResponse;
import com.myteam.respponse.GetWinningBreakUpResponse;
import com.myteam.respponse.JoinContestResponse;
import com.myteam.respponse.ServiceResponse;
import com.myteam.services.ICricAPIService;
import com.myteam.services.IPowerPlayService;
import com.myteam.services.IUserService;
import com.myteam.utils.StringUtils;

@Controller
public class PowerPlayAppController {
	
    private static final Logger LOG = LoggerFactory.getLogger(PowerPlayAppController.class);
    
    @Autowired
    ICricAPIService leaguesService ;
    
    @Autowired
    IPowerPlayService powerPlayService;
    
    @Autowired
    IUserService userService;
    
    @RequestMapping("/getAppBaseConfig")
	@ResponseBody
	public ServiceResponse getAppBaseConfig() {
    	LOG.info("Requesting to getAppBaseConfig");
    	AppBaseConfigResponse response = new AppBaseConfigResponse();
    	try {
    		response.setBaseUrl(CacheManager.getInstance().getCache(SystemPropertiesCache.class).getSystemPropertiesByName(SystemPropertiesCache.POWERPLAY_BASE_URL));
    		response.setS3Path(CacheManager.getInstance().getCache(SystemPropertiesCache.class).getSystemPropertiesByName(SystemPropertiesCache.POWERPLAY_S3_PATH));
    		response.setBannerUrls(StringUtils.getStringListFromString(CacheManager.getInstance().getCache(SystemPropertiesCache.class).getSystemPropertiesByName(SystemPropertiesCache.POWERPLAY_BANNER_URLS), ","));
    		response.setMaxNumberAllowed(Integer.parseInt(CacheManager.getInstance().getCache(SystemPropertiesCache.class).getSystemPropertiesByName(SystemPropertiesCache.POWERPLAY_MAXIMUM_ALLOWED_PLAYERS)));
    		response.setStatus(200);
			response.setMessage("success");
		} catch (Exception e) {
			LOG.info("Exception occurred :{}", e);
			response.setMessage("Someing went wrong");
			response.setStatus(500);
		}
		return response;
	}
    
    @RequestMapping("/getFutureMatches")
	@ResponseBody
	public ServiceResponse getMatches(@RequestBody GetFutureMatchRequest request) {
    	LOG.info("Request to get matches is :{}", request);
    	GetFutureMatchResponse response = new GetFutureMatchResponse();
    	try {
			response.setMatches(leaguesService.getMatches(request));
			response.setStatus(200);
			response.setMessage("success");
		} catch (Exception e) {
			LOG.info("Exception occurred :{}", e);
			response.setMessage("Someing went wrong");
			response.setStatus(500);
		}
		return response;
	}
    
    @RequestMapping("/getUserMatches")
	@ResponseBody
	public ServiceResponse getUserMatches(@RequestBody GetFutureMatchRequest request) {
    	LOG.info("Request to get user matches is :{}", request);
    	GetFutureMatchResponse response = new GetFutureMatchResponse();
    	UserAuthToken authToken = null;
		try {
			authToken = userService.getAuthToken(request.getAuthToken());
		} catch (Exception exception) {
			response.setStatus(500);
			response.setMessage("Something went wrong");
			return response;
		}
		if (authToken == null) {
			LOG.info("User not looged in :{}");
			response.setStatus(402);
			response.setMessage("User not looged in ");
			return response;
		}
		try {
			response.setMatches(leaguesService.getMatches(request));
			response.setStatus(200);
			response.setMessage("success");
		} catch (Exception e) {
			LOG.info("Exception occurred :{}", e);
			response.setMessage("Someing went wrong");
			response.setStatus(500);
		}
		return response;
	}
    
    @RequestMapping("/getMatchDetails")
	@ResponseBody
	public GetMatchDetailsResponse getMatchDetails(GetMatchDetailRequest request) {
    	LOG.info("Request to get matches detail is :{}", request);
    	GetMatchDetailsResponse response = new GetMatchDetailsResponse();
		try {
			response = leaguesService.getMatchDetails(request.getUnique_id());
			response.setStatus(200);
			response.setMessage("success");
			LOG.info("Response to get matches detail is :{}", response);
		} catch (MyTeamException mte) {
			LOG.error("Exception occurred :{}", mte);
			response.setMessage(mte.getMessage());
			response.setStatus(mte.getCode());
		}catch (Exception e) {
			LOG.error("Exception occurred :{}", e);
			response.setMessage("Someing went wrong");
			response.setStatus(500);
		}
		return response;
	}
    
    @RequestMapping("/getPlayerStats")
	@ResponseBody
	public ServiceResponse getPlayerStats(GetPlayerStatsRequest request) {
    	GetPlayerStatsResponse response = new GetPlayerStatsResponse();
		try {
			response = leaguesService.getPlayerStats(request.getPid());
			response.setStatus(200);
			response.setMessage("success");
		} catch (Exception e) {
			LOG.info("Exception occurred :{}", e);
			response.setMessage("Someing went wrong");
			response.setStatus(500);
		}
		return response;
	}
    
    @RequestMapping("/getMatchStats")
	@ResponseBody
	public ServiceResponse getMatchStats(@RequestBody GetMatchStatsRequest request) {
    	LOG.info("Request to get match stats :{}",request);
    	GetMatchStatsResponse response = new GetMatchStatsResponse();
		try {
			response = leaguesService.getMatchStats(request.getUnique_id());
			response.setStatus(200);
			response.setMessage("success");
			LOG.info("Response to get match stats :{}",response);	
		} catch (Exception e) {
			LOG.info("Exception occurred :{}", e);
			response.setMessage("Someing went wrong");
			response.setStatus(500);
		}
		return response;
	}
	
    @RequestMapping("/createTeam")
	@ResponseBody
	public CreateTeamPlayersResponse createTeam(@RequestBody CreateTeamPlayersRequest request) {
    	LOG.info("Request to create team is :{} :{}", request, request.getAuthToken());
    	CreateTeamPlayersResponse response = new CreateTeamPlayersResponse();
		try {
			UserAuthToken authToken = null;
			try {
				authToken = userService.getAuthToken(request.getAuthToken());
			} catch (Exception exception) {
				response.setStatus(500);
				response.setMessage("Something went wrong");
				return response;
			}
			if (authToken == null) {
				LOG.info("User not looged in :{}");
				response.setStatus(402);
				response.setMessage("User not looged in ");
				return response;
			}
			response = leaguesService.createTeam(request, userService.getUserByUserId(authToken.getUserId()));
			response.setStatus(200);
			response.setMessage("success");
		} catch (MyTeamException mte) {
			LOG.info("MyTeam Exception occurred :{}", mte);
			response.setMessage(mte.getMessage());
			response.setStatus(500);
		} catch (Exception e) {
			LOG.info("Exception occurred :{}", e);
			response.setMessage("Someing went wrong");
			response.setStatus(500);
		}
		return response;
	}
    
    @RequestMapping("/joinContest")
	@ResponseBody
	public JoinContestResponse joinContest(@RequestBody JoinContestRequest request) {
    	LOG.info("Request to join contest :{}", request);
    	JoinContestResponse response = new JoinContestResponse();
		try {
			UserAuthToken authToken = null;
			try {
				authToken = userService.getAuthToken(request.getAuthToken());
			} catch (Exception exception) {
				response.setStatus(500);
				response.setMessage("Something went wrong");
				return response;
			}
			if (authToken == null) {
				LOG.info("User not looged in :{}");
				response.setStatus(402);
				response.setMessage("User not looged in ");
				return response;
			}
			response = leaguesService.joinContest(request, userService.getUserByUserId(authToken.getUserId()));
		} catch (MyTeamException mte) {
			LOG.info("MyTeam Exception occurred :{}", mte);
			response.setMessage(mte.getMessage());
			response.setStatus(201);
		} catch (Exception e) {
			LOG.info("Exception occurred :{}", e);
			response.setMessage("Someing went wrong");
			response.setStatus(500);
		}
		return response;
	}
    
    @RequestMapping("/getAllTeamsPoints")
	@ResponseBody
	public GetAllTeamsPointsResponse getAllTeamsPoints(@RequestBody GetAllTeamsPointsRequest request) {
    	LOG.info("Request to get all teams :{}", request);
    	GetAllTeamsPointsResponse response = new GetAllTeamsPointsResponse();
		try {
			UserAuthToken authToken = null;
			try {
				authToken = userService.getAuthToken(request.getAuthToken());
			} catch (Exception exception) {
				response.setStatus(500);
				response.setMessage("Something went wrong");
				return response;
			}
			if (authToken == null) {
				LOG.info("User not looged in :{}");
				response.setStatus(402);
				response.setMessage("User not looged in ");
				return response;
			}
			response = powerPlayService.getUserMatchContestPlayersMappingByContestId(request, userService.getUserByUserId(authToken.getUserId()));
			response.setStatus(200);
			response.setMessage("success");
			LOG.info("Response to get all teams is :{}", response);
		} catch (MyTeamException mte) {
			LOG.info("MyTeam Exception occurred :{}", mte);
			response.setMessage(mte.getMessage());
			response.setStatus(500);
		} catch (Exception e) {
			LOG.info("Exception occurred :{}", e);
			response.setMessage("Someing went wrong");
			response.setStatus(500);
		}
		return response;
	}
    
    @RequestMapping("/getTeams")
	@ResponseBody
	public GetMatchWiseUserTeamResponse getMatchWiseUserTeam(@RequestBody GetMatchWiseUserTeamRequest request) {
    	LOG.info("Request to get teams :{}",request );
    	GetMatchWiseUserTeamResponse response = new GetMatchWiseUserTeamResponse();
		try {
			UserAuthToken authToken = null;
			try {
				authToken = userService.getAuthToken(request.getAuthToken());
			} catch (Exception exception) {
				response.setStatus(500);
				response.setMessage("Something went wrong");
				return response;
			}
			if (authToken == null) {
				LOG.info("User not looged in :{}");
				response.setStatus(402);
				response.setMessage("User not looged in ");
				return response;
			}
			response = powerPlayService.getTeamByMatchId(request, userService.getUserByUserId(authToken.getUserId()));
			response.setStatus(200);
			response.setMessage("success");
			LOG.info("Response to get teams :{}",response );
		} catch (MyTeamException mte) {
			LOG.info("MyTeam Exception occurred :{}", mte);
			response.setMessage(mte.getMessage());
			response.setStatus(500);
		} catch (Exception e) {
			LOG.info("Exception occurred :{}", e);
			response.setMessage("Someing went wrong");
			response.setStatus(500);
		}
		return response;
	}
    
    @RequestMapping("/getWinningBreakUp")
	@ResponseBody
	public GetWinningBreakUpResponse getWinningBreakUp(@RequestBody GetWinningBreakUpRequest request) {
    	LOG.info("Request to get winning break up is :{}", request);
    	GetWinningBreakUpResponse response = new GetWinningBreakUpResponse();
		try {
			response = powerPlayService.getWinningBreakUp(request);
			response.setStatus(200);
			response.setMessage("success");
		} catch (MyTeamException mte) {
			LOG.info("MyTeam Exception occurred :{}", mte);
			response.setMessage(mte.getMessage());
			response.setStatus(500);
		} catch (Exception e) {
			LOG.info("Exception occurred :{}", e);
			response.setMessage("Someing went wrong");
			response.setStatus(500);
		}
		return response;
	}
    
    @RequestMapping("/getSpecificTeamPoints")
	@ResponseBody
	public GetSpecificTeamPointsResponse getSpecificTeamPoints(@RequestBody GetSpecificTeamPointsRequest request) {
    	LOG.info("Request to get specific team points :{}",request);
    	GetSpecificTeamPointsResponse response = new GetSpecificTeamPointsResponse();
		try {
			response = powerPlayService.getSpecificTeamPointsResponse(request);
			response.setStatus(200);
			response.setMessage("success");
			LOG.info("Response to get specific team points :{}",response);
		} catch (MyTeamException mte) {
			LOG.info("MyTeam Exception occurred :{}", mte);
			response.setMessage(mte.getMessage());
			response.setStatus(500);
		} catch (Exception e) {
			LOG.info("Exception occurred :{}", e);
			response.setMessage("Someing went wrong");
			response.setStatus(500);
		}
		return response;
	}
    
    @RequestMapping("/getJoinedContests")
	@ResponseBody
	public JoinContestResponse getJoinedContests(@RequestBody GetJoinedContestRequest request) {
    	LOG.info("Request to get all joined contests :{}", request);
    	JoinContestResponse response = new JoinContestResponse();
    	UserAuthToken authToken = null;
		try {
			authToken = userService.getAuthToken(request.getAuthToken());
		} catch (Exception exception) {
			response.setStatus(500);
			response.setMessage("Something went wrong");
			return response;
		}
		if (authToken == null) {
			LOG.info("User not looged in :{}");
			response.setStatus(402);
			response.setMessage("User not looged in ");
			return response;
		}
    	try {
			response.setJoinedContests(powerPlayService.getJoinedContests(request, authToken.getUserId()));
			response.setStatus(200);
			response.setMessage("success");
		} catch (MyTeamException mte) {
			LOG.info("MyTeam Exception occurred :{}", mte);
			response.setMessage(mte.getMessage());
			response.setStatus(500);
		} catch (Exception e) {
			LOG.info("Exception occurred :{}", e);
			response.setMessage("Someing went wrong");
			response.setStatus(500);
		}
		return response;
	}
    
    @RequestMapping("/getTeamAndJoinedContestCount")
	@ResponseBody
	public GetTeamAndJoinedContestCountResponse getTeamAndJoinedContestCount(@RequestBody GetJoinedContestRequest request) {
    	LOG.info("Request to getTeamAndJoinedContestCount :{}", request);
    	GetTeamAndJoinedContestCountResponse response = new GetTeamAndJoinedContestCountResponse();
    	UserAuthToken authToken = null;
		try {
			authToken = userService.getAuthToken(request.getAuthToken());
		} catch (Exception exception) {
			response.setStatus(500);
			response.setMessage("Something went wrong");
			return response;
		}
		if (authToken == null) {
			LOG.info("User not looged in :{}");
			response.setStatus(402);
			response.setMessage("User not looged in ");
			return response;
		}
    	try {
    		response = powerPlayService.getTeamAndJoinedContestCountResponse(request, authToken.getUserId());
			response.setStatus(200);
			response.setMessage("success");
			LOG.info("Response to getTeamAndJoinedContestCount :{}", response);
		} catch (MyTeamException mte) {
			LOG.info("MyTeam Exception occurred :{}", mte);
			response.setMessage(mte.getMessage());
			response.setStatus(500);
		} catch (Exception e) {
			LOG.info("Exception occurred :{}", e);
			response.setMessage("Someing went wrong");
			response.setStatus(500);
		}
		return response;
	}
	
    @RequestMapping("/sendAppLink")
	@ResponseBody
	public ServiceResponse sendAppLink(@Valid GetAppLinkRequest request) {
		ServiceResponse response = new ServiceResponse();
		try {
			powerPlayService.sendAppLink(request);
			response.setMessage("success");
			response.setStatus(200);
		} catch (MyTeamException mte) {
			LOG.error("Exception occurred :{}", mte);
			response.setMessage(mte.getMessage());
			response.setStatus(mte.getCode());
		} catch (Exception e) {
			LOG.error("Exception occurred :{}", e);
			response.setMessage("Someing went wrong");
			response.setStatus(500);
		}
		return response;
	}
}
