package com.myteam.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.myteam.cron.CricApiHandler;
import com.myteam.dto.CreateContestRequest;
import com.myteam.services.ICricAPIService;
import com.myteam.services.IPowerPlayService;
import com.myteam.services.IPowerPlayStartupService;

@Controller
@RequestMapping("/cronTrigger")
public class CronTriggerController {
	
    private static final Logger LOG = LoggerFactory.getLogger(CronTriggerController.class);

	@Autowired
	CricApiHandler cricApiHandler;
	
	@Autowired
	IPowerPlayStartupService powerplayStartupService;
	
	@Autowired
	ICricAPIService cricApiService;
	
	@RequestMapping("/processFutureMatches")
    @ResponseBody
	public String processFutureMatches() {
    	cricApiHandler.processFutureMatches();
    	return "success";
    }
	
	@RequestMapping("/populateSquad")
    @ResponseBody
	public String populateSquad() {
    	cricApiHandler.populateSquad();
		return "success";
    	
    }
	
	@RequestMapping("/updatePoints")
    @ResponseBody
	public String updatePoints() {
    	cricApiHandler.updatePoints();
		return "success";
    	
    }
	
	@RequestMapping("/updateMatchPlayerFantasyPoints")
    @ResponseBody
	public String updateMatchPlayerFantasyPoints() {
    	cricApiHandler.updateMatchPlayerFantasyPoints();
		return "success";
    	
    }
	
	@RequestMapping("/updateWinningAmount")
    @ResponseBody
	public String updateWinningAmount() {
    	cricApiHandler.updateWinningAmount();
		return "success";
    	
    }
	
	@RequestMapping("/loadSystemProperties")
    @ResponseBody
	public String loadSystemProperties() {
		powerplayStartupService.loadSystemProperties();
		return "success";
    	
    }
	
	@RequestMapping("/createContests")
    @ResponseBody
	public String createContests(CreateContestRequest request) {
		LOG.info("Request to create contest is :{}",request);
		cricApiService.createContests(request.getMatchIds());
		return "success";
    	
    }
	
	@RequestMapping("/createMegaContests")
    @ResponseBody
	public String createMegaContests(CreateContestRequest request) {
		LOG.info("Request to create mega contest is :{}",request);
		cricApiService.createMegaContests(request.getMatchIds());
		return "success";
    	
    }
}


