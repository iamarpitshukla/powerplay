package com.myteam.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/file")
public class FileController {
	
	private static final Logger LOG = LoggerFactory.getLogger(FileController.class);

	@RequestMapping(value="/apk/{fileName:.+}")
    public void getApp(HttpSession session,HttpServletResponse response, @PathVariable("fileName") String fileName) throws Exception {
		LOG.info("Request to download file is");
		try {
            String filePathToBeServed = "/usr/local/tempdata/";
            if (!filePathToBeServed.endsWith("/")) {
                filePathToBeServed = filePathToBeServed + "/";
            }
            if(fileName.equals("Powerplay_Android.apk")){
                filePathToBeServed= filePathToBeServed + "apk/android/"+  fileName;
            }else{
            	  filePathToBeServed= filePathToBeServed + "apk/ios/"+  fileName;
            }
            File fileToDownload = new File(filePathToBeServed);
            InputStream inputStream = new FileInputStream(fileToDownload);
            response.setContentType("application/force-download");
            response.setHeader("Content-Disposition", "attachment; filename="+fileName); 
            IOUtils.copy(inputStream, response.getOutputStream());
            response.flushBuffer();
            inputStream.close();
        } catch (Exception e){
            e.printStackTrace();
        }

    }
}
