package com.myteam.controller;

import java.security.Principal;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.myteam.doc.UserAuthToken;
import com.myteam.dto.OtpVerificationRequest;
import com.myteam.dto.UserDTO;
import com.myteam.entity.User;
import com.myteam.exception.MyTeamException;
import com.myteam.mao.service.IPlayersMao;
import com.myteam.request.LoginRequest;
import com.myteam.request.LogoutRequest;
import com.myteam.request.ServiceRequest;
import com.myteam.request.UpdatePasswordRequest;
import com.myteam.respponse.GetUserPersonalDetailResponse;
import com.myteam.respponse.GetUserReferalDetailsResponse;
import com.myteam.respponse.GetUserWalletDetailResponse;
import com.myteam.respponse.LoginResponse;
import com.myteam.respponse.LogoutResponse;
import com.myteam.respponse.ServiceResponse;
import com.myteam.security.MyTeamUser;
import com.myteam.services.IPowerPlayService;
import com.myteam.services.IUserService;
import com.myteam.utils.EmailSenderUtils;
import com.myteam.utils.StringUtils;
import com.myteam.utils.WebContextUtils;

@Controller
@RequestMapping("/user")
public class UserController {
	
    private static final Logger LOG = LoggerFactory.getLogger(UserController.class);
	@Autowired
	IUserService userService;

	@Autowired
	IPlayersMao squadMao;
	
	@Autowired
	IPowerPlayService powerPlayService;
	
	@Autowired
	EmailSenderUtils emailService;
	
	@RequestMapping(value = "/applogin", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public LoginResponse login(LoginRequest request) {
        LoginResponse response = new LoginResponse();
        LOG.info("Request is :{}",request);
        if ((StringUtils.isEmpty(request.getMobile())) || StringUtils.isEmpty(request.getPassword())) {
        	response.setMessage("Invalid request");
        	response.setStatus(400);
		} else {
			User user = userService.getUserByMobileOrEmail(request.getMobile(), request.getEmail(), true);
			if(user != null && !user.isEnabled()) {
				LOG.info("User is not verified : {}", request.getMobile());
				if(!new BCryptPasswordEncoder().matches(request.getPassword(), user.getPassword())) {
					LOG.info("Login attempt failed for user: {}", request.getMobile());
					response.setMessage("Invalid Details");
					response.setStatus(201);
					return response;
				}
				response.setMessage("Invalid Details");
				response.setStatus(202);
				return response;
			}
			if (user == null || !user.isEnabled() || !new BCryptPasswordEncoder().matches(request.getPassword(), user.getPassword())) {
				LOG.info("Login attempt failed for user: {}", request.getMobile());
				response.setMessage("Invalid Details");
				response.setStatus(201);
			} else {
				try {
					UserAuthToken token = userService.generateAuthToken(user, request.getModel());
					response.setAuthToken(token.getKey());
					response.setStatus(200);
				} catch (Exception exception) {
					response.setStatus(500);
					response.setMessage("Something went wrong");
					return response;
				}
			}
		}
        return response;
    }
	
	@RequestMapping(value = "applogout", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
	public LogoutResponse logout(LogoutRequest request) {
		LogoutResponse response = new LogoutResponse();
		UserAuthToken authToken = null;
		try {
			authToken = userService.getAuthToken(request.getAuthToken());
		} catch (Exception exception) {
			response.setStatus(500);
			response.setMessage("Something went wrong");
			return response;
		}
		if (authToken == null) {
			response.setStatus(402);
			response.setMessage("Unable to logout. Please try again.");
			return response;
		}
		try {
			userService.invalidateAuthToken(authToken);
		} catch (Exception exception) {
			LOG.info("Unable to logout :{}", exception);
			response.setStatus(402);
			response.setMessage("Unable to logout. Please try again.");
			return response;
		}
		response.setStatus(200);
		response.setMessage("Successfully logged out");
		return response;
	}
	
	@RequestMapping("/register")
	@ResponseBody
	public ServiceResponse register(@Valid UserDTO dto) {
		LOG.info("Response to register user is :{}",dto);
		ServiceResponse response = new ServiceResponse();
		try {
			User user = userService.getUserByMobileOrEmail(dto.getUsername(), null, true);
			if (user != null) {
				if(!user.isEnabled()) {
					response.setMessage("Already Registered, please verify to proceed");
					response.setStatus(202);
					return response;
				}else {
					response.setMessage("Already Registered with same mobile.");
					response.setStatus(203);
					return response;
				}
			}
			powerPlayService.createNewUser(dto);
			response.setMessage("success");
			response.setStatus(200);
			LOG.info("Response to register user is :{}",response);
		} catch (MyTeamException mte) {
			LOG.error("Exception occurred :{}", mte);
			response.setMessage(mte.getMessage());
			response.setStatus(202);
		}catch (DataIntegrityViolationException mte) {
			LOG.error("Exception occurred :{}", mte);
			response.setMessage(mte.getMessage());
			response.setStatus(200);
		} catch (Exception e) {
			LOG.error("Exception occurred :{}", e);
			response.setMessage("Someing went wrong");
			response.setStatus(500);
		}
		return response;
	}
	
	@RequestMapping("/verifyNewUser")
	@ResponseBody
	public ServiceResponse verifyNewUser(@Valid OtpVerificationRequest request) {
		ServiceResponse response = new ServiceResponse();
		LOG.info("Request to verify user is :{}",request);
		try {
			powerPlayService.verifyNewUser(request);
			response.setMessage("success");
			response.setStatus(200);
		} catch (MyTeamException mte) {
			LOG.error("Exception occurred :{}", mte);
			response.setMessage(mte.getMessage());
			response.setStatus(mte.getCode());
		} catch (Exception e) {
			LOG.error("Exception occurred :{}", e);
			response.setMessage("Someing went wrong");
			response.setStatus(500);
		}
		return response;
	}
	
	@RequestMapping("/verifyOTP")
	@ResponseBody
	public ServiceResponse verifyOtp(@Valid OtpVerificationRequest request) {
		ServiceResponse response = new ServiceResponse();
		LOG.info("Request to verify user is :{}",request);
		try {
			powerPlayService.verifyNewUser(request);
			response.setMessage("success");
			response.setStatus(200);
		} catch (MyTeamException mte) {
			LOG.error("Exception occurred :{}", mte);
			response.setMessage(mte.getMessage());
			response.setStatus(mte.getCode());
		} catch (Exception e) {
			LOG.error("Exception occurred :{}", e);
			response.setMessage("Someing went wrong");
			response.setStatus(500);
		}
		return response;
	}
	
	@RequestMapping("/sendOtp")
	@ResponseBody
	public ServiceResponse sendOtp(@Valid OtpVerificationRequest request) {
		ServiceResponse response = new ServiceResponse();
		LOG.info("Request to send otp is :{}",request);
		try {
			powerPlayService.sendOtp(request);
			response.setMessage("success");
			response.setStatus(200);
		} catch (MyTeamException mte) {
			LOG.error("Exception occurred :{}", mte);
			response.setMessage(mte.getMessage());
			response.setStatus(mte.getCode());
		} catch (Exception e) {
			LOG.error("Exception occurred :{}", e);
			response.setMessage("Someing went wrong");
			response.setStatus(500);
		}
		return response;
	}
	
	@RequestMapping("/updatePassword")
	@ResponseBody
	public ServiceResponse updatePassword(@Valid UpdatePasswordRequest request) {
		ServiceResponse response = new ServiceResponse();
		try {
			powerPlayService.updatePassword(request);
			response.setMessage("success");
			response.setStatus(200);
		} catch (MyTeamException mte) {
			LOG.error("Exception occurred :{}", mte);
			response.setMessage(mte.getMessage());
			response.setStatus(mte.getCode());
		} catch (Exception e) {
			LOG.error("Exception occurred :{}", e);
			response.setMessage("Someing went wrong");
			response.setStatus(500);
		}
		return response;
	}
	
	
	@RequestMapping("/getUserDetails")
	@ResponseBody
	public ServiceResponse getUserDetails(Principal principal) {
		ServiceResponse response = new ServiceResponse();
		try {
			MyTeamUser user = WebContextUtils.getCurrentUser();
			if(user == null) {
				LOG.info("User not looged in :{}");
				response.setMessage("User not looged in ");
				response.setStatus(202);
				return response;
			}
			LOG.info("Found user :{}", user.getUser());
			response.setMessage("success");
			response.setStatus(200);
		} catch (Exception e) {
			LOG.info("Exception occurred :{}", e);
			response.setMessage("Someing went wrong");
			response.setStatus(500);
		}
		return response;
	}
	
	@RequestMapping("/getUserPersonalDetails")
	@ResponseBody
	public ServiceResponse getUserPersonalDetails(@RequestBody ServiceRequest request) {
		GetUserPersonalDetailResponse response = new GetUserPersonalDetailResponse();
		UserAuthToken authToken = null;
		try {
			authToken = userService.getAuthToken(request.getAuthToken());
		} catch (Exception exception) {
			response.setStatus(500);
			response.setMessage("Something went wrong");
			return response;
		}
		if (authToken == null) {
			response.setStatus(402);
			response.setMessage("Invalid user");
			return response;
		}
		try {
			response.setUser(userService.getUserDetailDTO(authToken.getUserId()));
			response.setMessage("success");
			response.setStatus(200);
		} catch (Exception e) {
			LOG.info("Exception occurred :{}", e);
			response.setMessage("Someing went wrong");
			response.setStatus(500);
		}
		return response;
	}
	
	@RequestMapping("/getUserWalletDetails")
	@ResponseBody
	public ServiceResponse getUserWalletDetails(@RequestBody ServiceRequest request) {
		GetUserWalletDetailResponse response = new GetUserWalletDetailResponse();
		LOG.info("request to getUserWalletDetails is :{}", request.getAuthToken());
		UserAuthToken authToken = null;
		try {
			authToken = userService.getAuthToken(request.getAuthToken());
		} catch (Exception exception) {
			response.setStatus(500);
			response.setMessage("Something went wrong");
			return response;
		}
		if (authToken == null) {
			response.setStatus(402);
			response.setMessage("Invalid user");
			return response;
		}
		try {
			response = userService.getUserWalletDetails(authToken.getUserId());
			response.setMessage("success");
			response.setStatus(200);
			LOG.info("Response is :{}", response);
		} catch (Exception e) {
			LOG.info("Exception occurred :{}", e);
			response.setMessage("Someing went wrong");
			response.setStatus(500);
		}
		return response;
	}
	
	@RequestMapping("/getUserReferralDetails")
	@ResponseBody
	public ServiceResponse getUserReferalDetails(@RequestBody ServiceRequest request) {
		GetUserReferalDetailsResponse response = new GetUserReferalDetailsResponse();
		LOG.info("request to getUserReferalDetails is :{}", request.getAuthToken());
		UserAuthToken authToken = null;
		try {
			authToken = userService.getAuthToken(request.getAuthToken());
		} catch (Exception exception) {
			response.setStatus(500);
			response.setMessage("Something went wrong");
			return response;
		}
		if (authToken == null) {
			response.setStatus(402);
			response.setMessage("Invalid user");
			return response;
		}
		try {
			LOG.info("user id is :{}", authToken.getUserId());
			response = userService.getUserReferalDetails(authToken.getUserId());
			response.setMessage("success");
			response.setStatus(200);
			LOG.info("Response is :{}", response);
		} catch (Exception e) {
			LOG.info("Exception occurred :{}", e);
			response.setMessage("Someing went wrong");
			response.setStatus(500);
		}
		return response;
	}
}
