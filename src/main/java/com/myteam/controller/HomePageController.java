package com.myteam.controller;

import java.security.Principal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.myteam.security.MyTeamUser;
import com.myteam.services.IUserService;
import com.myteam.utils.WebContextUtils;

@Controller
public class HomePageController {
	
    private static final Logger LOG = LoggerFactory.getLogger(HomePageController.class);
	
	@Autowired
	IUserService userService;

	@GetMapping("/")
	public String index(Model model, Principal principal) {
		return "homePage";
	}
	
	@GetMapping("/home")
	public String home(Model model, Principal principal) {
		return "homePage";
	}

	@GetMapping("/leagues")
	public String leagues(Model model, Principal principal) {
		MyTeamUser user = WebContextUtils.getCurrentUser();
		if(user != null) {
			LOG.info("User not looged in :{}");
		}
		model.addAttribute("user", user);
		model.addAttribute("leagues", "leagues");
		return "leagues";
	}

}
