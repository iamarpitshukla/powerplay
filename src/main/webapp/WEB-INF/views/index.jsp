<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="../statics/assets/css/main.css" rel="stylesheet" />

    <style>
        body {
            padding-top: 60px;
        }
    </style>
    <link href="../statics/css/login-register.css" rel="stylesheet" />
    <link href="../statics/bootstrap3/css/bootstrap.css" rel="stylesheet" />
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="../statics/js/login-register.js"> </script>
    <script type="text/javascript" src="../statics/bootstrap3/js/bootstrap.js"> </script>
</head>
<body>
    <div class="container">
        <div class="row">
        <h1>MyTeam</h1>
	<h2>${message}</h2>
   <hr />
            <div class="col-sm-3">
            
            </div>
            <div class="col-sm-6">
                 <a class="btn big-login" data-toggle="modal" href="javascript:void(0)" onclick="openLoginModal();">Log in</a>
                 <a class="btn big-register" data-toggle="modal" href="javascript:void(0)" onclick="openRegisterModal();">Register</a></div>
				 <a class="btn btn-danger" href="/logout" onclick="openLoginModal();">Log Out</a>
            <div class="col-sm-3"></div>
        </div>
       
         
         <div class="modal fade login" id="loginModal">
              <div class="modal-dialog login animated" style="width:600px;">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Login with</h4>
                    </div>
                    <div class="modal-body">  
                        <div class="box">
                             <div class="content">
                                <div class="social">
                                    <a id="google_login" class="circle google" href="/auth/google_oauth2">
                                        <i class="fa fa-google-plus fa-fw"></i>
                                    </a>
                                    <a id="facebook_login" class="circle facebook" href="/auth/facebook">
                                        <i class="fa fa-facebook fa-fw"></i>
                                    </a>
                                </div>
                                <div class="division">
                                    <div class="line l"></div>
                                      <span>or</span>
                                    <div class="line r"></div>
                                </div>
                                <div class="error"></div>
                                <div class="form loginBox">
                                    <form method="post" action="/loginAction" accept-charset="UTF-8">
                                    <input id="email" class="form-control" type="text" placeholder="Mobile" name="username">
                                    <input id="password" class="form-control" type="password" placeholder="Password" name="password">
                                    <button class="btn btn-default btn-login" type="submit">Login</button>
                                    </form>
                                </div>
                             </div>
                        </div>
                        <div class="box">
                            <div class="content registerBox" style="display:none;">
                             <div class="form">
                            	 <input id="name" class="form-control" type="text" placeholder="Name" name="name">
                                <input id="mobile" class="form-control" type="text" placeholder="Mobile" name="mobile">
                                <input id="email" class="form-control" type="text" placeholder="Email" name="email">
                                <input id="password" class="form-control" type="password" placeholder="Password" name="password">
                                <input id="password_confirmation" class="form-control" type="password" placeholder="Repeat Password" name="password_confirmation">
                                <input class="btn btn-default btn-register signup_button" type="submit" value="Create account" name="commit">
                             </div>
                            </div>
                        </div> 
                    </div>
                    <div class="modal-footer">
                        <div class="forgot login-footer">
                            <span>Looking to 
                                 <a href="javascript: showRegisterForm();">create an account</a>
                            ?</span>
                        </div>
                        <div class="forgot register-footer" style="display:none">
                             <span class="rigistrationResponse hide"></span><br>
                             <span>Already have an account?</span>
                             <a href="javascript: showLoginForm();">Login</a>
                        </div>
                    </div>        
                  </div>
              </div>
          </div>
    </div>
</body>
</html>

