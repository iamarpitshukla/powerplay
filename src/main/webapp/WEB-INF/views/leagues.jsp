<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../statics/assets/css/main.css" rel="stylesheet" />

<style>
body {
	padding-top: 60px;
}
</style>
<link href="../statics/css/login-register.css" rel="stylesheet" />
<link href="../statics/bootstrap3/css/bootstrap.css" rel="stylesheet" />
<link rel="stylesheet"
	href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="../statics/js/login-register.js"> </script>
<script type="text/javascript"
	src="../statics/bootstrap3/js/bootstrap.js"> </script>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-sm-6">
				<h1>MyTeam Leagues Page</h1>
				<hr />
				<h2>${leagues}</h2>
			</div>
			<div class="col-sm-3">
				<h2>Welcome: ${user.user.name}</h2>
			</div>
			<div class="col-sm-3">
				<a class="btn btn-danger" href="/logout" onclick="openLoginModal();">Log Out</a>
			</div>
		</div>
	</div>
</body>
</html>

