<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML>
<html>
<head>
    <title>Online Cricket Fantasy</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="../statics/assets/css/main.css" rel="stylesheet" />

    <style>
        body {
            padding-top: 60px;
        }
    </style>
    <link href="../statics/css/login-register.css" rel="stylesheet" />
    <link href="../statics/bootstrap3/css/bootstrap.css" rel="stylesheet" />
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="../statics/js/login-register.js"> </script>
    <script type="text/javascript" src="../statics/bootstrap3/js/bootstrap.js"> </script>
</head>
<body>

    <!-- Header -->
    <header id="header" class="md-toolbar.sticky._md._md-toolbar-transitions">
        <div class="logo"><a href="/"><h3><span><img src="../images/worldcricket.jpg" height="60px;" /></span></h3></a></div>
    </header>

    <!-- Banner -->
    <section id="banner">
        <div class="inner">
            <header>
                <p>
                    THE BEST FANTASY SPORT S EXPERIENCE<br />
                    EARN REAL MONEY!!<br />
                    SIMPLER | FASTER | BETTER
                </p>
              <a href="../file/apk/Powerplay_Android.apk"  style="cursor:pointer;font-size: 25">Download Android App</a>     
            </header>
        </div>
    </section>

    <!-- Main -->

    <div id="main">

        <!-- Section -->
        <section class="wrapper style1">
            <div class="inner">
                <!-- 2 Columns -->
                <div class="flex flex-2">
                    <div class="col col1">
                        <div class="image round fit">
                            <a href="" class="link"><img src="../images/stdplayer.jpg" alt="" /></a>
                        </div>
                    </div>
                    <div class="col col2">
                        <h1>How To Paly</h1>
                        <p>
                            Select A Match<br />
                            Choose any of the upcoming matches from the match center
                        </p><br />
                        <p>
                            Create Your Team<br />
                            Choose your best 11 players and select your Captain & Vice-Captain
                        </p>
                        <p>
                            Join Free<br />
                            Join from a variety of cash contests
                        </p><br />
                        <p>
                            Play for Free<br />
                            Do no miss out on the free rolls!! Join and win for free
                        </p>
						<form action="/leagues">
							<button type="submit" class="btn btn-md btn-success">JOIN
								NOW</button>
						</form>
					</div>
                </div>
            </div>
        </section>

        <!-- Section -->
        <section class="wrapper style2">
            <div class="inner">
                <div class="flex flex-2">
                    <div class="col col2">
                        <p>
                            Stay in the game with our<br />
                            New Online Cricket Fantasy App<br />
                            Download the App and get Rs 50 on Sign up.<br />
                            Play more to Win more.
                        </p>
                    </div>
                    <div class="col col1 first">
                        <div class="image round fit">
                            <a href="" class="link"><img src="../images/catch.jpg" alt="" /></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Section -->
        <section class="wrapper style1">
            <div class="inner">
                <header class="align-center">
                    <h2>A BIG THANKS TO OUR VALUABLE PLAYERS</h2>
                </header>
                <div class="flex flex-3">
                    <div class="col align-center">
                        <div class="image round fit">
                            <img src="../images/CricketBall.jpg" alt="" />
                        </div>
                        <p>Online Cricket Fantasy 24 hour withdrawal system is the best in the industry.I love to play & earn reward from Online Cricket Fantasy.Thanks Online Cricket Fantasy.</p>
                    </div>
                    <div class="col align-center">
                        <div class="image round fit">
                            <img src="../images/batmanwithbat.jpg" alt="" />
                        </div>
                        <p>The Best part of Online Cricket Fantasy is that it is easy to win and also give many offers. Their easy to understand app makes the game play amazing.</p>
                    </div>
                    <div class="col align-center">
                        <div class="image round fit">
                            <img src="../images/BallhitStamp.jpg" alt="" />
                        </div>
                        <p>Online Cricket Fantasy round the clock support team helped me to get my queries resolved within 12hrs. Best support experience ever.Thank you Online Cricket Fantasy.</p>
                    </div>
                </div>
            </div>
        </section>

        <!--Section-->
        <section class="wrapper style1">
            <div class="modal fade login" id="loginModal">
                <div class="modal-dialog login animated" style="width:50%;overflow-y: scroll;max-height:80%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" style="background-color: #f7f3f3;
color:#101010 !important" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Login with</h4>
                        </div>
                        <div class="modal-body">
                            <div class="box">
                                <div class="content">
                                    <div class="row social">
                                        <div class="col-md-4">
                                            <a class="circle github" href="/auth/github">
                                                <i class="fa fa-github fa-fw"></i>
                                            </a>
                                        </div>
                                        <div class="col-md-4">
                                            <a id="google_login" class="circle google" href="https://github.com/login">
                                                <i class="fa fa-google-plus fa-fw"></i>
                                            </a>
                                        </div>
                                        <div class="col-md-4">
                                            <a id="facebook_login" class="circle facebook" href="/auth/facebook">
                                                <i class="fa fa-facebook fa-fw"></i>
                                            </a>
                                        </div>

                                    </div>
                                    <div class="division">
                                        <div class="line l"></div>
                                        <span>or</span>
                                        <div class="line r"></div>
                                    </div>
                                    <div class="error"></div>
                                    <div class="form loginBox">
                                    <form method="post" action="/loginAction" default-target-url="/home" accept-charset="UTF-8">
                                    <input id="email" class="form-control" type="text" placeholder="Mobile" name="username">
                                    <input id="password" class="form-control" type="password" placeholder="Password" name="password">
                                    <button class="btn btn-default btn-login" type="submit">Login</button>
                                    </form>
                                </div>
                                </div>
                            </div>
                            <div class="box">
                                <div class="content registerBox" style="display:none;">
	                             <div class="form">
	                            	 <input id="name" class="form-control" type="text" placeholder="Name" name="name">
	                                <input id="mobile" class="form-control" type="text" placeholder="Mobile" name="mobile">
	                                <input id="email" class="form-control" type="text" placeholder="Email" name="email">
	                                <input id="password" class="form-control" type="password" placeholder="Password" name="password">
	                                <input id="password_confirmation" class="form-control" type="password" placeholder="Repeat Password" name="password_confirmation">
	                                <input class="btn btn-default btn-register signup_button" type="submit" value="Create account" name="commit">
	                             </div>
	                            </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="forgot login-footer">
                                <span>
                                    Looking to
                                    <a href="javascript: showRegisterForm();">create an account</a>
                                    ?
                                </span>
                            </div>
                            <div class="forgot register-footer" style="display:none">
                                <span>Already have an account?</span>
                                <a href="javascript: showLoginForm();">Login</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- Footer -->
    <footer id="footer">
        <div class="row">
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-6">
                        <a href="#"><span>ABOUT</span></a><br />
                        <a href="#"><span>CONTACT</span></a><br />
                        <a href="#"><span>HOW TO PLAY</span></a><br />
                        <a href="#"><span>POINTS SYSTEM</span></a>
                    </div>
                    <div class="col-md-6">
                        <a href="#"><span>PRIVACY POLICY</span></a><br />
                        <a href="#"><span>PROMOTIONS</span></a><br />
                        <a href="http://blog.ballebaazi.com/" target="_blank"><span>BLOG</span></a><br />
                        <a href="#"><span>FAQ</span></a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="col">
                    <div class="row" style="align-content:center">
                        <img src="../images/leaguelog0.png" height="100px" width="350px" />
                    </div>
                    <div class="row" style="align-content:center">
                        <a href="https://www.facebook.com" target='_blank'><img src="../images/facebook.png" alt="" height="30px"></a>
                        <a href="https://twitter.com" target='_blank'><img src="../images/Twitter.jpg" alt="" height="30px"></a>
                        <a href="https://www.instagram.com" target='_blank'><img src="../images/Instagram.jpg" alt="" height="30px"></a>
                        <a href="https://www.linkedin.com" target='_blank'><img src="../images/linkedind.jpg" alt="" height="30px"></a>
                        <a href="https://www.youtube.com" target='_blank'><img src="../images/Youtube.png" alt="" height="30px"></a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="col">
                    <div class="row" style="align-content:center">
                        <div class="col-md-3">
                            <img src="../images/Visalogo.png" height="80px" width="100px" />
                        </div>
                        &nbsp; &nbsp;
                        <div class="col-md-3">
                            <img src="../images/mastercard.jpg" height="80px" width="100px" />
                        </div>
                        &nbsp; &nbsp;
                        <div class="col-md-3">
                            <img src="../images/Rupay.png" height="80px" width="100px" />
                        </div>
                    </div>
                    &nbsp; &nbsp;
                    <div class="row">
                        <div class="col-md-3">
                            <img src="../images/paytm.png" height="80px" width="100px" />
                        </div>
                        <div class="col-md-3">
                            <img src="../images/freecharge.png" height="80px" width="100px" />
                        </div>
                    </div>
                    <div class="row"></div>
                </div>
            </div>
        </div>
    </footer>
</body>
</html>