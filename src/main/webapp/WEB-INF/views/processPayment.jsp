<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../statics/assets/css/main.css" rel="stylesheet" />

<style>
body {
	padding-top: 60px;
}
</style>
<link href="../statics/css/login-register.css" rel="stylesheet" />
<link href="../statics/bootstrap3/css/bootstrap.css" rel="stylesheet" />
<link rel="stylesheet"
	href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="../statics/js/login-register.js"> </script>
<script type="text/javascript"
	src="../statics/bootstrap3/js/bootstrap.js"> </script>
</head>
<body>
	<div class="container">
		<center><h1>Please do not refresh this page...</h1></center>
        	<form method="post" action="${makeTxnUrl}" name="f1">
            <table border="1">
                <tbody>
                    ${hiddenFields}
                </tbody>
            </table>
        <script type="text/javascript">
            document.f1.submit();
        </script>
        </form> 
	</div>
</body>
</html>

<!--"MOBILE_NO": "9695106880",
        "CUST_ID": "1",
        "CHANNEL_ID": "WEB",
        "ORDER_ID": "8891547276225627",
        "TXN_AMOUNT": "100.00",
        "CALLBACK_URL": "http://127.0.0.1:8000/payment/paytmResponse",
        "MID": "Powerp53888329068377",
        "CHECKSUMHASH": "oOWqwqB6MX+gYQhvo1m/O1WkjfeibGg135uyLNm301qVUFdMhB9fHFn6p9LFbLz4p+RdUGxd7UR/u0Wp/W76Zv6gdt346uKdB8rs8NYcmFc=",
        "EMAIL": "arpitshukla2013@gmail.com",
        "INDUSTRY_TYPE_ID": "Retail",
        "WEBSITE": " WEBSTAGING"
        
        <input type="hidden" name="MID" value='Powerp53888329068377'>
                    <input type="hidden" name="WEBSITE" value='WEBSTAGING'>
                    <input type="hidden" name="ORDER_ID" value='8891547276225627'>
                    <input type="hidden" name="CUST_ID" value='1'>
                    <input type="hidden" name="MOBILE_NO" value='9695106880'>
                    <input type="hidden" name="EMAIL" value='arpitshukla2013@gmail.com'>
                    <input type="hidden" name="INDUSTRY_TYPE_ID" value='Retail'>
                    <input type="hidden" name="CHANNEL_ID" value='WEB'>
                    <input type="hidden" name="TXN_AMOUNT" value='100.00'>
                    <input type="hidden" name="CALLBACK_URL" value='http://127.0.0.1:8000/payment/paytmResponse'>
                    <input type="hidden" name="CHECKSUMHASH" value='oOWqwqB6MX+gYQhvo1m/O1WkjfeibGg135uyLNm301qVUFdMhB9fHFn6p9LFbLz4p+RdUGxd7UR/u0Wp/W76Zv6gdt346uKdB8rs8NYcmFc='>
                
        
         -->
