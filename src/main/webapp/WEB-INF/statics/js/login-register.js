$(document).ready(function(){

$('.signup_button').on('click', function(e){
	console.log('clicked');
	registerUser();
});

});

function registerUser(){
	var params = {};
	params.username = $('.registerBox #mobile').val();
	params.name = $('.registerBox #name').val();
	params.email = $('.registerBox #email').val();
	params.password = $('.registerBox #password').val();
	params.repeatPassword = $('.registerBox #password_confirmation').val();
	var api = new ajaxAction();
	api.getResponse('/register', params, function(response){
		console.log(response);
		if(response.status == 200){
			
		}else{
			
		}
		$('.rigistrationResponse').removeClass('hide');
		$('.rigistrationResponse').text(response.message);
	});
}

function ajaxAction(){
	this.getResponse = function(url, params, callback){
		$.ajax({
			url:  '/user'+ url,
			type: 'POST',
			data:  params,
			dataType: 'json',
			success: function(data) {
				callback(data);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(textStatus, errorThrown);
				alert("An unkown error occured - try later or contact tech.");
			}
		});
	}
}

function showRegisterForm(){
    $('.loginBox').fadeOut('fast',function(){
        $('.registerBox').fadeIn('fast');
        $('.login-footer').fadeOut('fast',function(){
            $('.register-footer').fadeIn('fast');
        });
        $('.modal-title').html('Register with');
    }); 
    $('.error').removeClass('alert alert-danger').html('');
       
}
function showLoginForm(){
    $('#loginModal .registerBox').fadeOut('fast',function(){
        $('.loginBox').fadeIn('fast');
        $('.register-footer').fadeOut('fast',function(){
            $('.login-footer').fadeIn('fast');    
        });
        
        $('.modal-title').html('Login with');
    });       
     $('.error').removeClass('alert alert-danger').html(''); 
}

function openLoginModal(){
    showLoginForm();
    setTimeout(function(){
        $('#loginModal').modal('show');    
    }, 230);
    
}
function openRegisterModal(){
    showRegisterForm();
    setTimeout(function(){
        $('#loginModal').modal('show');    
    }, 230);
    
}

function loginAjax(){
    /*   Remove this comments when moving to server
    $.post( "/login", function( data ) {
            if(data == 1){
                window.location.replace("/home");            
            } else {
                 shakeModal(); 
            }
        });
    */

/*   Simulate error message from the server   */
     shakeModal();
}

function shakeModal(){
    $('#loginModal .modal-dialog').addClass('shake');
             $('.error').addClass('alert alert-danger').html("Invalid email/password combination");
             $('input[type="password"]').val('');
             setTimeout( function(){ 
                $('#loginModal .modal-dialog').removeClass('shake'); 
    }, 1000 ); 
}

   